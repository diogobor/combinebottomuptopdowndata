﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using PatternTools.MSParser;
using System.Linq;
using System.Windows;
using PatternTools.SpectraPrediction;
using Microsoft.Win32;
using System.Windows.Media.Imaging;
using PatternTools.PTMMods;

namespace SpectrumViewer2.PeptideSpectrumViewer
{
    /// <summary>
    /// Interaction logic for MSViewer.xaml
    /// </summary>
    public partial class MSViewer : UserControl
    {

        public List<Modification> Modifications
        {
            get;
            set;
        }

        public bool IonA {
            set {checkBoxA.IsChecked = value;}
        }

        public bool IonB {
            set {checkBoxB.IsChecked = value;}
        }

        public bool IonC {
            set {checkBoxC.IsChecked = value;}
        }

        public bool IonX {
            set {checkBoxX.IsChecked = value;}
        }

        public bool IonY {
            set {checkBoxY.IsChecked = value;}
        }

        public bool IonZ {
            set {checkBoxZ.IsChecked = value;}
        }

        public bool IonNH3 {
            set {checkBoxNLNH3.IsChecked = value;}
        }

        public bool IonH2O {
            set {checkBoxNLH2O.IsChecked = value;}
        }

        /// <summary>
        /// Please input the MS2 ions before giving a plot
        /// </summary>
        public List<Tuple<float,float>> MyMS2Ions { get; set; }

        //These variable must always be specified before calling any plotting method
        //We need them to find the precursor neutral losses
        public double RelativeIntensityThreshold
        {
            get { return double.Parse(textBoxRelativeIntensityThreshold.Text); }
            set {
                textBoxRelativeIntensityThreshold.Text = value.ToString();
            }
        }

        public double SetMS2PPM
        {
            set
            {
                textBoxPPM.Text = value.ToString();
            }
        }

        public string PeptideSequence
        {
            set
            {
                textBoxSequence.Text = value;
            }
        }



        public MSViewer()
        {
            InitializeComponent();
        }


        private void buttonCID_Click(object sender, RoutedEventArgs e)
        {
            SetToCIDMode();
        }

        public void SetToCIDMode()
        {
            checkBoxA.IsChecked = false;
            checkBoxB.IsChecked = true;
            checkBoxY.IsChecked = true;
            checkBoxX.IsChecked = false;
            checkBoxZ.IsChecked = false;
            checkBoxC.IsChecked = false;
            checkBoxNLH2O.IsChecked = true;
            checkBoxNLNH3.IsChecked = true;
            checkBoxIsMonoisotopic.IsChecked = true;
            Plot();
        }


        public void SetToETDMode()
        {
            checkBoxA.IsChecked = false;
            checkBoxB.IsChecked = false;
            checkBoxY.IsChecked = false;
            checkBoxX.IsChecked = false;
            checkBoxZ.IsChecked = true;
            checkBoxC.IsChecked = true;
            checkBoxNLH2O.IsChecked = true;
            checkBoxNLNH3.IsChecked = true;
            checkBoxIsMonoisotopic.IsChecked = true;
            Plot();
        }

        private void buttonPlot_Click(object sender, RoutedEventArgs e)
        {
            Plot();

            //Evaluate the spectrum quality
            PatternTools.SpectraPrediction.SpectralPredictionParameters spm = new SpectralPredictionParameters(
                true,
                true,
                true,
                true,
                true,
                true,
                false,
                false,
                1,
                true,
                Modifications
                );
                        
            PatternTools.SpectraPrediction.SpectraPredictor sp = new SpectraPredictor(spm);
            List<PredictedIon> theoretical = sp.PredictPeaks(textBoxSequence.Text);

            List<Tuple<float, float>> filteredList = MyMS2Ions.FindAll(a => a.Item2 > double.Parse(textBoxRelativeIntensityThreshold.Text));

        }


        public void Plot(List<PredictedIon> peaks = null)
        {
            MyMS2Ions.Sort((a, b) => a.Item1.CompareTo(b.Item1));

            if (peaks == null)
            {
                SpectralPredictionParameters spp = new SpectralPredictionParameters
                    ((bool)checkBoxA.IsChecked,
                    (bool)checkBoxB.IsChecked,
                    (bool)checkBoxC.IsChecked,
                    (bool)checkBoxX.IsChecked,
                    (bool)checkBoxY.IsChecked,
                    (bool)checkBoxZ.IsChecked,
                    (bool)checkBoxNLH2O.IsChecked,
                    (bool)checkBoxNLNH3.IsChecked,
                    3,
                    (bool)checkBoxIsMonoisotopic.IsChecked,
                    Modifications
                    );

                if (Modifications == null)
                {
                    spp.MyModifications = new List<Modification>();
                }
                PatternTools.SpectraPrediction.SpectraPredictor sp = new PatternTools.SpectraPrediction.SpectraPredictor(spp);

                if (textBoxSequence.Text.Length > 0)
                {
                    peaks = sp.PredictPeaks(textBoxSequence.Text);
                    if (!(bool)checkBoxZ1.IsChecked)
                    {
                        peaks.RemoveAll(a => a.Charge == 1 && a.Series != IonSeries.Precursor);
                    }
                    if (!(bool)checkBoxZ2.IsChecked)
                    {
                        peaks.RemoveAll(a => a.Charge == 2 && a.Series != IonSeries.Precursor);
                    }
                    if (!(bool)checkBoxZ3.IsChecked)
                    {
                        peaks.RemoveAll(a => a.Charge == 3 && a.Series != IonSeries.Precursor);
                    }
                } else
                {
                    peaks = new List<PredictedIon>();
                }
            }

            dataGridPeakViewer.ItemsSource = peaks;

            //New spec viewer ---------------------------------------------------------
            List<Tuple<float, float>> ionsUltraLight = MyMS2Ions.FindAll(a => a.Item2 > 0); 
            SpectrumEye1.DefaultStartMZ = ionsUltraLight[0].Item1 - 1;

            if (peaks != null)
            {
                double ppm = double.Parse(textBoxPPM.Text);
                Dictionary<Tuple<float, float>, List<PredictedIon>> anotation = new Dictionary<Tuple<float, float>, List<PredictedIon>>();

                foreach (Tuple<float,float> il in ionsUltraLight)
                {
                    List<PredictedIon> pMatches = peaks.FindAll(a => Math.Abs(PatternTools.pTools.PPM(il.Item1, a.MZ)) < ppm).ToList();

                    if (pMatches.Count > 0)
                    {
                        foreach (PredictedIon pp in pMatches)
                        {
                            pp.Accepted = true;
                            pp.Matched = true;
                        }

                        if (anotation.ContainsKey(il))
                        {
                            anotation[il].AddRange(pMatches);
                        } 
                        else
                        {
                            anotation.Add(il, pMatches);
                        }
                    }
                }

                double matches = peaks.FindAll(a => a.Matched == true).Count;
                double total = peaks.Count;
                labelPeaksProduced.Content = matches + " / " + total + " = " + Math.Round(matches / total, 1) * 100 + "%";


                SpectrumEye1.WantedSeries = new List<IonSeries>() { IonSeries.B, IonSeries.Y, IonSeries.Extra};
                SpectrumEye1.LoadSpectra(ionsUltraLight, anotation, peaks);

            } else
            {
                SpectrumEye1.LoadSpectra(ionsUltraLight);
            }
                      
            SpectrumEye1.Plot();

            textBoxSpectrumPeaks.Clear();
            foreach (Tuple<float, float> t in MyMS2Ions)
            {
                textBoxSpectrumPeaks.Text += t.Item1 + "\t" + t.Item2 + "\n";
            }
        }
    }
}
