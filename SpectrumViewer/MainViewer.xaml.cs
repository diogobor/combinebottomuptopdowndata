﻿using PatternTools.MSParserLight;
using PatternTools.PTMMods;
using PatternTools.SpectraPrediction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SpectrumViewer2
{
    /// <summary>
    /// Interaction logic for MainViewer.xaml
    /// </summary>
    public partial class MainViewer : UserControl
    {
        public MainViewer()
        {
            InitializeComponent();
        }

        public void PlotAnnotatedPeptide (List<Tuple<string,string,string>> annotatedSequece)
        {
            SequenceAnotation1.Plot(annotatedSequece);
        }

        //public void PlotSpectrum(MSLight msExperimental)
        //{
        //    //SpectrumEye1.LoadSpectra(msExperimental, 15);
        //}

        //public void PlotAnotatedSpectrum(MSLight msExperimental, double ppm, string peptide)
        //{
        //    //Predict the spectrum
        //    //Find PTMs

        //    List<Modification> mods = SpectraPredictor.GetVMods(peptide);
        //    SpectralPredictionParameters spp = new SpectralPredictionParameters(false, true, false, false, true, false, true, true, 2, true, mods);
        //    SpectraPredictor sp = new SpectraPredictor(spp);
        //    List<PredictedIon> theoreticalSpectrum = sp.PredictPeaks(peptide);

        //    DataGridResultTable.ItemsSource = theoreticalSpectrum;

        //    //SpectrumEye1.LoadSpectra(msExperimental, ppm, theoreticalSpectrum);
        //}
    }
}
