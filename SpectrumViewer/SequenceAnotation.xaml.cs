﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SpectrumViewer2
{
    /// <summary>
    /// Interaction logic for SequenceAnotation.xaml
    /// </summary>
    public partial class SequenceAnotation : UserControl
    {
        public SequenceAnotation()
        {
            InitializeComponent();
        }

        public double MyTopOffsetText { get; set; } = 15;
        public double MyFontSize { get; set; } = 18;
        public double MyStep { get; set; } = 10;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="annotations">The Sequence Element, Top Anotation, Bottom Annotation</param>
        public void Plot(List<Tuple<string, string, string>> annotations)
        {
            CanvasSequence.Children.Clear();

            double widthSum = 0;

            for (int countAnnot = 0; countAnnot < annotations.Count; countAnnot++)
            {
                TextBlock textBlock = new TextBlock()
                {
                    Foreground = new SolidColorBrush(Colors.Black),
                    FontFamily = new FontFamily("Courier New"),
                    FontSize = MyFontSize
                };

                if (annotations[countAnnot].Item1.Contains("("))
                {
                    textBlock.FontWeight = FontWeights.Bold;
                }

                textBlock.Text = PatternTools.pTools.CleanPeptide(annotations[countAnnot].Item1, true);

                Size tbSize = MeasureString(textBlock);

                Canvas.SetLeft(textBlock, widthSum);
                Canvas.SetTop(textBlock, MyTopOffsetText);

                CanvasSequence.Children.Add(textBlock);

                //include the pipe
                if (annotations[countAnnot].Item2.Length > 0 || annotations[countAnnot].Item3.Length > 0)
                {
                    Line _pipe = new Line()
                    {
                        X1 = 0,
                        X2 = 0,
                        Y1 = 0,
                        Y2 = tbSize.Height * 1.1,

                        Stroke = Brushes.Black,
                        StrokeThickness = 1
                    };

                    bool hasPipeY = false;
                    if (!String.IsNullOrEmpty(annotations[countAnnot].Item2) &&
                        !String.IsNullOrEmpty(annotations[countAnnot].Item3))
                    {
                        if (countAnnot > 0 && String.IsNullOrEmpty(annotations[countAnnot - 1].Item3))
                        {
                            //it's means the previous aminoacid does not have the pipe

                            Canvas.SetLeft(_pipe, tbSize.Width + widthSum + MyStep / 2);
                            Canvas.SetTop(_pipe, MyTopOffsetText - (tbSize.Height * 0.15));
                            CanvasSequence.Children.Add(_pipe);

                            Line _pipeY = new Line()
                            {
                                X1 = 0,
                                X2 = 0,
                                Y1 = 0,
                                Y2 = tbSize.Height * 1.1,

                                Stroke = Brushes.Black,
                                StrokeThickness = 1
                            };
                            Canvas.SetLeft(_pipeY, tbSize.Width + widthSum - MyStep - MyStep / 2);
                            Canvas.SetTop(_pipeY, MyTopOffsetText - (tbSize.Height * 0.15));
                            CanvasSequence.Children.Add(_pipeY);
                        }
                        else
                        {
                            //it's means the previous aminoacid has the pipe
                            Canvas.SetLeft(_pipe, tbSize.Width + widthSum + MyStep / 2);
                            Canvas.SetTop(_pipe, MyTopOffsetText - (tbSize.Height * 0.15));
                            CanvasSequence.Children.Add(_pipe);
                        }
                        hasPipeY = true;
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(annotations[countAnnot].Item2))
                            Canvas.SetLeft(_pipe, tbSize.Width + widthSum - MyStep - MyStep / 2);
                        else
                            Canvas.SetLeft(_pipe, tbSize.Width + widthSum + MyStep / 2);
                        Canvas.SetTop(_pipe, MyTopOffsetText - (tbSize.Height * 0.15));
                        CanvasSequence.Children.Add(_pipe);
                    }

                    double tickSize = 10;

                    // y series
                    if (annotations[countAnnot].Item2.Length > 0)
                    {
                        Line l2 = new Line()
                        {
                            X1 = 0,
                            X2 = tickSize,  // 150 too far
                            Y1 = tickSize,
                            Y2 = 0,

                            Stroke = Brushes.Blue,
                            StrokeThickness = 1
                        };
                        if (!hasPipeY)
                            Canvas.SetLeft(_pipe, tbSize.Width + widthSum - 2 * MyStep + 4);
                        Canvas.SetLeft(l2, tbSize.Width + widthSum - 2 * MyStep + 4);
                        Canvas.SetTop(l2, MyTopOffsetText - (tbSize.Height * 0.15) - tickSize);
                        CanvasSequence.Children.Add(l2);

                        //And now add the label
                        TextBlock tb = new TextBlock()
                        {
                            HorizontalAlignment = HorizontalAlignment.Left,
                            FontSize = 8
                        };
                        //tb.Text = i.Item2;
                        CanvasSequence.Children.Add(tb);
                        Size tbLabel = MeasureString(tb);
                        Canvas.SetLeft(tb, tbSize.Width + widthSum + MyStep + l2.X2);

                        //Canvas.SetTop(tb,);
                    }

                    // b series
                    if (annotations[countAnnot].Item3.Length > 0)
                    {
                        Line l3 = new Line()
                        {
                            X1 = 0,
                            X2 = -tickSize,
                            Y1 = 0,
                            Y2 = tickSize,

                            Stroke = Brushes.Red,
                            StrokeThickness = 1
                        };
                        Canvas.SetLeft(l3, tbSize.Width + widthSum + MyStep / 2);
                        Canvas.SetTop(l3, (MyTopOffsetText - (tbSize.Height * 0.15)) + (tbSize.Height * 1.1));
                        CanvasSequence.Children.Add(l3);
                    }

                }
                widthSum += tbSize.Width + MyStep;

            }
            widthSum += MyStep;

            //Console.WriteLine(widthSum);
            CanvasSequence.Width = widthSum;

        }

        private Size MeasureString(TextBlock candidate)
        {
            var formattedText = new FormattedText(
                candidate.Text,
                CultureInfo.CurrentUICulture,
                FlowDirection.LeftToRight,
                new Typeface(candidate.FontFamily, candidate.FontStyle, candidate.FontWeight, candidate.FontStretch),
                candidate.FontSize,
                Brushes.Black);

            return new Size(formattedText.Width, formattedText.Height);
        }
    }
}
