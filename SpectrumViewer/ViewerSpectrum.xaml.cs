﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SpectrumViewer2
{
    /// <summary>
    /// Interaction logic for ViewerSpectrum.xaml
    /// </summary>
    public partial class ViewerSpectrum : Window
    {

        public MainViewer MyViewer
        {
            get { return MainViewer; }
            set { MainViewer = value; }
        }

        public ViewerSpectrum()
        {
            InitializeComponent();
        }
    }
}
