﻿using PatternTools.MSParser;
using PatternTools.MSParserLight;
using PatternTools.SpectraPrediction;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Timers;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using SpectrumViewer2.PeakAnnotationManagement;
using Microsoft.Win32;
using System.IO;

namespace SpectrumViewer2
{
    /// <summary>
    /// Interaction logic for SpectrumEye.xaml
    /// </summary>
    public partial class SpectrumEye : UserControl
    {
        //Delegates
        public delegate List<Tuple<string, string, string>> GetAnnotations();
        public GetAnnotations GetAnnotationsFromExternalSource;

        public delegate void OnPeakDoubleClickDelegate(PredictedIon peakClicked);
        public OnPeakDoubleClickDelegate OnPeakDoubleClick;

        //Spectra
        List<ViewerPeak> allViewerPeaks; //All experimental peaks
        List<ViewerPeak> displayViewerPeaks; //Experimental peaks in view 

        List<PredictedIon> theoreticalSpectrum;
        List<ViewerPeak> theoreticalViewerPeaks;

        //Options
        public bool MatchByIsotopicEnvelope { get; set; } = false;
        public int MatchThickness { get; set; } = 3;
        public double DefaultStartMZ { get; set; } = -1;
        public double DefaultEndMZ { get; set; } = -1;
        public List<IonSeries> WantedSeries { get; set; }
        public bool ShowAnnotatedSequence { get; set; }


        //Annotation stuff
        internal PeakAnnotationManagement.AnnotationSettings MyAnnotationSettings { get; set; }

        //MZ positions
        double startMZ;
        double endMZ;

        //Zoom
        Point startX; //Used for calculating display range when mouse is down.
        Rectangle zoomSelection;
        bool zoomInProcess = false;

        //Double click
        int clickCount;
        Line clickedPeak;
        private DispatcherTimer dispatcherTimer;


        bool loaded = false;

        public SpectrumEye()
        {
            MyAnnotationSettings = new PeakAnnotationManagement.AnnotationSettings();
            WantedSeries = new List<IonSeries>();

            InitializeComponent();
            LabelStartMZ.Content = "";
            LabelEndMZ.Content = "";

            MenuItemContextZoomOut.IsEnabled = false;
            MenuItemContextChangeAnnotations.IsEnabled = false;

            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(DispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 200);

            clickCount = 0;
        }

        public void LoadSpectra(
            List<Tuple<float, float>> experimentalIons,
            Dictionary<Tuple<float, float>, List<PredictedIon>> matches = null,
            List<PredictedIon> tIons = null)
        {
            if (experimentalIons == null || experimentalIons.Count() == 0)
            {
                Console.WriteLine("SpectrumViewer received no ions to plot.");
                return;
            }

            theoreticalSpectrum = tIons;

            if (DefaultStartMZ == -1)
                DefaultStartMZ = 0;
            if (DefaultEndMZ == -1)
                DefaultEndMZ = experimentalIons.Max(a => a.Item1) + 50;

            startMZ = experimentalIons.Min(a => a.Item1);
            endMZ = experimentalIons.Max(a => a.Item1);

            SetupViewerPeaks(experimentalIons, matches);

            MenuItemContextZoomOut.IsEnabled = true;
            MenuItemContextChangeAnnotations.IsEnabled = true;

            loaded = true;
            ClearPlot();
            if (ShowAnnotatedSequence)
                AnnotateSpectrumSequence();
        }

        private void SetupViewerPeaks(List<Tuple<float, float>> experimentalIons, Dictionary<Tuple<float, float>, List<PredictedIon>> matches)
        {
            allViewerPeaks = experimentalIons.OrderByDescending(a => a.Item2).Select(
                a => new ViewerPeak(
                    a, (matches != null && matches.ContainsKey(a)) ? matches[a].ToArray() : null,
                    null, null)).ToList();

            //displayViewerPeaks.Sort((a, b) => b.Ion.Item2.CompareTo(a.Ion.Item2));

            DetermineInitialViewerPeakMatches();

            allViewerPeaks.ForEach(a => a.CurrentLine = BuildViewerPeakLine(a));
            allViewerPeaks.ForEach(a => a.RefreshAnnotations(MatchThickness));
        }

        private Line BuildViewerPeakLine(ViewerPeak vPeak)
        {
            Line peakLine = new Line()
            {
                Stroke = Brushes.Black,
                StrokeThickness = 1
            };

            peakLine.PreviewMouseLeftButtonDown += Peak_PreviewMouseLeftButtonDown;
            peakLine.MouseRightButtonDown += PeakLine_MouseRightButtonDown;

            return peakLine;
        }

        /// <summary>
        /// Selects from all matches of any given peak the one from the most complete envelope
        /// </summary>
        private void DetermineInitialViewerPeakMatches()
        {
            if (theoreticalSpectrum != null)
            {
                foreach (var vPeak in allViewerPeaks)
                {
                    if (vPeak.Matches == null || vPeak.Matches.Count() == 0)
                        continue;

                    if (vPeak.Matches.Count() > 1)
                    {
                        int mostEnvelopeMatches = 0;
                        int mostEnvelopeMatchesIndex = -1;
                        for (int i = 0; i < vPeak.Matches.Count(); i++)
                        {
                            var match = vPeak.Matches[i];
                            int envelopeMatches = (from a in theoreticalSpectrum
                                                   where (
                                                       a.Matched
                                                       && a.Number == match.Number
                                                       && a.Series == match.Series
                                                       && a.Charge == match.Charge)
                                                   select a).Count();

                            if (envelopeMatches > mostEnvelopeMatches)
                            {
                                mostEnvelopeMatches = envelopeMatches;
                                mostEnvelopeMatchesIndex = i;
                            }
                        }

                        vPeak.CurrentMatch = vPeak.Matches[mostEnvelopeMatchesIndex];
                        //TODO: This is not working here. Precursor starts annotated even if default is false.
                        vPeak.CircleIfIllegal(MyAnnotationSettings);
                    }
                }
            }
        }

        public void ClearPlot()
        {
            CanvasPeakDisplay.Children.Clear();
            LabelEndMZ.Content = "";
            LabelStartMZ.Content = "";

            MySequenceAnotation.CanvasSequence.Children.Clear();
        }

        public void Plot()
        {
            Plot(DefaultStartMZ, DefaultEndMZ);
        }

        public void Plot(double newStartMZ, double newEndMZ)
        {
            if (allViewerPeaks == null)
                return;

            zoomInProcess = false;

            var auxDisplayIons = allViewerPeaks.
                FindAll(a => a.Ion.Item1 >= newStartMZ && a.Ion.Item1 <= newEndMZ).
                Where(a => a.Ion.Item2 != 0).ToList();

            if (auxDisplayIons.Count == 0)
            {
                Console.WriteLine("Tried zooming on peakless region");
                return;
            }

            //allViewerPeaks.ForEach(a => a.CurrentLine = null);
            displayViewerPeaks = auxDisplayIons;

            startMZ = newStartMZ;
            endMZ = newEndMZ;

            LabelStartMZ.Content = Math.Round(startMZ, 2);
            LabelEndMZ.Content = Math.Round(endMZ, 2);

            CanvasPeakDisplay.Children.Clear();
            allViewerPeaks.ForEach(a => a.CurrentTextBlock.Visibility = Visibility.Collapsed);

            if (MyAnnotationSettings.TheoreticalSpectrum == true)
                PlotTheoreticalPeaks();

            PlotViewerPeaks();
        }

        private void PlotViewerPeaks()
        {
            double maxIntensity = displayViewerPeaks.Max(a => a.Ion.Item2);
            if (maxIntensity == 0)
                return;

            List<Tuple<double, double>> addedPositions = new List<Tuple<double, double>>(); //Used for printing labels in the peaks
            double minimumHorizonalPixelsBetweenLable = 10;


            //Loops update position of each ViewerPeak in display
            foreach (ViewerPeak vPeak in displayViewerPeaks)
            {
                double relativeIntensity = Math.Round(vPeak.Ion.Item2 / maxIntensity, 3);
                Tuple<double, double> p = ConvertMZToPixel(vPeak.Ion, maxIntensity, startMZ, endMZ);

                vPeak.CurrentLine.X1 = 0;
                vPeak.CurrentLine.X2 = 0;
                vPeak.CurrentLine.Y1 = 0;
                vPeak.CurrentLine.Y2 = p.Item2;

                Canvas.SetLeft(vPeak.CurrentLine, p.Item1);
                Canvas.SetBottom(vPeak.CurrentLine, 0);

                CanvasPeakDisplay.Children.Add(vPeak.CurrentLine);

                if (relativeIntensity > 0.02)
                {
                    var tb = vPeak.CurrentTextBlock;
                    tb.Visibility = Visibility.Visible;

                    tb.MouseDown += TextLabel_MouseDown;

                    tb.Measure(new Size(Double.PositiveInfinity, Double.PositiveInfinity));
                    Rect measureRect = new Rect(tb.DesiredSize);

                    double addedPos = p.Item1 - measureRect.Width / 2;
                    Tuple<double, double> thisRange = new Tuple<double, double>(p.Item1 - measureRect.Width / 2, p.Item1 + measureRect.Width / 2);

                    if (!addedPositions.Exists(
                        a =>
                        (Math.Abs(a.Item1 - thisRange.Item1) < minimumHorizonalPixelsBetweenLable)
                        ||
                        (Math.Abs(a.Item2 - thisRange.Item2) < minimumHorizonalPixelsBetweenLable)
                        ||
                        ((thisRange.Item1 > a.Item1) && (thisRange.Item1 < a.Item2))
                        ||
                        ((thisRange.Item2 > a.Item1)) && (thisRange.Item2 < a.Item2))
                    )
                    {
                        CanvasPeakDisplay.Children.Add(tb);
                        Canvas.SetLeft(tb, addedPos);
                        Canvas.SetBottom(tb, p.Item2);
                        addedPositions.Add(thisRange);
                    }
                }
            }
        }



        /// <summary>
        /// Updates wether a ViewerPeak is matched or not. Called from external source. 
        /// </summary>
        /// <param name="ion"></param>
        /// <param name="UpdateSpectrumSequence"></param>
        public void UpdateMatched(PredictedIon ion, bool UpdateSpectrumSequence = true)
        {
            if (allViewerPeaks == null || allViewerPeaks.Count() == 0)
                return;

            bool MatchesContainsIon(PredictedIon[] matches, PredictedIon target)
            {
                if (matches == null)
                    return false;
                foreach (var i in matches)
                {
                    if (i.MZ == target.MZ &&
                        i.Number == target.Number &&
                        i.Series == target.Series &&
                        i.IsotopeNumber == target.IsotopeNumber)
                        return true;
                }
                return false;
            }

            var vPeak = allViewerPeaks.Find(
                    a => MatchesContainsIon(a.Matches, ion));

            if (vPeak == null)
                return;

            //If vPeak has match, and it is not accepted, try changing match
            if (vPeak.CurrentMatch != null && !vPeak.CurrentMatch.Accepted)
                vPeak.CircleCurrentMatch(MyAnnotationSettings);

            //If vPeak has no match currently, try circling match
            if (vPeak.CurrentMatch == null)
                vPeak.CircleCurrentMatch(MyAnnotationSettings);

            //UpdateViewerPeakUI(vPeak);
            vPeak.RefreshAnnotations(MatchThickness);

            if (UpdateSpectrumSequence)
            {
                if (ShowAnnotatedSequence)
                    AnnotateSpectrumSequence();
            }
        }

        public void UpdateMatched(List<PredictedIon> ions)
        {
            if (allViewerPeaks == null || allViewerPeaks.Count() == 0)
                return;

            foreach (var ion in ions)
            {
                UpdateMatched(ion, false);
            }

            if (ShowAnnotatedSequence)
                AnnotateSpectrumSequence();
        }


        private void AnnotateSpectrumSequence()
        {
            if (theoreticalSpectrum == null)
                return;

            List<Tuple<string, string, string>> anotations = new List<Tuple<string, string, string>>();

            if (MatchByIsotopicEnvelope)
            {
                if (GetAnnotationsFromExternalSource != null)
                {
                    anotations = GetAnnotationsFromExternalSource();
                }
                else
                {
                    Console.WriteLine("SpectrumViewer tried matching by envelopes, but GetAnnotations delegate was null");
                    MatchByIsotopicEnvelope = false;
                }
            }

            if (!MatchByIsotopicEnvelope)
            {
                List<PredictedIon> auxIons = theoreticalSpectrum.FindAll(a => a.Number > -1 && WantedSeries.Contains(a.Series));
                auxIons.Sort((a, b) => a.Number.CompareTo(b.Number));

                List<string> originalMolecule = auxIons.GroupBy(a => a.Number).Select(a => a.First().FinalAA).ToList();

                for (int i = 0; i < originalMolecule.Count; i++)
                {
                    string a = "";
                    string b = "";

                    if (theoreticalSpectrum.Exists(
                        x => x.MZ >= startMZ
                        && x.MZ <= endMZ
                        && x.Number == i + 1
                        && x.Matched
                        && (x.Series == IonSeries.W || x.Series == IonSeries.X || x.Series == IonSeries.Y || x.Series == IonSeries.Z)))
                    {
                        a = "y1";
                    }

                    if (theoreticalSpectrum.Exists(
                        x => x.MZ >= startMZ
                        && x.MZ <= endMZ
                        && x.Number == i + 1
                        && x.Matched
                        && (x.Series == IonSeries.A || x.Series == IonSeries.B || x.Series == IonSeries.C || x.Series == IonSeries.D)))
                    {
                        b = "b2";
                    }

                    anotations.Add(new Tuple<string, string, string>(originalMolecule[i], a, b));
                }
            }


            MySequenceAnotation.Plot(anotations);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="i"></param>
        /// <param name="maxIntensity"></param>
        /// <param name="startMZ"></param>
        /// <param name="FinishMZ"></param>
        /// <returns>A tuple with the mz and intensity mapped to the canvas</returns>
        private Tuple<double, double> ConvertMZToPixel(Tuple<float, float> i, double maxIntensity, double startMZ, double FinishMZ)
        {
            double compressionFactor = 0.80;

            double deltaMZ = FinishMZ - startMZ;

            double x = ((i.Item1 - startMZ) / deltaMZ) * CanvasPeakDisplay.ActualWidth;
            double y = i.Item2 / maxIntensity * CanvasPeakDisplay.ActualHeight * compressionFactor;

            return new Tuple<double, double>(x, y);
        }

        private void PeakLine_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        /// <summary>
        /// Calls OnPeakDoubleClick handler on double click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Peak_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!loaded)
                return;

            if (clickCount == 0)
            {
                dispatcherTimer.Start();
                clickCount++;
                clickedPeak = (Line)sender;
            }
            else
            {
                clickCount++;
            }
        }

        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            if (clickCount == 1)
            {
                var vIon = displayViewerPeaks.Find(a => a.CurrentLine == clickedPeak);
                if (vIon == null)
                {
                    clickCount = 0;
                    dispatcherTimer.Stop();
                    return;
                }

                if (vIon != null && vIon.Matches != null && vIon.Matches.Count() != 0)
                {
                    vIon.CircleCurrentMatch(MyAnnotationSettings);

                    //UpdateViewerPeakUI(vIon);
                    vIon.RefreshAnnotations(MatchThickness);
                }


            }
            else if (clickCount >= 2)
            {

                var vIon = displayViewerPeaks.Find(a => a.CurrentLine == clickedPeak);
                //if (matches == null || vIon == null)
                if (vIon == null)
                {
                    clickCount = 0;
                    dispatcherTimer.Stop();
                    return;
                }

                //if (!matches.ContainsKey(vIon.Ion))
                if (vIon.Matches == null || vIon.Matches.Count() == 0)
                {
                    Console.WriteLine("Peak has no match");
                    return;
                }

                var tIon = vIon.CurrentMatch;

                //Console.WriteLine("Double Clicked Peak - MZ: " + tIon.MZ +
                //    " Number: " + tIon.Number + " Charge: " + tIon.Charge + " IsotopeNumber: " + tIon.IsotopeNumber);

                OnPeakDoubleClick?.Invoke(tIon);
            }

            clickCount = 0;
            dispatcherTimer.Stop();

        }

        //TODO: Remove this when safe
        private void UpdateViewerPeakUI(ViewerPeak vIon)
        {
            if (vIon.CurrentLine == null)
                return;

            vIon.CurrentLine.ToolTip = "";
            if (vIon.CurrentTextBlock != null)
            {
                vIon.CurrentTextBlock.Text = "";
                vIon.CurrentTextBlock.Text = "";
                vIon.CurrentTextBlock.Text += Math.Round(vIon.Ion.Item1, 3).ToString();
            }

            vIon.CurrentLine.StrokeThickness = 1;
            vIon.CurrentLine.Stroke = Brushes.Black;

            vIon.CurrentLine.ToolTip = "M/Z: " + vIon.Ion.Item1 + "\n";
            vIon.CurrentLine.ToolTip += "Intensity: " + vIon.Ion.Item2 + "\n";


            if (vIon.CurrentMatch == null || !vIon.CurrentMatch.Accepted)
                return;

            vIon.CurrentLine.StrokeThickness = MatchThickness;

            if (vIon.CurrentMatch.Series == IonSeries.A || vIon.CurrentMatch.Series == IonSeries.B || vIon.CurrentMatch.Series == IonSeries.C || vIon.CurrentMatch.Series == IonSeries.D || vIon.CurrentMatch.Series == IonSeries.AmB)
            {
                vIon.CurrentLine.ToolTip += "Match M/Z: " + Math.Round(vIon.CurrentMatch.MZ, 4) + "\n";
                vIon.CurrentLine.Stroke = Brushes.Red;
            }

            if (vIon.CurrentMatch.Series == IonSeries.X || vIon.CurrentMatch.Series == IonSeries.Y || vIon.CurrentMatch.Series == IonSeries.Z || vIon.CurrentMatch.Series == IonSeries.W)
            {
                vIon.CurrentLine.ToolTip += "Match M/Z: " + Math.Round(vIon.CurrentMatch.MZ, 4) + "\n";
                vIon.CurrentLine.Stroke = Brushes.Blue;
            }

            vIon.CurrentLine.ToolTip += "Z : " + vIon.CurrentMatch.Charge + "\n";
            if (vIon.CurrentMatch.Series == IonSeries.Precursor)
                vIon.CurrentLine.ToolTip += "Series: " + vIon.CurrentMatch.Series + "\n";
            else
                vIon.CurrentLine.ToolTip += "Series: " + vIon.CurrentMatch.Series + vIon.CurrentMatch.Number.ToString() + "\n";

            vIon.CurrentLine.ToolTip += "Sequence Item: " + vIon.CurrentMatch.FinalAA + "\n";
            vIon.CurrentLine.ToolTip += "Isotope Number: " + vIon.CurrentMatch.IsotopeNumber;


            if (vIon.CurrentMatch != null && vIon.CurrentTextBlock != null)
            {
                vIon.CurrentTextBlock.Text += "\n" + vIon.CurrentMatch.Series +
                    ((vIon.CurrentMatch.Series != IonSeries.Precursor) ?
                    vIon.CurrentMatch.Number.ToString() : "");

                vIon.CurrentTextBlock.Text += " Z: " + vIon.CurrentMatch.Charge;
            }
        }


        //TODO: Currently unused. Check TODO in ViewerPeak
        //TODO: Actually, check if this is working properly. It should probably share an event with the line click.
        private void TextLabel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (!loaded)
                return;

            if (e.ClickCount < 2)
            {
                TextBlock text = (TextBlock)sender;
                var vIon = displayViewerPeaks.Find(a => a.CurrentTextBlock == text);

                return;
            }
            else
            {
                TextBlock text = (TextBlock)sender;

                var vIon = displayViewerPeaks.Find(a => a.CurrentTextBlock == text);
                if (vIon.Matches == null || vIon.Matches.Count() == 0)
                {
                    Console.WriteLine("Peak has no match");
                    return;
                }

                var tIon = vIon.CurrentMatch;

                //Console.WriteLine("Double Clicked Peak - MZ: " + tIon.MZ +
                //    " Number: " + tIon.Number + " Charge: " + tIon.Charge + " IsotopeNumber: " + tIon.IsotopeNumber);

                OnPeakDoubleClick?.Invoke(tIon);
            }
        }



        private void CanvasPeakDisplay_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (!loaded)
                return;

            if (displayViewerPeaks != null && displayViewerPeaks.Count > 1)
            {
                Plot(startMZ, endMZ);
            }
        }

        private void CanvasPeakDisplay_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!loaded)
                return;

            zoomInProcess = true;
            startX = Mouse.GetPosition(CanvasPeakDisplay);

            zoomSelection = new Rectangle()
            {
                Height = CanvasPeakDisplay.ActualHeight,
                Width = 0,
                Opacity = 0.3,
                StrokeThickness = 3,
                Stroke = Brushes.DarkGray,
                Fill = Brushes.LightGray
            };
            //ZoomSelection.Opacity = 0.3;

            CanvasPeakDisplay.Children.Add(zoomSelection);
            Canvas.SetLeft(zoomSelection, startX.X);

            //Console.WriteLine(startX);
        }



        private void CanvasPeakDisplay_MouseMove(object sender, MouseEventArgs e)
        {
            if (!loaded)
                return;

            if (zoomInProcess)
            {
                double newPos = Mouse.GetPosition(CanvasPeakDisplay).X;

                double width = Math.Abs(startX.X - newPos);
                zoomSelection.Width = width;

                if (newPos >= startX.X)
                {
                    Canvas.SetLeft(zoomSelection, startX.X);
                }
                else
                {
                    Canvas.SetLeft(zoomSelection, startX.X - width);
                }
            }
        }

        private void CanvasPeakDisplay_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!loaded)
                return;

            Point endX = Mouse.GetPosition(CanvasPeakDisplay);

            if (endX.X < startX.X)
            {
                Point t;
                t = endX;
                endX = startX;
                startX = t;
            }

            double startMZTemp = ((startX.X / CanvasPeakDisplay.ActualWidth) * (endMZ - startMZ)) + startMZ;
            double finishMZTemp = ((endX.X / CanvasPeakDisplay.ActualWidth) * (endMZ - startMZ)) + startMZ;

            //Console.WriteLine("StartMz = " + startMZ);
            //Console.WriteLine("FinishMz = " + finishMZ);

            //Console.WriteLine("StartMzTmp = " + startMZTemp);
            //Console.WriteLine("FinishMzTmp = " + finishMZTemp);

            CanvasPeakDisplay.Children.Remove(zoomSelection);
            zoomSelection = null;

            Plot(startMZTemp, finishMZTemp);

        }



        private void ContextMenuZoomOut_click(object sender, RoutedEventArgs e)
        {
            if (!loaded)
                return;

            Plot(DefaultStartMZ, DefaultEndMZ);
        }


        private void ContextMenuChangeAnnotations_click(object sender, RoutedEventArgs e)
        {
            bool previousTheoreticalSpectrumSetting = MyAnnotationSettings.TheoreticalSpectrum;

            var controlAnnotations = new ControlAnnotationSelect(MyAnnotationSettings);

            var w = new Window()
            {
                ResizeMode = ResizeMode.NoResize,
                Content = controlAnnotations,
                SizeToContent = SizeToContent.WidthAndHeight,
                WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen
            };

            w.ShowDialog();

            MyAnnotationSettings = controlAnnotations.Settings;

            allViewerPeaks.ForEach(a => a.CircleIfIllegal(MyAnnotationSettings));
            allViewerPeaks.ForEach(a => a.RefreshAnnotations(MatchThickness));

            if (MyAnnotationSettings.TheoreticalSpectrum != previousTheoreticalSpectrumSetting)
            {
                Plot(startMZ, endMZ);
            }
        }



        private void PlotTheoreticalPeaks()
        {
            if (theoreticalViewerPeaks == null)
            {
                theoreticalViewerPeaks = theoreticalSpectrum.Select(
                    a => new ViewerPeak()
                    {
                        IsTheoreticalPeak = true,
                        Ion = new Tuple<float, float>((float)a.MZ, (float)a.Intensity)
                    }).ToList();

                theoreticalViewerPeaks.ForEach(a => a.CurrentLine = new Line()
                {
                    Stroke = Brushes.DarkSeaGreen,
                    StrokeThickness = 1
                });
            }

            foreach (var tPeak in theoreticalViewerPeaks)
            {
                Tuple<double, double> p = ConvertMZToPixel(tPeak.Ion, 1, startMZ, endMZ);

                tPeak.CurrentLine.X1 = 0;
                tPeak.CurrentLine.X2 = 0;
                tPeak.CurrentLine.Y1 = 0;
                tPeak.CurrentLine.Y2 = p.Item2;

                Canvas.SetLeft(tPeak.CurrentLine, p.Item1);
                Canvas.SetBottom(tPeak.CurrentLine, 0);

                CanvasPeakDisplay.Children.Add(tPeak.CurrentLine);
            }
        }

        private void MenuItemContextSaveImage_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "PNG Image (*.png)|*.png";

            if ((bool)sfd.ShowDialog() == true)
            {

                PngBitmapEncoder encoder = new PngBitmapEncoder();

                RenderTargetBitmap bitmap = new RenderTargetBitmap
                    (
                        (int)GridMainGrid.ActualWidth + 30,
                        (int)GridMainGrid.ActualHeight + 30,
                        96,
                        96,
                        PixelFormats.Pbgra32
                    );

                bitmap.Render(GridMainGrid);

                BitmapFrame frame = BitmapFrame.Create(bitmap);

                encoder.Frames.Add(frame);

                using (FileStream stream = new FileStream(sfd.FileName, FileMode.Create))
                {
                    encoder.Save(stream);
                }
            }
        }
    }
}