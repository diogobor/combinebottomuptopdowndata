﻿using PatternTools.PTMMods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SpectrumViewer2.PeprideSpectrumViewer
{
    /// <summary>
    /// Interaction logic for MSViewerWindow.xaml
    /// </summary>
    public partial class MSViewerWindow : Window
    {
        public MSViewerWindow()
        {
            InitializeComponent();
        }

        public void PlotSpectrum(List<Tuple<float, float>> theIons, double ms2ppm, string peptideSequence, List<Modification> thePTMs, bool showAnnotatedSequence = true, bool ionA = false, bool ionB = true, bool ionC = false, bool ionX = false, bool ionY = true, bool ionZ = false, int isotope = 2)
        {
            TheViewerControl.MyMS2Ions = theIons;
            TheViewerControl.SetMS2PPM = ms2ppm;
            TheViewerControl.Modifications = thePTMs;
            TheViewerControl.PeptideSequence = peptideSequence;
            TheViewerControl.ShowAnnotatedSequence = showAnnotatedSequence;
            TheViewerControl.IonA = ionA;
            TheViewerControl.IonB = ionB;
            TheViewerControl.IonC = ionC;
            TheViewerControl.IonX = ionX;
            TheViewerControl.IonY = ionY;
            TheViewerControl.IonZ = ionZ;
            TheViewerControl.Isotope = isotope;
            TheViewerControl.Plot();
        }
    }
}
