﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Linq;
using System.Windows;
using PatternTools.SpectraPrediction;
using PatternTools.PTMMods;

namespace SpectrumViewer2.PeptideSpectrumViewer
{
    /// <summary>
    /// Interaction logic for MSViewer.xaml
    /// </summary>
    public partial class MSViewer : UserControl
    {
        public bool ShowAnnotatedSequence { get; set; }

        public List<Modification> Modifications
        {
            get;
            set;
        }

        public int Isotope { get; set; }

        private bool _ionA;
        public bool IonA
        {
            get { return _ionA; }
            set
            {
                checkBoxA.IsChecked = value;
                _ionA = value;
            }
        }

        private bool _ionB;
        public bool IonB
        {
            get { return _ionB; }
            set
            {
                checkBoxB.IsChecked = value;
                _ionB = value;
            }
        }

        private bool _ionC;
        public bool IonC
        {
            get { return _ionC; }
            set
            {
                checkBoxC.IsChecked = value;
                _ionC = value;
            }
        }

        private bool _ionX;
        public bool IonX
        {
            get { return _ionX; }
            set
            {
                checkBoxX.IsChecked = value;
                _ionX = value;
            }
        }

        private bool _ionY;
        public bool IonY
        {
            get { return _ionY; }
            set
            {
                checkBoxY.IsChecked = value;
                _ionY = value;
            }
        }

        private bool _ionZ;
        public bool IonZ
        {
            get { return _ionZ; }
            set
            {
                checkBoxZ.IsChecked = value;
                _ionZ = value;
            }
        }

        public bool IonNH3
        {
            set { checkBoxNLNH3.IsChecked = value; }
        }

        public bool IonH2O
        {
            set { checkBoxNLH2O.IsChecked = value; }
        }

        /// <summary>
        /// Please input the MS2 ions before giving a plot
        /// </summary>
        public List<Tuple<float, float>> MyMS2Ions { get; set; }

        //These variable must always be specified before calling any plotting method
        //We need them to find the precursor neutral losses
        public double RelativeIntensityThreshold
        {
            get { return double.Parse(textBoxRelativeIntensityThreshold.Text); }
            set { textBoxRelativeIntensityThreshold.Text = value.ToString(); }
        }

        public double SetMS2PPM
        {
            set
            {
                textBoxPPM.Text = value.ToString();
            }
        }

        public string PeptideSequence
        {
            set
            {
                textBoxSequence.Text = value;
            }
        }

        public MSViewer()
        {
            InitializeComponent();
        }



        private void buttonPlot_Click(object sender, RoutedEventArgs e)
        {
            Plot();
        }


        public void Plot(List<PredictedIon> peaks = null)
        {
            MyMS2Ions.Sort((a, b) => a.Item1.CompareTo(b.Item1));

            if (peaks == null)
            {
                SpectrumWizard.PeptideMSPredictor sp = new SpectrumWizard.PeptideMSPredictor(0, 25, 200, 5000) { ChargeMax = Isotope };
                sp.StaticMods = Modifications.FindAll(a => a.IsVariable == false).ToList();
                if (IonA)
                    sp.AddASeries = true;
                if (IonB)
                    sp.AddBSeries = true;
                if (IonC)
                    sp.AddCSeries = true;
                if (IonX)
                    sp.AddXSeries = true;
                if (IonY)
                    sp.AddYSeries = true;
                if (IonZ)
                    sp.AddZSeries = true;

                if (textBoxSequence.Text.Length > 0)
                {
                    string peptide = PatternTools.pTools.CleanPeptide(textBoxSequence.Text, false);
                    peaks = sp.PredictAnnotationSpectrum(peptide);
                }
                else
                {
                    peaks = new List<PredictedIon>();
                }
            }

            dataGridPeakViewer.ItemsSource = peaks;

            //New spec viewer ---------------------------------------------------------
            List<Tuple<float, float>> ionsUltraLight = MyMS2Ions.FindAll(a => a.Item2 > 0);
            SpectrumEye1.DefaultStartMZ = ionsUltraLight[0].Item1 - 1;

            if (peaks != null)
            {
                double ppm = double.Parse(textBoxPPM.Text);
                Dictionary<Tuple<float, float>, List<PredictedIon>> anotation = new Dictionary<Tuple<float, float>, List<PredictedIon>>();

                foreach (Tuple<float, float> il in ionsUltraLight)
                {
                    List<PredictedIon> pMatches = peaks.FindAll(a => Math.Abs(PatternTools.pTools.PPM(il.Item1, a.MZ)) < ppm).ToList();

                    if (pMatches.Count > 0)
                    {
                        foreach (PredictedIon pp in pMatches)
                        {
                            pp.Accepted = true;
                            pp.Matched = true;
                        }

                        if (anotation.ContainsKey(il))
                        {
                            anotation[il].AddRange(pMatches);
                        }
                        else
                        {
                            anotation.Add(il, pMatches);
                        }
                    }
                }

                double matches = peaks.FindAll(a => a.Matched == true).Count;
                double total = peaks.Count;
                labelPeaksProduced.Content = matches + " / " + total + " = " + Math.Round(matches / total, 1) * 100 + "%";


                SpectrumEye1.ShowAnnotatedSequence = ShowAnnotatedSequence;
                SpectrumEye1.WantedSeries = new List<IonSeries>() { IonSeries.B, IonSeries.Y, IonSeries.C, IonSeries.Z, IonSeries.Extra };
                SpectrumEye1.LoadSpectra(ionsUltraLight, anotation, peaks);

            }
            else
            {
                SpectrumEye1.LoadSpectra(ionsUltraLight);
            }

            SpectrumEye1.Plot();

            textBoxSpectrumPeaks.Clear();
            foreach (Tuple<float, float> t in MyMS2Ions)
            {
                textBoxSpectrumPeaks.Text += t.Item1 + "\t" + t.Item2 + "\n";
            }
        }
    }
}
