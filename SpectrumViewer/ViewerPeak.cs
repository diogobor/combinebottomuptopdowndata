﻿using PatternTools.MSParser;
using PatternTools.SpectraPrediction;
using SpectrumViewer2.PeakAnnotationManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SpectrumViewer2
{
    public class ViewerPeak
    {
        public bool IsTheoreticalPeak = false;

        public Tuple<float, float> Ion;
        
        public PredictedIon[] Matches;
        public PredictedIon CurrentMatch
        {
            get
            {
                if (Matches == null || Matches.Count() == 0 || currentMatchIndex < 0)
                    return null;
                return Matches[currentMatchIndex];
            }
            set
            {
                for(int i = 0; i < Matches.Count(); i++)
                {
                    if(Matches[i] == value)
                    {
                        currentMatchIndex = i;
                        return;
                    }
                }
                currentMatchIndex = -1;
            }
        }
        private int currentMatchIndex;
        public Line CurrentLine;

        private TextBlock currentTextBlock;
        public TextBlock CurrentTextBlock {
            get => currentTextBlock ?? (currentTextBlock = SetupTextBlock());
            set => currentTextBlock = value; }

        private TextBlock SetupTextBlock()
        {
            TextBlock tb = new TextBlock()
            {
                HorizontalAlignment = HorizontalAlignment.Center,
                FontSize = 10,
                Text = ""
            };

            CurrentTextBlock = tb;

            tb.Text += Math.Round(Ion.Item1, 3).ToString();

            if (CurrentMatch != null)
            {
                tb.Text += "\n" + CurrentMatch.Series +
                    ((CurrentMatch.Series != IonSeries.Precursor) ?
                    CurrentMatch.Number.ToString() : "");

                tb.Text += "\nCharge: " + ((CurrentMatch.Charge > 0) ? "+" : "") + CurrentMatch.Charge;
            }

            return tb;
        }

        public ViewerPeak() { }

        public ViewerPeak(Tuple<float, float> a, PredictedIon[] matches, Line line, TextBlock text)
        {
            this.Ion = a;
            this.Matches = matches;
            this.CurrentLine = line;
            this.CurrentTextBlock = text;
            currentMatchIndex = 0;
        }
        
        public void CircleCurrentMatch(AnnotationSettings settings)
        {
            if (Matches == null)
                return;

            currentMatchIndex++;
            if (currentMatchIndex == Matches.Count())
            {
                currentMatchIndex = 0;
            }

            int tries = Matches.Count();
            while (tries > 0)
            {
                if (CurrentMatch.Accepted 
                    && settings.CheckLegal(CurrentMatch.Series) )
                {
                    break;
                }
                {
                    currentMatchIndex++;
                    if (currentMatchIndex == Matches.Count())
                    {
                        currentMatchIndex = 0;
                    }
                    tries--;
                }
            }

            if (tries == 0)
                currentMatchIndex = -1;
        }

        internal void CircleIfIllegal(AnnotationSettings myAnnotationSettings)
        {
            if (CurrentMatch == null)
                return;

            if (myAnnotationSettings.CheckLegal(CurrentMatch.Series) == false)
            {
                CircleCurrentMatch(myAnnotationSettings);
            }
        }

        internal void RefreshAnnotations(int matchThickness)
        {
            CurrentLine.ToolTip = "M/Z: " + Ion.Item1 + "\n";
            CurrentLine.ToolTip += "Intensity: " + Ion.Item2 + "\n";

            CurrentLine.StrokeThickness = 1;
            CurrentLine.Stroke = Brushes.Black;

            if (CurrentMatch != null)
            {
                if (CurrentMatch.Series == IonSeries.A || CurrentMatch.Series == IonSeries.B || CurrentMatch.Series == IonSeries.C || CurrentMatch.Series == IonSeries.D || CurrentMatch.Series == IonSeries.AmB)
                {
                    CurrentLine.ToolTip += $"Match M/Z: { Math.Round(CurrentMatch.MZ, 4)}\n";
                    CurrentLine.Stroke = Brushes.Red;
                    CurrentLine.StrokeThickness = matchThickness;
                }

                if (CurrentMatch.Series == IonSeries.X || CurrentMatch.Series == IonSeries.Y || CurrentMatch.Series == IonSeries.Z || CurrentMatch.Series == IonSeries.W)
                {
                    CurrentLine.ToolTip += $"Match M/Z: { Math.Round(CurrentMatch.MZ, 4)}\n";
                    CurrentLine.Stroke = Brushes.Blue;
                    CurrentLine.StrokeThickness = matchThickness;
                }

                if (CurrentMatch.Series == IonSeries.Precursor)
                {
                    CurrentLine.ToolTip += $"Match M/Z: { Math.Round(CurrentMatch.MZ, 4)}\n";
                    CurrentLine.Stroke = Brushes.Gold;
                    CurrentLine.StrokeThickness = matchThickness;
                }

                //if (CurrentMatch.FinalAA.Contains("-NH3") || CurrentMatch.FinalAA.Contains("-H20"))
                //{
                //    CurrentLine.Stroke = Brushes.Green;
                //    CurrentLine.StrokeThickness = matchThickness;
                //}

                CurrentLine.ToolTip += $"Z : " + CurrentMatch.Charge + "\n";
                if (CurrentMatch.Series == IonSeries.Precursor)
                    CurrentLine.ToolTip += "Series: " + CurrentMatch.Series + "\n";
                else
                    CurrentLine.ToolTip += "Series: " + CurrentMatch.Series + CurrentMatch.Number.ToString() + "\n";

                CurrentLine.ToolTip += "Sequence Item: " + CurrentMatch.FinalAA + "\n";
                CurrentLine.ToolTip += "Isotope Number: " + CurrentMatch.IsotopeNumber;

            }

            //Refresh textblock content
            if(currentTextBlock != null)
            {
                var tb = CurrentTextBlock;

                tb.Text = "";
                tb.Text += Math.Round(Ion.Item1, 3).ToString();

                if (CurrentMatch != null)
                {
                    tb.Text += "\n" + CurrentMatch.Series +
                        ((CurrentMatch.Series != IonSeries.Precursor) ?
                        CurrentMatch.Number.ToString() : "");

                    tb.Text += "\nCharge: " + ((CurrentMatch.Charge > 0) ? "+" : "") + CurrentMatch.Charge;
                }
            }
        }

    }
}
