﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SpectrumViewer2.PeakAnnotationManagement
{
    /// <summary>
    /// Interaction logic for ControlAnnotationSelect.xaml
    /// </summary>
    public  partial class ControlAnnotationSelect : UserControl
    {
        public AnnotationSettings Settings { get; set; }

        internal ControlAnnotationSelect(AnnotationSettings settings)
        {
            Settings = settings;
            InitializeComponent();

            CheckBoxLeftSide.IsChecked = Settings.LeftSide;
            CheckBoxRightSide.IsChecked = Settings.RightSide;
            CheckBoxTheoreticalSpectrum.IsChecked = Settings.TheoreticalSpectrum;
            CheckBoxInternalFragments.IsChecked = Settings.InternalFragments;
            CheckBoxPrecursor.IsChecked = Settings.Precursor;
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).Close();
        }

        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            Settings = new AnnotationSettings()
            {
                LeftSide = (bool)CheckBoxLeftSide.IsChecked,
                RightSide = (bool)CheckBoxRightSide.IsChecked,
                InternalFragments = (bool)CheckBoxInternalFragments.IsChecked,
                TheoreticalSpectrum = (bool)CheckBoxTheoreticalSpectrum.IsChecked,
                Precursor = (bool)CheckBoxPrecursor.IsChecked
            };
            Window.GetWindow(this).Close();
        }
    }
}
