﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PatternTools.SpectraPrediction;

namespace SpectrumViewer2.PeakAnnotationManagement
{
    public class AnnotationSettings
    {
        public bool LeftSide { get; set; } = true;
        public bool RightSide { get; set; } = true;

        public bool Precursor { get; set; } = true;

        public bool InternalFragments { get; set; } = false;
        public bool TheoreticalSpectrum { get; set; } = false;

        internal bool CheckLegal(IonSeries series)
        {
            if(series == IonSeries.AmB || series == IonSeries.A || series == IonSeries.B || series == IonSeries.C || series == IonSeries.D)
            {
                return LeftSide;
            } else if(series == IonSeries.W || series == IonSeries.X || series == IonSeries.Y || series == IonSeries.Z)
            {
                return RightSide;
            } else if(series == IonSeries.Precursor)
            {
                return Precursor;
            }

            return false;
        }
    }
}
