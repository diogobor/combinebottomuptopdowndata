﻿/**
 * Program:     ProteoCombiner - Integrating bottom-up & top-down proteomics data
 * Author:      Diogo Borges Lima
 * Created:     4/3/2019
 * Update by:   Diogo Borges Lima
 * Description: Class responsable for saving all program parameters
 */
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombineBottomUpTopDown.Utils
{
    [Serializable]
    [ProtoContract]
    public class ProgramParams
    {
        /// <summary>
        /// Public variables
        /// </summary>
        [ProtoMember(1)]
        public string BottomUpDirectory { get; set; }
        [ProtoMember(2)]
        public string TopDownDirectory { get; set; }
        [ProtoMember(3)]
        public byte[] ProcessingTime { get; set; }
        [ProtoMember(4)]
        public byte[] ProgramVersion { get; set; }
        [ProtoMember(5)]
        public bool RemoveContaminants { get; set; }
        [ProtoMember(6)]
        public bool RemoveRevSeq { get; set; }

        /// <summary>
        /// Null Constructor for serializing Class
        /// </summary>
        public ProgramParams() { }
    }
}
