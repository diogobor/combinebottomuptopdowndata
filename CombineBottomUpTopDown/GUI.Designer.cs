﻿namespace CombineBottomUpTopDown
{
    partial class GUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GUI));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.utilsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resultsBrowserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readMeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.timerStatus = new System.Windows.Forms.Timer(this.components);
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBoxLog = new System.Windows.Forms.GroupBox();
            this.listBoxConsole = new System.Windows.Forms.ListBox();
            this.groupBoxMainWindow = new System.Windows.Forms.GroupBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonTopDownResults = new System.Windows.Forms.Button();
            this.buttonBottomUpResults = new System.Windows.Forms.Button();
            this.textBoxTopDownData = new System.Windows.Forms.TextBox();
            this.textBoxBottomUpData = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBoxAdvancedParams = new System.Windows.Forms.GroupBox();
            this.checkBoxRemoveRevSeq = new System.Windows.Forms.CheckBox();
            this.checkBoxRemoveContaminants = new System.Windows.Forms.CheckBox();
            this.toolTips = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBoxLog.SuspendLayout();
            this.groupBoxMainWindow.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBoxAdvancedParams.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.utilsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(574, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripMenuItem.Image")));
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.openToolStripMenuItem.Text = "Load Results";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(180, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("exitToolStripMenuItem.Image")));
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // utilsToolStripMenuItem
            // 
            this.utilsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resultsBrowserToolStripMenuItem});
            this.utilsToolStripMenuItem.Name = "utilsToolStripMenuItem";
            this.utilsToolStripMenuItem.Size = new System.Drawing.Size(42, 20);
            this.utilsToolStripMenuItem.Text = "Utils";
            // 
            // resultsBrowserToolStripMenuItem
            // 
            this.resultsBrowserToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("resultsBrowserToolStripMenuItem.Image")));
            this.resultsBrowserToolStripMenuItem.Name = "resultsBrowserToolStripMenuItem";
            this.resultsBrowserToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.resultsBrowserToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.resultsBrowserToolStripMenuItem.Text = "Results Browser";
            this.resultsBrowserToolStripMenuItem.Click += new System.EventHandler(this.resultsBrowserToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readMeToolStripMenuItem,
            this.toolStripSeparator2,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // readMeToolStripMenuItem
            // 
            this.readMeToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("readMeToolStripMenuItem.Image")));
            this.readMeToolStripMenuItem.Name = "readMeToolStripMenuItem";
            this.readMeToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.readMeToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.readMeToolStripMenuItem.Text = "ReadMe";
            this.readMeToolStripMenuItem.Click += new System.EventHandler(this.readMeToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(133, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("aboutToolStripMenuItem.Image")));
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 341);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(574, 22);
            this.statusStrip1.TabIndex = 9;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(248, 17);
            this.toolStripStatusLabel1.Text = "@2020 - Institut Pasteur - All rights reserved®";
            // 
            // timerStatus
            // 
            this.timerStatus.Tick += new System.EventHandler(this.timerStatus_Tick);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, 27);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(574, 315);
            this.tabControl1.TabIndex = 11;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBoxLog);
            this.tabPage1.Controls.Add(this.groupBoxMainWindow);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(566, 289);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Combine";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBoxLog
            // 
            this.groupBoxLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxLog.Controls.Add(this.listBoxConsole);
            this.groupBoxLog.Location = new System.Drawing.Point(6, 125);
            this.groupBoxLog.Name = "groupBoxLog";
            this.groupBoxLog.Size = new System.Drawing.Size(552, 158);
            this.groupBoxLog.TabIndex = 12;
            this.groupBoxLog.TabStop = false;
            this.groupBoxLog.Text = "Log";
            // 
            // listBoxConsole
            // 
            this.listBoxConsole.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxConsole.FormattingEnabled = true;
            this.listBoxConsole.HorizontalScrollbar = true;
            this.listBoxConsole.Location = new System.Drawing.Point(6, 19);
            this.listBoxConsole.Name = "listBoxConsole";
            this.listBoxConsole.ScrollAlwaysVisible = true;
            this.listBoxConsole.Size = new System.Drawing.Size(540, 134);
            this.listBoxConsole.TabIndex = 15;
            // 
            // groupBoxMainWindow
            // 
            this.groupBoxMainWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxMainWindow.Controls.Add(this.buttonOK);
            this.groupBoxMainWindow.Controls.Add(this.buttonTopDownResults);
            this.groupBoxMainWindow.Controls.Add(this.buttonBottomUpResults);
            this.groupBoxMainWindow.Controls.Add(this.textBoxTopDownData);
            this.groupBoxMainWindow.Controls.Add(this.textBoxBottomUpData);
            this.groupBoxMainWindow.Controls.Add(this.label2);
            this.groupBoxMainWindow.Controls.Add(this.label1);
            this.groupBoxMainWindow.Location = new System.Drawing.Point(6, 2);
            this.groupBoxMainWindow.Name = "groupBoxMainWindow";
            this.groupBoxMainWindow.Size = new System.Drawing.Size(552, 117);
            this.groupBoxMainWindow.TabIndex = 11;
            this.groupBoxMainWindow.TabStop = false;
            // 
            // buttonOK
            // 
            this.buttonOK.Image = ((System.Drawing.Image)(resources.GetObject("buttonOK.Image")));
            this.buttonOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonOK.Location = new System.Drawing.Point(221, 87);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(135, 23);
            this.buttonOK.TabIndex = 4;
            this.buttonOK.Text = "OK";
            this.buttonOK.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonOK.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonTopDownResults
            // 
            this.buttonTopDownResults.Image = ((System.Drawing.Image)(resources.GetObject("buttonTopDownResults.Image")));
            this.buttonTopDownResults.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonTopDownResults.Location = new System.Drawing.Point(470, 48);
            this.buttonTopDownResults.Name = "buttonTopDownResults";
            this.buttonTopDownResults.Size = new System.Drawing.Size(71, 28);
            this.buttonTopDownResults.TabIndex = 3;
            this.buttonTopDownResults.Text = "Browse";
            this.buttonTopDownResults.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTips.SetToolTip(this.buttonTopDownResults, "Specify the directory where are the RAW and result files, as well as the protein " +
        "database. For more information, access the Read Me on Help menu.");
            this.buttonTopDownResults.UseVisualStyleBackColor = true;
            this.buttonTopDownResults.Click += new System.EventHandler(this.buttonTopDownResults_Click);
            // 
            // buttonBottomUpResults
            // 
            this.buttonBottomUpResults.Image = ((System.Drawing.Image)(resources.GetObject("buttonBottomUpResults.Image")));
            this.buttonBottomUpResults.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonBottomUpResults.Location = new System.Drawing.Point(470, 11);
            this.buttonBottomUpResults.Name = "buttonBottomUpResults";
            this.buttonBottomUpResults.Size = new System.Drawing.Size(71, 29);
            this.buttonBottomUpResults.TabIndex = 1;
            this.buttonBottomUpResults.Text = "Browse";
            this.buttonBottomUpResults.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTips.SetToolTip(this.buttonBottomUpResults, "Specify the directory where are the RAW and result files, as well as the protein " +
        "database. For more information, access the Read Me on Help menu.");
            this.buttonBottomUpResults.UseVisualStyleBackColor = true;
            this.buttonBottomUpResults.Click += new System.EventHandler(this.buttonBottomUpResults_Click);
            // 
            // textBoxTopDownData
            // 
            this.textBoxTopDownData.Location = new System.Drawing.Point(103, 52);
            this.textBoxTopDownData.Name = "textBoxTopDownData";
            this.textBoxTopDownData.Size = new System.Drawing.Size(361, 20);
            this.textBoxTopDownData.TabIndex = 2;
            this.toolTips.SetToolTip(this.textBoxTopDownData, "Specify the directory where are the RAW and result files, as well as the protein " +
        "database. For more information, access the Read Me on Help menu.");
            // 
            // textBoxBottomUpData
            // 
            this.textBoxBottomUpData.Location = new System.Drawing.Point(103, 15);
            this.textBoxBottomUpData.Name = "textBoxBottomUpData";
            this.textBoxBottomUpData.Size = new System.Drawing.Size(361, 20);
            this.textBoxBottomUpData.TabIndex = 0;
            this.toolTips.SetToolTip(this.textBoxBottomUpData, "Specify the directory where are the RAW and result files, as well as the protein " +
        "database. For more information, access the Read Me on Help menu.");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Top-down results:";
            this.toolTips.SetToolTip(this.label2, "Specify the directory where are the RAW and result files, as well as the protein " +
        "database. For more information, access the Read Me on Help menu.");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Bottom-up results:";
            this.toolTips.SetToolTip(this.label1, "Specify the directory where are the RAW and result files, as well as the protein " +
        "database. For more information, access the Read Me on Help menu.");
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBoxAdvancedParams);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(566, 289);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Parameters";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBoxAdvancedParams
            // 
            this.groupBoxAdvancedParams.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxAdvancedParams.Controls.Add(this.checkBoxRemoveRevSeq);
            this.groupBoxAdvancedParams.Controls.Add(this.checkBoxRemoveContaminants);
            this.groupBoxAdvancedParams.Location = new System.Drawing.Point(6, 2);
            this.groupBoxAdvancedParams.Name = "groupBoxAdvancedParams";
            this.groupBoxAdvancedParams.Size = new System.Drawing.Size(554, 281);
            this.groupBoxAdvancedParams.TabIndex = 12;
            this.groupBoxAdvancedParams.TabStop = false;
            // 
            // checkBoxRemoveRevSeq
            // 
            this.checkBoxRemoveRevSeq.AutoSize = true;
            this.checkBoxRemoveRevSeq.Checked = true;
            this.checkBoxRemoveRevSeq.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxRemoveRevSeq.Location = new System.Drawing.Point(13, 42);
            this.checkBoxRemoveRevSeq.Name = "checkBoxRemoveRevSeq";
            this.checkBoxRemoveRevSeq.Size = new System.Drawing.Size(166, 17);
            this.checkBoxRemoveRevSeq.TabIndex = 1;
            this.checkBoxRemoveRevSeq.Text = "Remove Reverse Sequences";
            this.checkBoxRemoveRevSeq.UseVisualStyleBackColor = true;
            // 
            // checkBoxRemoveContaminants
            // 
            this.checkBoxRemoveContaminants.AutoSize = true;
            this.checkBoxRemoveContaminants.Checked = true;
            this.checkBoxRemoveContaminants.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxRemoveContaminants.Location = new System.Drawing.Point(13, 19);
            this.checkBoxRemoveContaminants.Name = "checkBoxRemoveContaminants";
            this.checkBoxRemoveContaminants.Size = new System.Drawing.Size(133, 17);
            this.checkBoxRemoveContaminants.TabIndex = 0;
            this.checkBoxRemoveContaminants.Text = "Remove Contaminants";
            this.checkBoxRemoveContaminants.UseVisualStyleBackColor = true;
            // 
            // GUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 363);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "GUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ProteoCombiner: Integrating top-down, bottom-up and middle-down data";
            this.Load += new System.EventHandler(this.GUI_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBoxLog.ResumeLayout(false);
            this.groupBoxMainWindow.ResumeLayout(false);
            this.groupBoxMainWindow.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBoxAdvancedParams.ResumeLayout(false);
            this.groupBoxAdvancedParams.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem utilsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resultsBrowserToolStripMenuItem;
        private System.Windows.Forms.Timer timerStatus;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBoxLog;
        private System.Windows.Forms.ListBox listBoxConsole;
        private System.Windows.Forms.GroupBox groupBoxMainWindow;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonTopDownResults;
        private System.Windows.Forms.Button buttonBottomUpResults;
        private System.Windows.Forms.TextBox textBoxTopDownData;
        private System.Windows.Forms.TextBox textBoxBottomUpData;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBoxAdvancedParams;
        private System.Windows.Forms.CheckBox checkBoxRemoveRevSeq;
        private System.Windows.Forms.CheckBox checkBoxRemoveContaminants;
        private System.Windows.Forms.ToolStripMenuItem readMeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolTip toolTips;
    }
}

