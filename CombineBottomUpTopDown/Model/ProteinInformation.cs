﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombineBottomUpTopDown.Model
{ 
    public class ProteinInformation
    {
        public string ProteinSequence { get; set; }
        public string OriginalProteinSequence { get; set; }
        public string ProteinID { get; set; }
        public string IsReviewed { get; set; }
        public string AANumber { get; set; }
        public List<string> ACLines { get; set; }
        public List<string> DTLines { get; set; }
        public List<string> DELines { get; set; }
        public List<string> GNLines { get; set; }
        public List<string> OSLines { get; set; }
        public List<string> OCLines { get; set; }
        public List<string> OXLines { get; set; }
        public List<string> RNLines { get; set; }
        public List<string> RPLines { get; set; }
        public List<string> RCLines { get; set; }
        public List<string> RALines { get; set; }
        public List<string> RTLines { get; set; }
        public List<string> RLLines { get; set; }
        public List<string> CCLines { get; set; }
        public List<string> DRLines { get; set; }
        public List<string> PELines { get; set; }
        public List<string> KWLines { get; set; }
        public List<string> FTLines { get; set; }
        //Tuple<dataset, created, modified, version>
        public Tuple<string, string, string, string> EntryLine { get; set; }

        //List<Tuple<type,text>>
        public List<Tuple<string, string>> GeneLines { get; set; }

        //List<Tuple<key,Tuple<type,date,name,volume,first,last, title, List<Tuple<dbReferenceTupe,dbReferenceID>>>, scope, source/strain, List<person>>>
        public List<Tuple<int, Tuple<Tuple<string, string, string, string, string, string>, string, List<Tuple<string, string>>>, string, string, List<string>>> ReferenceLines { get; set; }

        //List<Tuple<Tuple<type, description, evidence or ID>, Tuple<original,variation, position>, Tuple<begin,end>>>
        public List<Tuple<Tuple<string, string, string>, Tuple<string, string, int>, Tuple<string, string>>> FeatureLines { get; set; }

        //List<Tuple<key, type>, Tuple<dbReferemceType, dbReferenceID>>
        public List<Tuple<Tuple<int, string>, Tuple<string, string>>> EvidenceLines { get; set; }

        //List<Tuple<length, mass, checksum, modified, version, sequence>>
        public Tuple<int, double, string, string, string, string> SequenceLine { get; set; }
        public string SQLine { get; set; }

        public ProteinInformation() { }

        public ProteinInformation(string proteinSequence,
            string proteinID,
            string isReviewed,
            string aaNumber,
            List<string> acLines,
            List<string> dtLines,
            List<string> deLines,
            List<string> gnLines,
            List<string> osLines,
            List<string> ocLines,
            List<string> oxLines,
            List<string> rnLines,
            List<string> rpLines,
            List<string> rcLines,
            List<string> raLines,
            List<string> rtLines,
            List<string> rlLines,
            List<string> ccLines,
            List<string> drLines,
            List<string> peLines,
            List<string> kwLines,
            List<string> ftLines,
            string sqLine,
            string originalProteinSequence
            )
        {
            ProteinSequence = proteinSequence;
            ProteinID = proteinID;
            IsReviewed = isReviewed;
            AANumber = aaNumber;
            ACLines = acLines;
            DTLines = dtLines;
            DELines = deLines;
            GNLines = gnLines;
            OSLines = osLines;
            OCLines = ocLines;
            OXLines = oxLines;
            RNLines = rnLines;
            RPLines = rpLines;
            RCLines = rcLines;
            RALines = raLines;
            RTLines = rtLines;
            RLLines = rlLines;
            CCLines = ccLines;
            DRLines = drLines;
            PELines = peLines;
            KWLines = kwLines;
            FTLines = ftLines;
            SQLine = sqLine;
            OriginalProteinSequence = originalProteinSequence;
        }

    }
}

