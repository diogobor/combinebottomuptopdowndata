﻿/**
 * Program:     ProteoCombiner - Integrating bottom-up & top-down proteomics data
 * Author:      Diogo Borges Lima
 * Created:     4/3/2019
 * Update by:   Diogo Borges Lima
 * Description: Class responsable for saving Proteins
 */
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombineBottomUpTopDown.Model
{
    [Serializable]
    [ProtoContract]
    public class Protein
    {
        /// <summary>
        /// Public variables
        /// </summary>
        [ProtoMember(1)]
        public List<Proteoform> Proteoforms { get; set; }
        [ProtoMember(2)]
        public List<Peptide> Peptides { get; set; }
        [ProtoMember(3)]
        public string Sequence { get; set; }
        [ProtoMember(4)]
        public string AccessionNumber { get; set; }
        [ProtoMember(5)]
        public string ProteinDescription { get; set; }
        [ProtoMember(6)]
        public double SequenceCoverage { get; set; }
        [ProtoMember(7)]
        public double Score { get; set; }

        /// <summary>
        /// Null Constructor for serializing Class
        /// </summary>
        public Protein() { }

        public Protein(List<Proteoform> proteoforms, List<Peptide> peptides, string sequence, string accessionNumber, string proteinDescription, double sequenceCoverage, double score)
        {
            Proteoforms = proteoforms;
            Peptides = peptides;
            Sequence = sequence;
            AccessionNumber = accessionNumber;
            ProteinDescription = proteinDescription;
            SequenceCoverage = sequenceCoverage;
            Score = score;
        }
    }

    public class ProteinComparer : IEqualityComparer<Protein>
    {
        public bool Equals(Protein x, Protein y)
        {
            //Check if the compared objects reference has same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check if any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check if the items' properties are equal.
            return (x.Sequence.Equals(y.Sequence) &&
                x.AccessionNumber.Equals(y.AccessionNumber) &&
               x.Score == y.Score &&
               x.ProteinDescription.Equals(y.ProteinDescription));
        }

        public int GetHashCode(Protein obj)
        {
            //return (int)obj.AnnotatedSequence.Length;
            return (int)obj.Sequence.Length;
        }
    }
}
