﻿/**
 * Program:     ProteoCombiner - Integrating bottom-up & top-down proteomics data
 * Author:      Diogo Borges Lima
 * Update:      4/3/2019
 * Update by:   Diogo Borges Lima
 * Description: ActivationType Enumaration
 */
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombineBottomUpTopDown.Model
{
    [Serializable]
    [ProtoContract]
    public enum ActivationType
    {
        CID, //collision-induced dissociation
        ETD, //electron transfer dissociation
        HCD, //high-energy collision-induced dissociation
        ECD, //electron capture dissociation
        MPD, //infrared multiphoton dissociation
        PQD, //pulsed q dissociation
        EThCD, //ETD and HCD
        NF,   //Not Found
    }
}
