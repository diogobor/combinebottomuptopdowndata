﻿/**
 * Program:     ProteoCombiner - Integrating bottom-up & top-down proteomics data
 * Author:      Diogo Borges Lima
 * Update:      4/3/2019
 * Update by:   Diogo Borges Lima
 * Description: Proteoform Class
 */
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CombineBottomUpTopDown.Model
{
    [Serializable]
    [ProtoContract]
    public class Proteoform
    {
        /// <summary>
        /// Public variables
        /// </summary>
        [ProtoMember(1)]
        public string Sequence { get; set; }
        [ProtoMember(2)]
        public string ProteinAccession { get; set; }
        [ProtoMember(3)]
        public string ProteinDescription { get; set; }
        [ProtoMember(4)]
        public string AnnotatedSequence { get; set; }
        [ProtoMember(5)]
        public List<ProteoformParams> PrSMs { get; set; }
        [ProtoMember(6)]
        public double SequenceCoverage { get; set; }

        /// <summary>
        /// Null Constructor for serializing Class
        /// </summary>
        public Proteoform() { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="sequence"></param>
        /// <param name="proteinAccession"></param>
        /// <param name="proteinDescription"></param>
        /// <param name="annotatedSequence"></param>
        /// <param name="_params"></param>
        public Proteoform(string sequence,
            string proteinAccession,
            string proteinDescription,
            string annotatedSequence,
            double sequenceCoverage,
            List<ProteoformParams> _params)
        {
            Sequence = sequence;
            ProteinAccession = proteinAccession;
            ProteinDescription = proteinDescription;
            AnnotatedSequence = annotatedSequence;
            SequenceCoverage = sequenceCoverage;
            PrSMs = _params;
        }

    }

    [Serializable]
    [ProtoContract]
    public class ProteoformParams
    {
        /// <summary>
        /// Public variables
        /// </summary>
        //[ProtoMember(1)]
        //public string Sequence { get; set; }
        //Tuple<aminoacid, position, description>
        [ProtoMember(1)]
        public List<Tuple<string, int, string>> Modifications { get; set; }
        //[ProtoMember(3)]
        //public string ProteinAccession { get; set; }
        //[ProtoMember(4)]
        //public string ProteinDescription { get; set; }
        [ProtoMember(2)]
        public int Charge { get; set; }
        [ProtoMember(3)]
        public double MZ { get; set; }
        [ProtoMember(4)]
        public double MHPlus { get; set; }
        [ProtoMember(5)]
        public double TheoreticalMass { get; set; }
        [ProtoMember(6)]
        public double DeltaMass { get; set; }
        [ProtoMember(7)]
        public double DeltaMZ { get; set; }
        [ProtoMember(8)]
        public int MatchedIons { get; set; }
        [ProtoMember(9)]
        public int TotalIons { get; set; }
        //Tuple<mz,intensity>
        [ProtoMember(10)]
        public List<Tuple<double, double>> FragmentIons { get; set; }
        [ProtoMember(11)]
        public int PrecursorCharge { get; set; }
        [ProtoMember(12)]
        public double Intensity { get; set; }
        [ProtoMember(13)]
        public ActivationType ActivationType { get; set; }
        [ProtoMember(14)]
        public double RetentionTime { get; set; }
        [ProtoMember(15)]
        public int ScanNumber { get; set; } // FirstScan column
        [ProtoMember(16)]
        public string SpectrumFile { get; set; }
        [ProtoMember(17)]
        public double LogPScore { get; set; }
        [ProtoMember(18)]
        public double LogEValue { get; set; }
        [ProtoMember(19)]
        public string FragmentMap { get; set; }
        [ProtoMember(20)]
        public double FinalScore { get; set; }
        //[ProtoMember(24)]
        //public string AnnotatedSequence { get; set; }
        [ProtoMember(21)]
        public bool IsValid { get; set; }
        [ProtoMember(22)]
        public string ProteinSequence { get; set; }//TopPIC identifications
        [ProtoMember(23)]
        public string SearchEngine { get; set; }//Prosight or TopPIC or pTop
        [ProtoMember(24)]
        public int IdentifiedNode { get; set; } = 3;//3 - intact proteoform; 6 - Truncated proteoform ; 9- Tagged Proteoform

        /// <summary>
        /// Constructor - Prosight identifications
        /// </summary>
        /// <param name="sequence"></param>
        /// <param name="modifications"></param>
        /// <param name="proteinAccession"></param>
        /// <param name="proteinDescription"></param>
        /// <param name="charge"></param>
        /// <param name="mZ"></param>
        /// <param name="mHPlus"></param>
        /// <param name="theoreticalMass"></param>
        /// <param name="deltaMass"></param>
        /// <param name="deltaMZ"></param>
        /// <param name="matchedIons"></param>
        /// <param name="totalIons"></param>
        /// <param name="intensity"></param>
        /// <param name="activationType"></param>
        /// <param name="retentionTime"></param>
        /// <param name="scanNumber"></param>
        /// <param name="spectrumFile"></param>
        /// <param name="logPScore"></param>
        /// <param name="logEValue"></param>
        /// <param name="fragmentMap"></param>
        public ProteoformParams(int identifiedNode, /*string sequence,*/ string modifications, /*string proteinAccession, string proteinDescription,*/ int charge, double mZ, double mHPlus, double theoreticalMass, double deltaMass, double deltaMZ, int matchedIons, int totalIons, double intensity, double retentionTime, int scanNumber, string spectrumFile, double logPScore, double logEValue, string fragmentMap/*, string annotatedSequence*/)
        {
            IdentifiedNode = identifiedNode;
            Modifications = SelectPTMsFromProsight(modifications);
            //Sequence = sequence;
            //ProteinAccession = proteinAccession;
            //ProteinDescription = proteinDescription;
            Charge = charge;
            MZ = mZ;
            MHPlus = mHPlus;
            TheoreticalMass = theoreticalMass;
            DeltaMass = deltaMass;
            DeltaMZ = deltaMZ;
            MatchedIons = matchedIons;
            TotalIons = totalIons;
            Intensity = intensity;
            RetentionTime = retentionTime;
            ScanNumber = scanNumber;
            SpectrumFile = spectrumFile;
            LogPScore = logPScore;
            LogEValue = logEValue;
            FragmentMap = fragmentMap;
            //AnnotatedSequence = annotatedSequence;
        }

        /// <summary>
        /// Constructor - TopPIC csv identifications
        /// </summary>
        /// <param name="sequence"></param>
        /// <param name="proteinAccession"></param>
        /// <param name="charge"></param>
        /// <param name="theoreticalMass"></param>
        /// <param name="matchedIons"></param>
        /// <param name="totalIons"></param>
        /// <param name="intensity"></param>
        /// <param name="scanNumber"></param>
        /// <param name="logPScore"></param>
        /// <param name="logEValue"></param>
        /// <param name="retentionTime"></param>
        /// <param name="proteinDescription"></param>
        /// <param name="spectrumFile"></param>
        public ProteoformParams(/*string sequence, string proteinAccession,*/ int charge, double theoreticalMass, int matchedIons, int totalIons, double intensity, int scanNumber, double logPScore, double logEValue, double retentionTime, /*string proteinDescription,*/ string spectrumFile, double deltaMass, List<Tuple<string, int, string>> modifications, int identifiedNode)
        {
            //Sequence = sequence;
            //ProteinAccession = proteinAccession;
            //ProteinDescription = proteinDescription;
            Charge = charge;
            TheoreticalMass = theoreticalMass;
            MZ = (TheoreticalMass / Charge) + ((Utils.Util.HYDROGENMASS / Charge) * (Charge - 1.0));
            MHPlus = TheoreticalMass;
            MatchedIons = matchedIons;
            TotalIons = totalIons;
            Intensity = intensity;
            ScanNumber = scanNumber;
            LogPScore = logPScore;
            LogEValue = logEValue;
            RetentionTime = retentionTime;
            SpectrumFile = spectrumFile;
            FragmentIons = new List<Tuple<double, double>>();
            //AnnotatedSequence = Sequence;
            Modifications = modifications;
            DeltaMass = deltaMass;
            IdentifiedNode = identifiedNode;
        }

        /// <summary>
        /// Constructor - TopPIC html identifications
        /// </summary>
        /// <param name="sequence"></param>
        /// <param name="proteinAccession"></param>
        /// <param name="charge"></param>
        /// <param name="mz"></param>
        /// <param name="theoreticalMass"></param>
        /// <param name="matchedIons"></param>
        /// <param name="totalIons"></param>
        /// <param name="intensity"></param>
        /// <param name="scanNumber"></param>
        /// <param name="logPScore"></param>
        /// <param name="logEValue"></param>
        /// <param name="fragmentIons"></param>
        /// <param name="proteinSequence"></param>
        public ProteoformParams( int charge, double mz, double theoreticalMass, int matchedIons, int totalIons, double intensity, int scanNumber, double logPScore, double logEValue, List<Tuple<double, double>> fragmentIons/*, string proteinSequence*/)
        {
            Charge = charge;
            TheoreticalMass = theoreticalMass;
            MZ = (TheoreticalMass / Charge) + ((Utils.Util.HYDROGENMASS / Charge) * (Charge - 1.0));
            MHPlus = TheoreticalMass;
            MatchedIons = matchedIons;
            TotalIons = totalIons;
            Intensity = intensity;
            ScanNumber = scanNumber;
            LogPScore = logPScore;
            LogEValue = logEValue;
            SpectrumFile = string.Empty;
            FragmentIons = fragmentIons;
            Modifications = new List<Tuple<string, int, string>>();
        }

        /// <summary>
        /// Constructor - pTop Identifications
        /// </summary>
        /// <param name="sequence"></param>
        /// <param name="proteinAccession"></param>
        /// <param name="charge"></param>
        /// <param name="theoreticalMass"></param>
        /// <param name="deltaMZ"></param>
        /// <param name="matchedIons"></param>
        /// <param name="scanNumber"></param>
        /// <param name="spectrumFile"></param>
        /// <param name="logPScore"></param>
        /// <param name="logEValue"></param>
        public ProteoformParams( int charge, double theoreticalMass, double deltaMZ, double deltaMass, int matchedIons, int scanNumber, string spectrumFile, double logPScore, double logEValue, string modifications)
        {
            Charge = charge;
            TheoreticalMass = theoreticalMass;
            MHPlus = TheoreticalMass;
            MZ = (TheoreticalMass / Charge) + ((Utils.Util.HYDROGENMASS / Charge) * (Charge - 1.0));
            DeltaMass = deltaMass;
            DeltaMZ = deltaMZ;
            MatchedIons = matchedIons;
            TotalIons = MatchedIons;
            ScanNumber = scanNumber;
            SpectrumFile = spectrumFile;
            LogPScore = logPScore;
            LogEValue = logEValue;
            FragmentMap = string.Empty;
            Modifications = SelectPTMsFromPTop(modifications);
            FragmentIons = new List<Tuple<double, double>>();
            ActivationType = ActivationType.CID;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="sequence"></param>
        /// <param name="modifications"></param>
        /// <param name="proteinAccession"></param>
        /// <param name="proteinDescription"></param>
        /// <param name="charge"></param>
        /// <param name="mZ"></param>
        /// <param name="mHPlus"></param>
        /// <param name="theoreticalMass"></param>
        /// <param name="deltaMass"></param>
        /// <param name="deltaMZ"></param>
        /// <param name="matchedIons"></param>
        /// <param name="totalIons"></param>
        /// <param name="intensity"></param>
        /// <param name="retentionTime"></param>
        /// <param name="scanNumber"></param>
        /// <param name="spectrumFile"></param>
        /// <param name="logPScore"></param>
        /// <param name="logEValue"></param>
        /// <param name="fragmentMap"></param>
        public ProteoformParams(List<Tuple<string, int, string>> modifications, /*string proteinAccession, string proteinDescription,*/ int charge, double mZ, double mHPlus, double theoreticalMass, double deltaMass, double deltaMZ, int matchedIons, int totalIons, double intensity, double retentionTime, int scanNumber, string spectrumFile, double logPScore, double logEValue, string fragmentMap/*, string annotatedSequence*/, bool isValid, string searchEngine, int identifiedNode)
        {
            //Sequence = sequence;
            Modifications = modifications;
            //ProteinAccession = proteinAccession;
            //ProteinDescription = proteinDescription;
            Charge = charge;
            MZ = mZ;
            MHPlus = mHPlus;
            TheoreticalMass = theoreticalMass;
            DeltaMass = deltaMass;
            DeltaMZ = deltaMZ;
            MatchedIons = matchedIons;
            TotalIons = totalIons;
            Intensity = intensity;
            RetentionTime = retentionTime;
            ScanNumber = scanNumber;
            SpectrumFile = spectrumFile;
            LogPScore = logPScore;
            LogEValue = logEValue;
            FragmentMap = fragmentMap;
            //AnnotatedSequence = annotatedSequence;
            IsValid = isValid;
            SearchEngine = searchEngine;
            IdentifiedNode = identifiedNode;
        }

        private List<Tuple<string, int, string>> SelectPTMsFromProsight(string modifications_original)
        {
            //Example: K20(Dimethyl) -> aminoacidPosition(description)
            if (String.IsNullOrEmpty(modifications_original)) return new List<Tuple<string, int, string>>();

            //Tuple<aminoacid, position, description>
            List<Tuple<string, int, string>> ptmList = new List<Tuple<string, int, string>>();

            string[] modifications = Regex.Split(modifications_original, ";");
            foreach (string modification in modifications)
            {
                if (modification.Trim().StartsWith("N-Term"))
                {
                    string description = modification.Trim().Substring(7, modification.Trim().Length - 8);
                    ptmList.Add(Tuple.Create("}", 1, description));
                }
                else
                {
                    string aminoacid = modification.Trim()[0].ToString();
                    int indexFirstPar = modification.Trim().IndexOf('(');
                    string position = modification.Trim().Substring(1, indexFirstPar - 1);
                    string description = modification.Trim().Substring(indexFirstPar + 1, modification.Trim().Length - indexFirstPar - 2);
                    ptmList.Add(Tuple.Create(aminoacid, Convert.ToInt32(position), description));
                }
            }

            return ptmList;
        }

        private List<Tuple<string, int, string>> SelectPTMsFromPTop(string modifications_original)
        {
            //Example: (20)Dimethyl[K] -> (position)description[amino acid]
            if (String.IsNullOrEmpty(modifications_original) || modifications_original.Equals("NULL")) return new List<Tuple<string, int, string>>();

            //Tuple<aminoacid, position, description>
            List<Tuple<string, int, string>> ptmList = new List<Tuple<string, int, string>>();

            string[] modifications = Regex.Split(modifications_original, ";");
            foreach (string modification in modifications)
            {
                int indexFirstClosePar = modification.Trim().IndexOf(')');
                int position = Convert.ToInt32(modification.Trim().Substring(1, indexFirstClosePar - 1));
                int indexFirstBraket = modification.Trim().IndexOf('[');
                string description = modification.Trim().Substring(indexFirstClosePar + 1, indexFirstBraket - 1 - indexFirstClosePar);
                string aminoacid = modification.Trim().Substring(indexFirstBraket + 1, 1);
                if (position == 0)
                    ptmList.Add(Tuple.Create("}", 1, description));
                else
                    ptmList.Add(Tuple.Create(aminoacid, position, description));
            }

            return ptmList;
        }

        /// <summary>
        /// Null Constructor for serializing Class
        /// </summary>
        public ProteoformParams() { Modifications = new List<Tuple<string, int, string>>(); }
    }

    public class ProteoformComparer : IEqualityComparer<Proteoform>
    {
        public bool Equals(Proteoform x, Proteoform y)
        {
            //Check if the compared objects reference has same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check if any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check if the items' properties are equal.
            return (x.Sequence.Equals(y.Sequence) &&
                x.ProteinAccession.Equals(y.ProteinAccession) &&
               x.ProteinDescription.Equals(y.ProteinDescription) &&
               x.PrSMs[0].Charge == y.PrSMs[0].Charge &&
               x.PrSMs[0].MZ == y.PrSMs[0].MZ &&
               x.PrSMs[0].MHPlus == y.PrSMs[0].MHPlus &&
               x.PrSMs[0].TheoreticalMass == y.PrSMs[0].TheoreticalMass &&
               x.PrSMs[0].ScanNumber == y.PrSMs[0].ScanNumber &&
               x.PrSMs[0].SpectrumFile.Equals(y.PrSMs[0].SpectrumFile) &&
               x.PrSMs[0].LogPScore == y.PrSMs[0].LogPScore &&
               x.AnnotatedSequence.Equals(y.AnnotatedSequence));
        }

        public int GetHashCode(Proteoform obj)
        {
            //return (int)obj.AnnotatedSequence.Length;
            return (int)obj.PrSMs[0].TheoreticalMass;
        }
    }

    public class MergeProteoformFromDiffTool : IEqualityComparer<Proteoform>
    {
        public bool Equals(Proteoform x, Proteoform y)
        {
            //Check if the compared objects reference has same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check if any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check if the items' properties are equal.
            if (x.PrSMs.Count == 1 && y.PrSMs.Count == 1 &&
                x.PrSMs[0].Modifications.Count == y.PrSMs[0].Modifications.Count &&
                x.Sequence.Equals(y.Sequence) &&
                 (x.ProteinAccession.Contains(y.ProteinAccession) || y.ProteinAccession.Contains(x.ProteinAccession)))

            {
                x.PrSMs.Add(y.PrSMs[0]);
                x.PrSMs.Sort((a, b) => b.FinalScore.CompareTo(a.FinalScore));
                if (!String.IsNullOrEmpty(x.ProteinDescription) && !x.ProteinDescription.Equals(y.ProteinDescription))
                    x.ProteinDescription += " - " + y.ProteinDescription;
                else
                    x.ProteinDescription = y.ProteinDescription;
                return true;
            }
            else if (x.PrSMs.Count > 1 && y.PrSMs.Count > 1 &&
                x.PrSMs.Count == y.PrSMs.Count &&
                x.Sequence.Equals(y.Sequence) &&
                 (x.ProteinAccession.Contains(y.ProteinAccession) || y.ProteinAccession.Contains(x.ProteinAccession)))

            {
                //Check modification amount
                bool hasSameMods = true;
                for (int i = 0; i < x.PrSMs.Count; i++)
                {
                    for (int j = 0; j < x.PrSMs[i].Modifications.Count; j++)
                    {
                        if (!(x.PrSMs[i].Modifications.Count == y.PrSMs[i].Modifications.Count &&
                              x.PrSMs[i].Modifications[j].Item1.Equals(y.PrSMs[i].Modifications[j].Item1) &&
                              x.PrSMs[i].Modifications[j].Item2 == y.PrSMs[i].Modifications[j].Item2 &&
                              x.PrSMs[i].Modifications[j].Item3.Equals(y.PrSMs[i].Modifications[j].Item3)))
                        {
                            hasSameMods = false;
                            break;
                        }
                    }
                }
                if (hasSameMods)
                {
                    x.PrSMs.AddRange(y.PrSMs);
                    x.PrSMs.Sort((a, b) => b.FinalScore.CompareTo(a.FinalScore));

                    if (!String.IsNullOrEmpty(x.ProteinDescription) && !x.ProteinDescription.Equals(y.ProteinDescription))
                        x.ProteinDescription += " - " + y.ProteinDescription;
                    else
                        x.ProteinDescription = y.ProteinDescription;
                    return true;
                }
                else
                    return false;
            }
            else if (x.PrSMs.Count == 1 && y.PrSMs.Count > 1 &&
                x.Sequence.Equals(y.Sequence) &&
                 (x.ProteinAccession.Contains(y.ProteinAccession) || y.ProteinAccession.Contains(x.ProteinAccession)))
            {
                //Check modification amount
                bool hasSameMods = true;

                for (int i = 0; i < y.PrSMs.Count; i++)
                {
                    for (int j = 0; j < y.PrSMs[i].Modifications.Count; j++)
                    {
                        if (!(x.PrSMs[0].Modifications.Count == y.PrSMs[i].Modifications.Count &&
                              x.PrSMs[0].Modifications[j].Item1.Equals(y.PrSMs[i].Modifications[j].Item1) &&
                              x.PrSMs[0].Modifications[j].Item2 == y.PrSMs[i].Modifications[j].Item2 &&
                              x.PrSMs[0].Modifications[j].Item3.Equals(y.PrSMs[i].Modifications[j].Item3)))
                        {
                            hasSameMods = false;
                            break;
                        }
                    }
                }
                if (hasSameMods)
                {
                    x.PrSMs.AddRange(y.PrSMs);
                    x.PrSMs.Sort((a, b) => b.FinalScore.CompareTo(a.FinalScore));

                    if (!String.IsNullOrEmpty(x.ProteinDescription) && !x.ProteinDescription.Equals(y.ProteinDescription))
                        x.ProteinDescription += " - " + y.ProteinDescription;
                    else
                        x.ProteinDescription = y.ProteinDescription;
                    return true;
                }
                else
                    return false;
            }
            else if (x.PrSMs.Count > 1 && y.PrSMs.Count == 1 &&
                x.Sequence.Equals(y.Sequence) &&
                 (x.ProteinAccession.Contains(y.ProteinAccession) || y.ProteinAccession.Contains(x.ProteinAccession)))
            {
                //Check modification amount
                bool hasSameMods = true;

                for (int i = 0; i < x.PrSMs.Count; i++)
                {
                    for (int j = 0; j < x.PrSMs[i].Modifications.Count; j++)
                    {
                        if (!(x.PrSMs[i].Modifications.Count == y.PrSMs[0].Modifications.Count &&
                              x.PrSMs[i].Modifications[j].Item1.Equals(y.PrSMs[0].Modifications[j].Item1) &&
                              x.PrSMs[i].Modifications[j].Item2 == y.PrSMs[0].Modifications[j].Item2 &&
                              x.PrSMs[i].Modifications[j].Item3.Equals(y.PrSMs[0].Modifications[j].Item3)))
                        {
                            hasSameMods = false;
                            break;
                        }
                    }
                }
                if (hasSameMods)
                {
                    x.PrSMs.AddRange(y.PrSMs);
                    x.PrSMs.Sort((a, b) => b.FinalScore.CompareTo(a.FinalScore));

                    if (!String.IsNullOrEmpty(x.ProteinDescription) && !x.ProteinDescription.Equals(y.ProteinDescription))
                        x.ProteinDescription += " - " + y.ProteinDescription;
                    else
                        x.ProteinDescription = y.ProteinDescription;
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        public int GetHashCode(Proteoform obj)
        {
            //return (int)obj.AnnotatedSequence.Length;
            return (int)obj.PrSMs[0].TheoreticalMass;
        }
    }

    public class SelectBestParamsForEachTool : IEqualityComparer<ProteoformParams>
    {
        public bool Equals(ProteoformParams x, ProteoformParams y)
        {
            //Check if the compared objects reference has same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check if any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check if the items' properties are equal.
            return (x.SearchEngine.Equals(y.SearchEngine));
        }

        public int GetHashCode(ProteoformParams obj)
        {
            //return (int)obj.AnnotatedSequence.Length;
            return (int)obj.TheoreticalMass;
        }
    }

    public class ProteoformSimpleBinarySearchComparer : IComparer<Proteoform>
    {
        public int Compare(Proteoform x, Proteoform y)
        {
            if (x.PrSMs == null) return -1;
            if (y.PrSMs == null) return 1;
            if (String.IsNullOrEmpty(x.PrSMs[0].SpectrumFile)) return -1;
            if (String.IsNullOrEmpty(y.PrSMs[0].SpectrumFile)) return 1;

            if (x.PrSMs[0].SpectrumFile.Contains(y.PrSMs[0].SpectrumFile))
            {
                if (x.PrSMs[0].ScanNumber == y.PrSMs[0].ScanNumber)
                    return 0;
                else if (x.PrSMs[0].ScanNumber > y.PrSMs[0].ScanNumber)
                    return 1;
                else
                    return -1;
            }
            else
                return 1;
        }
    }

    public class ProteoformBinarySearchComparer : IComparer<Proteoform>
    {
        public int Compare(Proteoform x, Proteoform y)
        {
            if (x.PrSMs == null) return -1;
            if (y.PrSMs == null) return 1;
            if (String.IsNullOrEmpty(x.Sequence)) return -1;
            if (String.IsNullOrEmpty(y.Sequence)) return 1;
            if (String.IsNullOrEmpty(x.PrSMs[0].SpectrumFile)) return -1;
            if (String.IsNullOrEmpty(y.PrSMs[0].SpectrumFile)) return 1;

            int retval = x.Sequence.Length.CompareTo(y.Sequence.Length);

            if (retval != 0)
            {
                return retval;
            }
            else
            {
                retval = x.Sequence.CompareTo(y.Sequence);

                if (retval != 0)
                {
                    return retval;
                }
                else
                {
                    retval = x.PrSMs[0].SpectrumFile.Length.CompareTo(y.PrSMs[0].SpectrumFile.Length);

                    if (retval != 0)
                    {
                        return retval;
                    }
                    else
                    {
                        retval = x.PrSMs[0].SpectrumFile.CompareTo(y.PrSMs[0].SpectrumFile);
                        if (retval != 0)
                            return retval;
                        else
                        {
                            if (x.PrSMs[0].ScanNumber == y.PrSMs[0].ScanNumber &&
                                x.PrSMs[0].TheoreticalMass == y.PrSMs[0].TheoreticalMass &&
                                x.PrSMs[0].LogPScore == y.PrSMs[0].LogPScore &&
                                x.PrSMs[0].LogEValue == y.PrSMs[0].LogEValue)
                                return 0;
                            else if (x.PrSMs[0].ScanNumber > y.PrSMs[0].ScanNumber)
                                return 1;
                            else
                                return -1;
                        }
                    }
                }
            }
        }
    }
}
