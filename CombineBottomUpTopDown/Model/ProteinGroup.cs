﻿/**
 * Program:     ProteoCombiner - Integrating bottom-up & top-down proteomics data
 * Author:      Diogo Borges Lima
 * Update:      4/3/2019
 * Update by:   Diogo Borges Lima
 * Description: ProteinGroupMQ
 */
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CombineBottomUpTopDown.Model
{
    [Serializable]
    [ProtoContract]
    public class ProteinGroup
    {
        /// <summary>
        /// variables of proteingroup.txt file
        /// </summary>
        [ProtoMember(1)]
        public string ProteinID { get; set; }
        [ProtoMember(2)]
        public List<string> FastaHeader { get; set; }
        [ProtoMember(3)]
        public int SequenceLength { get; set; }
        [ProtoMember(4)]
        public ulong MSMSCount { get; set; }
        [ProtoMember(5)]
        public List<ProteinGroupParams> Params { get; set; }
        [ProtoMember(6)]
        public string Sequence { get; set; }


        /// <summary>
        /// Null Constructor for serializing Class
        /// </summary>
        public ProteinGroup() { }

        /// <summary>
        /// Constructor - MaxQuant/Andromeda
        /// </summary>
        /// <param name="proteinID"></param>
        /// <param name="proteinGroupID"></param>
        /// <param name="peptideCounts"></param>
        /// <param name="fastaHeader"></param>
        /// <param name="sequenceLength"></param>
        /// <param name="proteinScore"></param>
        /// <param name="mSMSCount"></param>
        /// <param name="peptideIds"></param>
        /// <param name="modIds"></param>
        /// <param name="mSMSIds"></param>
        /// <param name="bestMSMSIds"></param>
        public ProteinGroup(string proteinID,
            ulong proteinGroupID,
            string peptideCounts,
            string fastaHeader,
            int sequenceLength,
            double proteinScore,
            ulong mSMSCount,
            string peptideIds,
            string modIds,
            string mSMSIds,
            string bestMSMSIds,
            string searchEngine,
            string sequence)
        {
            ProteinID = proteinID;
            string[] cols = Regex.Split(fastaHeader, ";");
            FastaHeader = new List<string>(cols);
            SequenceLength = sequenceLength;
            MSMSCount = mSMSCount;

            cols = Regex.Split(peptideIds, ";");
            List<ulong> PeptideIds = (from peptID in cols
                                      select Convert.ToUInt64(peptID)).ToList();
            cols = Regex.Split(peptideCounts, ";");
            List<ulong> PeptideCounts = (from peptCount in cols
                                         select Convert.ToUInt64(peptCount)).ToList();
            cols = Regex.Split(modIds, ";");
            List<ulong> ModIds = (from modID in cols
                                  select Convert.ToUInt64(modID)).ToList();
            cols = Regex.Split(mSMSIds, ";");
            List<ulong> MSMSIds = (from MSMSID in cols
                                   select Convert.ToUInt64(MSMSID)).ToList();
            cols = Regex.Split(bestMSMSIds, ";");
            List<ulong> BestMSMSIds = (from BestMSMS in cols
                                       select Convert.ToUInt64(BestMSMS)).ToList();

            Sequence = sequence;

            Params = new List<ProteinGroupParams>() { new ProteinGroupParams(proteinGroupID, PeptideCounts, proteinScore, PeptideIds, ModIds, MSMSIds, BestMSMSIds, searchEngine) };
        }
    }

    [Serializable]
    [ProtoContract]
    public class ProteinGroupParams
    {
        [ProtoMember(1)]
        public ulong ProteinGroupID { get; set; }
        [ProtoMember(2)]
        public List<ulong> PeptideCounts { get; set; }
        [ProtoMember(3)]
        public double ProteinScore { get; set; }
        [ProtoMember(4)]
        public List<ulong> PeptideIds { get; set; }
        [ProtoMember(5)]
        public List<ulong> ModIds { get; set; }
        [ProtoMember(6)]
        public List<ulong> MSMSIds { get; set; }
        [ProtoMember(7)]
        public List<ulong> BestMSMSIds { get; set; }
        [ProtoMember(8)]
        public string SearchEngine { get; set; }

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public ProteinGroupParams() { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="proteinGroupID"></param>
        /// <param name="peptideCounts"></param>
        /// <param name="proteinScore"></param>
        /// <param name="peptideIds"></param>
        /// <param name="modIds"></param>
        /// <param name="mSMSIds"></param>
        /// <param name="bestMSMSIds"></param>
        public ProteinGroupParams(ulong proteinGroupID, List<ulong> peptideCounts, double proteinScore, List<ulong> peptideIds, List<ulong> modIds, List<ulong> mSMSIds, List<ulong> bestMSMSIds, string searchEngine)
        {
            ProteinGroupID = proteinGroupID;
            PeptideCounts = peptideCounts;
            ProteinScore = proteinScore;
            PeptideIds = peptideIds;
            ModIds = modIds;
            MSMSIds = mSMSIds;
            BestMSMSIds = bestMSMSIds;
            SearchEngine = searchEngine;
        }
    }

    public class ProteinGroupComparer : IEqualityComparer<ProteinGroup>
    {
        public bool Equals(ProteinGroup x, ProteinGroup y)
        {
            //Check if the compared objects reference has same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check if any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check if the items' properties are equal.
            return (x.Params[0].ProteinGroupID == y.Params[0].ProteinGroupID && x.ProteinID.Equals(y.ProteinID));
        }

        public int GetHashCode(ProteinGroup obj)
        {
            return (int)obj.ProteinID.GetHashCode();
        }
    }

    public class MergeProteinGroupFromDiffTool : IEqualityComparer<ProteinGroup>
    {
        public bool Equals(ProteinGroup x, ProteinGroup y)
        {
            //Check if the compared objects reference has same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check if any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            if (String.IsNullOrEmpty(x.Sequence) || String.IsNullOrEmpty(y.Sequence)) return false;

            //Check if the items' properties are equal.
            if (x.Sequence == y.Sequence)
            {
                x.Params.AddRange(y.Params);
                x.FastaHeader.AddRange(y.FastaHeader);
                x.MSMSCount = (ulong)(x.Params.Sum(item => item.PeptideIds.Count) + y.Params.Sum(item => item.PeptideIds.Count));
                x.Params.Sort((a, b) => b.ProteinScore.CompareTo(a.ProteinScore));
                x.ProteinID += "|" + y.ProteinID;
                return true;
            }
            else
                return false;
        }

        public int GetHashCode(ProteinGroup obj)
        {
            return (int)obj.SequenceLength + (int)obj.Params.Count.GetHashCode();
        }
    }

    public class ProteinGroupBinarySearchComparer : IComparer<ProteinGroup>
    {
        public int Compare(ProteinGroup x, ProteinGroup y)
        {
            if (String.IsNullOrEmpty(x.ProteinID)) return -1;
            if (String.IsNullOrEmpty(y.ProteinID)) return 1;

            if (x.ProteinID.Contains(y.ProteinID))
                return 0;
            else
            {
                int retval = x.ProteinID.Length.CompareTo(y.ProteinID.Length);

                if (retval != 0)
                    return retval;
                else
                    return x.ProteinID.CompareTo(y.ProteinID);
            }
        }
    }
}
