﻿/**
 * Program:     ProteoCombiner - Integrating bottom-up & top-down proteomics data
 * Author:      Diogo Borges Lima
 * Created:     4/3/2019
 * Update by:   Diogo Borges Lima
 * Description: Model class
 */
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombineBottomUpTopDown.Model
{
    [Serializable]
    [ProtoContract]
    public class ModificationItem
    {
        /// <summary>
        /// Public variables
        /// </summary>
        [ProtoMember(1)]
        public string AminoAcid { get; set; }
        [ProtoMember(2)]
        public string Representation
        {
            get
            {
                return AminoAcid + "(" + MonoIsotopicMass + ")";
            }
            set { }
        }
        [ProtoMember(3)]
        public string Description { get; set; }
        [ProtoMember(4)]
        public bool OnlyCTerminus { get; set; }
        [ProtoMember(5)]
        public bool OnlyNTerminus { get; set; }
        [ProtoMember(6)]
        public bool IsVariable { get; set; }
        [ProtoMember(7)]
        public bool IsActive { get; set; }
        [ProtoMember(8)]
        public byte AminoAcidByte { get; set; }
        [ProtoMember(9)]
        public double AverageMass { get; set; }
        [ProtoMember(10)]
        public double MonoIsotopicMass { get; set; }
        [ProtoMember(11)]
        public bool IsReaction { get; set; }

        /// <summary>
        /// This constructor should not be used and is just here to make this serializable
        /// </summary>
        public ModificationItem() { }

        public ModificationItem(
            string aminoacid,
            string description,
            double deltaMass,
            bool onlyCTerminus = false,
            bool onlyNTerminus = false,
            bool isVariable = false,
            bool isActive = false
            )
        {
            this.AminoAcid = aminoacid;
            this.MonoIsotopicMass = deltaMass;
            this.Description = description;
            this.OnlyCTerminus = onlyCTerminus;
            this.OnlyNTerminus = onlyNTerminus;
            this.IsVariable = isVariable;
            IsActive = isActive;
        }
    }

}
