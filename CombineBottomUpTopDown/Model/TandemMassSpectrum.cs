﻿/**
 * Program:     ProteoCombiner - Integrating bottom-up & top-down proteomics data
 * Author:      Diogo Borges Lima
 * Created:     4/3/2019
 * Update by:   Diogo Borges Lima
 * Description: Class responsable for representing ms/ms output file of maxquant
 */
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CombineBottomUpTopDown.Model
{
    [Serializable]
    [ProtoContract]
    public class TandemMassSpectrum
    {
        /// <summary>
        /// Public variables
        /// </summary>
        [ProtoMember(1)]
        public string RAWfile { get; set; }
        [ProtoMember(2)]
        public int ScanNumber { get; set; }
        [ProtoMember(3)]
        public string Sequence { get; set; }
        [ProtoMember(4)]
        public string ModifiedSequence { get; set; }
        //Tuple<aminoacid, position, description>
        [ProtoMember(5)]
        public List<Tuple<string, int, string>> Modifications { get; set; }
        [ProtoMember(6)]
        public List<string> Proteins { get; set; }
        [ProtoMember(7)]
        public int Charge { get; set; }
        [ProtoMember(8)]
        public double MZ { get; set; }
        [ProtoMember(9)]
        public double Mass { get; set; }
        [ProtoMember(10)]
        public double PPMError { get; set; }
        [ProtoMember(11)]
        public double RetentionTime { get; set; }
        [ProtoMember(12)]
        public double Score { get; set; }
        [ProtoMember(13)]
        public string MatchedIons { get; set; }
        [ProtoMember(14)]
        public List<double> IntensitiesFragIons { get; set; }
        [ProtoMember(15)]
        public List<double> MassesFragIons { get; set; }
        [ProtoMember(16)]
        public int NumberOfMatches { get; set; }
        [ProtoMember(17)]
        public ulong MSMSID { get; set; }
        [ProtoMember(18)]
        public List<ulong> ProteinGroupID { get; set; }
        [ProtoMember(19)]
        public ulong PeptideID { get; set; }
        //Tuple<charge, mass>
        [ProtoMember(20)]
        public Tuple<int, double> Precursor { get; set; }
        [ProtoMember(21)]
        public ActivationType Fragmentation { get; set; }
        [ProtoMember(22)]
        public int PrecursorScanNumber { get; set; }

        /// <summary>
        /// Null Constructor for serializing Class
        /// </summary>
        public TandemMassSpectrum() { Modifications = new List<Tuple<string, int, string>>(); }

        public TandemMassSpectrum(string rAWfile,
            int scanNumber,
            string sequence,
            string modifiedSequence,
            string modifications,
            List<string> proteins,
            int charge,
            double mZ,
            double mass,
            double pPMError,
            double retentionTime,
            double score,
            string matchedIons,
            string intensities,
            string masses,
            int numberOfMatches,
            ulong mSMSID,
            List<ulong> proteinGroupID,
            ulong peptideID)
        {
            RAWfile = rAWfile;
            ScanNumber = scanNumber;
            Sequence = sequence;
            ModifiedSequence = modifiedSequence;
            Modifications = SelectPTMs(modifications);
            Proteins = proteins;
            Charge = charge;
            MZ = mZ;
            Mass = mass;
            PPMError = pPMError;
            RetentionTime = retentionTime;
            Score = score;
            MatchedIons = matchedIons;
            string[] cols = Regex.Split(intensities, ";");
            IntensitiesFragIons = (from intensity in cols
                                   where !String.IsNullOrEmpty(intensity)
                                   select Convert.ToDouble(intensity)).ToList();
            cols = Regex.Split(masses, ";");
            MassesFragIons = (from eachMass in cols
                              where !String.IsNullOrEmpty(eachMass)
                              select Convert.ToDouble(eachMass)).ToList();
            NumberOfMatches = numberOfMatches;
            MSMSID = mSMSID;
            ProteinGroupID = proteinGroupID;
            PeptideID = peptideID;
        }

        public TandemMassSpectrum(string rAWfile,
            int scanNumber,
            string sequence,
            string modifiedSequence,
            List<Tuple<string, int, string>> modifications,
            List<string> proteins,
            int charge,
            double mZ,
            double mass,
            double pPMError,
            double retentionTime,
            double score,
            string matchedIons,
            string intensities,
            string masses,
            int numberOfMatches,
            ulong mSMSID,
            List<ulong> proteinGroupID,
            ulong peptideID)
        {
            RAWfile = rAWfile;
            ScanNumber = scanNumber;
            Sequence = sequence;
            ModifiedSequence = modifiedSequence;
            Modifications = modifications;
            Proteins = proteins;
            Charge = charge;
            MZ = mZ;
            Mass = mass;
            PPMError = pPMError;
            RetentionTime = retentionTime;
            Score = score;
            MatchedIons = matchedIons;
            string[] cols = Regex.Split(intensities, ";");
            IntensitiesFragIons = (from intensity in cols
                                   where !String.IsNullOrEmpty(intensity)
                                   select Convert.ToDouble(intensity)).ToList();
            cols = Regex.Split(masses, ";");
            MassesFragIons = (from eachMass in cols
                              where !String.IsNullOrEmpty(eachMass)
                              select Convert.ToDouble(eachMass)).ToList();
            NumberOfMatches = numberOfMatches;
            MSMSID = mSMSID;
            ProteinGroupID = proteinGroupID;
            PeptideID = peptideID;
        }

        public TandemMassSpectrum(string rAWfile,
            int scanNumber,
            string sequence,
            string modifiedSequence,
            List<Tuple<string, int, string>> modifications,
            List<string> proteins,
            int charge,
            double mZ,
            double mass,
            double pPMError,
            double retentionTime,
            double score,
            string matchedIons,
            List<double> intensities,
            List<double> masses,
            int numberOfMatches,
            ulong mSMSID,
            List<ulong> proteinGroupID,
            ulong peptideID)
        {
            RAWfile = rAWfile;
            ScanNumber = scanNumber;
            Sequence = sequence;
            ModifiedSequence = modifiedSequence;
            Modifications = modifications;
            Proteins = proteins;
            Charge = charge;
            MZ = mZ;
            Mass = mass;
            PPMError = pPMError;
            RetentionTime = retentionTime;
            Score = score;
            MatchedIons = matchedIons;
            IntensitiesFragIons = intensities;
            MassesFragIons = masses;
            NumberOfMatches = numberOfMatches;
            MSMSID = mSMSID;
            ProteinGroupID = proteinGroupID;
            PeptideID = peptideID;
        }

        /// <summary>
        /// Constructor - Raw file
        /// </summary>
        /// <param name="scan_number"></param>
        /// <param name="retention_time"></param>
        /// <param name="activation"></param>
        /// <param name="precursor"></param>
        /// <param name="masses"></param>
        /// <param name="intensities"></param>
        /// <param name="rawFile"></param>
        public TandemMassSpectrum(
            int scan_number,
            double retention_time,
            ActivationType activation,
            Tuple<int, double> precursor,
            List<double> masses,
            List<double> intensities,
            string rawFile,
            int charge,
            int precursorScanNumber
            )
        {
            ScanNumber = scan_number;
            RetentionTime = retention_time;
            Fragmentation = activation;
            Precursor = precursor;
            MassesFragIons = masses;
            IntensitiesFragIons = intensities;
            RAWfile = rawFile;
            Charge = charge;
            PrecursorScanNumber = precursorScanNumber;
        }

        private List<Tuple<string, int, string>> SelectPTMs(string modifications_original)
        {
            if (String.IsNullOrEmpty(modifications_original) || modifications_original.Equals("Unmodified")) return new List<Tuple<string, int, string>>();
            //Tuple<aminoacid, position, description>
            List<Tuple<string, int, string>> ptmList = new List<Tuple<string, int, string>>();

            string[] modifications = Regex.Split(modifications_original, "[;|,]");
            for (int countModification = 0; countModification < modifications.Length; countModification++)
            {
                string[] cols = Regex.Split(modifications[countModification], "[(]");

                string description = cols[0].Trim();
                if (description.Any(char.IsDigit))
                {
                    string[] desc_cols = Regex.Split(description, @"[^a-z|^A-Z]");
                    description = (from c in desc_cols
                                   where !String.IsNullOrEmpty(c)
                                   select c).ToList()[0];
                }
                string aminoacid = string.Empty;
                if (cols.Length == 1 && cols[0].Contains("Gln->Glu"))
                {
                    aminoacid = "Q";
                    description = "Gln->Glu";
                }
                else
                    aminoacid = cols[1].Replace(")", "").Trim();
                if (aminoacid.Contains("Protein N-term"))
                    ptmList.Add(Tuple.Create("}", 1, description + " - " + aminoacid));
                else
                {
                    string pattern = aminoacid + "\\(";
                    string[] pos_mods = Regex.Split(ModifiedSequence, pattern);
                    int finalPos = 0;
                    int countPosMod = 0;
                    bool isValid = false;
                    while (countPosMod < pos_mods.Length)
                    {
                        isValid = false;
                        if (!String.IsNullOrEmpty(pos_mods[countPosMod]))
                        {
                            int indexOpenParentheses = pos_mods[countPosMod].IndexOf("(");
                            int indexCloseParentheses = pos_mods[countPosMod].IndexOf(")");
                            if ((indexOpenParentheses != -1 && indexOpenParentheses < indexCloseParentheses) || (indexOpenParentheses == -1 && indexCloseParentheses == -1))
                                finalPos += Utils.Util.CleanPeptide(pos_mods[countPosMod].Replace("_", "")).Length;
                            else
                            {
                                if (countPosMod != pos_mods.Length - 1)
                                {
                                    ptmList.Add(Tuple.Create(aminoacid, finalPos + 1, description + " - " + aminoacid));
                                    string[] unmodifiedAAs = Regex.Split(pos_mods[countPosMod], @"[^A-Z]+");
                                    foreach (string unmod in unmodifiedAAs)
                                    {
                                        if (!String.IsNullOrEmpty(unmod))
                                        {
                                            if (!isValid)
                                                finalPos += unmod.Length + 1;
                                            else
                                                finalPos += unmod.Length;
                                            isValid = true;
                                        }
                                    }
                                }
                                else if (countPosMod == 1)
                                    isValid = true;//There is only one ptm
                                if (isValid)
                                    ptmList.Add(Tuple.Create(aminoacid, finalPos + 1, description + " - " + aminoacid));
                            }
                        }
                        countPosMod++;
                    }
                }
            }

            return ptmList.Distinct().ToList();
        }
    }

    public class TandemMassSpectrumComparer : IEqualityComparer<TandemMassSpectrum>
    {
        public bool Equals(TandemMassSpectrum x, TandemMassSpectrum y)
        {
            //Check if the compared objects reference has same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check if any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check if the items' properties are equal.
            return (x.ModifiedSequence.Equals(y.ModifiedSequence) && x.Modifications.Count == y.Modifications.Count &&
                x.MSMSID == y.MSMSID && x.Charge == y.Charge && x.ScanNumber == y.ScanNumber);
        }

        public int GetHashCode(TandemMassSpectrum obj)
        {
            return (int)obj.ModifiedSequence.Length;
        }
    }

    public class TandemMassSpectrumIDSequenceComparer : IEqualityComparer<TandemMassSpectrum>
    {
        public bool Equals(TandemMassSpectrum x, TandemMassSpectrum y)
        {
            //Check if the compared objects reference has same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check if any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check if the items' properties are equal.
            return (x.ModifiedSequence.Equals(y.ModifiedSequence) && x.Modifications.Count == y.Modifications.Count &&
               x.Score == y.Score && x.Charge == y.Charge && x.ScanNumber == y.ScanNumber && x.RAWfile.Equals(y.RAWfile));
        }

        public int GetHashCode(TandemMassSpectrum obj)
        {
            return (int)obj.ModifiedSequence.Length;
        }
    }

    public class TandemMassSpectrumSimpleComparer : IEqualityComparer<TandemMassSpectrum>
    {
        public bool Equals(TandemMassSpectrum x, TandemMassSpectrum y)
        {
            //Check if the compared objects reference has same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check if any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check if the items' properties are equal.
            return (x.ModifiedSequence.Equals(y.ModifiedSequence) && x.Modifications.Count == y.Modifications.Count);
        }

        public int GetHashCode(TandemMassSpectrum obj)
        {
            return (int)obj.ModifiedSequence.Length;
        }
    }

    public class TandemMassSpectrumBinarySearchComparer : IComparer<TandemMassSpectrum>
    {

        public int Compare(TandemMassSpectrum x, TandemMassSpectrum y)
        {
            if (String.IsNullOrEmpty(x.RAWfile)) return -1;
            if (String.IsNullOrEmpty(y.RAWfile)) return 1;

            if (x.RAWfile.Equals(y.RAWfile))
            {
                if (x.ScanNumber == y.ScanNumber)
                    return 0;
                else if (x.ScanNumber > y.ScanNumber)
                    return 1;
                else
                    return -1;
            }
            else
                return 1;
        }
    }
}
