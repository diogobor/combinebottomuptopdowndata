﻿/**
 * Program:     ProteoCombiner - Integrating bottom-up & top-down proteomics data
 * Author:      Diogo Borges Lima
 * Update:      4/8/2019
 * Update by:   Diogo Borges Lima
 * Description: Residue Mass class
 */
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CombineBottomUpTopDown.Model
{
    [Serializable]
    [ProtoContract]
    public class ResidueMass
    {
        /// <summary>
        /// Public variables
        /// </summary>
        [ProtoMember(1)]
        public double[] MonoAAMasses = new double[127];
        [ProtoMember(2)]
        public double[] AverageAAMasses = new double[127];
        [ProtoMember(3)]
        public static ResidueLib residueLib = new ResidueLib();
        /// <summary>
        /// Regex that split the Peptide Sequence
        /// </summary>
        private Regex caracterCaptured = new Regex("[A-Z]", RegexOptions.Compiled);

        /// <summary>
        /// Null Constructor for serializing Class
        /// </summary>
        public ResidueMass() { InitializeResidueMass(); }

        /// <summary>
        /// Building the Isotopic dictionary object
        /// </summary>
        /// <param name="modificationItemList"></param>
        public ResidueMass(List<ModificationItem> modificationItemList = null)
        {
            InitializeResidueMass(modificationItemList);
        }

        /// <summary>
        /// Method responsible for initializing ResidueMass array
        /// </summary>
        /// <param name="modificationItemList"></param>
        public void InitializeResidueMass(List<ModificationItem> modificationItemList = null)
        {
            buildMonoisotopicDictionary();

            if (modificationItemList != null)
            {
                foreach (ModificationItem m in modificationItemList)
                {
                    if (!m.IsVariable && m.IsActive && !m.AminoAcid.Equals("*") && !m.IsReaction)
                    {
                        MonoAAMasses[m.AminoAcidByte] += m.MonoIsotopicMass;
                        AverageAAMasses[m.AminoAcidByte] += m.AverageMass;
                    }
                }
            }
        }

        /// <summary>
        /// Method responsable for calculating the molecule mass
        /// </summary>
        /// <param name="mol"></param>
        /// <returns></returns>
        public double CalculatorMassFromMolecule(byte[] currentMolecule)
        {
            double mass = 0.0;

            int countPTM;
            List<byte> qtdAA_prepare = new List<byte>();

            List<byte> aa_ion_mass_prepare = new List<byte>();

            for (int count = 0; count < currentMolecule.LongLength; count++)
            {

                //amount aa
                if (currentMolecule[count] > 47 && currentMolecule[count] < 58)
                {
                    aa_ion_mass_prepare.Add(currentMolecule[count]);
                    continue;
                }

                int aa_ion_mass = -1;
                if (aa_ion_mass_prepare.Count > 0)
                {
                    aa_ion_mass = int.Parse(System.Text.ASCIIEncoding.Default.GetString(aa_ion_mass_prepare.ToArray()));
                    aa_ion_mass_prepare.Clear();
                }

                byte molecule = currentMolecule[count];

                switch (molecule)
                {
                    case 67://Cystein (C)
                        {
                            molecule = 35;//Carbon
                        }
                        break;

                    case 83://Serine (S)
                        {
                            molecule = 34; //Sufur
                        }
                        break;

                    case 72://Histidine (H)
                        {
                            molecule = 33;//Hydrogen
                        }
                        break;

                    case 78://Asparagine (N)
                        {
                            molecule = 36;//Nitrogen
                        }
                        break;

                    case 79://(O)
                        {
                            molecule = 64;//Oxygen
                        }
                        break;

                    case 80://Proline
                        {
                            molecule = 32;//Phosphorus
                        }
                        break;
                }

                //open PTM Mass with "(" character
                if ((count + 1) < currentMolecule.LongLength && currentMolecule[count + 1] == 40)
                {
                    for (countPTM = count + 2; countPTM < currentMolecule.LongLength; countPTM++)
                    {
                        //close PTM Mass with ")" character
                        if (currentMolecule[countPTM] == 41)
                        {
                            break;
                        }
                        else
                        {
                            qtdAA_prepare.Add(currentMolecule[countPTM]);
                        }
                    }
                    count = countPTM;

                    int qtdAA = 1;
                    if (qtdAA_prepare.Count > 0)
                    {
                        qtdAA = int.Parse(System.Text.ASCIIEncoding.Default.GetString(qtdAA_prepare.ToArray()));
                        qtdAA_prepare.Clear();
                    }

                    if (aa_ion_mass == -1)
                        mass += qtdAA * MonoAAMasses[molecule];
                    else
                        mass += qtdAA * aa_ion_mass;
                    //mass += (double.Parse(System.Text.ASCIIEncoding.Default.GetString(qtdAA_prepare.ToArray())) * aa_ion_mass);

                }
                else
                {
                    if (aa_ion_mass == -1)
                        mass += MonoAAMasses[molecule];
                    else
                        mass += aa_ion_mass;
                }


            }

            return mass;
        }

        /// <summary>
        /// Currently does not consider modifification of masses within parenthesis
        /// </summary>
        /// <param name="cleanSequence"></param>
        /// <param name="addAWater"></param>
        /// <returns></returns>
        public double CalculatorMonoisotopicMass(byte[] sequence, bool addAWater, bool addHydrogenMass)
        {
            double mass = 0;

            int countPTM;
            List<byte> ptm = new List<byte>();
            for (int count = 0; count < sequence.LongLength; count++)
            {
                //open PTM Mass with "(" character
                if (sequence[count] == 40)
                {
                    for (countPTM = count + 1; countPTM < sequence.LongLength; countPTM++)
                    {
                        //close PTM Mass with ")" character
                        if (sequence[countPTM] == 41)
                        {
                            break;
                        }
                        else
                        {
                            ptm.Add(sequence[countPTM]);
                        }
                    }
                    count = countPTM;
                    mass += double.Parse(System.Text.ASCIIEncoding.Default.GetString(ptm.ToArray()));
                    ptm.Clear();
                }
                else
                {
                    mass += MonoAAMasses[sequence[count]];
                }
            }
            if (addAWater)
            {
                mass += MonoAAMasses[38];// H2O
            }

            if (addHydrogenMass)
            {
                mass += MonoAAMasses[33]; // Hydrogen
            }
            return mass;
        }

        /// <summary>
        /// Currently does not consider modifification of masses within parenthesis
        /// </summary>
        /// <param name="cleanSequence"></param>
        /// <param name="addAWater"></param>
        /// <returns></returns>
        public double CalculatorAverageMass(byte[] sequence, bool addAWater, bool addHydrogenMass)
        {
            double mass = 0;

            int countPTM;
            List<byte> ptm = new List<byte>();
            for (int count = 0; count < sequence.LongLength; count++)
            {
                //open PTM Mass with "(" character
                if (sequence[count] == 40)
                {
                    for (countPTM = count + 1; countPTM < sequence.LongLength; countPTM++)
                    {
                        //close PTM Mass with ")" character
                        if (sequence[countPTM] == 41)
                        {
                            break;
                        }
                        else
                        {
                            ptm.Add(sequence[countPTM]);
                        }
                    }
                    count = countPTM;
                    mass += double.Parse(System.Text.ASCIIEncoding.Default.GetString(ptm.ToArray()));
                    ptm.Clear();
                }
                else
                {
                    mass += AverageAAMasses[sequence[count]];
                }
            }
            if (addAWater)
            {
                mass += AverageAAMasses[38];// H2O
            }

            if (addHydrogenMass)
            {
                mass += AverageAAMasses[33]; // Hydrogen
            }
            return mass;
        }

        /// <summary>
        /// Method responsible for filling isotopic dictionary object with respectively values
        /// </summary>
        public void buildMonoisotopicDictionary()
        {

            StringReader serializedMods = new StringReader(ProteoCombiner.Properties.Settings.Default.ResidueLib);
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(residueLib.GetType());
                residueLib = (ResidueLib)xmlSerializer.Deserialize(serializedMods);
            }
            catch
            {
                residueLib = new ResidueLib();
                residueLib.LoadDefaultResidue();
            }

            foreach (ResidueToLib residue in residueLib.MyResidues)
            {
                int index = residue.LetterCode[0];
                MonoAAMasses[index] = residue.MonoIsotopicMass;
                AverageAAMasses[index] = residue.AverageMass;
            }

            //MonoAAMasses[71] = 57.021465301513672;  //Glycine (G)
            //MonoAAMasses[65] = 71.037117004394531;  //Alanine (A)  
            //MonoAAMasses[83] = 87.032028198242188;  //Serine (S)
            //MonoAAMasses[80] = 97.052764892578125;  //Proline (P)
            //MonoAAMasses[86] = 99.068412780761719;  //Valine (V)
            //MonoAAMasses[84] = 101.04767608642578;  //Threonine (T)
            //MonoAAMasses[67] = 103.00918579101562;  //Cystein (C)
            //MonoAAMasses[76] = 113.08406066894531;  //Isoleucine (I)
            //MonoAAMasses[73] = 113.08406066894531;  //Leucine (L)
            //MonoAAMasses[88] = 113.08406066894531;  //Undetermined AA, L or I. (X)
            //MonoAAMasses[78] = 114.04293060302734;  //Asparagine (N)
            //MonoAAMasses[79] = 114.07931518554687;  // (O)
            //MonoAAMasses[66] = 114.53493499755859;  //Asparagine or Aspartic Acid (B)
            //MonoAAMasses[68] = 115.02693939208984;  //Aspartic Acid (D)
            //MonoAAMasses[81] = 128.05857849121094;  //Glutamine (Q)
            //MonoAAMasses[75] = 128.09495544433594;  //Lysine (K)
            //MonoAAMasses[90] = 128.55058288574219;  //Glutamic Acid or Glutamine (Z)
            //MonoAAMasses[69] = 129.04258728027344;  //Glutamic Acid (E)
            //MonoAAMasses[77] = 131.04048156738281;  //Methionine (M)
            //MonoAAMasses[72] = 137.05891418457031;  //Histidine (H)
            //MonoAAMasses[70] = 147.06842041015625;  //Phenilanyne (F)
            //MonoAAMasses[85] = 150.95364;           //Selenocysteine (U)
            //MonoAAMasses[82] = 156.10110473632812;  //Arginine (R)
            //MonoAAMasses[89] = 163.06332397460937;  //Tyrosine (Y)
            //MonoAAMasses[87] = 186.07931518554687;  //Tryptophan (W)
            //MonoAAMasses[79] = 255.15829;           //Pyrrolysine (O)
            //MonoAAMasses[92] = 177.045959;          //N-Formylmethionine(fMet)

            //Special Features - molecular monoisotopic mass
            MonoAAMasses[32] = 30.973762;        //Phosphorus (space)
            MonoAAMasses[33] = 1.00782503214;    //Hydrogen (!)
            MonoAAMasses[34] = 31.972072002926;    //Sulfur (")
            MonoAAMasses[35] = 12;          //Carbon (#)
            MonoAAMasses[64] = 15.9949146221;    //Oxygen (@)
            MonoAAMasses[42] = 13.00335483778;   //Carbon-13 (*)
            MonoAAMasses[36] = 14.00307400529;   //Nitrogen ($)
            MonoAAMasses[43] = 15.00010897;      //Nitrogen-15 (+)
            MonoAAMasses[37] = 17.0265491;       //NH3 - Ammonia (%)
            MonoAAMasses[39] = 27.9949146221;    //CO - Carbon monoxide (')
            MonoAAMasses[38] = 18.01056468638;   //H2O - Water (&)
            MonoAAMasses[125] = 0;               //N-Terminal (})
            MonoAAMasses[126] = 0;               //C-Terminal (~)

            AverageAAMasses[32] = 30.973762;        //Phosphorus (space)
            AverageAAMasses[33] = 1.00782503214;    //Hydrogen (!)
            AverageAAMasses[34] = 31.972072002926;    //Sulfur (")
            AverageAAMasses[35] = 12;          //Carbon (#)
            AverageAAMasses[64] = 15.9949146221;    //Oxygen (@)
            AverageAAMasses[42] = 13.00335483778;   //Carbon-13 (*)
            AverageAAMasses[36] = 14.00307400529;   //Nitrogen ($)
            AverageAAMasses[43] = 15.00010897;      //Nitrogen-15 (+)
            AverageAAMasses[37] = 17.0265491;       //NH3 - Ammonia (%)
            AverageAAMasses[39] = 27.9949146221;    //CO - Carbon monoxide (')
            AverageAAMasses[38] = 18.01056468638;   //H2O - Water (&)
            AverageAAMasses[125] = 0;               //N-Terminal (})
            AverageAAMasses[126] = 0;               //C-Terminal (~)

            //isotopicDictionary.Clear();
            //isotopicDictionary.Add("*", 0.0);           //Start or Stop sequence
            //isotopicDictionary.Add("G", 57.0214637);    //Glycine
            //isotopicDictionary.Add("A", 71.0371138);    //Alanine
            //isotopicDictionary.Add("S", 87.0320284);    //Serine
            //isotopicDictionary.Add("P", 97.0527638);    //Proline
            //isotopicDictionary.Add("V", 99.0684139);    //Valine
            //isotopicDictionary.Add("T", 101.047678);    //Threonine
            //isotopicDictionary.Add("C", 103.009185);    //Cystein
            //isotopicDictionary.Add("I", 113.084064);    //Isoleucine
            //isotopicDictionary.Add("L", 113.084064);    //Leucine
            //isotopicDictionary.Add("N", 114.042927);    //Asparagine
            //isotopicDictionary.Add("D", 115.026943);    //Aspartic Acid
            //isotopicDictionary.Add("Q", 128.058578);    //Glutamine
            //isotopicDictionary.Add("K", 128.094963);    //Lysine
            //isotopicDictionary.Add("E", 129.042593);    //Glutamic Acid
            //isotopicDictionary.Add("M", 131.040485);    //Methionine
            //isotopicDictionary.Add("H", 137.058912);    //Histidine
            //isotopicDictionary.Add("F", 147.068414);    //Phenilanyne
            //isotopicDictionary.Add("U", 150.95364);     //Selenocysteine
            //isotopicDictionary.Add("R", 156.101111);    //Arginine
            //isotopicDictionary.Add("X", 113.08406);     //Undetermined AA, L or I.
            //isotopicDictionary.Add("J", 113.08406);     //L or I.
            //isotopicDictionary.Add("Y", 163.063329);    //Tyrosine
            //isotopicDictionary.Add("W", 186.079313);    //Tryptophan
            //isotopicDictionary.Add("O", 255.166692);    //Pyrrolysine - the 22nd amino acid

            //Special Features - molecular monoisotopic mass
            //isotopicDictionary.Add("Hydrogen", 1.00782503214);
            //isotopicDictionary.Add("Oxygen", 15.9949146221);
            //isotopicDictionary.Add("Carbon", 12);
            //isotopicDictionary.Add("Nitrogen", 14.00307400529);
            //isotopicDictionary.Add("NH3", 17.0265491);
            //isotopicDictionary.Add("CO", 27.9949146221);
            //isotopicDictionary.Add("H2O", 18.01056468638);
            //isotopicDictionary.Add("B", 114.042927); //Aspargine os aspartic acid, not able to be determined by sequencing method, here we pick aspargine 
            //isotopicDictionary.Add("Z", 128.058578); //glutamic acid or glutamine, we chose glutamine

        }
    }
}

