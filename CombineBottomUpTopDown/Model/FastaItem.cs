﻿using CombineBottomUpTopDown.Utils;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CombineBottomUpTopDown.Model
{
    [Serializable]
    [ProtoContract]
    public class FastaItem
    {
        double mass = -1;
        double coverage = -1;

        /// <summary>
        /// Public variables
        /// </summary>
        [ProtoMember(1)]
        public string SequenceIdentifier { get; set; }
        [ProtoMember(2)]
        public string Description { get; set; }
        [ProtoMember(3)]
        public string Sequence { get; set; }
        [ProtoMember(4)]
        public double ExtraDouble { get; set; }
        [ProtoMember(5)]
        public byte[] SequenceInBytes { get; set; }

        //Tuple<description, startPosition, endPosition>
        [ProtoMember(6)]
        public List<Tuple<string, int, int>> Chains { get; set; }

        //Tuple<description, position>
        [ProtoMember(7)]
        public List<Tuple<string, int>> TheoreticalPTMs { get; set; }


        public void ResetCoverage()
        {
            coverage = -1;
        }

        public double Coverage(List<string> uniquePeptides, bool forceRecalculate = false)
        {

            if (coverage == -1 || forceRecalculate)
            {
                //calculate coverage
                //First we create an array of 0's of the size of the sequence
                int[] matchLocations = new int[Sequence.Length];

                foreach (string peptide in uniquePeptides)
                {
                    //Make sure the peptide is clean
                    string cleanedPeptide = Util.CleanPeptide(peptide, true);
                    Sequence = Sequence.Replace("*", "");

                    Match location = Regex.Match(Sequence, cleanedPeptide);
                    for (int i = location.Index; i < location.Index + location.Length; i++)
                    {
                        matchLocations.SetValue(1, i);
                    }
                }

                double sum = matchLocations.Sum();
                coverage = sum / (double)Sequence.Length;

            }

            return coverage;
        }

        public List<int> CoverageLocations(List<string> peps)
        {
            List<int> matchLocations = new int[Sequence.Length].ToList();

            foreach (string peptide in peps)
            {
                string cleanPeptide = Util.CleanPeptide(peptide, true);

                Match location = Regex.Match(Sequence, cleanPeptide);

                for (int i = location.Index; i < location.Index + location.Length; i++)
                {
                    matchLocations[i] = 1;
                }
            }
            return matchLocations;
        }

        public double MonoisotopicMass
        {
            get
            {
                if (mass == -1)
                {
                    AminoacidMasses aa = new AminoacidMasses();
                    mass = aa.CalculatePeptideMass(Sequence, false, true, false);
                }
                return mass;
            }
        }

        /// <summary>
        /// Null Constructor for serializing Class
        /// </summary>
        public FastaItem()
        {
            Chains = new List<Tuple<string, int, int>>();
            TheoreticalPTMs = new List<Tuple<string, int>>();
        }

        public FastaItem(string protID, string sequence, string description)
        {
            SequenceIdentifier = protID;
            Sequence = sequence;
            Description = description;
            Chains = new List<Tuple<string, int, int>>();
            TheoreticalPTMs = new List<Tuple<string, int>>();
        }

        public FastaItem(string protID, string description)
        {
            SequenceIdentifier = protID;
            Description = description;
            Sequence = "";
            Chains = new List<Tuple<string, int, int>>();
            TheoreticalPTMs = new List<Tuple<string, int>>();
        }

    }

    public class FastaComparer : IEqualityComparer<FastaItem>
    {
        public bool Equals(FastaItem x, FastaItem y)
        {
            //Check if the compared objects reference has same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check if any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check if the items' properties are equal.
            if (x.Sequence.Equals(y.Sequence) && x.MonoisotopicMass == y.MonoisotopicMass)
            {
                string[] seq1 = x.SequenceIdentifier.Split('|');
                string[] seq2 = y.SequenceIdentifier.Split('|');
                List<string> ids = new List<string>(seq1);

                if (!seq1.SequenceEqual(seq2))
                {
                    foreach (string strID in seq2)
                    {
                        if (!ids.Contains(strID))
                            ids.Add(strID);
                    }
                    x.SequenceIdentifier = String.Join("|", ids);
                }
                x.Chains = y.Chains;
                x.TheoreticalPTMs = y.TheoreticalPTMs;

                return true;
            }
            else
                return false;
        }

        public int GetHashCode(FastaItem obj)
        {
            return (int)(obj.MonoisotopicMass + obj.Sequence.GetHashCode());
        }
    }
}
