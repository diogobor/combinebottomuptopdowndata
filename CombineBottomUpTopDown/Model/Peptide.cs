﻿/**
 * Program:     ProteoCombiner - Integrating bottom-up & top-down proteomics data
 * Author:      Diogo Borges Lima
 * Created:     4/3/2019
 * Update by:   Diogo Borges Lima
 * Description: PeptideMQ
 */
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CombineBottomUpTopDown.Model
{
    [Serializable]
    [ProtoContract]
    public class Peptide
    {
        /// <summary>
        /// Public variables
        /// </summary>
        [ProtoMember(1)]
        public string Sequence { get; set; }
        [ProtoMember(3)]
        public List<string> Proteins { get; set; }
        [ProtoMember(4)]
        public int StartPosition { get; set; }
        [ProtoMember(5)]
        public int EndPosition { get; set; }
        [ProtoMember(6)]
        public List<int> Charges { get; set; }
        [ProtoMember(7)]
        public List<PeptideParams> PSMs { get; set; }

        /// <summary>
        /// Null Constructor for serializing Class
        /// </summary>
        public Peptide() { }

        /// <summary>
        /// Constructor - MaxQuant/Andromeda
        /// </summary>
        /// <param name="sequence"></param>
        /// <param name="mass"></param>
        /// <param name="proteins"></param>
        /// <param name="startPosition"></param>
        /// <param name="endPosition"></param>
        /// <param name="charges"></param>
        /// <param name="andromedaScore"></param>
        /// <param name="peptideID"></param>
        /// <param name="proteinGroupIDs"></param>
        /// <param name="modPeptIds"></param>
        /// <param name="mSMSIds"></param>
        /// <param name="bestMSMS"></param>
        /// <param name="mSMSCount"></param>
        /// <param name="isUnique"></param>
        public Peptide(string sequence,
            double mass,
            string proteins,
            int startPosition,
            int endPosition,
            string charges,
            double andromedaScore,
            ulong peptideID,
            string proteinGroupIDs,
            string modPeptIds,
            string mSMSIds,
            ulong bestMSMS,
            ulong mSMSCount,
            bool isUnique,
            string searchEngine)
        {
            Sequence = sequence;
            string[] cols = Regex.Split(proteins, ";");
            Proteins = new List<string>(cols);
            StartPosition = startPosition;
            EndPosition = endPosition;
            cols = Regex.Split(charges, ";");
            Charges = (from charge in cols
                       select Convert.ToInt32(charge)).ToList();

            cols = Regex.Split(proteinGroupIDs, ";");
            List<ulong> proteinGroupIDsList = (from proteinGroupID in cols
                                               select Convert.ToUInt64(proteinGroupID)).ToList();

            cols = Regex.Split(modPeptIds, ";");
            List<ulong> modPeptIdsList = (from ModPeptID in cols
                                          select Convert.ToUInt64(ModPeptID)).ToList();

            cols = Regex.Split(mSMSIds, ";");
            List<ulong> mSMSIdsList = (from MSMSID in cols
                                       select Convert.ToUInt64(MSMSID)).ToList();
            PSMs = new List<PeptideParams>() {
                new PeptideParams(
                    andromedaScore,
                    peptideID,
                    proteinGroupIDsList,
                    modPeptIdsList,
                    mSMSIdsList,
                    bestMSMS,
                    mSMSCount,
                    isUnique,
                    searchEngine,
                    mass)
            };
        }
    }

    [Serializable]
    [ProtoContract]
    public class PeptideParams
    {
        [ProtoMember(1)]
        public double PeptideScore { get; set; }
        [ProtoMember(2)]
        public ulong PeptideID { get; set; }
        [ProtoMember(3)]
        public List<ulong> ProteinGroupIDs { get; set; }
        [ProtoMember(4)]
        public List<ulong> ModPeptIds { get; set; }
        [ProtoMember(5)]
        public List<ulong> MSMSIds { get; set; }
        [ProtoMember(6)]
        public ulong BestMSMS { get; set; }
        [ProtoMember(7)]
        public ulong MSMSCount { get; set; }
        [ProtoMember(8)]
        public bool IsUnique { get; set; }
        [ProtoMember(9)]
        public string SearchEngine { get; set; }
        [ProtoMember(10)]
        public double Mass { get; set; }
        [ProtoMember(11)]
        public double NormalizedPeptideScore { get; set; }

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public PeptideParams() { }

        public PeptideParams(double peptideScore, ulong peptideID, List<ulong> proteinGroupIDs, List<ulong> modPeptIds, List<ulong> mSMSIds, ulong bestMSMS, ulong mSMSCount, bool isUnique, string searchEngine, double mass, double normalizaedPeptideScore = 0)
        {
            PeptideScore = peptideScore;
            PeptideID = peptideID;
            ProteinGroupIDs = proteinGroupIDs;
            ModPeptIds = modPeptIds;
            MSMSIds = mSMSIds;
            BestMSMS = bestMSMS;
            MSMSCount = mSMSCount;
            IsUnique = isUnique;
            SearchEngine = searchEngine;
            Mass = mass;
            NormalizedPeptideScore = normalizaedPeptideScore;
        }
    }

    public class PeptideComparer : IEqualityComparer<Peptide>
    {
        public bool Equals(Peptide x, Peptide y)
        {
            //Check if the compared objects reference has same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check if any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check if the items' properties are equal.
            return (x.Sequence.Equals(y.Sequence) && x.StartPosition == y.StartPosition);
        }

        public int GetHashCode(Peptide obj)
        {
            return (int)obj.Sequence.GetHashCode();
        }
    }

    public class PeptideTupleComparer : IEqualityComparer<Tuple<string, bool, List<Tuple<double, string>>, List<string>, List<string>, List<Tuple<string, int, string>>>>
    {
        public bool Equals(Tuple<string, bool, List<Tuple<double, string>>, List<string>, List<string>, List<Tuple<string, int, string>>> x, Tuple<string, bool, List<Tuple<double, string>>, List<string>, List<string>, List<Tuple<string, int, string>>> y)
        {
            //Check if the compared objects reference has same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check if any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check if the items' properties are equal.
            return (x.Item1.Equals(y.Item1) &&
                x.Item2 == y.Item2 &&
                x.Item4.Count == y.Item4.Count &&
                x.Item5.Count == y.Item5.Count &&
                x.Item6.Count == y.Item6.Count);
        }

        public int GetHashCode(Tuple<string, bool, List<Tuple<double, string>>, List<string>, List<string>, List<Tuple<string, int, string>>> obj)
        {
            return (int)obj.Item1.Length;
        }
    }

    public class MergePeptideFromDiffTool : IEqualityComparer<Peptide>
    {
        public bool Equals(Peptide x, Peptide y)
        {
            //Check if the compared objects reference has same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check if any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check if the items' properties are equal.
            if (x.Sequence.Equals(y.Sequence))
            {
                if (!x.PSMs.Exists(item => item.SearchEngine.Equals(y.PSMs[0].SearchEngine) && item.PeptideScore == y.PSMs[0].PeptideScore))
                {
                    x.PSMs.AddRange(y.PSMs);
                    x.PSMs.Sort((a, b) => b.PeptideScore.CompareTo(a.PeptideScore));
                }
                return true;
            }

            //if (x.Params.Count == 1 && y.Params.Count == 1 &&
            //    x.Sequence.Equals(y.Sequence) &&
            //    x.Mass == y.Mass && 
            //    x.EndPosition == y.EndPosition &&
            //    x.StartPosition == y.StartPosition)

            //{
            //    x.Params.Add(y.Params[0]);
            //    x.Params.Sort((a, b) => b.PeptideScore.CompareTo(a.PeptideScore));
            //    return true;
            //}
            //else if (x.Params.Count > 0 && y.Params.Count > 0 &&
            //    x.Params.Count == y.Params.Count &&
            //    x.Sequence.Equals(y.Sequence) &&
            //    x.Mass == y.Mass &&
            //    x.EndPosition == y.EndPosition &&
            //    x.StartPosition == y.StartPosition)

            //{
            //    x.Params.AddRange(y.Params);
            //    x.Params.Sort((a, b) => b.PeptideScore.CompareTo(a.PeptideScore));
            //    return true;
            //}
            //else if (x.Params.Count == 1 && y.Params.Count > 1 &&
            //    x.Sequence.Equals(y.Sequence) &&
            //     x.ProteinAccession.Contains(y.ProteinAccession) || y.ProteinAccession.Contains(x.ProteinAccession))
            //{
            //    //Check modification amount
            //    bool hasSameMods = true;

            //    for (int i = 0; i < y.Params.Count; i++)
            //    {
            //        for (int j = 0; j < y.Params[i].Modifications.Count; j++)
            //        {
            //            if (!(x.Params[0].Modifications.Count == y.Params[i].Modifications.Count &&
            //                  x.Params[0].Modifications[j].Item1.Equals(y.Params[i].Modifications[j].Item1) &&
            //                  x.Params[0].Modifications[j].Item2 == y.Params[i].Modifications[j].Item2 &&
            //                  x.Params[0].Modifications[j].Item3.Equals(y.Params[i].Modifications[j].Item3)))
            //            {
            //                hasSameMods = false;
            //                break;
            //            }
            //        }
            //    }
            //    if (hasSameMods)
            //    {
            //        x.Params.AddRange(y.Params);
            //        x.Params.Sort((a, b) => b.FinalScore.CompareTo(a.FinalScore));

            //        if (!String.IsNullOrEmpty(x.ProteinDescription) && !x.ProteinDescription.Equals(y.ProteinDescription))
            //            x.ProteinDescription += " - " + y.ProteinDescription;
            //        else
            //            x.ProteinDescription = y.ProteinDescription;
            //        return true;
            //    }
            //    else
            //        return false;
            //}
            //else if (x.Params.Count > 1 && y.Params.Count == 1 &&
            //    x.Sequence.Equals(y.Sequence) &&
            //     x.ProteinAccession.Contains(y.ProteinAccession) || y.ProteinAccession.Contains(x.ProteinAccession))
            //{
            //    //Check modification amount
            //    bool hasSameMods = true;

            //    for (int i = 0; i < x.Params.Count; i++)
            //    {
            //        for (int j = 0; j < x.Params[i].Modifications.Count; j++)
            //        {
            //            if (!(x.Params[i].Modifications.Count == y.Params[0].Modifications.Count &&
            //                  x.Params[i].Modifications[j].Item1.Equals(y.Params[0].Modifications[j].Item1) &&
            //                  x.Params[i].Modifications[j].Item2 == y.Params[0].Modifications[j].Item2 &&
            //                  x.Params[i].Modifications[j].Item3.Equals(y.Params[0].Modifications[j].Item3)))
            //            {
            //                hasSameMods = false;
            //                break;
            //            }
            //        }
            //    }
            //    if (hasSameMods)
            //    {
            //        x.Params.AddRange(y.Params);
            //        x.Params.Sort((a, b) => b.FinalScore.CompareTo(a.FinalScore));

            //        if (!String.IsNullOrEmpty(x.ProteinDescription) && !x.ProteinDescription.Equals(y.ProteinDescription))
            //            x.ProteinDescription += " - " + y.ProteinDescription;
            //        else
            //            x.ProteinDescription = y.ProteinDescription;
            //        return true;
            //    }
            //    else
            //        return false;
            //}
            else
                return false;
        }

        public int GetHashCode(Peptide obj)
        {
            return (int)obj.Sequence.GetHashCode();
        }
    }

    public class PeptideBinarySearchComparer : IComparer<Peptide>
    {

        public int Compare(Peptide x, Peptide y)
        {
            if (String.IsNullOrEmpty(x.Sequence)) return -1;
            if (String.IsNullOrEmpty(y.Sequence)) return 1;

            int retval = x.Sequence.Length.CompareTo(y.Sequence.Length);

            if (retval != 0)
            {
                return retval;
            }
            else
            {
                if (x.Proteins.Count == y.Proteins.Count)
                {
                    retval = x.Proteins[0].Length.CompareTo(y.Proteins[0].Length);
                    if (retval != 0)
                    {
                        return retval;
                    }
                    else
                    {
                        retval = x.Proteins[0].CompareTo(y.Proteins[0]);

                        if (retval != 0)
                            return retval;
                        else
                        {
                            if (x.Sequence.Equals(y.Sequence))
                            {
                                return retval;
                            }
                            else
                                return x.Sequence.CompareTo(y.Sequence);
                        }
                    }
                }
                else if (x.Proteins.Count > y.Proteins.Count)
                    return 1;
                else
                    return -1;
            }
        }
    }
}
