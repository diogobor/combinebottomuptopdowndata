﻿/**
 * Program:     ProteoCombiner - Integrating bottom-up & top-down proteomics data
 * Author:      Diogo Borges Lima
 * Update:      4/8/2019
 * Update by:   Diogo Borges Lima
 * Description: Residue Lib class
 */
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombineBottomUpTopDown.Model
{
    [Serializable]
    [ProtoContract]
    public class ResidueLib
    {
        /// <summary>
        /// public variable
        /// </summary>
        [ProtoMember(1)]
        public List<ResidueToLib> MyResidues { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public ResidueLib()
        {
            MyResidues = new List<ResidueToLib>();
        }

        public void LoadDefaultResidue()
        {
            MyResidues.Add(new ResidueToLib("Glycine", new byte[1] { 71 }, 57.021465301513672, 57.0519, "C(2)H(3)NO"));  //Glycine (G)
            MyResidues.Add(new ResidueToLib("Alanine", new byte[1] { 65 }, 71.037117004394531, 71.0788, "C(3)H(5)NO"));  //Alanine (A)  
            MyResidues.Add(new ResidueToLib("Serine", new byte[1] { 83 }, 87.032028198242188, 87.0782, "C(3)H(5)NO(2)"));  //Serine (S)
            MyResidues.Add(new ResidueToLib("Proline", new byte[1] { 80 }, 97.052764892578125, 97.1167, "C(5)H(7)NO"));  //Proline (P)
            MyResidues.Add(new ResidueToLib("Valine", new byte[1] { 86 }, 99.068412780761719, 99.1326, "C(5)H(9)NO"));  //Valine (V)
            MyResidues.Add(new ResidueToLib("Threonine", new byte[1] { 84 }, 101.04767608642578, 101.1051, "C(4)H(7)NO(2)"));  //Threonine (T)
            MyResidues.Add(new ResidueToLib("Cysteine", new byte[1] { 67 }, 103.00918579101562, 103.1388, "C(3)H(5)NOS"));  //Cystein (C)
            MyResidues.Add(new ResidueToLib("Isoleucine", new byte[1] { 73 }, 113.08406066894531, 113.1594, "C(6)H(11)NO"));  //Isoleucine (I)
            MyResidues.Add(new ResidueToLib("Leucine", new byte[1] { 76 }, 113.08406066894531, 113.1594, "C(6)H(11)NO"));  //Leucine (L)
            MyResidues.Add(new ResidueToLib("Undetermined AA, L or I", new byte[1] { 88 }, 113.08406066894531, 113.1594, "C(6)H(11)NO"));  //Undetermined AA, L or I. (X)
            MyResidues.Add(new ResidueToLib("Asparagine", new byte[1] { 78 }, 114.04293060302734, 114.1038, "C(4)H(6)N(2)O(2)"));  //Asparagine (N)
            MyResidues.Add(new ResidueToLib(" --- ", new byte[1] { 79 }, 114.07931518554687, 0));  // (O)
            MyResidues.Add(new ResidueToLib("Asparagine or Aspartic Acid", new byte[1] { 66 }, 114.53493499755859, 0));  //Asparagine or Aspartic Acid (B)
            MyResidues.Add(new ResidueToLib("Aspartic Acid", new byte[1] { 68 }, 115.02693939208984, 115.0886, "C(4)H(5)NO(3)"));  //Aspartic Acid (D)
            MyResidues.Add(new ResidueToLib("Glutamine", new byte[1] { 81 }, 128.05857849121094, 128.1307, "C(5)H(8)N(2)O(2)"));  //Glutamine (Q)
            MyResidues.Add(new ResidueToLib("Lysine", new byte[1] { 75 }, 128.09495544433594, 128.1741, "C(6)H(12)N(2)O"));  //Lysine (K)
            MyResidues.Add(new ResidueToLib("Glutamic Acid or Glutamine", new byte[1] { 90 }, 128.55058288574219, 128.1741));  //Glutamic Acid or Glutamine (Z)
            MyResidues.Add(new ResidueToLib("Glutamic Acid", new byte[1] { 69 }, 129.04258728027344, 129.1155, "C(5)H(7)NO(3)"));  //Glutamic Acid (E)
            MyResidues.Add(new ResidueToLib("Methionine", new byte[1] { 77 }, 131.04048156738281, 131.1926, "C(5)H(9)NOS"));  //Methionine (M)
            MyResidues.Add(new ResidueToLib("Histidine", new byte[1] { 72 }, 137.05891418457031, 137.1411, "C(6)H(7)N(3)O"));  //Histidine (H)
            MyResidues.Add(new ResidueToLib("Phenylalanine", new byte[1] { 70 }, 147.06842041015625, 147.1766, "C(9)H(9)NO"));  //Phenilalanyne (F)
            MyResidues.Add(new ResidueToLib("Selenocysteine", new byte[1] { 85 }, 150.95364, 150.0379, "C(3)H(7)NO(2)S")); //Selenocysteine (U)
            MyResidues.Add(new ResidueToLib("Arginine", new byte[1] { 82 }, 156.10110473632812, 156.1875, "C(6)H(12)N(4)O"));  //Arginine (R)
            MyResidues.Add(new ResidueToLib("Tyrosine", new byte[1] { 89 }, 163.06332397460937, 163.1733, "C(9)H(9)NO(2)"));  //Tyrosine (Y)
            MyResidues.Add(new ResidueToLib("Tryptophan", new byte[1] { 87 }, 186.07931518554687, 186.2132, "C(11)H(10)N(2)O"));  //Tryptophan (W)
        }

        public void LoadDefaultResidueMarked()
        {
            MyResidues.Add(new ResidueToLib("4-13C Arginine", new byte[1] { 82 }, 160.11452408744813, 0, "13C(4)C(2)H(12)N(4)O", new byte[1] { 247 }));
            MyResidues.Add(new ResidueToLib("6-13C Lysine", new byte[1] { 75 }, 134.11508447101593, 0, "13C(6)H(12)N(2)O", new byte[1] { 248 }));
        }
    }

    [Serializable]
    [ProtoContract]
    public class ResidueToLib
    {
        /// <summary>
        /// public variables
        /// </summary>
        [ProtoMember(2)]
        public string Name { get; set; }
        [ProtoMember(3)]
        public byte[] LetterCode { get; set; }
        [ProtoMember(4)]
        public double MonoIsotopicMass { get; set; }
        [ProtoMember(5)]
        public double AverageMass { get; set; }
        [ProtoMember(6)]
        public string Composition { get; set; }
        [ProtoMember(7)]
        public byte[] AA { get; set; }

        /// <summary>
        /// Null Constructor for serializing Class
        /// </summary>
        public ResidueToLib() { }

        public ResidueToLib(string name, byte[] letterCode, double monoIsotopicMass, double averageMass, string composition = null, byte[] aa = null)
        {
            Name = name;
            LetterCode = letterCode;
            MonoIsotopicMass = monoIsotopicMass;
            AverageMass = averageMass;
            Composition = composition;
            AA = aa;
        }
    }
}
