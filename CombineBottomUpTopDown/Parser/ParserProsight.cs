﻿/**
 * Program:     ProteoCombiner - Integrating bottom-up & top-down proteomics data
 * Author:      Diogo Borges Lima
 * Created:      4/3/2019
 * Update by:   Diogo Borges Lima
 * Description: Class responsable for reading output file of Prosight
 */
using CombineBottomUpTopDown.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CombineBottomUpTopDown.Parser
{
    public class ParserProsight
    {
        private int BYTEPERLINE = 1190;

        public ParserProsight() { }

        /// <summary>
        /// private variables
        /// </summary>
        private Regex containsNumber = new Regex("[0-9]", RegexOptions.Compiled);
        private string[] HeaderLine { get; set; }
        private Proteoform proteoform { get; set; }
        private string sequence { get; set; }
        private string annotatedSequence { get; set; }
        private string modifications { get; set; }
        private string proteinAccession { get; set; }
        private string proteinDescription { get; set; }
        private int charge { get; set; }
        private double mZ { get; set; }
        private double mHPlus { get; set; }
        private double theoreticalMass { get; set; }
        private double deltaMass { get; set; }
        private double deltaMZ { get; set; }
        private int matchedIons { get; set; }
        private int totalIons { get; set; }
        private double intensity { get; set; }
        private ActivationType activationType { get; set; }
        private double retentionTime { get; set; }
        private int scanNumber { get; set; } // FirstScan column
        private string spectrumFile { get; set; }
        private double logPScore { get; set; }
        private double logEValue { get; set; }
        private string fragmentMap { get; set; }
        private int identifiedNode { get; set; }

        private Regex numberCaptured = new Regex("[0-9|\\.]+", RegexOptions.Compiled);

        public List<Proteoform> ParseFile(String fileName)
        {
            List<Proteoform> proteoformList = new List<Proteoform>();
            StreamReader sr = null;
            long lengthFile = 0;
            try
            {
                sr = new StreamReader(fileName);
                FileInfo fileInfo = new FileInfo(fileName);
                lengthFile = fileInfo.Length / BYTEPERLINE;
            }
            catch (Exception)
            {
                return proteoformList;
            }
            string line = "";
            int proteoform_processed = 0;
            int old_progress = 0;

            try
            {
                while ((line = sr.ReadLine()) != null)
                {
                    if (line.Length > 0)
                    {
                        line = Regex.Replace(line, "\"", "");
                        if (!line.StartsWith("Checked") && //PD version until 2.4
                            !line.Contains("Checked"))//Newer PD
                        {
                            Process(line);
                            if (proteoform != null)
                            {
                                proteoformList.Add(proteoform);
                                proteoform = null;
                            }
                        }
                        else
                            ProcessHeader(line);

                        proteoform_processed++;
                        int new_progress = (int)((double)proteoform_processed / (lengthFile) * 100);
                        if (new_progress > old_progress)
                        {
                            old_progress = new_progress;
                            Console.Write(" Reading ProSight file: " + old_progress + "%");
                        }
                    }
                }
                Console.Write(" Reading ProSight file: 100%");
                sr.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine();
            }
            proteoformList.Sort((a, b) => b.PrSMs[0].LogPScore.CompareTo(a.PrSMs[0].LogPScore));
            return proteoformList;
        }

        /// <summary>
        /// Method responsible for processing the header line
        /// </summary>
        /// <param name="row"></param>
        private void ProcessHeader(string row)
        {
            HeaderLine = Regex.Split(row, "\t");
        }

        /// <summary>
        /// Method responsible for processing each line file
        /// </summary>
        /// <param name="row"></param>
        private void Process(string row)
        {
            string[] cols = Regex.Split(row, "\t");
            int index = Array.IndexOf(HeaderLine, "Identifying Node");
            if (index == -1) return;
            identifiedNode = Convert.ToInt32(cols[index].Substring(cols[index].Length - 2, 1));
            index = Array.IndexOf(HeaderLine, "Annotated Sequence");
            if (index == -1) return;
            annotatedSequence = cols[index];
            index = Array.IndexOf(HeaderLine, "Sequence");
            if (index == -1)
                sequence = annotatedSequence.ToUpper();
            else
                sequence = cols[index];
            index = Array.IndexOf(HeaderLine, "Modifications");
            if (index == -1) return;
            modifications = cols[index];
            index = Array.IndexOf(HeaderLine, "Protein Descriptions");
            if (index == -1)
                proteinDescription = string.Empty;
            else
                proteinDescription = cols[index];
            index = Array.IndexOf(HeaderLine, "Protein Accessions");
            if (index == -1) return;
            string[] accessionNumbers = Regex.Split(cols[index], "-");
            proteinAccession = accessionNumbers[0];
            if (proteinDescription.ToLower().Contains("splice"))
                proteinAccession = accessionNumbers[0] + "_isoform";
            index = Array.IndexOf(HeaderLine, "Charge");
            if (index == -1) return;
            charge = Convert.ToInt32(cols[index]);
            index = Array.IndexOf(HeaderLine, "m/z [Da]");
            if (index == -1) return;
            mZ = Convert.ToDouble(cols[index]);
            index = Array.IndexOf(HeaderLine, "MH+ [Da]");//PD Version until 2.4
            if (index == -1)
            {
                index = Array.IndexOf(HeaderLine, "Mass [Da]");
                if (index == -1) return;
            }
            mHPlus = Convert.ToDouble(cols[index]);
            index = Array.IndexOf(HeaderLine, "Theo. MH+ [Da]");//PD Version until 2.4
            if (index == -1)
            {
                index = Array.IndexOf(HeaderLine, "Theo. Mass [Da]");
                if (index == -1) return;
            }
            theoreticalMass = Convert.ToDouble(cols[index]);
            index = Array.IndexOf(HeaderLine, "DeltaM [ppm]");//PD Version until 2.4
            if (index == -1)
            {
                index = Array.IndexOf(HeaderLine, "DeltaMass [ppm]");
                if (index == -1) return;
            }
            deltaMass = containsNumber.IsMatch(cols[index]) ? Convert.ToDouble(cols[index]) : 0;
            index = Array.IndexOf(HeaderLine, "Deltam/z [Da]");//PD Version until 2.4
            if (index == -1)
            {
                index = Array.IndexOf(HeaderLine, "DeltaMass [Da]");
                if (index == -1) return;
            }
            deltaMZ = containsNumber.IsMatch(cols[index]) ? Convert.ToDouble(cols[index]) : 0;
            index = Array.IndexOf(HeaderLine, "Matched Ions");
            if (index == -1)
                matchedIons = 0;
            else
                matchedIons = Convert.ToInt32(cols[index]);
            index = Array.IndexOf(HeaderLine, "Total Ions");
            if (index == -1)
                totalIons = 0;
            else
                totalIons = Convert.ToInt32(cols[index]);
            index = Array.IndexOf(HeaderLine, "Intensity");
            if (index == -1)
                intensity = 0;
            else
                intensity = Convert.ToDouble(cols[index]);
            index = Array.IndexOf(HeaderLine, "RT [min]");
            if (index == -1)
                retentionTime = 0;
            else
                retentionTime = Convert.ToDouble(cols[index]);
            index = Array.IndexOf(HeaderLine, "First Scan");
            if (index == -1)
            {
                index = Array.IndexOf(HeaderLine, "Fragmentation Scan(s)");
                if (index == -1) return;
            }
            scanNumber = Convert.ToInt32(cols[index]);
            index = Array.IndexOf(HeaderLine, "Spectrum File");
            if (index == -1) return;
            spectrumFile = cols[index];
            index = Array.IndexOf(HeaderLine, "-Log P-Score");
            if (index == -1) return;
            logPScore = containsNumber.IsMatch(cols[index]) ? Convert.ToDouble(cols[index]) : 0;
            index = Array.IndexOf(HeaderLine, "-Log E-Value");
            if (index == -1) return;
            logEValue = containsNumber.IsMatch(cols[index]) ? Convert.ToDouble(cols[index]) : 0;
            index = Array.IndexOf(HeaderLine, "Fragment Map");
            if (index == -1)
                fragmentMap = string.Empty;
            else
                fragmentMap = cols[index];

            ProteoformParams proteoformParams = new ProteoformParams(
                identifiedNode,
                modifications,
                charge,
                mZ,
                mHPlus,
                theoreticalMass,
                deltaMass,
                deltaMZ,
                matchedIons,
                totalIons,
                intensity,
                retentionTime,
                scanNumber,
                spectrumFile,
                logPScore,
                logEValue,
                fragmentMap
                );
            proteoformParams.SearchEngine = "Prosight";

            index = Array.IndexOf(HeaderLine, "Activation Type");
            if (index == -1) return;
            switch (cols[index])
            {
                case "EThcD":
                    proteoformParams.ActivationType = ActivationType.EThCD;
                    break;
                case "CID":
                    proteoformParams.ActivationType = ActivationType.CID;
                    break;
                case "ETD":
                    proteoformParams.ActivationType = ActivationType.ETD;
                    break;
                case "HCD":
                    proteoformParams.ActivationType = ActivationType.HCD;
                    break;
                case "ECD":
                    proteoformParams.ActivationType = ActivationType.ECD;
                    break;
                case "MPD":
                    proteoformParams.ActivationType = ActivationType.MPD;
                    break;
                case "PQD":
                    proteoformParams.ActivationType = ActivationType.PQD;
                    break;
                default:
                    proteoformParams.ActivationType = ActivationType.NF;
                    break;
            }

            proteoformParams.FragmentIons = GetFragmentIons(Regex.Replace(fragmentMap, ">;", ">\n"));

            proteoform = new Proteoform(sequence, proteinAccession, proteinDescription, annotatedSequence, 0, new List<ProteoformParams>() { proteoformParams });
        }

        private List<Tuple<double, double>> GetFragmentIons(string fragmentMap)
        {
            //List<mz,intensity>
            List<Tuple<double, double>> FragmentIons = new List<Tuple<double, double>>();
            string[] cols = Regex.Split(fragmentMap, "\n");
            foreach (string line in cols)
            {
                if (line.Trim().StartsWith("<fragment id="))
                {
                    string mass_intensity_Str = Regex.Split(line.Trim(), "mass=")[1];
                    double mass = Convert.ToDouble(numberCaptured.Matches(mass_intensity_Str)[0].Value);
                    double intensity = Convert.ToDouble(numberCaptured.Matches(mass_intensity_Str)[1].Value);
                    FragmentIons.Add((mass, intensity).ToTuple());
                }
            }
            return FragmentIons;
        }
    }
}
