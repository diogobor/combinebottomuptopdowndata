﻿using CombineBottomUpTopDown.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace CombineBottomUpTopDown.Parser
{
    public class ParserXMLDB
    {
        public ParserXMLDB() { }

        /// <summary>
        /// private variables
        /// </summary>
        private string ProteinID { get; set; }
        private List<string> ACLines { get; set; }
        private List<string> DELines { get; set; }
        private List<string> GNLines { get; set; }
        private List<string> OSLines { get; set; }
        private List<string> OCLines { get; set; }

        //Tuple<dataset, created, modified, version>
        private Tuple<string, string, string, string> EntryLine { get; set; }
        //List<Tuple<type,text>>
        private List<Tuple<string, string>> GeneLines { get; set; }
        //List<Tuple<key,Tuple<type,date,name,volume,first,last, title, List<Tuple<dbReferenceTupe,dbReferenceID>>>, scope, source/strain, List<person>>>
        private List<Tuple<int, Tuple<Tuple<string, string, string, string, string, string>, string, List<Tuple<string, string>>>, string, string, List<string>>> ReferenceLines { get; set; }
        //List<Tuple<Tuple<type, description, evidence or ID>, Tuple<original,variation, position>, Tuple<begin,end>>>
        private List<Tuple<Tuple<string, string, string>, Tuple<string, string, int>, Tuple<string, string>>> FeatureLines { get; set; }
        //List<Tuple<key, type>, Tuple<dbReferemceType, dbReferenceID>>
        private List<Tuple<Tuple<int, string>, Tuple<string, string>>> EvidenceLines { get; set; }
        //List<Tuple<length, mass, checksum, modified, version, sequence>>
        private Tuple<int, double, string, string, string, string> SequenceLine { get; set; }

        /// <summary>
        /// public variables
        /// </summary>
        public List<ProteinInformation> ProteinList { get; set; }
        public List<FastaItem> FastaItemsList { get; set; }

        /// <summary>
        /// Method responsible for parsing XML file
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="tagInformation"></param>
        public void ParseXMLFile(string fileName)
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            XmlReader reader = XmlReader.Create(fileName, settings);
            bool isProteinIDRead = false;

            #region Start lists
            ProteinList = new List<ProteinInformation>();
            #endregion

            while (reader.Read())
            {
                // Do some work here on the data.
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element: // The node is an element.

                        switch (reader.Name)
                        {
                            case "entry":
                                //Clean objects
                                #region Clean lists
                                ACLines = new List<string>();
                                DELines = new List<string>();
                                GNLines = new List<string>();
                                OSLines = new List<string>();
                                OCLines = new List<string>();
                                GeneLines = new List<Tuple<string, string>>();
                                ReferenceLines = new List<Tuple<int, Tuple<Tuple<string, string, string, string, string, string>, string, List<Tuple<string, string>>>, string, string, List<string>>>();
                                FeatureLines = new List<Tuple<Tuple<string, string, string>, Tuple<string, string, int>, Tuple<string, string>>>();
                                EvidenceLines = new List<Tuple<Tuple<int, string>, Tuple<string, string>>>();
                                isProteinIDRead = false;
                                #endregion

                                reader.MoveToNextAttribute();
                                string entryDataset = reader.Value;

                                reader.MoveToNextAttribute();
                                string entryCreated = reader.Value;

                                reader.MoveToNextAttribute();
                                string entryModified = reader.Value;

                                reader.MoveToNextAttribute();
                                string entryVersion = reader.Value;

                                EntryLine = Tuple.Create(entryDataset, entryCreated, entryModified, entryVersion);
                                break;
                            case "accession":
                                reader.Read();
                                ACLines.Add(reader.Value);
                                break;

                            case "name": // protein ID
                                reader.Read();
                                if (!isProteinIDRead)
                                {
                                    ProteinID = reader.Value;
                                    isProteinIDRead = true;
                                }
                                break;

                            case "fullName": // protein description
                                reader.Read();
                                DELines.Add(reader.Value);
                                break;

                            case "gene": // gene description
                                reader.Read();//name tag
                                while (reader.Name.Equals("name"))
                                {
                                    if (reader.NodeType == XmlNodeType.EndElement)
                                    {
                                        reader.Read();
                                        continue;
                                    }
                                    reader.MoveToNextAttribute();
                                    string geneType = reader.Value;
                                    reader.Read();

                                    GeneLines.Add(Tuple.Create(geneType, reader.Value));
                                    reader.Read();//next
                                }
                                break;

                            case "organism": // organism description
                                reader.Read();// tag 'name'
                                reader.Read();// text
                                OSLines.Add(reader.Value);
                                break;

                            case "taxon": // taxonomy description
                                reader.Read();// text
                                OCLines.Add(reader.Value);
                                break;

                            case "reference": // reference description
                                #region Reference
                                reader.MoveToNextAttribute(); // Read the key attribute.
                                int key = int.Parse(reader.Value);
                                reader.Read();// citation tag

                                string type = "";
                                string date = "";
                                string name_or_db = "";
                                string vol = "";
                                string first = "";
                                string last = "";

                                while (reader.MoveToNextAttribute())
                                {
                                    switch (reader.Name)
                                    {
                                        case "type":
                                            type = reader.Value;
                                            break;
                                        case "date":
                                            date = reader.Value;
                                            break;
                                        case "name":
                                            name_or_db = reader.Value;
                                            break;
                                        case "volume":
                                            vol = reader.Value;
                                            break;
                                        case "first":
                                            first = reader.Value;
                                            break;
                                        case "last":
                                            last = reader.Value;
                                            break;
                                        case "db":
                                            name_or_db = reader.Value + "#";
                                            break;
                                    }
                                }

                                reader.Read();//title tag
                                string title = "";
                                if (reader.Name.Equals("title"))
                                {
                                    reader.Read();//title text
                                    title = reader.Value;
                                    reader.Read();//</title>
                                    reader.Read();//authorlist
                                }

                                List<string> editor_or_person_or_consortiumList = new List<string>();

                                if (reader.Name.Equals("editorList"))
                                {
                                    while (reader.Read())//person list
                                    {
                                        if (reader.Name.Equals("person"))
                                        {
                                            reader.MoveToNextAttribute();//name attribute
                                            editor_or_person_or_consortiumList.Add("editorList" + reader.Value);
                                        }
                                        else if (reader.Name.Equals("consortium"))
                                        {
                                            reader.MoveToNextAttribute();//name attribute
                                            editor_or_person_or_consortiumList.Add("editorList" + reader.Value + "#");
                                        }
                                        else
                                            break;
                                    }
                                    reader.Read();//</editorList>
                                }

                                if (reader.Name.Equals("authorList"))
                                {
                                    while (reader.Read())//person list
                                    {
                                        if (reader.Name.Equals("person"))
                                        {
                                            reader.MoveToNextAttribute();//name attribute
                                            editor_or_person_or_consortiumList.Add("authorList" + reader.Value);
                                        }
                                        else if (reader.Name.Equals("consortium"))
                                        {
                                            reader.MoveToNextAttribute();//name attribute
                                            editor_or_person_or_consortiumList.Add("authorList" + reader.Value + "#");
                                        }
                                        else
                                            break;
                                    }
                                }

                                List<Tuple<string, string>> dbReferenceList = new List<Tuple<string, string>>();

                                while (reader.Read())//dbReference list
                                {
                                    if (reader.Name.Equals("dbReference"))
                                    {
                                        reader.MoveToNextAttribute();//name attribute
                                        string dbRefTypeCurrent = "";
                                        string dbRefIDCurrent;
                                        dbRefTypeCurrent = reader.Value;
                                        reader.MoveToNextAttribute();
                                        dbRefIDCurrent = reader.Value;
                                        dbReferenceList.Add(Tuple.Create(dbRefTypeCurrent, dbRefIDCurrent));
                                    }
                                    else
                                        break;
                                }
                                reader.Read();//scope tag

                                StringBuilder sbScope = new StringBuilder();
                                while (reader.Name.Equals("scope"))
                                {
                                    if (reader.NodeType == XmlNodeType.EndElement)
                                    {
                                        reader.Read();
                                        continue;
                                    }
                                    reader.Read();//scope text
                                    sbScope.Append(reader.Value);
                                    sbScope.Append("#");
                                    reader.Read();
                                }
                                string scope = sbScope.ToString();
                                scope = scope.Substring(0, scope.Length - 1);

                                string strain_or_plasmid = "";
                                StringBuilder sbStrain = new StringBuilder();
                                if (reader.NodeType != XmlNodeType.EndElement)
                                {
                                    reader.Read();//strain tag

                                    while (reader.Name.Equals("strain")) // 1st - strain; 2nd - plasmid
                                    {
                                        if (reader.NodeType == XmlNodeType.EndElement)
                                        {
                                            reader.Read();
                                            continue;
                                        }
                                        reader.Read();//scope text
                                        sbStrain.Append(reader.Value);
                                        sbStrain.Append("#");
                                        reader.Read();
                                    }

                                    while (reader.Name.Equals("plasmid"))
                                    {
                                        if (reader.NodeType == XmlNodeType.EndElement)
                                        {
                                            reader.Read();
                                            continue;
                                        }
                                        reader.Read();//scope text
                                        sbStrain.Append(reader.Value);
                                        sbStrain.Append("@");
                                        reader.Read();
                                    }

                                    while (reader.Name.Equals("strain"))// 1st- plasmid; 2nd - strain
                                    {
                                        if (reader.NodeType == XmlNodeType.EndElement)
                                        {
                                            reader.Read();
                                            continue;
                                        }
                                        reader.Read();//scope text
                                        sbStrain.Append(reader.Value);
                                        sbStrain.Append("#");
                                        reader.Read();
                                    }
                                    strain_or_plasmid = sbStrain.ToString();
                                }

                                Tuple<Tuple<string, string, string, string, string, string>, string, List<Tuple<string, string>>> citation =
                                new Tuple<Tuple<string, string, string, string, string, string>, string, List<Tuple<string, string>>>(
                                   Tuple.Create(type,
                                date,
                                name_or_db,
                                vol,
                                first,
                                last),
                                title,
                                dbReferenceList);
                                ReferenceLines.Add(Tuple.Create(key, citation, scope, strain_or_plasmid, editor_or_person_or_consortiumList));
                                #endregion
                                break;

                            case "feature": // feature description

                                #region Feature
                                string featType = "";
                                string featDesc = "";
                                string featEvidence_or_ID = "";
                                reader.MoveToNextAttribute(); // Read the type attribute.
                                featType = reader.Value;
                                reader.MoveToNextAttribute(); // Read the description attribute.
                                if (reader.Name.Equals("description"))
                                {
                                    featDesc = reader.Value;
                                    if (featDesc.Contains("\""))
                                        featDesc = featDesc.Replace("\"", "&quot;");
                                    reader.MoveToNextAttribute(); // Read the evidence attribute.
                                    if (!reader.Name.Equals("description"))
                                        featEvidence_or_ID = reader.Value;
                                }
                                else
                                {
                                    featEvidence_or_ID = reader.Value;
                                }
                                reader.Read();// can be original / variation tags
                                string featOriginal = "";
                                string featVariation = "";
                                int position = 0;
                                string begin = "";
                                string end = "";
                                if (reader.Name.Equals("original"))
                                {
                                    reader.Read();//original text
                                    featOriginal = reader.Value;
                                    reader.Read();//</original>
                                    reader.Read();//variation tag
                                    reader.Read();//variation text
                                    featVariation = reader.Value;
                                    while (reader.Read())//</variation>
                                    {
                                        if (reader.NodeType != XmlNodeType.EndElement)
                                        {
                                            if (reader.Name.Equals("variation"))
                                            {
                                                reader.Read();//variation text
                                                featVariation += "#" + reader.Value;
                                            }
                                            else
                                                break;
                                        }
                                    }
                                    reader.Read();//position
                                    reader.MoveToNextAttribute();
                                    position = int.Parse(reader.Value);
                                }
                                else // begin/end
                                {
                                    reader.Read();//begin
                                    if (reader.Name.Equals("position"))
                                    {
                                        reader.MoveToNextAttribute();
                                        position = int.Parse(reader.Value);
                                    }
                                    else
                                    {
                                        reader.MoveToNextAttribute();
                                        begin = reader.Value + "#" + reader.Name;

                                        reader.Read();//end
                                        reader.MoveToNextAttribute();
                                        end = reader.Value + "#" + reader.Name;
                                    }
                                }

                                Tuple<string, string, string> featInitial = Tuple.Create(featType, featDesc, featEvidence_or_ID);
                                Tuple<string, string, int> featMut = Tuple.Create(featOriginal, featVariation, position);
                                Tuple<string, string> featPos = Tuple.Create(begin, end);

                                FeatureLines.Add(Tuple.Create(featInitial, featMut, featPos));

                                #endregion
                                break;

                            case "evidence": // evidence description

                                #region Evidence
                                while (reader.Name.Equals("evidence"))
                                {
                                    int evidKey;
                                    string evidType = "";
                                    reader.MoveToNextAttribute();
                                    evidKey = int.Parse(reader.Value);
                                    reader.MoveToNextAttribute();
                                    evidType = reader.Value;
                                    string dbRefType = "";
                                    string dbRefID = "";
                                    reader.Read();// source tag

                                    if (reader.Name.Equals("source"))
                                    {
                                        reader.Read();// dbReference tag
                                        reader.MoveToNextAttribute();
                                        dbRefType = reader.Value;
                                        reader.MoveToNextAttribute();
                                        dbRefID = reader.Value;
                                        EvidenceLines.Add(Tuple.Create(Tuple.Create(evidKey, evidType), Tuple.Create(dbRefType, dbRefID)));
                                        break;
                                    }
                                    else
                                    {
                                        EvidenceLines.Add(Tuple.Create(Tuple.Create(evidKey, evidType), Tuple.Create(dbRefType, dbRefID)));
                                    }

                                }
                                #endregion

                                if (reader.Name.Equals("sequence")) // If the evidence there is no source tag.
                                {
                                    #region Sequence
                                    reader.MoveToNextAttribute();
                                    int length = int.Parse(reader.Value);

                                    reader.MoveToNextAttribute();
                                    double mass = double.Parse(reader.Value);

                                    reader.MoveToNextAttribute();
                                    string checksum = reader.Value;

                                    reader.MoveToNextAttribute();
                                    string modified = reader.Value;

                                    reader.MoveToNextAttribute();
                                    string version = reader.Value;

                                    reader.Read();
                                    string sequence = reader.Value;

                                    SequenceLine = Tuple.Create(length, mass, checksum, modified, version, sequence.Replace(" ", "").Trim());
                                    #endregion
                                }
                                break;

                            case "sequence": // sequence description; if evidence tag contains source tag.

                                #region Sequence
                                reader.MoveToNextAttribute();

                                if (!reader.Name.Equals("length")) break;//It's not the protein sequence.
                                int seqLength = int.Parse(reader.Value);

                                reader.MoveToNextAttribute();
                                double seqMass = double.Parse(reader.Value);

                                reader.MoveToNextAttribute();
                                string seqChecksum = reader.Value;

                                reader.MoveToNextAttribute();
                                string seqModified = reader.Value;

                                reader.MoveToNextAttribute();
                                string seqVersion = reader.Value;

                                reader.Read();
                                string seqSequence = reader.Value;

                                SequenceLine = Tuple.Create(seqLength, seqMass, seqChecksum, seqModified, seqVersion, seqSequence.Replace(" ", "").Trim());
                                #endregion
                                break;

                        }
                        break;

                    case XmlNodeType.EndElement: //Display the end of the element.
                        if (reader.Name.Equals("entry"))
                        {
                            //Save protein
                            ProteinInformation ptnInfo = new ProteinInformation();
                            ptnInfo.ProteinID = ProteinID;
                            ptnInfo.ACLines = ACLines;
                            ptnInfo.DELines = DELines;
                            ptnInfo.GNLines = GNLines;
                            ptnInfo.OSLines = OSLines;
                            ptnInfo.OCLines = OCLines;
                            ptnInfo.ReferenceLines = ReferenceLines;
                            ptnInfo.FeatureLines = FeatureLines;
                            ptnInfo.EvidenceLines = EvidenceLines;
                            ptnInfo.SequenceLine = SequenceLine;
                            ptnInfo.OriginalProteinSequence = SequenceLine.Item6;
                            if (ptnInfo.OriginalProteinSequence.StartsWith("\n"))
                                ptnInfo.ProteinSequence = SequenceLine.Item6.Substring(1, SequenceLine.Item6.Length - 1).Replace("\n", "");
                            else if (ptnInfo.OriginalProteinSequence.Contains("\n"))
                                ptnInfo.ProteinSequence = SequenceLine.Item6.Replace("\n", "");
                            else
                                ptnInfo.ProteinSequence = ptnInfo.OriginalProteinSequence;
                            ptnInfo.EntryLine = EntryLine;
                            ptnInfo.GeneLines = GeneLines;
                            ProteinList.Add(ptnInfo);
                        }
                        break;
                }
            }
            ProcessProteins(ProteinList);
        }

        private void ProcessProteins(List<ProteinInformation> ProteinList = null)
        {
            if (ProteinList != null)
            {
                #region Fill fastalist object
                FastaItemsList = new List<FastaItem>();
                foreach (ProteinInformation ptn in ProteinList)
                {
                    List<string> deLines = (from line in ptn.DELines
                                            where !String.IsNullOrEmpty(line)
                                            select line).ToList();
                    string protID = !String.IsNullOrEmpty(ptn.ProteinID) ? ptn.ProteinID + "|" + String.Join("|", ptn.ACLines) : String.Join("|", ptn.ACLines);
                    FastaItem fasta = new FastaItem(protID, ptn.ProteinSequence, String.Join(", ", deLines));
                    //List<Tuple<Tuple<type, description, evidence or ID>, Tuple<original,variation, position>, Tuple<begin,end>>>

                    //Tuple<description, startPosition, endPosition>
                    List<Tuple<string, int, int>> chains = new List<Tuple<string, int, int>>();
                    ptn.FeatureLines.Where(item => item.Item1.Item1.Equals("chain") || item.Item1.Item1.Equals("signal peptide")).ToList().ForEach(feature =>
                    {
                        string[] colsStartPos = Regex.Split(feature.Item3.Item1, "#position");
                        string[] colsEndPos = Regex.Split(feature.Item3.Item2, "#position");

                        if (colsStartPos.Length == 2 && colsEndPos.Length == 2)
                        {
                            if (feature.Item1.Item1.Equals("signal peptide"))
                                chains.Add(Tuple.Create(feature.Item1.Item1, Convert.ToInt32(colsStartPos[0]), Convert.ToInt32(colsEndPos[0])));
                            else
                                chains.Add(Tuple.Create(feature.Item1.Item2, Convert.ToInt32(colsStartPos[0]), Convert.ToInt32(colsEndPos[0])));
                        }
                    });

                    //List<(Description, original sequence, modification sequence, position to be modified)>
                    List<(string, string, string, int)> spliceList = new List<(string, string, string, int)>();
                    ptn.FeatureLines.Where(item => item.Item1.Item1.Equals("splice variant")).ToList().ForEach(feature =>
                    {
                        string spliceDescription = feature.Item1.Item2;
                        string original = feature.Item2.Item1;
                        string variation = feature.Item2.Item2;
                        int position = feature.Item2.Item3;
                        spliceList.Add((spliceDescription, original, variation, position));
                    });

                    //Tuple<description, position>
                    List<Tuple<string, int>> theoreticalPTMs = new List<Tuple<string, int>>();
                    ptn.FeatureLines.Where(item => item.Item1.Item1.Equals("modified residue") || item.Item1.Item1.Equals("disulfide bond")).ToList().ForEach(feature =>
                    {
                        if (String.IsNullOrEmpty(feature.Item3.Item1) && String.IsNullOrEmpty(feature.Item3.Item2))
                        {
                            theoreticalPTMs.Add(Tuple.Create(feature.Item1.Item2, Convert.ToInt32(feature.Item2.Item3)));
                        }
                        else
                        {
                            string[] colsStartPos = Regex.Split(feature.Item3.Item1, "#position");
                            string[] colsEndPos = Regex.Split(feature.Item3.Item2, "#position");

                            if (colsStartPos.Length == 2 && colsEndPos.Length == 2)
                            {
                                if (colsStartPos[0].Equals(colsEndPos[0]))//There is only one PTM
                                {
                                    if (feature.Item1.Item1.Equals("disulfide bond"))
                                        theoreticalPTMs.Add(Tuple.Create(feature.Item1.Item1, Convert.ToInt32(colsStartPos[0])));
                                    else
                                        theoreticalPTMs.Add(Tuple.Create(feature.Item1.Item2, Convert.ToInt32(colsStartPos[0])));
                                }
                                else// There are two PTMs
                                {
                                    if (feature.Item1.Item1.Equals("disulfide bond"))
                                    {
                                        theoreticalPTMs.Add(Tuple.Create(feature.Item1.Item1, Convert.ToInt32(colsStartPos[0])));
                                        theoreticalPTMs.Add(Tuple.Create(feature.Item1.Item1, Convert.ToInt32(colsEndPos[0])));
                                    }
                                    else
                                    {
                                        theoreticalPTMs.Add(Tuple.Create(feature.Item1.Item2, Convert.ToInt32(colsStartPos[0])));
                                        theoreticalPTMs.Add(Tuple.Create(feature.Item1.Item2, Convert.ToInt32(colsEndPos[0])));
                                    }
                                }
                            }
                        }
                    });
                    fasta.Chains = chains;
                    fasta.TheoreticalPTMs = theoreticalPTMs;
                    fasta.SequenceInBytes = System.Text.ASCIIEncoding.Default.GetBytes(ptn.ProteinSequence);
                    FastaItemsList.Add(fasta);

                    //(Description, original sequence, modification sequence, position to be modified)
                    foreach ((string, string, string, int) splice in spliceList)
                    {
                        if (String.IsNullOrEmpty(splice.Item2) || String.IsNullOrEmpty(splice.Item3)) continue;
                        var newSequence = ptn.ProteinSequence;
                        var aStringBuilder = new StringBuilder(newSequence);
                        aStringBuilder.Remove(splice.Item4 - 1, splice.Item2.Length);
                        aStringBuilder.Insert(splice.Item4 - 1, splice.Item3);
                        newSequence = aStringBuilder.ToString();
                        string[] colsProtID = Regex.Split(protID, "\\|");
                        StringBuilder sbProtID = new StringBuilder();
                        foreach (string colProtID in colsProtID)
                            sbProtID.Append(colProtID + "_isoform|");
                        FastaItem fastaSplice = new FastaItem(sbProtID.ToString().Substring(0, sbProtID.ToString().Length - 1), newSequence, splice.Item1);
                        fastaSplice.SequenceInBytes = System.Text.ASCIIEncoding.Default.GetBytes(newSequence);
                        FastaItemsList.Add(fastaSplice);
                    }
                }
                #endregion
            }
        }
    }
}
