﻿/**
 * Program:     ProteoCombiner - Integrating bottom-up & top-down proteomics data
 * Author:      Diogo Borges Lima
 * Created:     5/3/2019
 * Update by:   Diogo Borges Lima
 * Description: Class responsable for parsing PTMs from Uniprot
 */
using CombineBottomUpTopDown.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CombineBottomUpTopDown.Parser
{
    public class ParserPTMuniprot
    {
        public List<ModificationItem> PTMs { get; set; }

        public void ParserPTM()
        {
            double mm = 0;
            double ma = 0;
            string aminoacid = string.Empty;
            byte aminoacidAA = 0;
            string description = string.Empty;
            PTMs = new List<ModificationItem>();
            bool isReactionPTM = false;

            string ptmsStored = ProteoCombiner.Properties.Settings.Default.PTMs;
            string[] ptms = Regex.Split(ptmsStored, "\r\n");

            bool start = false;
            bool finish = false;
            foreach (string ptm in ptms)
            {
                if (ptm.StartsWith("ID   "))
                    start = true;
                if (ptm.StartsWith("//"))
                {
                    start = false;
                    finish = true;
                }

                if (start)
                {
                    if (ptm.StartsWith("ID   "))
                    {
                        string[] cols = Regex.Split(ptm, "   ");
                        description = cols[1];
                    }
                    else if (ptm.StartsWith("MM   "))//monoisotopic mass
                    {
                        string[] cols = Regex.Split(ptm, "   ");
                        mm = Convert.ToDouble(cols[1]);
                    }
                    else if (ptm.StartsWith("MA   "))//average mass
                    {
                        string[] cols = Regex.Split(ptm, "   ");
                        ma = Convert.ToDouble(cols[1]);
                    }
                    else if (ptm.StartsWith("TG   "))//target amino acid
                    {
                        string[] cols = Regex.Split(ptm, "   ");
                        aminoacid = cols[1].Replace(".", "");
                        if (aminoacid.Contains("-"))
                        {
                            string[] cols2 = Regex.Split(aminoacid, "-");

                            aminoacid = string.Empty;
                            foreach (string eachPtm in cols2)
                                aminoacid += GetAA(eachPtm);
                        }
                        else if (aminoacid.Contains(" or "))
                        {
                            string[] cols2 = Regex.Split(aminoacid, " or ");

                            aminoacid = string.Empty;
                            foreach (string eachPtm in cols2)
                                aminoacid += GetAA(eachPtm);
                        }
                        else
                            aminoacid = GetAA(aminoacid);
                        aminoacidAA = !String.IsNullOrEmpty(aminoacid) ? System.Text.ASCIIEncoding.Default.GetBytes(aminoacid)[0] : (byte)0;
                    }

                    if (ptm.StartsWith("FT   "))
                    {
                        string[] cols = Regex.Split(ptm, "   ");
                        if (cols[1].Equals("CROSSLNK"))
                            isReactionPTM = true;
                        else
                            isReactionPTM = false;
                    }
                }
                if (finish)
                {
                    finish = false;
                    ModificationItem mod = new ModificationItem();
                    mod.AminoAcid = aminoacid;
                    mod.AminoAcidByte = aminoacidAA;
                    mod.Description = description;
                    mod.AverageMass = ma;
                    mod.MonoIsotopicMass = mm;
                    mod.IsActive = true;
                    mod.IsReaction = isReactionPTM;
                    PTMs.Add(mod);
                }
            }
        }

        public string GetAA(string aminoacid)
        {
            switch (aminoacid)
            {
                case "Asparagine":
                    aminoacid = "N";
                    break;
                case "Glycine":
                    aminoacid = "G";
                    break;
                case "Aspartate":
                    aminoacid = "D";
                    break;
                case "Arginine":
                    aminoacid = "R";
                    break;
                case "Histidine":
                    aminoacid = "H";
                    break;
                case "Proline":
                    aminoacid = "P";
                    break;
                case "Isoleucine":
                    aminoacid = "I";
                    break;
                case "Leucine":
                    aminoacid = "L";
                    break;
                case "Lysine":
                    aminoacid = "K";
                    break;
                case "Tyrosine":
                    aminoacid = "Y";
                    break;
                case "Cysteine":
                    aminoacid = "C";
                    break;
                case "Serine":
                    aminoacid = "S";
                    break;
                case "Threonine":
                    aminoacid = "T";
                    break;
                case "Tryptophan":
                    aminoacid = "W";
                    break;
                case "Phenylalanine":
                    aminoacid = "F";
                    break;
                case "Alanine":
                    aminoacid = "A";
                    break;
                case "Glutamate":
                    aminoacid = "E";
                    break;
                case "Glutamine":
                    aminoacid = "Q";
                    break;
                case "Methionine":
                    aminoacid = "M";
                    break;
                case "Selenocysteine":
                    aminoacid = "U";
                    break;
                case "Valine":
                    aminoacid = "V";
                    break;
                default:
                    aminoacid = "";
                    break;
            }
            return aminoacid;
        }
    }
}
