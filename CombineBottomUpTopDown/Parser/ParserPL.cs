﻿/**
 * Program:     ProteoCombiner - Integrating bottom-up & top-down proteomics data
 * Author:      Diogo Borges Lima
 * Created:     6/11/2019
 * Update by:   Diogo Borges Lima
 * Description: Class responsable for reading output file of PatternLab for Proteomics
 */
using CombineBottomUpTopDown.Model;
using PatternTools.SQTParser;
using SEPRPackage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombineBottomUpTopDown.Parser
{
    public class ParserPL
    {
        /// <summary>
        /// Local variable
        /// </summary>
        private ResultPackage SeproResults { get; set; }

        /// <summary>
        /// Public variables
        /// </summary>
        public ulong LastProteinGroupID { get; set; }
        public ulong LastPeptideID { get; set; }
        public ulong LastMSMSID { get; set; }
        public List<ProteinGroup> ProteinGroups { get; set; }
        public List<Peptide> Peptides { get; set; }
        public List<TandemMassSpectrum> TandemMassSpectra { get; set; }

        public ParserPL() { }

        public void ParseFile(String fileName)
        {
            SeproResults = ResultPackage.Load(fileName);

            int protein_processed = 0;
            int old_progress = 0;
            int lengthFile = SeproResults.MyProteins.MyProteinList.Count;

            ulong ProteinGroupID = ++LastProteinGroupID;
            ulong PeptideID = ++LastPeptideID;
            ulong MSMSID = ++LastMSMSID;
            ProteinGroups = new List<ProteinGroup>();
            Peptides = new List<Peptide>();
            TandemMassSpectra = new List<TandemMassSpectrum>();
            foreach (MyProtein protein in SeproResults.MyProteins.MyProteinList)
            {
                ProteinGroup proteinGroup = new ProteinGroup();
                proteinGroup.Sequence = protein.Sequence;
                proteinGroup.ProteinID = protein.Locus;
                proteinGroup.FastaHeader = new List<string>() { ">" + protein.Locus + " " + protein.Description };
                proteinGroup.SequenceLength = (int)protein.Length;
                ProteinGroupParams proteinGroupParams = new ProteinGroupParams();
                proteinGroupParams.SearchEngine = "PatternLab";
                proteinGroupParams.ProteinScore = protein.ProteinScore;
                proteinGroupParams.ProteinGroupID = ProteinGroupID;
                proteinGroupParams.PeptideCounts = new List<ulong>() { (ulong)protein.PeptideResults.Count };
                proteinGroup.MSMSCount = (ulong)protein.SpectrumCount;
                proteinGroup.Params = new List<ProteinGroupParams>() { proteinGroupParams };
                List<ulong> AllMSMSIds = new List<ulong>();

                List<ulong> PeptideIDs = new List<ulong>();
                #region Peptides

                foreach (PeptideResult peptResult in protein.PeptideResults)
                {
                    Peptide peptide = new Peptide();
                    peptide.Sequence = Utils.Util.CleanPeptide(peptResult.CleanedPeptideSequence);
                    peptide.Proteins = new List<string>();
                    peptide.Proteins.AddRange(peptResult.MyMapableProteins);
                    List<SQTScan> sortedTMS = peptResult.MyScans;
                    sortedTMS.Sort((a, b) => b.PrimaryScore.CompareTo(a.PrimaryScore));
                    peptide.Charges = new List<int>() { sortedTMS[0].ChargeState };
                    string cleanPeptide = peptide.Sequence;
                    int PeptOffset = protein.Sequence.IndexOf(cleanPeptide);
                    peptide.StartPosition = PeptOffset + 1;
                    peptide.EndPosition = peptide.StartPosition + cleanPeptide.Length;

                    Peptide comparablePeptide = new Peptide();
                    comparablePeptide.Sequence = peptide.Sequence;
                    comparablePeptide.Proteins = new List<string>();
                    comparablePeptide.Proteins.AddRange(peptResult.MyMapableProteins);

                    Peptides = Peptides.OrderBy(x => x.Sequence.Length).ThenBy(x => x.Proteins.Count).ThenBy(x => x.Proteins[0]).ToList();
                    int _index = Peptides.BinarySearch(comparablePeptide, new PeptideBinarySearchComparer());
                    Peptide existsPeptide = new Peptide();
                    if (_index > -1)
                        existsPeptide = Peptides[_index];

                    if (String.IsNullOrEmpty(existsPeptide.Sequence))
                    {
                        PeptideParams _params = new PeptideParams(sortedTMS[0].PrimaryScore, PeptideID, new List<ulong>() { ProteinGroupID }, new List<ulong>(), new List<ulong>(), MSMSID, (ulong)peptResult.NoMyScans, sortedTMS[0].IsUnique, "PatternLab", peptResult.MyScans[0].MeasuredMH);
                        List<ulong> MSMSIds = new List<ulong>();
                        #region Tandem Mass Spectra

                        foreach (SQTScan sqtScan in sortedTMS)
                        {
                            TandemMassSpectrum tms = new TandemMassSpectrum();
                            tms.MSMSID = MSMSID;
                            tms.RAWfile = sqtScan.FileName.Substring(0, sqtScan.FileName.Length - 4);
                            tms.ScanNumber = sqtScan.ScanNumber;
                            tms.Sequence = Utils.Util.CleanPeptide(sqtScan.PeptideSequenceCleaned);
                            tms.ModifiedSequence = sqtScan.PeptideSequenceCleaned;
                            tms.Modifications = ProcessingSequence(sqtScan.PeptideSequenceCleaned);
                            tms.Proteins = new List<string>();
                            tms.Proteins.AddRange(peptResult.MyMapableProteins);
                            tms.Charge = sqtScan.ChargeState;
                            tms.MZ = (sqtScan.MeasuredMH / tms.Charge) + ((Utils.Util.HYDROGENMASS / tms.Charge) * (tms.Charge - 1.0));
                            tms.Mass = sqtScan.MeasuredMH;
                            tms.PPMError = sqtScan.PPM_Orbitrap;
                            tms.Score = sqtScan.PrimaryScore;
                            tms.MatchedIons = string.Empty;
                            tms.NumberOfMatches = sqtScan.PeaksMatched;
                            tms.ProteinGroupID = new List<ulong>() { ProteinGroupID };
                            tms.PeptideID = PeptideID;
                            tms.Fragmentation = ActivationType.HCD;
                            TandemMassSpectra.Add(tms);
                            MSMSIds.Add(MSMSID);
                            MSMSID++;
                        }

                        #endregion
                        _params.MSMSIds = MSMSIds;
                        peptide.PSMs = new List<PeptideParams>() { _params };
                        PeptideIDs.Add(PeptideID);
                        Peptides.Add(peptide);
                        PeptideID++;
                        AllMSMSIds.AddRange(MSMSIds);
                    }
                    else
                    {
                        if (!existsPeptide.PSMs[0].ProteinGroupIDs.Contains(ProteinGroupID))
                        {
                            existsPeptide.PSMs[0].ProteinGroupIDs.Add(ProteinGroupID);
                            List<ulong> msmsIDs = existsPeptide.PSMs[0].MSMSIds;

                            List<TandemMassSpectrum> storedTMS = TandemMassSpectra.Where(item => msmsIDs.Contains(item.MSMSID)).AsParallel().ToList();
                            storedTMS.ForEach(item =>
                            {
                                item.ProteinGroupID.Add(ProteinGroupID);
                            });
                        }

                        List<ulong> MSMSIds = new List<ulong>();
                        #region Tandem Mass Spectra

                        bool hasNewTMS = false;
                        foreach (SQTScan sqtScan in sortedTMS)
                        {
                            TandemMassSpectrum hasTMS = TandemMassSpectra.Where(item => item.ModifiedSequence.Equals(sqtScan.PeptideSequenceCleaned) &&
                            item.Charge == sqtScan.ChargeState &&
                            item.Mass == sqtScan.MeasuredMH &&
                            item.PPMError == sqtScan.PPM_Orbitrap &&
                            item.Score == sqtScan.PrimaryScore &&
                            item.NumberOfMatches == sqtScan.PeaksMatched).AsParallel().FirstOrDefault();
                            if (hasTMS == null)
                            {
                                hasNewTMS = true;
                                TandemMassSpectrum tms = new TandemMassSpectrum();
                                tms.MSMSID = MSMSID;
                                tms.RAWfile = sqtScan.FileName.Substring(0, sqtScan.FileName.Length - 4);
                                tms.ScanNumber = sqtScan.ScanNumber;
                                tms.Sequence = Utils.Util.CleanPeptide(sqtScan.PeptideSequenceCleaned);
                                tms.ModifiedSequence = sqtScan.PeptideSequenceCleaned;
                                tms.Modifications = ProcessingSequence(sqtScan.PeptideSequenceCleaned);
                                tms.Proteins = existsPeptide.Proteins;
                                tms.Charge = sqtScan.ChargeState;
                                tms.MZ = (sqtScan.MeasuredMH / tms.Charge) + ((Utils.Util.HYDROGENMASS / tms.Charge) * (tms.Charge - 1.0));
                                tms.Mass = sqtScan.MeasuredMH;
                                tms.PPMError = sqtScan.PPM_Orbitrap;
                                tms.Score = sqtScan.PrimaryScore;
                                tms.MatchedIons = string.Empty;
                                tms.NumberOfMatches = sqtScan.PeaksMatched;
                                tms.ProteinGroupID = existsPeptide.PSMs[0].ProteinGroupIDs;
                                tms.PeptideID = existsPeptide.PSMs[0].PeptideID;
                                tms.Fragmentation = ActivationType.HCD;
                                TandemMassSpectra.Add(tms);
                                MSMSIds.Add(MSMSID);
                                MSMSID++;
                            }
                            else
                                break;
                        }
                        if (hasNewTMS)
                        {
                            AllMSMSIds.AddRange(MSMSIds);
                            existsPeptide.PSMs[0].MSMSIds.AddRange(MSMSIds);
                            existsPeptide.PSMs[0].MSMSIds = existsPeptide.PSMs[0].MSMSIds.Distinct().ToList();
                        }
                        #endregion

                        PeptideIDs.Add(existsPeptide.PSMs[0].PeptideID);
                        AllMSMSIds.AddRange(existsPeptide.PSMs[0].MSMSIds);
                    }
                }
                #endregion
                proteinGroup.Params[0].PeptideIds = PeptideIDs.Distinct().ToList();
                proteinGroup.Params[0].MSMSIds = AllMSMSIds.Distinct().ToList();
                ProteinGroups.Add(proteinGroup);
                ProteinGroupID++;

                protein_processed++;
                int new_progress = (int)((double)protein_processed / (lengthFile) * 100);
                if (new_progress > old_progress)
                {
                    old_progress = new_progress;
                    Console.Write(" Reading PatternLab file: " + old_progress + "%");
                }
            }
            ProteinGroups = ProteinGroups.Distinct(new ProteinGroupComparer()).ToList();
            ProteinGroups.Sort((a, b) => b.Params[0].ProteinScore.CompareTo(a.Params[0].ProteinScore));
            Peptides = Peptides.Distinct(new PeptideComparer()).AsParallel().ToList();
            TandemMassSpectra = TandemMassSpectra.Distinct(new TandemMassSpectrumIDSequenceComparer()).AsParallel().ToList();
        }

        private List<Tuple<string, int, string>> ProcessingSequence(string peptideSequence)
        {
            List<Tuple<string, int, string>> ptms = new List<Tuple<string, int, string>>();
            byte[] sequence = null;
            sequence = System.Text.ASCIIEncoding.Default.GetBytes(peptideSequence);
            List<byte> ptm = new List<byte>();

            if (peptideSequence.Contains("("))
            {
                int countPTM = 0;
                int indexStartPTM = -1;
                for (int count = 0; count < sequence.LongLength; count++)
                {
                    //'('
                    if (sequence[count] == 40)
                    {
                        indexStartPTM = count;
                        for (countPTM = count + 1; countPTM < sequence.LongLength; countPTM++)
                        {
                            //close PTM aminoacids with ")" character
                            if (sequence[countPTM] == 41)
                            {
                                break;
                            }
                            else if (sequence[countPTM] == 43)//'+'
                            {
                                continue;
                            }
                            else
                            {
                                ptm.Add(sequence[countPTM]);
                            }
                        }
                    }

                    if (sequence[count] == 41)
                    {
                        string ptmStr = System.Text.ASCIIEncoding.Default.GetString(ptm.ToArray());
                        ptms.Add(Tuple.Create(peptideSequence[indexStartPTM - 1].ToString(), indexStartPTM, peptideSequence[indexStartPTM - 1].ToString() + " (" + ptmStr + ")"));
                        ptm.Clear();
                        indexStartPTM = -1;
                    }
                }
            }
            else if (peptideSequence.Contains("["))
            {
                int countPTM = 0;
                int indexStartPTM = -1;
                for (int count = 0; count < sequence.LongLength; count++)
                {
                    //'['
                    if (sequence[count] == 91)
                    {
                        indexStartPTM = count;
                        for (countPTM = count + 1; countPTM < sequence.LongLength; countPTM++)
                        {
                            //close PTM aminoacids with "]" character
                            if (sequence[countPTM] == 93)
                            {
                                break;
                            }
                            else if (sequence[countPTM] == 43)//'+'
                            {
                                continue;
                            }
                            else
                            {
                                ptm.Add(sequence[countPTM]);
                            }
                        }
                    }

                    if (sequence[count] == 93)
                    {
                        string ptmStr = System.Text.ASCIIEncoding.Default.GetString(ptm.ToArray());
                        ptms.Add(Tuple.Create(peptideSequence[indexStartPTM - 1].ToString(), indexStartPTM, peptideSequence[indexStartPTM - 1].ToString() + " (" + ptmStr + ")"));
                        ptm.Clear();
                        indexStartPTM = -1;
                    }
                }
            }
            return ptms;
        }
    }
}
