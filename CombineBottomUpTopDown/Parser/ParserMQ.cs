﻿/**
 * Program:     ProteoCombiner - Integrating bottom-up & top-down proteomics data
 * Author:      Diogo Borges Lima
 * Created:     4/3/2019
 * Update by:   Diogo Borges Lima
 * Description: Class responsable for reading output file of Max Quant
 */
using CombineBottomUpTopDown.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CombineBottomUpTopDown.Parser
{
    public class ParserMQ
    {
        private int BYTEPERLINE = 1190;
        private string[] HeaderLine { get; set; }

        /// <summary>
        /// Variables of summary.txt file
        /// </summary>
        public int NumberOfExperiments { get; set; }
        public List<string> ModificationsList { get; set; }
        private int TotalModificationsWithoutN_term
        {
            get
            {
                return ModificationsList.Where(item => !item.Contains("N-term")).Count();
            }
        }

        private int TotalModificationsN_term
        {
            get
            {
                return ModificationsList.Where(item => item.Contains("N-term")).Count();
            }
        }

        private bool HasPhospho
        {
            get
            {
                return ModificationsList.Where(item => item.Contains("Phospho")).Count() > 0;
            }
        }

        /// <summary>
        /// Variables of proteingroup.txt file
        /// </summary>
        private ProteinGroup proteinGroup { get; set; }
        private string ProteinID { get; set; }
        private ulong ProteinGroupID { get; set; }
        private string PeptideCounts { get; set; }
        private string FastaHeader { get; set; }
        private int SequenceLength { get; set; }
        private double ProteinScore { get; set; }
        private ulong MSMSCountProteinGroup { get; set; }
        private string PeptideIds { get; set; }
        private string ModIds { get; set; }
        private string MSMSIdsProteinGroup { get; set; }
        private string BestMSMSIdsProteinGroup { get; set; }

        /// <summary>
        /// Variables of peptides.txt file
        /// </summary>
        private Peptide peptideGroup { get; set; }
        private string Sequence { get; set; }
        private double Mass { get; set; }
        private string Proteins { get; set; }
        private int StartPosition { get; set; }
        private int EndPosition { get; set; }
        private string Charge { get; set; }
        private double PeptideScore { get; set; }
        private ulong PeptideID { get; set; }
        private string ProteinGroupIDs { get; set; }
        private string ModPeptIds { get; set; }
        private string MSMSIdsPeptide { get; set; }
        private ulong BestMSMSPeptide { get; set; }
        private ulong MSMSCountPeptide { get; set; }
        private bool IsUnique { get; set; }

        /// <summary>
        /// Variables of msms.txt file
        /// </summary>
        private TandemMassSpectrum TandemMassSpectrum { get; set; }
        private string RAWfile { get; set; }
        private int ScanNumber { get; set; }
        private string SequenceMSMS { get; set; }
        private string ModifiedSequenceMSMS { get; set; }
        private string Modifications { get; set; }
        private string ProteinsMSMS { get; set; }
        private int ChargeMSMS { get; set; }
        private double MZ { get; set; }
        private double MassMSMS { get; set; }
        private double PPMError { get; set; }
        private double RetentionTime { get; set; }
        private double Score { get; set; }
        private string MatchedIons { get; set; }
        private string IntensitiesFragIons { get; set; }
        private string MassesFragIons { get; set; }
        private int NumberOfMatches { get; set; }
        private ulong MSMSID { get; set; }
        private ulong ProteinGroupIDMSMS { get; set; }
        private ulong PeptideIDMSMS { get; set; }

        public void ParseSummaryFile(String fileName)
        {
            NumberOfExperiments = 0;
            ModificationsList = new List<string>();
            StreamReader sr = null;
            long lengthFile = 0;
            try
            {
                sr = new StreamReader(fileName);
                FileInfo fileInfo = new FileInfo(fileName);
                lengthFile = fileInfo.Length / BYTEPERLINE;
            }
            catch (Exception)
            {
                return;
            }
            string line = "";
            int summary_processed = 0;
            int old_progress = 0;

            while ((line = sr.ReadLine()) != null)
            {
                if (line.Length > 0)
                {
                    line = Regex.Replace(line, "\"", "");
                    if (!line.StartsWith("Raw file"))
                    {
                        string[] cols = Regex.Split(line, "\t");
                        if (cols.Length > 0 && !String.IsNullOrEmpty(cols[1]))
                        {
                            NumberOfExperiments++;
                            string[] mod_cols = Regex.Split(cols[7], ";");
                            ModificationsList.AddRange(mod_cols);
                        }
                    }
                    else continue;

                    summary_processed++;
                    int new_progress = (int)((double)summary_processed / (lengthFile) * 100);
                    if (new_progress > old_progress)
                    {
                        old_progress = new_progress;
                        Console.Write(" Reading Summary file: " + old_progress + "%");
                    }
                }
            }
            ModificationsList = ModificationsList.Distinct().ToList();
            Console.Write(" Reading Summary file: 100%");
            sr.Close();
        }

        /// <summary>
        /// Method responsible for processing Max Quant proteing group file
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public List<ProteinGroup> ParseProteinGroupFile(String fileName)
        {
            LinkedList<ProteinGroup> proteinGroupList = new LinkedList<ProteinGroup>();
            StreamReader sr = null;
            long lengthFile = 0;
            try
            {
                sr = new StreamReader(fileName);
                FileInfo fileInfo = new FileInfo(fileName);
                lengthFile = fileInfo.Length / BYTEPERLINE;
            }
            catch (Exception ex)
            {
                Console.WriteLine(" ERROR: " + ex.Message);
                return proteinGroupList.ToList();
            }

            string line = "";
            int proteinGroup_processed = 0;
            int old_progress = 0;

            while ((line = sr.ReadLine()) != null)
            {
                if (line.Length > 0)
                {
                    line = Regex.Replace(line, "\"", "");
                    if (!line.StartsWith("Protein"))
                    {
                        ProcessProteinGroup(line);
                        if (proteinGroup != null)
                        {
                            proteinGroupList.AddLast(proteinGroup);
                            proteinGroup = null;
                        }
                    }
                    else
                        ProcessHeader(line);

                    proteinGroup_processed++;
                    int new_progress = (int)((double)proteinGroup_processed / (lengthFile) * 100);
                    if (new_progress > old_progress)
                    {
                        old_progress = new_progress;
                        Console.Write(" Reading ProteinGroup file: " + old_progress + "%");
                    }
                }
            }
            Console.Write(" Reading ProteinGroup file: 100%");
            sr.Close();
            return proteinGroupList.ToList();
        }

        /// <summary>
        /// Method responsible for processing the header line
        /// </summary>
        /// <param name="row"></param>
        private void ProcessHeader(string row)
        {
            HeaderLine = Regex.Split(row, "\t");
        }

        /// <summary>
        /// Method responsible for processing each line of proteinGroup file
        /// </summary>
        /// <param name="row"></param>
        private void ProcessProteinGroup(string row)
        {
            string[] cols = Regex.Split(row, "\t");
            int index = Array.IndexOf(HeaderLine, "Majority protein IDs");
            if (index == -1) return;
            ProteinID = cols[index];
            index = Array.IndexOf(HeaderLine, "Peptide counts (all)");
            if (index == -1) return;
            PeptideCounts = cols[index];
            index = Array.IndexOf(HeaderLine, "Fasta headers");
            if (index == -1) return;
            FastaHeader = cols[index];
            index = Array.IndexOf(HeaderLine, "Sequence length");
            if (index == -1) return;
            SequenceLength = Convert.ToInt32(cols[index]);
            index = Array.IndexOf(HeaderLine, "Score");
            if (index == -1) return;
            ProteinScore = Convert.ToDouble(cols[index]);
            index = Array.IndexOf(HeaderLine, "MS/MS Count");
            if (index == -1)
            {
                index = Array.IndexOf(HeaderLine, "MS/MS count");
                if (index == -1) return;
            }
            MSMSCountProteinGroup = Convert.ToUInt64(cols[index]);
            index = Array.IndexOf(HeaderLine, "id");
            if (index == -1) return;
            ProteinGroupID = Convert.ToUInt64(cols[index]);
            index = Array.IndexOf(HeaderLine, "Peptide IDs");
            if (index == -1) return;
            PeptideIds = cols[index];
            index = Array.IndexOf(HeaderLine, "Mod. peptide IDs");
            if (index == -1) return;
            ModIds = cols[index];
            index = Array.IndexOf(HeaderLine, "MS/MS IDs");
            if (index == -1) return;
            MSMSIdsProteinGroup = cols[index];
            index = Array.IndexOf(HeaderLine, "Best MS/MS");
            if (index == -1) return;
            BestMSMSIdsProteinGroup = cols[index];

            proteinGroup = new ProteinGroup(ProteinID,
                                            ProteinGroupID,
                                            PeptideCounts,
                                            FastaHeader,
                                            SequenceLength,
                                            ProteinScore,
                                            MSMSCountProteinGroup,
                                            PeptideIds,
                                            ModIds,
                                            MSMSIdsProteinGroup,
                                            BestMSMSIdsProteinGroup,
                                            "Andromeda",
                                            string.Empty);

        }
        public List<Peptide> ParsePeptideFile(String fileName)
        {
            LinkedList<Peptide> peptideList = new LinkedList<Peptide>();
            StreamReader sr = null;
            long lengthFile = 0;
            try
            {
                sr = new StreamReader(fileName);
                FileInfo fileInfo = new FileInfo(fileName);
                lengthFile = fileInfo.Length / BYTEPERLINE;
            }
            catch (Exception ex)
            {
                Console.WriteLine(" ERROR: " + ex.Message);
                return peptideList.ToList();
            }
            string line = "";
            int peptide_processed = 0;
            int old_progress = 0;

            while ((line = sr.ReadLine()) != null)
            {
                if (line.Length > 0)
                {
                    line = Regex.Replace(line, "\"", "");
                    if (!line.StartsWith("Sequence"))
                    {
                        ProcessPeptideGroup(line);
                        if (peptideGroup != null)
                        {
                            peptideList.AddLast(peptideGroup);
                            peptideGroup = null;
                        }
                    }
                    else
                        ProcessHeader(line);

                    peptide_processed++;
                    int new_progress = (int)((double)peptide_processed / (lengthFile) * 100);
                    if (new_progress > old_progress)
                    {
                        old_progress = new_progress;
                        Console.Write(" Reading Peptide file: " + old_progress + "%");
                    }
                }
            }
            Console.Write(" Reading Peptide file: 100%");
            sr.Close();
            return peptideList.ToList();
        }

        /// <summary>
        /// Method responsible for processing each line of peptide file
        /// </summary>
        /// <param name="row"></param>
        private void ProcessPeptideGroup(string row)
        {
            string[] cols = Regex.Split(row, "\t");
            int index = Array.IndexOf(HeaderLine, "Sequence");
            if (index == -1) return;
            Sequence = cols[index];
            index = Array.IndexOf(HeaderLine, "Mass");
            if (index == -1) return;
            Mass = Convert.ToDouble(cols[index]);
            index = Array.IndexOf(HeaderLine, "Proteins");
            if (index == -1) return;
            Proteins = cols[index];
            index = Array.IndexOf(HeaderLine, "Start position");
            if (index == -1) return;
            StartPosition = !String.IsNullOrEmpty(cols[index]) ? Convert.ToInt32(cols[index]) : 0;
            index = Array.IndexOf(HeaderLine, "End position");
            if (index == -1) return;
            EndPosition = !String.IsNullOrEmpty(cols[index]) ? Convert.ToInt32(cols[index]) : 0;
            index = Array.IndexOf(HeaderLine, "Charges");
            if (index == -1) return;
            if (!String.IsNullOrEmpty(cols[index]))
                Charge = cols[index];
            else
                Charge = "0";
            index = Array.IndexOf(HeaderLine, "Score");
            if (index == -1) return;
            if (!String.IsNullOrEmpty(cols[index]) && !cols[index].Equals("NaN"))
                PeptideScore = Convert.ToDouble(cols[index]);
            else
                PeptideScore = 0;
            index = Array.IndexOf(HeaderLine, "id");
            if (index == -1) return;
            PeptideID = Convert.ToUInt64(cols[index]);
            index = Array.IndexOf(HeaderLine, "Protein group IDs");
            if (index == -1) return;
            ProteinGroupIDs = cols[index];
            index = Array.IndexOf(HeaderLine, "Mod. peptide IDs");
            if (index == -1) return;
            if (!String.IsNullOrEmpty(cols[index]))
                ModPeptIds = cols[index];
            else
                ModPeptIds = "0";
            index = Array.IndexOf(HeaderLine, "MS/MS IDs");
            if (index == -1) return;
            if (!String.IsNullOrEmpty(cols[index]))
                MSMSIdsPeptide = cols[index];
            else
                MSMSIdsPeptide = "0";
            index = Array.IndexOf(HeaderLine, "Best MS/MS");
            if (index == -1) return;
            if (!String.IsNullOrEmpty(cols[index]))
                BestMSMSPeptide = Convert.ToUInt64(cols[index]);
            else
                BestMSMSPeptide = 0;

            index = Array.IndexOf(HeaderLine, "MS/MS Count");
            if (index == -1)
            {
                index = Array.IndexOf(HeaderLine, "MS/MS count");
                if (index == -1) return;
            }
            if (!String.IsNullOrEmpty(cols[index]))
                MSMSCountPeptide = Convert.ToUInt64(cols[index]);
            else
                MSMSCountPeptide = 0;

            index = Array.IndexOf(HeaderLine, "Unique (Proteins)");
            if (index == -1) return;
            IsUnique = cols[index].Equals("yes");

            peptideGroup = new Peptide(Sequence,
            Mass,
            Proteins,
            StartPosition,
            EndPosition,
            Charge,
            PeptideScore,
            PeptideID,
            ProteinGroupIDs,
            ModPeptIds,
            MSMSIdsPeptide,
            BestMSMSPeptide,
            MSMSCountPeptide,
            IsUnique,
            "Andromeda"
                );

        }

        /// <summary>
        /// Method responsible for processing ms ms file
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public List<TandemMassSpectrum> ParseMSMSFile(String fileName)
        {
            LinkedList<TandemMassSpectrum> msmsList = new LinkedList<TandemMassSpectrum>();
            StreamReader sr = null;
            long lengthFile = 0;
            try
            {
                sr = new StreamReader(fileName);
                FileInfo fileInfo = new FileInfo(fileName);
                lengthFile = fileInfo.Length / BYTEPERLINE;
            }
            catch (Exception ex)
            {
                Console.WriteLine(" ERROR: " + ex.Message);
                return msmsList.ToList();
            }
            string line = "";
            int peptide_processed = 0;
            int old_progress = 0;

            try
            {
                while ((line = sr.ReadLine()) != null)
                {
                    if (line.Length > 0)
                    {
                        line = Regex.Replace(line, "\"", "");
                        if (!line.StartsWith("Raw file"))
                        {
                            ProcessTandemMassSpectra(line);
                            if (TandemMassSpectrum != null)
                            {
                                msmsList.AddLast(TandemMassSpectrum);
                                TandemMassSpectrum = null;
                            }
                        }
                        else
                            ProcessHeader(line);

                        peptide_processed++;
                        int new_progress = (int)((double)peptide_processed / (lengthFile) * 100);
                        if (new_progress > old_progress)
                        {
                            old_progress = new_progress;
                            Console.Write(" Reading MSMS file: " + old_progress + "%");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(" ERROR: MSMS file.");
            }
            Console.Write(" Reading MSMS file: 100%");
            sr.Close();
            return msmsList.ToList();
        }

        /// <summary>
        /// Method responsible for processing each line of msms file
        /// </summary>
        /// <param name="row"></param>
        private void ProcessTandemMassSpectra(string row)
        {
            string[] cols = Regex.Split(row, "\t");
            int index = Array.IndexOf(HeaderLine, "Raw file");
            if (index == -1) return;
            RAWfile = cols[index];
            index = Array.IndexOf(HeaderLine, "Scan number");
            if (index == -1) return;
            ScanNumber = Convert.ToInt32(cols[index]);
            index = Array.IndexOf(HeaderLine, "Sequence");
            if (index == -1) return;
            SequenceMSMS = cols[index];
            index = Array.IndexOf(HeaderLine, "Modified sequence");
            if (index == -1) return;
            ModifiedSequenceMSMS = cols[index];
            index = Array.IndexOf(HeaderLine, "Modifications");
            if (index == -1) return;
            Modifications = cols[index];
            index = Array.IndexOf(HeaderLine, "Proteins");
            if (index == -1) return;
            ProteinsMSMS = cols[index];
            index = Array.IndexOf(HeaderLine, "Charge");
            if (index == -1) return;
            ChargeMSMS = Convert.ToInt32(cols[index]);
            index = Array.IndexOf(HeaderLine, "m/z");
            if (index == -1) return;
            MZ = Convert.ToDouble(cols[index]);
            index = Array.IndexOf(HeaderLine, "Mass");
            if (index == -1) return;
            MassMSMS = Convert.ToDouble(cols[index]);
            index = Array.IndexOf(HeaderLine, "Mass Error [ppm]");
            if (index == -1)
            {
                index = Array.IndexOf(HeaderLine, "Mass error [ppm]");
                if (index == -1)
                {
                    return;
                }
            }
            PPMError = Convert.ToDouble(cols[index]);
            index = Array.IndexOf(HeaderLine, "Retention time");
            if (index == -1) return;
            RetentionTime = Convert.ToDouble(cols[index]);
            index = Array.IndexOf(HeaderLine, "Score");
            if (index == -1) return;
            Score = Convert.ToDouble(cols[index]);
            index = Array.IndexOf(HeaderLine, "Matches");
            if (index == -1) return;
            MatchedIons = cols[index];
            index = Array.IndexOf(HeaderLine, "Intensities");
            if (index == -1) return;
            IntensitiesFragIons = cols[index];
            index = Array.IndexOf(HeaderLine, "Masses");
            if (index == -1) return;
            MassesFragIons = cols[index];
            index = Array.IndexOf(HeaderLine, "Number of Matches");
            if (index == -1)
            {
                index = Array.IndexOf(HeaderLine, "Number of matches");
                if (index == -1)
                {
                    return;
                }
            }
            NumberOfMatches = Convert.ToInt32(cols[index]);
            index = Array.IndexOf(HeaderLine, "id");
            if (index == -1) return;
            MSMSID = Convert.ToUInt64(cols[index]);
            index = Array.IndexOf(HeaderLine, "Protein group IDs");
            if (index == -1) return;
            if (!cols[index].Contains(";"))
                ProteinGroupIDMSMS = Convert.ToUInt64(cols[index]);
            else
                ProteinGroupIDMSMS = Convert.ToUInt64(Regex.Split(cols[index], ";")[0]);
            index = Array.IndexOf(HeaderLine, "Peptide ID");
            if (index == -1) return;
            PeptideIDMSMS = Convert.ToUInt64(cols[index]);

            TandemMassSpectrum = new TandemMassSpectrum(RAWfile,
                                                ScanNumber,
                                                SequenceMSMS,
                                                ModifiedSequenceMSMS,
                                                Modifications,
                                                new List<string>() { ProteinsMSMS },
                                                ChargeMSMS,
                                                MZ,
                                                MassMSMS,
                                                PPMError,
                                                RetentionTime,
                                                Score,
                                                MatchedIons,
                                                IntensitiesFragIons,
                                                MassesFragIons,
                                                NumberOfMatches,
                                                MSMSID,
                                                new List<ulong>() { ProteinGroupIDMSMS },
                                                PeptideIDMSMS);

            index = Array.IndexOf(HeaderLine, "Fragmentation");
            if (index == -1) return;
            switch (cols[index])
            {
                case "EThcD":
                    TandemMassSpectrum.Fragmentation = ActivationType.EThCD;
                    break;
                case "CID":
                    TandemMassSpectrum.Fragmentation = ActivationType.CID;
                    break;
                case "ETD":
                    TandemMassSpectrum.Fragmentation = ActivationType.ETD;
                    break;
                case "HCD":
                    TandemMassSpectrum.Fragmentation = ActivationType.HCD;
                    break;
                case "ECD":
                    TandemMassSpectrum.Fragmentation = ActivationType.ECD;
                    break;
                case "MPD":
                    TandemMassSpectrum.Fragmentation = ActivationType.MPD;
                    break;
                case "PQD":
                    TandemMassSpectrum.Fragmentation = ActivationType.PQD;
                    break;
                default:
                    TandemMassSpectrum.Fragmentation = ActivationType.NF;
                    break;
            }

        }

        /// <summary>
        /// Method responsible for reading fasta file/format
        /// </summary>
        /// <param name="sequenceDBPath"></param>
        /// <param name="isFile"></param>
        /// <returns></returns>
        public List<FastaItem> ParseFastaFile(string sequenceDBPath, bool isFile = true)
        {
            ParserFastaFile fastaParser = new ParserFastaFile();
            if (isFile)
                fastaParser.ParseFile(
                    new System.IO.StreamReader(File.OpenRead(sequenceDBPath)),
                    false,
                    DBTypes.IDSpaceDescription
                    );
            else
            {
                string[] cols = Regex.Split(sequenceDBPath, "[\r\n]");
                List<string> dbLines = new List<string>(cols);
                dbLines.RemoveAll(item => String.IsNullOrEmpty(item));
                fastaParser.ParseFile(
                dbLines,
                false,
                DBTypes.IDSpaceDescription
                );
            }
            fastaParser.IncludeByteSequences();
            return fastaParser.MyItems;
        }
    }
}