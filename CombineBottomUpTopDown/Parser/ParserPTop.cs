﻿/**
 * Program:     ComparisonTDPSearchEngines
 * Author:      Diogo Borges Lima and Paulo Costa Carvalho
 * Created:     3/24/2016
 * Update by:   Diogo Borges Lima
 * Description: Class responsible for reading pTOP output files.
 */
using CombineBottomUpTopDown.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CombineBottomUpTopDown.Parser
{
    public class ParserPTop
    {

        /// <summary>
        /// Private variables
        /// </summary>
        private int ID { get; set; }
        private int ChargeState { get; set; }
        private double PrecursorMass { get; set; }
        private double TheoreticalMass { get; set; }
        private double massDifPPM { get; set; }
        private double massDifDa { get; set; }
        private int NumberOfPeaksMatched { get; set; }
        private double LogPValue { get; set; }
        private List<string> ProteinID { get; set; }
        private string CompleteProteinID { get; set; }
        private string Sequence { get; set; }
        private double LogEvalue { get; set; }
        private int ScanNumber { get; set; }
        private string SpectrumFile { get; set; }
        private string PTMs { get; set; }

        public List<Proteoform> ProteoformList = new List<Proteoform>();

        public ParserPTop() { }

        /// <summary>
        /// Method responsible for parsing pTop output file
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns>Return a list with all identified tags according to defined parameters</returns>
        public void ParseFile(string fileName)
        {
            StreamReader sr = new StreamReader(fileName);
            string line = "";


            while ((line = sr.ReadLine()) != null)
            {
                if (String.IsNullOrEmpty(line) || line.StartsWith("ID")) continue;

                Proteoform currentProteoform = ProcessLine(line);
                currentProteoform.PrSMs[0].SearchEngine = "pTop";

                List<Proteoform> existsProtein = (from ptn in ProteoformList
                                                  where ptn.AnnotatedSequence.Equals(currentProteoform.AnnotatedSequence)
                                                  && ptn.PrSMs[0].LogPScore == currentProteoform.PrSMs[0].LogPScore
                                                  select ptn).ToList();

                if (existsProtein.Count > 0)
                {
                    if (currentProteoform.PrSMs[0].LogPScore > existsProtein[0].PrSMs[0].LogPScore)
                    {
                        ProteoformList.Remove(existsProtein[0]);
                        ProteoformList.Add(currentProteoform);
                    }
                }
                else
                {
                    ProteoformList.Add(currentProteoform);
                }
            }
            sr.Close();

            //ProteoformList.Sort(delegate (SpectrumPtop c1, SpectrumPtop c2) { return c2.Score.CompareTo(c1.Score); });
        }

        private Proteoform ProcessLine(string line)
        {
            string[] cols = Regex.Split(line, ",");

            ID = Convert.ToInt32(cols[0]);
            SpectrumFile = cols[1];

            string[] title = Regex.Split(cols[1], "\\.");
            ScanNumber = Convert.ToInt32(title[1]);

            ChargeState = Convert.ToInt32(cols[2]);
            PrecursorMass = Convert.ToDouble(cols[3]);
            TheoreticalMass = Convert.ToDouble(cols[4]);
            massDifDa = Convert.ToDouble(cols[5]);
            massDifPPM = Convert.ToDouble(cols[6]);
            NumberOfPeaksMatched = Convert.ToInt32(cols[7]);
            LogPValue = Convert.ToDouble(cols[8]);
            if (cols[9].Contains("|"))
            {
                string[] tmpPtnID = Regex.Split(cols[9], "\\|");
                ProteinID = new List<string>() { tmpPtnID[1] };
            }
            else if (cols[9].Contains("_"))
            {
                string[] tmpPtnID = Regex.Split(cols[9], "_");
                ProteinID = new List<string>(tmpPtnID);
                ProteinID.RemoveAll(item => String.IsNullOrEmpty(item));
            }
            else
            {
                ProteinID = new List<string>() { cols[9] };
            }
            CompleteProteinID = cols[9];
            Sequence = cols[10];
            PTMs = cols[11];
            LogEvalue = Math.Log(Convert.ToDouble(cols[12])) * -1;


            ProteoformParams proteoformParams = new ProteoformParams(
                //Sequence,
                //String.Join("|", ProteinID),
                ChargeState,
                TheoreticalMass,
                massDifPPM,
                massDifDa,
                NumberOfPeaksMatched,
                ScanNumber,
                SpectrumFile,
                LogPValue,
                LogEvalue,
                PTMs);
            Proteoform proteoform = new Proteoform(Sequence, String.Join("|", ProteinID),string.Empty, Sequence, 0, new List<ProteoformParams>() { proteoformParams});

            return proteoform;
        }

        private SpectrumPtop Process(string line)
        {
            string[] cols = Regex.Split(line, ",");

            ID = Convert.ToInt32(cols[0]);
            SpectrumFile = cols[1];
            ChargeState = Convert.ToInt32(cols[2]);
            PrecursorMass = Convert.ToDouble(cols[3]);
            TheoreticalMass = Convert.ToDouble(cols[4]);
            massDifPPM = Convert.ToDouble(cols[6]);
            NumberOfPeaksMatched = Convert.ToInt32(cols[7]);
            LogPValue = Convert.ToDouble(cols[8]);
            if (cols[9].Contains("|"))
            {
                string[] tmpPtnID = Regex.Split(cols[9], "\\|");
                ProteinID = new List<string>() { tmpPtnID[1] };
            }
            else if (cols[9].Contains("_"))
            {
                string[] tmpPtnID = Regex.Split(cols[9], "_");
                ProteinID = new List<string>(tmpPtnID);
                ProteinID.RemoveAll(item => String.IsNullOrEmpty(item));
            }
            else
            {
                ProteinID = new List<string>() { cols[9] };
            }
            CompleteProteinID = cols[9];
            Sequence = cols[10];
            LogEvalue = Math.Log(Convert.ToDouble(cols[12])) * -1;
            string[] title = Regex.Split(cols[1], "\\.");
            ScanNumber = Convert.ToInt32(title[1]);

            SpectrumPtop protein = new SpectrumPtop(
                            ID,
                            ChargeState,
                            PrecursorMass,
                            TheoreticalMass,
                            massDifPPM,
                            NumberOfPeaksMatched,
                            LogPValue,
                            ProteinID,
                            CompleteProteinID,
                            Sequence,
                            LogEvalue,
                            ScanNumber,
                            SpectrumFile
                );

            return protein;
        }
    }

    public class SpectrumPtop
    {
        /// <summary>
        /// Private variables
        /// </summary>
        public int ID { get; set; }
        public int ChargeState { get; set; }
        public double PrecursorMass { get; set; }
        public double TheoreticalMass { get; set; }
        public double massDifPPM { get; set; }
        public int NumberOfPeaksMatched { get; set; }
        public double Score { get; set; }
        public List<string> ProteinID { get; set; }
        public string CompleteProteinID { get; set; }
        public string Sequence { get; set; }
        public double Evalue { get; set; }
        public int ScanNumber { get; set; }
        public string SpectrumFile { get; set; }

        public SpectrumPtop(int ID,
                            int ChargeState,
                            double PrecursorMass,
                            double TheoreticalMass,
                            double massDifPPM,
                            int NumberOfPeaksMatched,
                            double Score,
                            List<string> ProteinID,
                            string CompleteProteinID,
                            string Sequence,
                            double Evalue,
                            int ScanNumber,
                            string SpectrumFile)
        {
            this.ID = ID;
            this.ChargeState = ChargeState;
            this.PrecursorMass = PrecursorMass;
            this.TheoreticalMass = TheoreticalMass;
            this.massDifPPM = massDifPPM;
            this.NumberOfPeaksMatched = NumberOfPeaksMatched;
            this.Score = Score;
            this.ProteinID = ProteinID;
            this.CompleteProteinID = CompleteProteinID;
            this.Sequence = Sequence;
            this.Evalue = Evalue;
            this.ScanNumber = ScanNumber;
            this.SpectrumFile = SpectrumFile;
        }
    }
}
