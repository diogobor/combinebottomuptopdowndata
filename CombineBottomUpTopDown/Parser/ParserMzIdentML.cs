﻿/**
* Program:     ProteoCombiner - Integrating bottom-up & top-down proteomics data
* Author:      Diogo Borges Lima
* Created:     5/18/2020
* Update by:   Diogo Borges Lima
* Description: Parser for exporting and loading mzIdentML format file
*/
using CombineBottomUpTopDown.Model;
using EhuBio.Proteomics.Hupo.mzIdentML;
using EhuBio.Proteomics.Hupo.mzIdentML1_2;
using EhuBio.Proteomics.Inference;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CombineBottomUpTopDown.Parser
{
    public class ParserMzIdentML : Mapper
    {
        /// <summary>
        /// Private variables
        /// </summary>
        private mzidFile1_2 MzidML1_2;
        private mzidFile1_1 MzidML1_1;

        //Variables for saving mzIdML
        private struct PeptideMzIdML1_2
        {
            public PeptideType PeptideType;
            public List<string> Proteins;
        }

        private struct PeptideMzIdML1_1
        {
            public EhuBio.Proteomics.Hupo.mzIdentML1_1.PeptideType PeptideType;
            public List<string> Proteins;
        }

        private List<DBSequenceType> ProteinList1_2;
        private List<PeptideMzIdML1_2> PeptideEvidenceList1_2;

        private List<EhuBio.Proteomics.Hupo.mzIdentML1_1.DBSequenceType> ProteinList1_1;
        private List<PeptideMzIdML1_1> PeptideEvidenceList1_1;

        public string SoftwareName { get; set; }

        //Variables for loading mzIdML
        private List<PeptideType> PeptideList1_2;
        private List<SpectrumIdentificationResultType> SpectrumList1_2;

        private List<EhuBio.Proteomics.Hupo.mzIdentML1_1.PeptideType> PeptideList1_1;
        private List<EhuBio.Proteomics.Hupo.mzIdentML1_1.SpectrumIdentificationResultType> SpectrumList1_1;

        private Regex numberCaptured = new Regex("[0-9|\\.]+", RegexOptions.Compiled);

        private static Mapper.Software sw;

        /// <summary>
        /// Public variables
        /// </summary>
        public static int progress_reading_mzIdentML = 0;

        public List<ProteinGroup> ProteinGroups { get; set; }
        public List<CombineBottomUpTopDown.Model.Peptide> Peptides { get; set; }
        public List<TandemMassSpectrum> TandemMassSpectra { get; set; }
        public ulong LastProteinGroupID { get; set; }
        public ulong LastPeptideID { get; set; }
        public ulong LastMSMSID { get; set; }

        public List<FastaItem> FastaItems { get; set; }

        private bool isVersion1_2 { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public ParserMzIdentML()
            : base(sw)
        {
            m_Type = Mapper.SourceType.mzIdentML110;

            #region version 1.2
            MzidML1_2 = new mzidFile1_2();
            MzidML1_2.Data = new MzIdentMLType();

            #region cv List
            List<cvType> cvList = new List<cvType>();
            cvType cv = new cvType();
            cv.id = "PSI-MS";
            cv.uri = "https://raw.githubusercontent.com/HUPO-PSI/psi-ms-CV/master/psi-ms.obo";
            cv.version = "4.0.11";
            cv.fullName = "Mass Spectrometry CV (PSI-MS)";
            cvList.Add(cv);

            cvType cv2 = new cvType();
            cv2.id = "UNIMOD";
            cv2.uri = "http://www.unimod.org/obo/unimod.obo";
            cv2.fullName = "UNIMOD";
            cvList.Add(cv2);

            cvType cv3 = new cvType();
            cv3.id = "UO";
            cv3.uri = "https://raw.githubusercontent.com/bio-ontology-research-group/unit-ontology/master/unit.obo";
            cv3.fullName = "UNIT-ONTOLOGY";
            cvList.Add(cv3);
            MzidML1_2.Data.cvList = cvList.ToArray();
            #endregion

            MzidML1_2.Data.version = "1.2.0";

            MzidML1_2.ListEvidences = new List<PeptideEvidenceType>();

            ProteinList1_2 = new List<DBSequenceType>();
            MzidML1_2.ListProteins = ProteinList1_2;

            PeptideEvidenceList1_2 = new List<PeptideMzIdML1_2>();
            MzidML1_2.ListPeptides = new List<PeptideType>();

            PeptideList1_2 = new List<PeptideType>();
            SpectrumList1_2 = new List<SpectrumIdentificationResultType>();
            #endregion

            #region version 1.1
            MzidML1_1 = new mzidFile1_1();
            MzidML1_1.Data = new EhuBio.Proteomics.Hupo.mzIdentML1_1.MzIdentMLType();

            #region cv List
            List<EhuBio.Proteomics.Hupo.mzIdentML1_1.cvType> cvList1_1 = new List<EhuBio.Proteomics.Hupo.mzIdentML1_1.cvType>();
            EhuBio.Proteomics.Hupo.mzIdentML1_1.cvType cv1_1 = new EhuBio.Proteomics.Hupo.mzIdentML1_1.cvType();
            cv1_1.id = "PSI-MS";
            cv1_1.uri = "https://raw.githubusercontent.com/HUPO-PSI/psi-ms-CV/master/psi-ms.obo";
            cv1_1.version = "4.0.11";
            cv1_1.fullName = "Mass Spectrometry CV (PSI-MS)";
            cvList1_1.Add(cv1_1);

            EhuBio.Proteomics.Hupo.mzIdentML1_1.cvType cv2_1_1 = new EhuBio.Proteomics.Hupo.mzIdentML1_1.cvType();
            cv2_1_1.id = "UNIMOD";
            cv2_1_1.uri = "http://www.unimod.org/obo/unimod.obo";
            cv2_1_1.fullName = "UNIMOD";
            cvList1_1.Add(cv2_1_1);

            EhuBio.Proteomics.Hupo.mzIdentML1_1.cvType cv3_1_1 = new EhuBio.Proteomics.Hupo.mzIdentML1_1.cvType();
            cv3_1_1.id = "UO";
            cv3_1_1.uri = "https://raw.githubusercontent.com/bio-ontology-research-group/unit-ontology/master/unit.obo";
            cv3_1_1.fullName = "UNIT-ONTOLOGY";
            cvList1_1.Add(cv3_1_1);
            MzidML1_1.Data.cvList = cvList1_1.ToArray();
            #endregion

            MzidML1_1.Data.version = "1.1.0";

            MzidML1_1.ListEvidences = new List<EhuBio.Proteomics.Hupo.mzIdentML1_1.PeptideEvidenceType>();

            ProteinList1_1 = new List<EhuBio.Proteomics.Hupo.mzIdentML1_1.DBSequenceType>();
            MzidML1_1.ListProteins = ProteinList1_1;

            PeptideEvidenceList1_1 = new List<PeptideMzIdML1_1>();
            MzidML1_1.ListPeptides = new List<EhuBio.Proteomics.Hupo.mzIdentML1_1.PeptideType>();

            PeptideList1_1 = new List<EhuBio.Proteomics.Hupo.mzIdentML1_1.PeptideType>();
            SpectrumList1_1 = new List<EhuBio.Proteomics.Hupo.mzIdentML1_1.SpectrumIdentificationResultType>();
            #endregion
        }

        /// <summary>
        /// Gets the name of the parser.
        /// </summary>
        /// <value>
        /// The name of the parser.
        /// </value>
        public override string ParserName
        {
            get
            {
                return "PSI-PI mzIdentML (v1.2.0)";
            }
        }

        /// <summary>
        /// Method responsible for loading mzIdentML file
        /// </summary>
        /// <param name="mzid"></param>
        protected override void Load(string mzid)
        {
            try
            {
                MzidML1_2 = new mzidFile1_2();
                MzidML1_2.Load(mzid);
                ProteinList1_2 = MzidML1_2.ListProteins;
                PeptideList1_2 = MzidML1_2.ListPeptides;
                if (MzidML1_2.Data.DataCollection.AnalysisData.SpectrumIdentificationList.Length > 0)
                    SpectrumList1_2 = new List<SpectrumIdentificationResultType>(MzidML1_2.Data.DataCollection.AnalysisData.SpectrumIdentificationList[0].SpectrumIdentificationResult);

                PeptideEvidenceList1_2 = GetPeptidesFromMzIdentFile1_2(PeptideList1_2, MzidML1_2.ListEvidences);
                SoftwareName = MzidML1_2.ListSW[0].name;
                if (MzidML1_2.ListSW[0].id.Contains("Proteome"))//PD saves its name in ID
                    SoftwareName = MzidML1_2.ListSW[0].id;

                isVersion1_2 = true;

            }
            catch (Exception)
            {
                MzidML1_1 = new mzidFile1_1();
                MzidML1_1.Load(mzid);
                ProteinList1_1 = MzidML1_1.ListProteins;
                PeptideList1_1 = MzidML1_1.ListPeptides;
                if (MzidML1_1.Data.DataCollection.AnalysisData.SpectrumIdentificationList.Length > 0)
                    SpectrumList1_1 = new List<EhuBio.Proteomics.Hupo.mzIdentML1_1.SpectrumIdentificationResultType>(MzidML1_1.Data.DataCollection.AnalysisData.SpectrumIdentificationList[0].SpectrumIdentificationResult);

                PeptideEvidenceList1_1 = GetPeptidesFromMzIdentFile1_1(PeptideList1_1, MzidML1_1.ListEvidences);
                SoftwareName = MzidML1_1.ListSW[0].name;
                if (MzidML1_1.ListSW[0].id.Contains("Proteome"))//PD saves its name in ID
                    SoftwareName = MzidML1_1.ListSW[0].id;

                isVersion1_2 = false;
            }
        }

        /// <summary>
        /// Method responsible for getting all peptides in mzIdentML and associate them with their protein list
        /// </summary>
        /// <param name="PeptideList"></param>
        /// <param name="ProteinList"></param>
        /// <param name="PeptEvidenceList"></param>
        /// <returns></returns>
        private List<PeptideMzIdML1_2> GetPeptidesFromMzIdentFile1_2(List<PeptideType> PeptideList, List<PeptideEvidenceType> PeptEvidenceList)
        {
            List<PeptideMzIdML1_2> returnList = new List<PeptideMzIdML1_2>();
            foreach (PeptideType pept in PeptideList)
            {
                PeptideMzIdML1_2 peptMzIdML = new PeptideMzIdML1_2();
                peptMzIdML.PeptideType = pept;

                #region get protein
                List<PeptideEvidenceType> peptEvidenceListTMP = (from peptEvidence in PeptEvidenceList.AsParallel()
                                                                 where peptEvidence.peptide_ref.Equals(pept.id)
                                                                 select peptEvidence).ToList();
                if (peptEvidenceListTMP.Count == 0)
                {
                    throw new Exception(" It was not found the peptide sequence with reference = " + pept.id);
                }
                peptMzIdML.Proteins = new List<string>() { peptEvidenceListTMP[0].dBSequence_ref };
                #endregion

                returnList.Add(peptMzIdML);
            }

            return returnList;
        }

        /// <summary>
        /// Method responsible for getting all peptides in mzIdentML and associate them with their protein list
        /// </summary>
        /// <param name="PeptideList"></param>
        /// <param name="ProteinList"></param>
        /// <param name="PeptEvidenceList"></param>
        /// <returns></returns>
        private List<PeptideMzIdML1_1> GetPeptidesFromMzIdentFile1_1(List<EhuBio.Proteomics.Hupo.mzIdentML1_1.PeptideType> PeptideList, List<EhuBio.Proteomics.Hupo.mzIdentML1_1.PeptideEvidenceType> PeptEvidenceList)
        {
            List<PeptideMzIdML1_1> returnList = new List<PeptideMzIdML1_1>();
            foreach (EhuBio.Proteomics.Hupo.mzIdentML1_1.PeptideType pept in PeptideList)
            {
                PeptideMzIdML1_1 peptMzIdML = new PeptideMzIdML1_1();
                peptMzIdML.PeptideType = pept;

                #region get protein
                List<EhuBio.Proteomics.Hupo.mzIdentML1_1.PeptideEvidenceType> peptEvidenceListTMP = (from peptEvidence in PeptEvidenceList.AsParallel()
                                                                                                     where peptEvidence.peptide_ref.Equals(pept.id)
                                                                                                     select peptEvidence).ToList();
                if (peptEvidenceListTMP.Count == 0)
                {
                    throw new Exception(" It was not found the peptide sequence with reference = " + pept.id);
                }
                peptMzIdML.Proteins = new List<string>() { peptEvidenceListTMP[0].dBSequence_ref };
                #endregion

                returnList.Add(peptMzIdML);
            }

            return returnList;
        }

        /// <summary>
        /// Method resposible for deserializing mzIdentML file
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="dataFile"></param>
        /// <returns></returns>
        public void ParseFiles(string fileName, string[] dataFile = null)
        {
            //Load mzIdentFile
            Console.WriteLine(" Reading mzIdentML file: " + fileName);

            if (FastaItems == null)
                FastaItems = new List<FastaItem>(0);

            Load(fileName, false);

            ProteinGroups = new List<ProteinGroup>();
            Peptides = new List<CombineBottomUpTopDown.Model.Peptide>();
            TandemMassSpectra = new List<TandemMassSpectrum>();

            ulong ProteinGroupID = ++LastProteinGroupID;
            ulong PeptideID = ++LastPeptideID;
            ulong MSMSID = ++LastMSMSID;

            if (isVersion1_2)
            {
                #region Proteins version 1.2

                //Get all SpectraDataType -> all SearchQueries.SourceFile
                List<SpectraDataType> spectraDataTypeAllList1_2 = new List<SpectraDataType>(MzidML1_2.Data.DataCollection.Inputs.SpectraData);
                try
                {


                    foreach (DBSequenceType mzIdPtn in ProteinList1_2)
                    {
                        ProteinGroup proteinGroup = new ProteinGroup();
                        proteinGroup.ProteinID = mzIdPtn.accession;

                        FastaItem fasta = FastaItems.Where(a => a.SequenceIdentifier.Contains(mzIdPtn.accession)).FirstOrDefault();
                        if (fasta != null)
                        {
                            proteinGroup.Sequence = fasta.Sequence;
                            proteinGroup.SequenceLength = fasta.Sequence.Length;
                            proteinGroup.FastaHeader = new List<string>() { ">" + fasta.SequenceIdentifier + " " + fasta.Description };
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(mzIdPtn.Seq))
                            {
                                proteinGroup.Sequence = mzIdPtn.Seq;
                                proteinGroup.SequenceLength = mzIdPtn.Seq.Length;
                                proteinGroup.FastaHeader = new List<string>() { ">" + mzIdPtn.accession + " " + mzIdPtn.name };
                            }
                            else
                            {
                                Console.WriteLine(" WARNING: Protein " + mzIdPtn.accession + " not found in database!");
                                continue;
                            }
                        }

                        ProteinGroupParams proteinGroupParams = new ProteinGroupParams();
                        proteinGroupParams.SearchEngine = SoftwareName;
                        proteinGroupParams.ProteinGroupID = ProteinGroupID;
                        proteinGroup.Params = new List<ProteinGroupParams>() { proteinGroupParams };
                        List<ulong> AllMSMSIds = new List<ulong>();
                        List<ulong> PeptideIDs = new List<ulong>();
                        List<double> peptideScores = new List<double>();

                        #region Peptides

                        #region take peptides from protein
                        List<PeptideMzIdML1_2> peptMzIdML_list = (from pept in PeptideEvidenceList1_2.AsParallel()
                                                                  where pept.Proteins.Contains(mzIdPtn.accession)
                                                                  select pept).ToList();
                        if (peptMzIdML_list.Count == 0)
                        {
                            peptMzIdML_list = (from pept in PeptideEvidenceList1_2.AsParallel()
                                               where pept.Proteins.Contains(mzIdPtn.id)
                                               select pept).ToList();

                            if (peptMzIdML_list.Count == 0)
                            {
                                Console.WriteLine(" WARNING: No peptides found to Protein {0}!", mzIdPtn.accession);
                                continue;
                            }
                        }

                        #endregion

                        proteinGroupParams.PeptideCounts = new List<ulong>(peptMzIdML_list.Count);
                        foreach (PeptideMzIdML1_2 peptide_structure in peptMzIdML_list)
                        {
                            PeptideType peptideType = peptide_structure.PeptideType;

                            CombineBottomUpTopDown.Model.Peptide peptide = new CombineBottomUpTopDown.Model.Peptide();
                            peptide.Sequence = Utils.Util.CleanPeptide(peptideType.PeptideSequence);
                            peptide.Proteins = new List<string>() { mzIdPtn.accession };
                            List<SpectrumIdentificationResultType> spectraList = (from spectraMzIdML in SpectrumList1_2.AsParallel()
                                                                                  where spectraMzIdML.SpectrumIdentificationItem[0].peptide_ref.Equals(peptideType.id)
                                                                                  select spectraMzIdML).ToList();

                            if (spectraList == null || spectraList.Count == 0)//Check if exists another spectrum to the same peptide
                                                                              //The linq is done separately to optimize the performance
                            {
                                try
                                {
                                    spectraList = (from spectraMzIdML in SpectrumList1_2.AsParallel()
                                                   where spectraMzIdML.SpectrumIdentificationItem.Length > 1 && spectraMzIdML.SpectrumIdentificationItem[1].peptide_ref.Equals(peptideType.id)
                                                   select spectraMzIdML).ToList();
                                }
                                catch (System.IndexOutOfRangeException e)
                                {
                                    spectraList = new List<SpectrumIdentificationResultType>();
                                }

                            }

                            List<int> peptideCharges = (from spec in spectraList.AsParallel()
                                                        select spec.SpectrumIdentificationItem[0].chargeState).Distinct().ToList();
                            peptide.Charges = peptideCharges;

                            string cleanPeptide = peptide.Sequence;
                            if (!String.IsNullOrEmpty(proteinGroup.Sequence))
                            {
                                int PeptOffset = proteinGroup.Sequence.IndexOf(cleanPeptide);
                                peptide.StartPosition = PeptOffset + 1;
                                peptide.EndPosition = peptide.StartPosition + cleanPeptide.Length;
                            }

                            #region Modifications
                            List<Tuple<string, int, string>> ptms = new List<Tuple<string, int, string>>();
                            if (peptideType.Modification != null)
                            {
                                foreach (ModificationType modType in peptideType.Modification)
                                {
                                    if (modType.residues == null) continue;
                                    double ptm_mass = modType.monoisotopicMassDelta;
                                    string[] residues = modType.residues;
                                    int location = modType.location;

                                    foreach (string res in residues)
                                    {
                                        ptms.Add(Tuple.Create(res, (location + 1), res + " (" + ptm_mass + ")"));
                                    }
                                }
                            }
                            #endregion
                            List<(double, double)> tmsScores_Mass = new List<(double, double)>();

                            Model.Peptide existsPeptide = Peptides.Where(item => item.Sequence.Equals(peptide.Sequence) && item.StartPosition == peptide.StartPosition && peptide.Proteins[0].Equals(mzIdPtn.accession)).FirstOrDefault();
                            if (existsPeptide == null)
                            {
                                PeptideParams _params = new PeptideParams(0, PeptideID, new List<ulong>() { ProteinGroupID }, new List<ulong>(), new List<ulong>(), MSMSID, 0, true, SoftwareName, 0);
                                List<ulong> MSMSIds = new List<ulong>();

                                //Maximum spectrum score
                                double peptideScore = 0;

                                #region Tandem Mass Spectra
                                foreach (SpectrumIdentificationResultType spectraMzIdML in spectraList)
                                {
                                    string SourceFileName = string.Empty;
                                    #region Get SourceFile Name List
                                    List<SpectraDataType> spectraDataTypeListForCurrentSpecMzIdML = (from item_spec in spectraDataTypeAllList1_2
                                                                                                     where item_spec.id == spectraMzIdML.spectraData_ref
                                                                                                     select item_spec).ToList();
                                    if (spectraDataTypeListForCurrentSpecMzIdML.Count == 0) throw new Exception("The experimental spectrum source file, ID = " + spectraMzIdML.spectraData_ref + ", was not found.");
                                    string originalPath = spectraDataTypeListForCurrentSpecMzIdML[0].location;
                                    string[] fullNameCols = originalPath.Split(System.IO.Path.DirectorySeparatorChar);
                                    SourceFileName = fullNameCols[fullNameCols.Length - 1];
                                    #endregion

                                    TandemMassSpectrum tms = new TandemMassSpectrum();
                                    tms.MSMSID = MSMSID;
                                    int idExtesion = SourceFileName.IndexOf(".");
                                    if (idExtesion != -1)
                                        tms.RAWfile = SourceFileName.Substring(0, idExtesion);
                                    else
                                        tms.RAWfile = SourceFileName;
                                    tms.ScanNumber = Convert.ToInt32(numberCaptured.Matches(spectraMzIdML.spectrumID)[0].Value);
                                    tms.Sequence = peptide.Sequence;
                                    tms.ModifiedSequence = peptideType.PeptideSequence;
                                    tms.Modifications = ptms;

                                    tms.Proteins = new List<string>() { proteinGroup.ProteinID };
                                    tms.Charge = spectraMzIdML.SpectrumIdentificationItem[0].chargeState;
                                    tms.MZ = (spectraMzIdML.SpectrumIdentificationItem[0].experimentalMassToCharge / tms.Charge) + ((Utils.Util.HYDROGENMASS / tms.Charge) * (tms.Charge - 1.0));
                                    tms.Mass = spectraMzIdML.SpectrumIdentificationItem[0].experimentalMassToCharge;
                                    tms.PPMError = Math.Abs(((spectraMzIdML.SpectrumIdentificationItem[0].experimentalMassToCharge - spectraMzIdML.SpectrumIdentificationItem[0].calculatedMassToCharge) * 1000000) / spectraMzIdML.SpectrumIdentificationItem[0].calculatedMassToCharge);

                                    List<string> score_list = new List<string>();
                                    if (spectraMzIdML.SpectrumIdentificationItem[0].Items != null)
                                    {
                                        score_list = (from item in spectraMzIdML.SpectrumIdentificationItem[0].Items.AsParallel()
                                                      where item.name.Contains("score") || item.name.Contains("Score")
                                                      select item.value).Distinct().ToList();
                                    }


                                    double score = 0;
                                    foreach (string score_str in score_list)
                                    {
                                        try
                                        {
                                            score = Convert.ToDouble(score_str);
                                            break;
                                        }
                                        catch (Exception) { }
                                    }

                                    tms.Score = score;
                                    tmsScores_Mass.Add((score, tms.Mass));
                                    tms.MatchedIons = string.Empty;
                                    tms.NumberOfMatches = 0;
                                    tms.ProteinGroupID = new List<ulong>() { ProteinGroupID };
                                    tms.PeptideID = PeptideID;
                                    tms.Fragmentation = ActivationType.HCD;
                                    double retentionTime = 0;
                                    if (spectraMzIdML.Items != null)
                                    {
                                        List<string> retention_time_list = (from item in spectraMzIdML.Items.AsParallel()
                                                                            where item.name.ToLower().Contains("retention time")
                                                                            select item.value).Distinct().ToList();
                                        if (retention_time_list != null)
                                        {
                                            foreach (string retention_time_str in retention_time_list)
                                            {
                                                try
                                                {
                                                    retentionTime = Convert.ToDouble(retention_time_str);
                                                    break;
                                                }
                                                catch (Exception) { }
                                            }
                                        }
                                    }
                                    tms.RetentionTime = retentionTime / 60;
                                    TandemMassSpectra.Add(tms);
                                    MSMSIds.Add(MSMSID);
                                    MSMSID++;
                                }

                                int _index = tmsScores_Mass.FindIndex(a => a.Item1 == tmsScores_Mass.Max(b => b.Item1));
                                if (_index > -1)
                                {
                                    peptideScore = tmsScores_Mass[_index].Item1;
                                    peptideScores.Add(peptideScore);
                                    _params.PeptideScore = peptideScore;
                                    _params.MSMSCount = (ulong)tmsScores_Mass.Count;
                                    _params.Mass = tmsScores_Mass[_index].Item2;
                                }
                                #endregion

                                _params.MSMSIds = MSMSIds;
                                peptide.PSMs = new List<PeptideParams>() { _params };

                                PeptideIDs.Add(PeptideID);
                                Peptides.Add(peptide);
                                PeptideID++;
                                AllMSMSIds.AddRange(MSMSIds);
                            }
                            else
                            {
                                if (!existsPeptide.PSMs[0].ProteinGroupIDs.Contains(ProteinGroupID))
                                {
                                    existsPeptide.Proteins.Add(mzIdPtn.accession);
                                    existsPeptide.PSMs[0].ProteinGroupIDs.Add(ProteinGroupID);
                                    List<ulong> msmsIDs = existsPeptide.PSMs[0].MSMSIds;

                                    List<TandemMassSpectrum> storedTMS = TandemMassSpectra.Where(item => msmsIDs.Contains(item.MSMSID)).AsParallel().ToList();
                                    storedTMS.ForEach(item =>
                                    {
                                        item.ProteinGroupID.Add(ProteinGroupID);
                                        item.Proteins.Add(mzIdPtn.accession);
                                    });
                                }

                                List<ulong> MSMSIds = new List<ulong>();
                                #region Tandem Mass Spectra

                                bool hasNewTMS = false;
                                foreach (SpectrumIdentificationResultType spectraMzIdML in spectraList)
                                {
                                    TandemMassSpectrum hasTMS = TandemMassSpectra.Where(item => item.ScanNumber == Convert.ToInt32(numberCaptured.Matches(spectraMzIdML.spectrumID)[0].Value) &&
                                    item.Charge == spectraMzIdML.SpectrumIdentificationItem[0].chargeState &&
                                    item.MZ == ((spectraMzIdML.SpectrumIdentificationItem[0].experimentalMassToCharge / item.Charge) + ((Utils.Util.HYDROGENMASS / item.Charge) * (item.Charge - 1.0))) &&
                                    item.Mass == spectraMzIdML.SpectrumIdentificationItem[0].experimentalMassToCharge).AsParallel().FirstOrDefault();
                                    if (hasTMS == null)
                                    {
                                        hasNewTMS = true;
                                        string SourceFileName = string.Empty;
                                        #region Get SourceFile Name List
                                        List<SpectraDataType> spectraDataTypeListForCurrentSpecMzIdML = (from item_spec in spectraDataTypeAllList1_2
                                                                                                         where item_spec.id == spectraMzIdML.spectraData_ref
                                                                                                         select item_spec).ToList();
                                        if (spectraDataTypeListForCurrentSpecMzIdML.Count == 0) throw new Exception("The experimental spectrum source file, ID = " + spectraMzIdML.spectraData_ref + ", was not found.");
                                        string originalPath = spectraDataTypeListForCurrentSpecMzIdML[0].location;
                                        string[] fullNameCols = originalPath.Split(System.IO.Path.DirectorySeparatorChar);
                                        SourceFileName = fullNameCols[fullNameCols.Length - 1];
                                        #endregion
                                        TandemMassSpectrum tms = new TandemMassSpectrum();
                                        tms.MSMSID = MSMSID;
                                        int idExtesion = SourceFileName.IndexOf(".");
                                        if (idExtesion != -1)
                                            tms.RAWfile = SourceFileName.Substring(0, idExtesion);
                                        else
                                            tms.RAWfile = SourceFileName;
                                        tms.ScanNumber = Convert.ToInt32(numberCaptured.Matches(spectraMzIdML.spectrumID)[0].Value);
                                        tms.Sequence = peptide.Sequence;
                                        tms.ModifiedSequence = peptideType.PeptideSequence;
                                        tms.Modifications = ptms;
                                        tms.Proteins = new List<string>() { proteinGroup.ProteinID };
                                        tms.Charge = spectraMzIdML.SpectrumIdentificationItem[0].chargeState;
                                        tms.MZ = (spectraMzIdML.SpectrumIdentificationItem[0].experimentalMassToCharge / tms.Charge) + ((Utils.Util.HYDROGENMASS / tms.Charge) * (tms.Charge - 1.0));
                                        tms.Mass = spectraMzIdML.SpectrumIdentificationItem[0].experimentalMassToCharge;
                                        tms.PPMError = Math.Abs(((spectraMzIdML.SpectrumIdentificationItem[0].experimentalMassToCharge - spectraMzIdML.SpectrumIdentificationItem[0].calculatedMassToCharge) * 1000000) / spectraMzIdML.SpectrumIdentificationItem[0].calculatedMassToCharge);

                                        double score = 0;
                                        if (spectraMzIdML.SpectrumIdentificationItem.Length > 0 &&
                                            spectraMzIdML.SpectrumIdentificationItem[0].Items != null)
                                        {
                                            List<string> score_list = (from item in spectraMzIdML.SpectrumIdentificationItem[0].Items.AsParallel()
                                                                       where item.name.Contains("score") || item.name.Contains("Score")
                                                                       select item.value).Distinct().ToList();

                                            foreach (string score_str in score_list)
                                            {
                                                try
                                                {
                                                    score = Convert.ToDouble(score_str);
                                                    break;
                                                }
                                                catch (Exception) { }
                                            }
                                        }

                                        tms.Score = score;
                                        tmsScores_Mass.Add((score, tms.Mass));
                                        tms.MatchedIons = string.Empty;
                                        tms.NumberOfMatches = 0;
                                        tms.ProteinGroupID = existsPeptide.PSMs[0].ProteinGroupIDs;
                                        tms.PeptideID = existsPeptide.PSMs[0].PeptideID;
                                        tms.Fragmentation = ActivationType.HCD;
                                        double retentionTime = 0;
                                        if (spectraMzIdML.Items != null)
                                        {
                                            List<string> retention_time_list = (from item in spectraMzIdML.Items.AsParallel()
                                                                                where item.name.ToLower().Contains("retention time")
                                                                                select item.value).Distinct().ToList();
                                            if (retention_time_list != null)
                                            {
                                                foreach (string retention_time_str in retention_time_list)
                                                {
                                                    try
                                                    {
                                                        retentionTime = Convert.ToDouble(retention_time_str);
                                                        break;
                                                    }
                                                    catch (Exception) { }
                                                }
                                            }
                                        }
                                        tms.RetentionTime = retentionTime / 60;
                                        TandemMassSpectra.Add(tms);
                                        MSMSIds.Add(MSMSID);
                                        MSMSID++;
                                    }
                                    else break;
                                }
                                if (hasNewTMS)
                                {
                                    AllMSMSIds.AddRange(MSMSIds);
                                    existsPeptide.PSMs[0].MSMSIds.AddRange(MSMSIds);
                                    existsPeptide.PSMs[0].MSMSIds = existsPeptide.PSMs[0].MSMSIds.Distinct().ToList();
                                    tmsScores_Mass.Add((existsPeptide.PSMs[0].PeptideScore, existsPeptide.PSMs[0].Mass));

                                    int _index = tmsScores_Mass.FindIndex(a => a.Item1 == tmsScores_Mass.Max(b => b.Item1));
                                    if (_index > -1)
                                    {
                                        double peptideScore = tmsScores_Mass[_index].Item1;
                                        existsPeptide.PSMs[0].PeptideScore = tmsScores_Mass[_index].Item1;
                                        existsPeptide.PSMs[0].Mass = tmsScores_Mass[_index].Item2;
                                    }
                                }
                                #endregion

                                PeptideIDs.Add(existsPeptide.PSMs[0].PeptideID);
                                AllMSMSIds.AddRange(existsPeptide.PSMs[0].MSMSIds);
                            }
                        }

                        #endregion
                        proteinGroupParams.ProteinScore = peptideScores.Sum();
                        proteinGroup.Params[0].PeptideIds = PeptideIDs.Distinct().ToList();
                        proteinGroup.Params[0].PeptideCounts = new List<ulong>(1) { (ulong)proteinGroup.Params[0].PeptideIds.Count };
                        proteinGroup.Params[0].MSMSIds = AllMSMSIds.Distinct().ToList();
                        proteinGroup.MSMSCount = (ulong)proteinGroup.Params[0].MSMSIds.Count;
                        ProteinGroups.Add(proteinGroup);
                        ProteinGroupID++;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine();
                }
                #endregion
            }
            else
            {
                #region Proteins version 1.1
                try
                {
                    //Get all SpectraDataType -> all SearchQueries.SourceFile
                    List<EhuBio.Proteomics.Hupo.mzIdentML1_1.SpectraDataType> spectraDataTypeAllList1_1 = new List<EhuBio.Proteomics.Hupo.mzIdentML1_1.SpectraDataType>(MzidML1_1.Data.DataCollection.Inputs.SpectraData);

                    foreach (EhuBio.Proteomics.Hupo.mzIdentML1_1.DBSequenceType mzIdPtn in ProteinList1_1)
                    {
                        ProteinGroup proteinGroup = new ProteinGroup();
                        proteinGroup.ProteinID = mzIdPtn.accession;

                        FastaItem fasta = FastaItems.Where(a => a.SequenceIdentifier.Contains(mzIdPtn.accession)).FirstOrDefault();
                        if (fasta != null)
                        {
                            proteinGroup.Sequence = fasta.Sequence;
                            proteinGroup.SequenceLength = fasta.Sequence.Length;
                            proteinGroup.FastaHeader = new List<string>() { ">" + fasta.SequenceIdentifier + " " + fasta.Description };
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(mzIdPtn.Seq))
                            {
                                proteinGroup.Sequence = mzIdPtn.Seq;
                                proteinGroup.SequenceLength = mzIdPtn.Seq.Length;
                                proteinGroup.FastaHeader = new List<string>() { ">" + mzIdPtn.accession + " " + mzIdPtn.name };
                            }
                            else
                            {
                                Console.WriteLine(" WARNING: Protein " + mzIdPtn.accession + " not found in database!");
                                continue;
                            }
                        }

                        ProteinGroupParams proteinGroupParams = new ProteinGroupParams();
                        proteinGroupParams.SearchEngine = SoftwareName;
                        proteinGroupParams.ProteinGroupID = ProteinGroupID;
                        proteinGroup.Params = new List<ProteinGroupParams>() { proteinGroupParams };
                        List<ulong> AllMSMSIds = new List<ulong>();
                        List<ulong> PeptideIDs = new List<ulong>();
                        List<double> peptideScores = new List<double>();

                        #region Peptides

                        #region take peptides from protein
                        List<PeptideMzIdML1_1> peptMzIdML_list = (from pept in PeptideEvidenceList1_1.AsParallel()
                                                                  where pept.Proteins.Contains(mzIdPtn.accession)
                                                                  select pept).ToList();
                        if (peptMzIdML_list.Count == 0)
                        {
                            peptMzIdML_list = (from pept in PeptideEvidenceList1_1.AsParallel()
                                               where pept.Proteins.Contains(mzIdPtn.id)
                                               select pept).ToList();

                            if (peptMzIdML_list.Count == 0)
                            {
                                Console.WriteLine(" WARNING: No peptides found to Protein {0}!", mzIdPtn.accession);
                                continue;
                            }
                        }

                        #endregion

                        proteinGroupParams.PeptideCounts = new List<ulong>(peptMzIdML_list.Count);
                        foreach (PeptideMzIdML1_1 peptide_structure in peptMzIdML_list)
                        {
                            EhuBio.Proteomics.Hupo.mzIdentML1_1.PeptideType peptideType = peptide_structure.PeptideType;

                            CombineBottomUpTopDown.Model.Peptide peptide = new CombineBottomUpTopDown.Model.Peptide();
                            peptide.Sequence = Utils.Util.CleanPeptide(peptideType.PeptideSequence);
                            peptide.Proteins = new List<string>() { mzIdPtn.accession };
                            List<EhuBio.Proteomics.Hupo.mzIdentML1_1.SpectrumIdentificationResultType> spectraList = (from spectraMzIdML in SpectrumList1_1.AsParallel()
                                                                                                                      where spectraMzIdML.SpectrumIdentificationItem[0].peptide_ref.Equals(peptideType.id)
                                                                                                                      select spectraMzIdML).ToList();

                            if (spectraList == null || spectraList.Count == 0)//Check if exists another spectrum to the same peptide
                                                                              //The linq is done separately to optimize the performance
                            {
                                try
                                {
                                    spectraList = (from spectraMzIdML in SpectrumList1_1.AsParallel()
                                                   where spectraMzIdML.SpectrumIdentificationItem.Length > 1 && spectraMzIdML.SpectrumIdentificationItem[1].peptide_ref.Equals(peptideType.id)
                                                   select spectraMzIdML).ToList();
                                }
                                catch (System.IndexOutOfRangeException e)
                                {
                                    spectraList = new List<EhuBio.Proteomics.Hupo.mzIdentML1_1.SpectrumIdentificationResultType>();
                                }

                            }

                            List<int> peptideCharges = (from spec in spectraList.AsParallel()
                                                        select spec.SpectrumIdentificationItem[0].chargeState).Distinct().ToList();
                            peptide.Charges = peptideCharges;

                            string cleanPeptide = peptide.Sequence;
                            if (!String.IsNullOrEmpty(proteinGroup.Sequence))
                            {
                                int PeptOffset = proteinGroup.Sequence.IndexOf(cleanPeptide);
                                peptide.StartPosition = PeptOffset + 1;
                                peptide.EndPosition = peptide.StartPosition + cleanPeptide.Length;
                            }

                            #region Modifications
                            List<Tuple<string, int, string>> ptms = new List<Tuple<string, int, string>>();
                            if (peptideType.Modification != null)
                            {
                                foreach (EhuBio.Proteomics.Hupo.mzIdentML1_1.ModificationType modType in peptideType.Modification)
                                {
                                    if (modType.residues == null) continue;
                                    double ptm_mass = modType.monoisotopicMassDelta;
                                    string[] residues = modType.residues;
                                    int location = modType.location;

                                    foreach (string res in residues)
                                    {
                                        ptms.Add(Tuple.Create(res, (location + 1), res + " (" + ptm_mass + ")"));
                                    }
                                }
                            }
                            #endregion
                            List<(double, double)> tmsScores_Mass = new List<(double, double)>();

                            Model.Peptide existsPeptide = Peptides.Where(item => item.Sequence.Equals(peptide.Sequence) && item.StartPosition == peptide.StartPosition && peptide.Proteins[0].Equals(mzIdPtn.accession)).FirstOrDefault();
                            if (existsPeptide == null)
                            {
                                PeptideParams _params = new PeptideParams(0, PeptideID, new List<ulong>() { ProteinGroupID }, new List<ulong>(), new List<ulong>(), MSMSID, 0, true, SoftwareName, 0);
                                List<ulong> MSMSIds = new List<ulong>();

                                //Maximum spectrum score
                                double peptideScore = 0;

                                #region Tandem Mass Spectra
                                foreach (EhuBio.Proteomics.Hupo.mzIdentML1_1.SpectrumIdentificationResultType spectraMzIdML in spectraList)
                                {
                                    string SourceFileName = string.Empty;
                                    #region Get SourceFile Name List
                                    List<EhuBio.Proteomics.Hupo.mzIdentML1_1.SpectraDataType> spectraDataTypeListForCurrentSpecMzIdML = (from item_spec in spectraDataTypeAllList1_1
                                                                                                                                         where item_spec.id == spectraMzIdML.spectraData_ref
                                                                                                                                         select item_spec).ToList();
                                    if (spectraDataTypeListForCurrentSpecMzIdML.Count == 0) throw new Exception("The experimental spectrum source file, ID = " + spectraMzIdML.spectraData_ref + ", was not found.");

                                    string originalPath = spectraDataTypeListForCurrentSpecMzIdML[0].location;
                                    string[] fullNameCols = originalPath.Split(System.IO.Path.DirectorySeparatorChar);
                                    SourceFileName = fullNameCols[fullNameCols.Length - 1];
                                    #endregion

                                    TandemMassSpectrum tms = new TandemMassSpectrum();
                                    tms.MSMSID = MSMSID;
                                    int idExtesion = SourceFileName.IndexOf(".");
                                    if (idExtesion != -1)
                                        tms.RAWfile = SourceFileName.Substring(0, idExtesion);
                                    else
                                        tms.RAWfile = SourceFileName;
                                    tms.ScanNumber = Convert.ToInt32(numberCaptured.Matches(spectraMzIdML.spectrumID)[0].Value);
                                    tms.Sequence = peptide.Sequence;
                                    tms.ModifiedSequence = peptideType.PeptideSequence;
                                    tms.Modifications = ptms;

                                    tms.Proteins = new List<string>() { proteinGroup.ProteinID };
                                    tms.Charge = spectraMzIdML.SpectrumIdentificationItem[0].chargeState;
                                    tms.MZ = (spectraMzIdML.SpectrumIdentificationItem[0].experimentalMassToCharge / tms.Charge) + ((Utils.Util.HYDROGENMASS / tms.Charge) * (tms.Charge - 1.0));
                                    tms.Mass = spectraMzIdML.SpectrumIdentificationItem[0].experimentalMassToCharge;
                                    tms.PPMError = Math.Abs(((spectraMzIdML.SpectrumIdentificationItem[0].experimentalMassToCharge - spectraMzIdML.SpectrumIdentificationItem[0].calculatedMassToCharge) * 1000000) / spectraMzIdML.SpectrumIdentificationItem[0].calculatedMassToCharge);

                                    List<string> score_list = new List<string>();
                                    if (spectraMzIdML.SpectrumIdentificationItem[0].Items != null)
                                    {
                                        score_list = (from item in spectraMzIdML.SpectrumIdentificationItem[0].Items.AsParallel()
                                                      where item.name.Contains("score") || item.name.Contains("Score")
                                                      select item.value).Distinct().ToList();
                                    }


                                    double score = 0;
                                    foreach (string score_str in score_list)
                                    {
                                        try
                                        {
                                            score = Convert.ToDouble(score_str);
                                            break;
                                        }
                                        catch (Exception) { }
                                    }

                                    tms.Score = score;
                                    tmsScores_Mass.Add((score, tms.Mass));
                                    tms.MatchedIons = string.Empty;
                                    tms.NumberOfMatches = 0;
                                    tms.ProteinGroupID = new List<ulong>() { ProteinGroupID };
                                    tms.PeptideID = PeptideID;
                                    tms.Fragmentation = ActivationType.HCD;
                                    double retentionTime = 0;
                                    if (spectraMzIdML.Items != null)
                                    {
                                        List<string> retention_time_list = (from item in spectraMzIdML.Items.AsParallel()
                                                                            where item.name.ToLower().Contains("retention time")
                                                                            select item.value).Distinct().ToList();
                                        if (retention_time_list != null)
                                        {
                                            foreach (string retention_time_str in retention_time_list)
                                            {
                                                try
                                                {
                                                    retentionTime = Convert.ToDouble(retention_time_str);
                                                    break;
                                                }
                                                catch (Exception) { }
                                            }
                                        }
                                    }
                                    tms.RetentionTime = retentionTime / 60;
                                    TandemMassSpectra.Add(tms);
                                    MSMSIds.Add(MSMSID);
                                    MSMSID++;
                                }

                                int _index = tmsScores_Mass.FindIndex(a => a.Item1 == tmsScores_Mass.Max(b => b.Item1));
                                if (_index > -1)
                                {
                                    peptideScore = tmsScores_Mass[_index].Item1;
                                    peptideScores.Add(peptideScore);
                                    _params.PeptideScore = peptideScore;
                                    _params.MSMSCount = (ulong)tmsScores_Mass.Count;
                                    _params.Mass = tmsScores_Mass[_index].Item2;
                                }
                                #endregion

                                _params.MSMSIds = MSMSIds;
                                peptide.PSMs = new List<PeptideParams>() { _params };

                                PeptideIDs.Add(PeptideID);
                                Peptides.Add(peptide);
                                PeptideID++;
                                AllMSMSIds.AddRange(MSMSIds);
                            }
                            else
                            {
                                if (!existsPeptide.PSMs[0].ProteinGroupIDs.Contains(ProteinGroupID))
                                {
                                    existsPeptide.Proteins.Add(mzIdPtn.accession);
                                    existsPeptide.PSMs[0].ProteinGroupIDs.Add(ProteinGroupID);
                                    List<ulong> msmsIDs = existsPeptide.PSMs[0].MSMSIds;

                                    List<TandemMassSpectrum> storedTMS = TandemMassSpectra.Where(item => msmsIDs.Contains(item.MSMSID)).AsParallel().ToList();
                                    storedTMS.ForEach(item =>
                                    {
                                        item.ProteinGroupID.Add(ProteinGroupID);
                                        item.Proteins.Add(mzIdPtn.accession);
                                    });
                                }

                                List<ulong> MSMSIds = new List<ulong>();
                                #region Tandem Mass Spectra

                                bool hasNewTMS = false;
                                foreach (EhuBio.Proteomics.Hupo.mzIdentML1_1.SpectrumIdentificationResultType spectraMzIdML in spectraList)
                                {
                                    TandemMassSpectrum hasTMS = TandemMassSpectra.Where(item => item.ScanNumber == Convert.ToInt32(numberCaptured.Matches(spectraMzIdML.spectrumID)[0].Value) &&
                                    item.Charge == spectraMzIdML.SpectrumIdentificationItem[0].chargeState &&
                                    item.MZ == ((spectraMzIdML.SpectrumIdentificationItem[0].experimentalMassToCharge / item.Charge) + ((Utils.Util.HYDROGENMASS / item.Charge) * (item.Charge - 1.0))) &&
                                    item.Mass == spectraMzIdML.SpectrumIdentificationItem[0].experimentalMassToCharge).AsParallel().FirstOrDefault();
                                    if (hasTMS == null)
                                    {
                                        hasNewTMS = true;
                                        string SourceFileName = string.Empty;
                                        #region Get SourceFile Name List
                                        List<EhuBio.Proteomics.Hupo.mzIdentML1_1.SpectraDataType> spectraDataTypeListForCurrentSpecMzIdML = (from item_spec in spectraDataTypeAllList1_1
                                                                                                                                             where item_spec.id == spectraMzIdML.spectraData_ref
                                                                                                                                             select item_spec).ToList();
                                        if (spectraDataTypeListForCurrentSpecMzIdML.Count == 0) throw new Exception("The experimental spectrum source file, ID = " + spectraMzIdML.spectraData_ref + ", was not found.");
                                        string originalPath = spectraDataTypeListForCurrentSpecMzIdML[0].location;
                                        string[] fullNameCols = originalPath.Split(System.IO.Path.DirectorySeparatorChar);
                                        SourceFileName = fullNameCols[fullNameCols.Length - 1];

                                        #endregion
                                        TandemMassSpectrum tms = new TandemMassSpectrum();
                                        tms.MSMSID = MSMSID;
                                        int idExtesion = SourceFileName.IndexOf(".");
                                        if (idExtesion != -1)
                                            tms.RAWfile = SourceFileName.Substring(0, idExtesion);
                                        else
                                            tms.RAWfile = SourceFileName;
                                        tms.ScanNumber = Convert.ToInt32(numberCaptured.Matches(spectraMzIdML.spectrumID)[0].Value);
                                        tms.Sequence = peptide.Sequence;
                                        tms.ModifiedSequence = peptideType.PeptideSequence;
                                        tms.Modifications = ptms;
                                        tms.Proteins = new List<string>() { proteinGroup.ProteinID };
                                        tms.Charge = spectraMzIdML.SpectrumIdentificationItem[0].chargeState;
                                        tms.MZ = (spectraMzIdML.SpectrumIdentificationItem[0].experimentalMassToCharge / tms.Charge) + ((Utils.Util.HYDROGENMASS / tms.Charge) * (tms.Charge - 1.0));
                                        tms.Mass = spectraMzIdML.SpectrumIdentificationItem[0].experimentalMassToCharge;
                                        tms.PPMError = Math.Abs(((spectraMzIdML.SpectrumIdentificationItem[0].experimentalMassToCharge - spectraMzIdML.SpectrumIdentificationItem[0].calculatedMassToCharge) * 1000000) / spectraMzIdML.SpectrumIdentificationItem[0].calculatedMassToCharge);

                                        double score = 0;
                                        if (spectraMzIdML.SpectrumIdentificationItem.Length > 0 &&
                                            spectraMzIdML.SpectrumIdentificationItem[0].Items != null)
                                        {
                                            List<string> score_list = (from item in spectraMzIdML.SpectrumIdentificationItem[0].Items.AsParallel()
                                                                       where item.name.Contains("score") || item.name.Contains("Score")
                                                                       select item.value).Distinct().ToList();

                                            foreach (string score_str in score_list)
                                            {
                                                try
                                                {
                                                    score = Convert.ToDouble(score_str);
                                                    break;
                                                }
                                                catch (Exception) { }
                                            }
                                        }

                                        tms.Score = score;
                                        tmsScores_Mass.Add((score, tms.Mass));
                                        tms.MatchedIons = string.Empty;
                                        tms.NumberOfMatches = 0;
                                        tms.ProteinGroupID = existsPeptide.PSMs[0].ProteinGroupIDs;
                                        tms.PeptideID = existsPeptide.PSMs[0].PeptideID;
                                        tms.Fragmentation = ActivationType.HCD;
                                        double retentionTime = 0;
                                        if (spectraMzIdML.Items != null)
                                        {
                                            List<string> retention_time_list = (from item in spectraMzIdML.Items.AsParallel()
                                                                                where item.name.ToLower().Contains("retention time")
                                                                                select item.value).Distinct().ToList();
                                            if (retention_time_list != null)
                                            {
                                                foreach (string retention_time_str in retention_time_list)
                                                {
                                                    try
                                                    {
                                                        retentionTime = Convert.ToDouble(retention_time_str);
                                                        break;
                                                    }
                                                    catch (Exception) { }
                                                }
                                            }
                                        }
                                        tms.RetentionTime = retentionTime / 60;
                                        TandemMassSpectra.Add(tms);
                                        MSMSIds.Add(MSMSID);
                                        MSMSID++;
                                    }
                                    else break;
                                }
                                if (hasNewTMS)
                                {
                                    AllMSMSIds.AddRange(MSMSIds);
                                    existsPeptide.PSMs[0].MSMSIds.AddRange(MSMSIds);
                                    existsPeptide.PSMs[0].MSMSIds = existsPeptide.PSMs[0].MSMSIds.Distinct().ToList();
                                    tmsScores_Mass.Add((existsPeptide.PSMs[0].PeptideScore, existsPeptide.PSMs[0].Mass));

                                    int _index = tmsScores_Mass.FindIndex(a => a.Item1 == tmsScores_Mass.Max(b => b.Item1));
                                    if (_index > -1)
                                    {
                                        double peptideScore = tmsScores_Mass[_index].Item1;
                                        existsPeptide.PSMs[0].PeptideScore = tmsScores_Mass[_index].Item1;
                                        existsPeptide.PSMs[0].Mass = tmsScores_Mass[_index].Item2;
                                    }
                                }
                                #endregion

                                PeptideIDs.Add(existsPeptide.PSMs[0].PeptideID);
                                AllMSMSIds.AddRange(existsPeptide.PSMs[0].MSMSIds);
                            }
                        }

                        #endregion
                        proteinGroupParams.ProteinScore = peptideScores.Sum();
                        proteinGroup.Params[0].PeptideIds = PeptideIDs.Distinct().ToList();
                        proteinGroup.Params[0].PeptideCounts = new List<ulong>(1) { (ulong)proteinGroup.Params[0].PeptideIds.Count };
                        proteinGroup.Params[0].MSMSIds = AllMSMSIds.Distinct().ToList();
                        proteinGroup.MSMSCount = (ulong)proteinGroup.Params[0].MSMSIds.Count;
                        ProteinGroups.Add(proteinGroup);
                        ProteinGroupID++;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine();
                }
                #endregion
            }
            ProteinGroups = ProteinGroups.Distinct(new ProteinGroupComparer()).ToList();
            ProteinGroups.Sort((a, b) => b.Params[0].ProteinScore.CompareTo(a.Params[0].ProteinScore));
            Peptides = Peptides.Distinct(new PeptideComparer()).AsParallel().ToList();
            TandemMassSpectra = TandemMassSpectra.Distinct(new TandemMassSpectrumIDSequenceComparer()).AsParallel().ToList();
        }

        /// <summary>
        /// Class responsible for sorting PeptideMzIdML struct
        /// </summary>
        private class PeptideMzIdMLComparer : IEqualityComparer<PeptideMzIdML1_2>
        {
            public bool Equals(PeptideMzIdML1_2 x, PeptideMzIdML1_2 y)
            {
                //Check if the compared objects reference has same data.
                if (Object.ReferenceEquals(x, y)) return true;

                //Check if any of the compared objects is null.
                if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                    return false;

                //Check if the items' properties are equal.
                return (x.PeptideType.PeptideSequence.Equals(y.PeptideType.PeptideSequence));
            }

            public int GetHashCode(PeptideMzIdML1_2 obj)
            {
                return obj.PeptideType.PeptideSequence.GetHashCode();
            }
        }
    }
}