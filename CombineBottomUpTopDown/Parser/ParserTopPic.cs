﻿/**
 * Program:     ComparisonTDPSearchEngines
 * Author:      Diogo Borges Lima and Paulo Costa Carvalho
 * Created:     4/9/2016
 * Update by:   Diogo Borges Lima
 * Description: Class responsible for reading TopPic output files.
 */
using CombineBottomUpTopDown.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CombineBottomUpTopDown.Parser
{
    public class ParserTopPic
    {
        /// <summary>
        /// Private Variables
        /// </summary>
        private List<string> ProteinID { get; set; }
        private string[] HeaderLine { get; set; }
        private int ScanNumber { get; set; }
        private int ChargeState { get; set; }
        private double PrecursorMZ { get; set; }
        private double TheoreticalMass { get; set; }
        private double LogEvalue { get; set; }
        private double LogPScore { get; set; }
        private string ProteinSequence { get; set; }
        private string ProteoformSequence { get; set; }
        private int MatchedIons { get; set; }
        private int TotalIons { get; set; }
        //Tuple<mz,intensity>
        private List<Tuple<double, double>> FragmentIons { get; set; }
        private string SpectrumFile { get; set; }
        private double RetentionTime { get; set; }
        private double Intensity { get; set; }
        private string ProteinAccession { get; set; }
        private string ProteinDescription { get; set; }
        private double DeltaMass { get; set; }
        //Tuple<aminoacid, position, description>
        private List<Tuple<string, int, string>> Modifications { get; set; }
        private int IdentifiedNode { get; set; }//3 - intact proteoform; 6 - Truncated proteoform ; 9- Tagged Proteoform

        private StringBuilder sbProteinSeq = new StringBuilder();
        private StringBuilder sbProteoformSeq = new StringBuilder();
        private bool getSequence { get; set; }
        private bool startFragIons { get; set; }

        public List<SpectrumTopPic> TopPicList = new List<SpectrumTopPic>();
        public Proteoform Proteoform { get; set; }//Html file
        public List<Proteoform> ProteoformList = new List<Proteoform>();//CSV file

        public ParserTopPic() { }

        /// <summary>
        /// Method responsible for parsing TopPic output file
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns>Return a list with all identified tags according to defined parameters</returns>
        public void ParseDir(string dirName)
        {
            string[] files = Directory.GetFiles(dirName, "*.html");
            foreach (string fileName in files)
                ParseHtmlFile(fileName);
        }

        public void ParseCSVFile(string fileName)
        {
            StreamReader sr = new StreamReader(fileName);
            string line = "";
            int countParams = 0;

            while ((line = sr.ReadLine()) != null)
            {
                if (String.IsNullOrEmpty(line)) continue;
                if (line.StartsWith("********************** Parameters **********************")) countParams++;
                if (countParams < 2) continue;

                if (line.StartsWith("********************** Parameters **********************"))
                {
                    line = sr.ReadLine();
                    ProcessHeader(line);
                    continue;
                }

                Proteoform currentProteoform = ProcessCSVLine(line);
                currentProteoform.PrSMs[0].SearchEngine = "TopPIC";

                List<Proteoform> existsProtein = (from ptn in ProteoformList
                                                  where ptn.AnnotatedSequence.Equals(currentProteoform.AnnotatedSequence)
                                                  && ptn.PrSMs[0].LogPScore == currentProteoform.PrSMs[0].LogPScore
                                                  select ptn).ToList();

                if (existsProtein.Count > 0)
                {
                    if (currentProteoform.PrSMs[0].LogPScore > existsProtein[0].PrSMs[0].LogPScore)
                    {
                        ProteoformList.Remove(existsProtein[0]);
                        ProteoformList.Add(currentProteoform);
                    }
                }
                else
                {
                    ProteoformList.Add(currentProteoform);
                }
            }
            sr.Close();
        }

        /// <summary>
        /// Method responsible for processing the header line
        /// </summary>
        /// <param name="row"></param>
        private void ProcessHeader(string row)
        {
            HeaderLine = Regex.Split(row, ",");
        }

        private Proteoform ProcessCSVLine(string line)
        {
            string[] cols = Regex.Split(line, ",");

            int index = Array.IndexOf(HeaderLine, "Data file name");
            if (index == -1) return null;
            SpectrumFile = cols[index];

            index = Array.IndexOf(HeaderLine, "Scan(s)");
            if (index == -1) return null;
            ScanNumber = Convert.ToInt32(cols[index]);

            index = Array.IndexOf(HeaderLine, "Retention time");
            if (index == -1) return null;
            RetentionTime = Convert.ToDouble(cols[index]);

            index = Array.IndexOf(HeaderLine, "#peaks");
            if (index == -1) return null;
            TotalIons = Convert.ToInt32(cols[index]);

            index = Array.IndexOf(HeaderLine, "Charge");
            if (index == -1) return null;
            ChargeState = Convert.ToInt32(cols[index]);

            index = Array.IndexOf(HeaderLine, "Adjusted precursor mass");
            if (index == -1) return null;
            TheoreticalMass = Convert.ToDouble(cols[index]);

            index = Array.IndexOf(HeaderLine, "Feature intensity");
            if (index == -1) return null;
            Intensity = Convert.ToDouble(cols[index]);

            index = Array.IndexOf(HeaderLine, "Protein accession");
            if (index == -1) return null;
            ProteinAccession = cols[index];

            index = Array.IndexOf(HeaderLine, "Protein description");
            if (index == -1) return null;
            ProteinDescription = cols[index].Replace("\"", "");

            index = Array.IndexOf(HeaderLine, "Proteoform");
            if (index == -1) return null;
            ProteoformSequence = ProcessProteinSequence(cols[index]);

            index = Array.IndexOf(HeaderLine, "#matched peaks");
            if (index == -1) return null;
            MatchedIons = Convert.ToInt32(cols[index]);

            index = Array.IndexOf(HeaderLine, "P-value");
            if (index == -1) return null;
            LogPScore = -Math.Log(Convert.ToDouble(cols[index]));

            index = Array.IndexOf(HeaderLine, "E-value");
            if (index == -1) return null;
            LogEvalue = -Math.Log(Convert.ToDouble(cols[index]));

            ProteoformParams proteoformParams = new ProteoformParams(
                //ProteoformSequence,
                //ProteinAccession,
                ChargeState,
                TheoreticalMass,
                MatchedIons,
                TotalIons,
                Intensity,
                ScanNumber,
                LogPScore,
                LogEvalue,
                RetentionTime,
                //ProteinDescription,
                SpectrumFile,
                DeltaMass,
                Modifications,
                IdentifiedNode
                );

            index = Array.IndexOf(HeaderLine, "Fragmentation");
            if (index == -1) return null;

            switch (cols[index])
            {
                case "EThcD":
                    proteoformParams.ActivationType = ActivationType.EThCD;
                    break;
                case "CID":
                    proteoformParams.ActivationType = ActivationType.CID;
                    break;
                case "ETD":
                    proteoformParams.ActivationType = ActivationType.ETD;
                    break;
                case "HCD":
                    proteoformParams.ActivationType = ActivationType.HCD;
                    break;
                case "ECD":
                    proteoformParams.ActivationType = ActivationType.ECD;
                    break;
                case "MPD":
                    proteoformParams.ActivationType = ActivationType.MPD;
                    break;
                case "PQD":
                    proteoformParams.ActivationType = ActivationType.PQD;
                    break;
                default:
                    proteoformParams.ActivationType = ActivationType.NF;
                    break;
            }

            Proteoform proteoform = new Proteoform(ProteoformSequence, ProteinAccession, ProteinDescription, ProteoformSequence, 0, new List<ProteoformParams>() { proteoformParams });
            return proteoform;
        }

        private string ProcessProteinSequence(string originalSequence)
        {
            int indexStartPeptide = originalSequence.IndexOf(".") + 1;
            char[] charArray = originalSequence.ToCharArray();
            Array.Reverse(charArray);
            string reverseString = new string(charArray);
            int indexEndPeptide = originalSequence.Length - reverseString.IndexOf(".") - 1;
            originalSequence = originalSequence.Substring(indexStartPeptide, indexEndPeptide - indexStartPeptide);

            byte[] sequence = System.Text.ASCIIEncoding.Default.GetBytes(originalSequence);
            //Tuple<aminoacid, position, description>
            Modifications = new List<Tuple<string, int, string>>();
            double deltaMassTotal = 0;
            int countPTM;
            List<byte> ptm = new List<byte>();
            char[] cleannedPeptide = new char[originalSequence.Length];
            List<char> ptmAA = new List<char>();
            int startAAPtm = -1;
            int totalPtmOffset = 0;
            int previousPtmOffset = 0;
            int PTMscount = 0;
            bool IsTherePTMInside = false;//There is more than one PTM embedded.
            int firstParanthesis = originalSequence.IndexOf("(");
            for (int count = 0; count < sequence.LongLength; count++)
            {
                //open PTM amino acids with "(" character
                if (sequence[count] == 40)
                {
                    PTMscount++;
                    if (count <= firstParanthesis)
                        startAAPtm = count;
                    else
                        startAAPtm = count + 1;
                    previousPtmOffset = totalPtmOffset;


                    for (countPTM = count + 1; countPTM < sequence.LongLength; countPTM++)
                    {
                        while (sequence[countPTM] == 40)
                        {
                            PTMscount++;
                            countPTM++;
                            IsTherePTMInside = true;
                        }

                        //close PTM aminoacids with ")" character
                        if (sequence[countPTM] == 41)
                        {
                            totalPtmOffset++;
                            count = countPTM;
                            break;
                        }
                        else
                        {
                            totalPtmOffset++;
                            ptmAA.Add(originalSequence[countPTM]);
                            cleannedPeptide[countPTM] = originalSequence[countPTM];
                        }
                    }
                }
                //open PTM mass with "[" character
                if (sequence[count] == 91)
                {
                    for (countPTM = count + 1; countPTM < sequence.LongLength; countPTM++)
                    {
                        //close PTM mass with "]" character
                        if (sequence[countPTM] == 93)
                        {
                            totalPtmOffset += 2;
                            break;
                        }
                        else
                        {
                            totalPtmOffset++;
                            ptm.Add(sequence[countPTM]);
                        }
                    }
                    count = countPTM;
                    totalPtmOffset++;//add parenthesis
                    string ptmStr = System.Text.ASCIIEncoding.Default.GetString(ptm.ToArray());
                    double ptmValue = 0;
                    if (!String.IsNullOrEmpty(ptmStr))
                    {
                        int countItem = -1;
                        if (IsTherePTMInside)
                            countItem = startAAPtm - previousPtmOffset - ptmAA.Count + 1 > 0 ? startAAPtm - previousPtmOffset - ptmAA.Count + 1 : startAAPtm;
                        else
                            countItem = startAAPtm - previousPtmOffset > 0 ? startAAPtm - previousPtmOffset : startAAPtm;
                        countItem++;
                        foreach (char item in ptmAA)
                        {
                            if (!double.TryParse(ptmStr, out ptmValue))
                            {
                                Modifications.Add(Tuple.Create(item.ToString(), countItem, ptmStr));
                                IdentifiedNode = 3;
                            }
                            else
                            {
                                Modifications.Add(Tuple.Create(item.ToString(), countItem, "Unknown PTM [" + ptmStr + "]"));
                                IdentifiedNode = 9;
                            }
                            countItem++;
                        }
                    }
                    else
                        IdentifiedNode = 3;
                    deltaMassTotal += Math.Abs(ptmValue);
                    ptm.Clear();
                    ptmAA.Clear();
                }
                else
                    cleannedPeptide[count] = originalSequence[count];

                if (PTMscount > 0)
                {
                    if (sequence[count] > 64 && sequence[count] < 91)
                    {
                        startAAPtm++;
                        ptmAA.Add(originalSequence[count]);
                    }
                    if (sequence[count] == 41)
                        PTMscount--;
                }
                else
                    IsTherePTMInside = false;
            }

            DeltaMass = deltaMassTotal;

            string cleannedPeptideStr = new string(cleannedPeptide);

            cleannedPeptideStr = Utils.Util.CleanPeptide(cleannedPeptideStr);

            return cleannedPeptideStr;
        }

        public void ParseHtmlFile(string fileName)
        {
            StreamReader sr = new StreamReader(fileName);
            string line = "";
            getSequence = false;

            int countAHREF = 0;
            int countTD = 0;
            bool getScanNumber = false;
            int countDIV = 0;
            FragmentIons = new List<Tuple<double, double>>();
            startFragIons = false;
            bool isFinishedFragIons = false;

            while ((line = sr.ReadLine()) != null)
            {
                if (line.Contains("<a href="))
                {
                    countAHREF++;
                }

                //Get the ProteinID
                if (countAHREF == 2)
                {
                    ProcessHtmlLine(line, true);
                    getScanNumber = true;
                }

                //Get ScanNumber, PrecursorMZ, ProteoformMass, MatchedIons, LogEvalue, LogPvalue, sequence
                if (getScanNumber && line.Contains("<td>"))
                {
                    ProcessHtmlLine(line, false, countTD);
                    countTD++;
                }

                //Get total ions number
                if (getScanNumber && getSequence && line.StartsWith("<div style="))
                {
                    ProcessHtmlLine(line, false, countTD, countDIV);
                    countTD++;
                    countDIV++;
                }

                //Get fragment ions
                if (getScanNumber && getSequence && line.StartsWith("<td align=\"center\">"))
                    ProcessHtmlLine(line, false, countTD, countDIV);

                if (startFragIons && line.StartsWith("</table>"))
                    isFinishedFragIons = true;

                if (isFinishedFragIons)
                {
                    //TopPicList.Add(GetProtein());
                    Proteoform = GetProteoform();
                    getScanNumber = false;
                    getSequence = false;
                    startFragIons = false;
                    isFinishedFragIons = false;
                    countTD = -1;
                    countDIV = -1;
                    break;
                }
            }
            sr.Close();
        }

        private void ProcessHtmlLine(string line, bool ahref = false, int countTD = 0, int countDIV = 0)
        {
            string[] cols = null;


            if (ahref)
            {
                cols = Regex.Split(line, ">");
                string[] proteinIDcols = Regex.Split(cols[1].Replace("</a", "").Replace(" ", ""), "_");
                ProteinID = new List<string>(proteinIDcols);
                ProteinID = ProteinID.Distinct().ToList();
                ProteinID.RemoveAll(a => a.Equals(""));
            }
            else if (countTD == 0)
            {
                cols = Regex.Split(line, "<td>");
                ScanNumber = Convert.ToInt32(cols[4].Replace("</td>", ""));
                ChargeState = Convert.ToInt32(cols[6].Replace("</td>", ""));
            }
            else if (countTD == 1)
            {
                cols = Regex.Split(line, "<td>");
                PrecursorMZ = Convert.ToDouble(cols[2].Replace("</td>", ""));
                TheoreticalMass = Convert.ToDouble(cols[6].Replace("</td>", ""));
            }
            else if (countTD == 2)
            {
                cols = Regex.Split(line, "<td>");
                MatchedIons = Convert.ToInt32(cols[2].Replace("</td>", ""));
            }
            else if (countTD == 3)
            {
                cols = Regex.Split(line, "<td>");
                LogEvalue = -Math.Log(Convert.ToDouble(cols[2].Replace("</td>", "")));
                LogPScore = -Math.Log(Convert.ToDouble(cols[4].Replace("</td>", "")));
            }
            else if (line.StartsWith("<td width=\"40px\""))//Sequence
            {
                cols = Regex.Split(line, "<td width=");
                foreach (string aminoacid in cols)
                {
                    if (String.IsNullOrEmpty(aminoacid)) continue;
                    string[] sublines = Regex.Split(aminoacid, ">");
                    string subline = sublines[1].Replace("</td", "");
                    if (String.IsNullOrEmpty(subline) || Regex.IsMatch(subline, @"^\d") || subline.Length > 1 || subline.Equals("]")) continue;
                    sbProteinSeq.Append(subline);
                    if (!sublines[0].Contains("color:grey"))
                        sbProteoformSeq.Append(subline);
                }
                getSequence = true;
            }
            else if (countDIV == 1 && getSequence && line.StartsWith("<div style="))//Sequence
            {
                ProteinSequence = sbProteinSeq.ToString().Replace("[", "");
                ProteoformSequence = sbProteoformSeq.ToString().Replace("[", "");
                sbProteinSeq.Clear();
                sbProteoformSeq.Clear();

                int startIndex = line.IndexOf("All peaks");
                int endIndex = line.IndexOf(")</a>&nbsp;&nbsp;<");
                TotalIons = Convert.ToInt32(line.Substring(startIndex + 11, endIndex - startIndex - 11));
            }
            else if (line.StartsWith("<td align=\"center\">"))
            {
                startFragIons = true;
                cols = Regex.Split(line, "<td align=\"center\">");
                FragmentIons.Add((Convert.ToDouble(cols[4].Replace("</td>", "")), Convert.ToDouble(cols[5].Replace("</td>", ""))).ToTuple());
            }
        }

        private SpectrumTopPic GetProtein()
        {
            FragmentIons = FragmentIons.Distinct().ToList();
            FragmentIons.Sort((a, b) => a.Item1.CompareTo(b.Item1));
            SpectrumTopPic protein = new SpectrumTopPic(
                    ProteinID,
                    ScanNumber,
                    ChargeState,
                    PrecursorMZ,
                    TheoreticalMass,
                    LogEvalue,
                    LogPScore,
                    ProteinSequence,
                    ProteoformSequence,
                    MatchedIons,
                    TotalIons,
                    FragmentIons
                    );
            return protein;
        }

        private Proteoform GetProteoform()
        {
            FragmentIons = FragmentIons.Distinct().ToList();
            FragmentIons.Sort((a, b) => a.Item1.CompareTo(b.Item1));
            ProteoformParams proteoformParams = new ProteoformParams(
                ChargeState,
                PrecursorMZ,
                TheoreticalMass,
                MatchedIons,
                TotalIons,
                FragmentIons.Select(item => item.Item2).Sum(),
                ScanNumber,
                LogPScore,
                LogEvalue,
            FragmentIons);
            proteoformParams.SearchEngine = "TopPIC";

            Proteoform proteoform = new Proteoform(ProteoformSequence, String.Join(", ", ProteinID), string.Empty, ProteinSequence, 0, new List<ProteoformParams>() { proteoformParams });

            return proteoform;
        }
    }

    public class SpectrumTopPic
    {
        public List<string> ProteinID { get; set; }
        public int ScanNumber { get; set; }
        public int PrecursorCharge { get; set; }
        public double PrecursorMZ { get; set; }
        public double ProteoformMass { get; set; }
        public double LogEvalue { get; set; }
        public double LogPScore { get; set; }
        public string ProteinSequence { get; set; }
        public string ProteoformSequence { get; set; }
        public int MatchedIons { get; set; }
        public int TotalIons { get; set; }
        //Tuple<mz,intensity>
        public List<Tuple<double, double>> FragmentIons { get; set; }

        public SpectrumTopPic(
            List<string> ProteinID,
            int ScanNumber,
            int PrecursorCharge,
            double PrecursorMZ,
            double ProteoformMass,
            double LogEvalue,
            double LogPScore,
            string ProteinSequence,
            string ProteoformSequence,
            int MatchedIons,
            int TotalIons,
            List<Tuple<double, double>> FragmentIons
            )
        {
            this.ProteinID = ProteinID;
            this.ScanNumber = ScanNumber;
            this.PrecursorCharge = PrecursorCharge;
            this.PrecursorMZ = PrecursorMZ;
            this.ProteoformMass = ProteoformMass;
            this.LogEvalue = LogEvalue;
            this.LogPScore = LogPScore;
            this.ProteinSequence = ProteinSequence;
            this.ProteoformSequence = ProteoformSequence;
            this.MatchedIons = MatchedIons;
            this.TotalIons = TotalIons;
            this.FragmentIons = FragmentIons;
        }
    }
}
