﻿/**
 * Program:     ProteoCombiner - Integrating bottom-up & top-down proteomics data
 * Author:      Diogo Borges Lima
 * Created:     5/15/2019
 * Update by:   Diogo Borges Lima
 * Description: Class responsable for reading Thermo Raw files
 */
using CombineBottomUpTopDown.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;
using ThermoFisher.CommonCore.Data.Business;
using ThermoFisher.CommonCore.Data.FilterEnums;
using ThermoFisher.CommonCore.Data.Interfaces;
using ThermoFisher.CommonCore.RawFileReader;

namespace CombineBottomUpTopDown.Parser
{
    public class ParserRAWThermo
    {
        private const bool ALWAYS_USE_PRECURSOR_CHARGE_STATE_RANGE = false;
        private const double HYDROGEN_MASS = 1.00782503214;
        private int maximumNumberOfPeaks = 400;
        private double relativeThresholdPercent = 0.01;
        private int absoluteThreshold = 10;
        private int minimumAssumedPrecursorChargeState = 2;
        private int maximumAssumedPrecursorChargeState = 4;

        /// <summary>
        /// Method responsible for parsing RAW file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="MsnLevel"></param>
        /// <returns></returns>
        public List<TandemMassSpectrum> ParseFile(string filename, List<int> ScanNumbers = null, int MsnLevel = 2)
        {
            List<TandemMassSpectrum> tmsList = null;
            try
            {
                if (string.IsNullOrEmpty(filename))
                {
                    Console.WriteLine("ERROR: No RAW file specified!");

                    return new List<TandemMassSpectrum>();
                }

                // Check to see if the specified RAW file exists
                if (!File.Exists(filename))
                {
                    Console.WriteLine(@" ERROR: The file doesn't exist in the specified location - " + filename);

                    return new List<TandemMassSpectrum>();
                }

                // Create the IRawDataPlus object for accessing the RAW file
                var rawFile = RawFileReaderAdapter.FileFactory(filename);

                if (!rawFile.IsOpen || rawFile.IsError)
                {
                    Console.WriteLine(" ERROR: Unable to access the RAW file: " + filename + " using the RawFileReader class!");

                    return new List<TandemMassSpectrum>();
                }

                // Check for any errors in the RAW file
                if (rawFile.IsError)
                {
                    Console.WriteLine(" ERROR: Error opening ({0}) - {1}", rawFile.FileError, filename);

                    return new List<TandemMassSpectrum>();
                }

                // Check if the RAW file is being acquired
                if (rawFile.InAcquisition)
                {
                    Console.WriteLine(" ERROR: RAW file still being acquired - " + filename);

                    return new List<TandemMassSpectrum>();
                }

                // Get the number of instruments (controllers) present in the RAW file and set the 
                // selected instrument to the MS instrument, first instance of it
                rawFile.SelectInstrument(Device.MS, 1);

                // Get the first and last scan from the RAW file
                int firstScanNumber = rawFile.RunHeaderEx.FirstSpectrum;
                int lastScanNumber = rawFile.RunHeaderEx.LastSpectrum;

                // Read each spectrum
                tmsList = ReadAllSpectra(rawFile, firstScanNumber, lastScanNumber, ScanNumbers, MsnLevel);
            }
            catch (Exception ex)
            {
                Console.WriteLine(" ERROR: " + ex.Message);
            }
            return tmsList;
        }

        /// <summary>
        /// Method responsible for reading all spectra
        /// </summary>
        /// <param name="rawFile"></param>
        /// <param name="firstScanNumber"></param>
        /// <param name="lastScanNumber"></param>
        /// <param name="MsnLevel"></param>
        /// <returns></returns>
        [HandleProcessCorruptedStateExceptions]
        private List<TandemMassSpectrum> ReadAllSpectra(IRawDataPlus rawFile, int firstScanNumber, int lastScanNumber, List<int> ScanNumbers = null, int MsnLevel = 2)
        {
            object progress_lock = new object();
            int spectra_processed = 0;
            int old_progress = 0;

            FileInfo fileNameInfo = new FileInfo(rawFile.FileName);
            string fileName = fileNameInfo.Name.Substring(0, fileNameInfo.Name.Length - 4);

            //We will only retrieve certain scan numbers
            if (ScanNumbers == null)
            {
                ScanNumbers = new List<int>(lastScanNumber + 1 - firstScanNumber);
                for (int scan_number = firstScanNumber; scan_number <= lastScanNumber + 1; scan_number++)
                    ScanNumbers.Add(scan_number);
                ScanNumbers.Sort();
            }

            double lengthFile = ScanNumbers.Count;

            List<TandemMassSpectrum> tmsList = new List<TandemMassSpectrum>();
            Parallel.ForEach(ScanNumbers,
                new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount },
                scanNumber =>
                {
                    //foreach (int scanNumber in ScanNumbers)
                    //{
                    try
                    {
                        // Get the scan filter for the spectrum
                        var scanFilter = rawFile.GetFilterForScanNumber(scanNumber);

                        if (!string.IsNullOrEmpty(scanFilter.ToString()) &&
                        (scanFilter.MSOrder == MSOrderType.Ms && MsnLevel == 1 ||
                        scanFilter.MSOrder == MSOrderType.Ms2 && MsnLevel == 2 ||
                        scanFilter.MSOrder == MSOrderType.Ms3 && MsnLevel == 3))
                        {

                            // Get the retention time for this scan number.  This is one of two comparable functions that will
                            // convert between retention time and scan number.
                            double retention_time = rawFile.RetentionTimeFromScanNumber(scanNumber);

                            // Get the scan event for this scan number
                            var scanEvent = rawFile.GetScanEventForScanNumber(scanNumber);

                            double ionInjectionTime = 0.0;
                            double monoIsotopicMass = 0.0;

                            double precursor_mz = 0.0;
                            // Get the reaction information for the first precursor
                            try
                            {
                                var reaction = scanEvent.GetReaction(0);
                                precursor_mz = reaction.PrecursorMass;
                            }
                            catch (Exception) { }
                            int chargeState = 0;

                            // Get the trailer extra data for this scan and then look for the monoisotopic m/z value in the 
                            // trailer extra data list
                            var trailerData = rawFile.GetTrailerExtraInformation(scanNumber);

                            for (int i = 0; i < trailerData.Length; i++)
                            {
                                if (trailerData.Labels[i] == "Monoisotopic M/Z:")
                                    monoIsotopicMass = Convert.ToDouble(trailerData.Values[i]);
                                else if (trailerData.Labels[i] == "Charge State:")
                                    chargeState = Convert.ToInt32(trailerData.Values[i]);
                                else if (trailerData.Labels[i] == "Ion Injection Time (ms):")
                                    ionInjectionTime = Convert.ToDouble(trailerData.Values[i]);
                            }

                            monoIsotopicMass = monoIsotopicMass == 0 ? precursor_mz : monoIsotopicMass;

                            // Get a spectrum from the RAW file.  
                            List<(double, double)> peaksTuple = GetFragmentIons(rawFile, scanNumber, scanFilter.ToString());
                            List<Ion> peaks = (from peak in peaksTuple
                                               select new Ion(peak.Item1, peak.Item2)).ToList();

                            peaks = FilterPeaks(peaks, absoluteThreshold, relativeThresholdPercent, maximumNumberOfPeaks);
                            (int, double) Precursor = (0, 0);

                            if (peaks.Count > 0)
                            {
                                List<Precursor> precursorList = new List<Precursor>();
                                (double, double, int) precursorIntensityMass = (0.0, 0.0, 0);

                                if (scanEvent.MSOrder != MSOrderType.Ms)
                                {
                                    precursorIntensityMass = GetPrecursorIntensityScanNumber(rawFile, scanNumber, monoIsotopicMass, retention_time);
                                    monoIsotopicMass = precursorIntensityMass.Item2;

                                    for (int charge = (ALWAYS_USE_PRECURSOR_CHARGE_STATE_RANGE || chargeState == 0 ? minimumAssumedPrecursorChargeState : chargeState);
                                    charge <= (ALWAYS_USE_PRECURSOR_CHARGE_STATE_RANGE || chargeState == 0 ? maximumAssumedPrecursorChargeState : chargeState); charge++)
                                    {
                                        double precursorMass = (precursor_mz * charge) - ((charge - 1) * HYDROGEN_MASS);
                                        Precursor = (charge, precursorMass);
                                    }

                                }
                                else
                                    chargeState = chargeState == 0 ? 1 : chargeState;

                                peaks.Sort((a, b) => a.MZ.CompareTo(b.MZ));

                                Model.ActivationType activation = GetFragmentationMethod(scanFilter.ToString());

                                List<double> masses = (from peak in peaks
                                                       select peak.MZ).ToList();

                                List<double> intensities = (from peak in peaks
                                                            select peak.Intensity).ToList();


                                TandemMassSpectrum spectrum = new TandemMassSpectrum(
                                    scanNumber,
                                    retention_time,
                                    activation,
                                    Precursor.ToTuple(),
                                    masses,
                                    intensities,
                                    fileName,
                                    chargeState,
                                    precursorIntensityMass.Item3
                                    );

                                lock (tmsList)
                                {
                                    tmsList.Add(spectrum);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(" Error reading spectrum {0} - {1}", scanNumber, ex.Message);
                    }

                    lock (progress_lock)
                    {
                        spectra_processed++;
                        int new_progress = (int)((double)spectra_processed / (lengthFile) * 100);
                        //int new_progress = (int)((double)spectra_processed / (lastScanNumber - firstScanNumber + 1) * 100);
                        if (new_progress > old_progress)
                        {
                            old_progress = new_progress;
                            Console.Write(" Reading RAW File: " + old_progress + "%");
                        }

                    }
                }
        );
            tmsList.Sort((a, b) => a.ScanNumber.CompareTo(b.ScanNumber));
            return tmsList;
        }

        /// <summary>
        /// Method responsible for getting precursor intensity, precursor mass and precursor scan number
        /// </summary>
        /// <param name="rawFile"></param>
        /// <param name="scanNumber"></param>
        /// <param name="precursorMass"></param>
        /// <param name="retentionTime"></param>
        /// <returns></returns>
        private (double, double, int) GetPrecursorIntensityScanNumber(IRawDataPlus rawFile, int scanNumber, double precursorMass, double retentionTime)
        {
            scanNumber--;
            int precursor_scanNumber = -1;
            double intensity = -1;
            while (scanNumber >= 1)
            {
                var scan_filter = rawFile.GetFilterForScanNumber(scanNumber);
                if (scan_filter.MSOrder == MSOrderType.Ms)
                {
                    precursor_scanNumber = scanNumber;
                    (intensity, precursorMass) = GetPrecursorIntensity(rawFile, precursor_scanNumber, precursorMass, retentionTime);
                    break;
                }
                else
                    scanNumber--;
            }

            return (intensity, precursorMass, precursor_scanNumber);
        }

        /// <summary>
        /// Method responsible for getting the precursor intensity
        /// </summary>
        /// <param name="rawFile"></param>
        /// <param name="scanNumber"></param>
        /// <param name="precursorMass"></param>
        /// <param name="retentionTime"></param>
        /// <returns></returns>
        private (double, double) GetPrecursorIntensity(IRawDataPlus rawFile, int scanNumber, double precursorMass, double retentionTime)
        {
            double precursorIntensity = 0;

            // Get the scan from the RAW file
            var scan = Scan.FromFile(rawFile, scanNumber);

            // Get the scan statistics from the RAW file for this scan number
            var scanStatistics = rawFile.GetScanStatsForScanNumber(scanNumber);

            var centroidStream = rawFile.GetCentroidStream(scanNumber, false);
            if (scan.CentroidScan.Length > 0)
            {
                for (var i = 0; i < centroidStream.Length; i++)
                {
                    if (Math.Abs(precursorMass - centroidStream.Masses[i]) < 0.01)
                    {
                        precursorMass = centroidStream.Masses[i];
                        precursorIntensity = centroidStream.Intensities[i];
                        break;
                    }
                }
            }

            return (precursorIntensity, precursorMass);
        }

        /// <summary>
        /// Method responsible for getting fragment ions
        /// </summary>
        /// <param name="rawFile"></param>
        /// <param name="scanNumber"></param>
        /// <param name="scanFilter"></param>
        /// <returns></returns>
        private List<(double, double)> GetFragmentIons(IRawDataPlus rawFile, int scanNumber, string scanFilter)
        {
            //List<(mz,intensity)>
            List<(double, double)> peaks = new List<(double, double)>();
            // Check for a valid scan filter
            if (string.IsNullOrEmpty(scanFilter))
            {
                return peaks;
            }

            // Get the scan statistics from the RAW file for this scan number
            var scanStatistics = rawFile.GetScanStatsForScanNumber(scanNumber);

            // Check to see if the scan has centroid data or profile data.  Depending upon the
            // type of data, different methods will be used to read the data.  While the ReadAllSpectra
            // method demonstrates reading the data using the Scan.FromFile method, generating the
            // Scan object takes more time and memory to do, so that method isn't optimum.
            //if (scanStatistics.IsCentroidScan)
            //{
            // Get the centroid (label) data from the RAW file for this scan
            var centroidStream = rawFile.GetCentroidStream(scanNumber, false);

            // Add the spectral data (mass, intensity, charge values).  Not all of the information in the high resolution centroid 
            // (label data) object is reported in this example.  Please check the documentation for more information about what is
            // available in high resolution centroid (label) data.

            if (centroidStream.Length > 0)
            {
                for (int i = 0; i < centroidStream.Length; i++)
                {
                    if (centroidStream.Intensities[i] > 0)
                        peaks.Add((centroidStream.Masses[i], centroidStream.Intensities[i]));
                }
            }
            else
            {
                // Get the segmented (low res and profile) scan data
                var segmentedScan = rawFile.GetSegmentedScanFromScanNumber(scanNumber, scanStatistics);

                // Add the spectral data (mass, intensity values)
                for (int i = 0; i < segmentedScan.Positions.Length; i++)
                {
                    if (segmentedScan.Intensities[i] > 0)
                        peaks.Add((segmentedScan.Positions[i], segmentedScan.Intensities[i]));
                }
            }
            //}
            //else
            //{
            //// Get the segmented (low res and profile) scan data
            //var segmentedScan = rawFile.GetSegmentedScanFromScanNumber(scanNumber, scanStatistics);

            //// Add the spectral data (mass, intensity values)
            //for (int i = 0; i < segmentedScan.Positions.Length; i++)
            //{
            //    if (segmentedScan.Intensities[i] > 0)
            //        peaks.Add((segmentedScan.Positions[i], segmentedScan.Intensities[i]));
            //}
            //}

            return peaks;
        }

        private Model.ActivationType GetFragmentationMethod(string scanFilter)
        {
            if (scanFilter.Contains("cid"))
            {
                return Model.ActivationType.CID;
            }
            else if (scanFilter.Contains("mpd"))
            {
                return Model.ActivationType.MPD;
            }
            else if (scanFilter.Contains("pqd"))
            {
                return Model.ActivationType.PQD;
            }
            else if (scanFilter.Contains("hcd") && scanFilter.Contains("etd"))
            {
                return Model.ActivationType.EThCD;
            }
            else if (scanFilter.Contains("hcd"))
            {
                return Model.ActivationType.HCD;
            }
            else if (scanFilter.Contains("ecd"))
            {
                return Model.ActivationType.ECD;
            }
            else if (scanFilter.Contains("etd"))
            {
                return Model.ActivationType.ETD;
            }
            else
            {
                //not found
                return Model.ActivationType.NF;
            }
        }

        private List<Ion> FilterPeaks(List<Ion> peaks, double absoluteThreshold, double relativeThresholdPercent, int maximumNumberOfPeaks)
        {
            List<Ion> filtered_peaks = new List<Ion>(peaks);

            double relative_threshold = -1.0;
            if (relativeThresholdPercent > 0.0)
            {
                double max_intensity = -1.0;
                foreach (Ion peak in filtered_peaks)
                {
                    double intensity = peak.Intensity;
                    if (intensity > max_intensity)
                    {
                        max_intensity = intensity;
                    }
                }
                relative_threshold = max_intensity * relativeThresholdPercent / 100.0;
            }

            double threshold = Math.Max(absoluteThreshold, relative_threshold);

            int p = 0;
            while (p < filtered_peaks.Count)
            {
                Ion peak = filtered_peaks[p];
                if (peak.Intensity < threshold)
                {
                    filtered_peaks.RemoveAt(p);
                }
                else
                {
                    p++;
                }
            }

            if (maximumNumberOfPeaks > 0 && filtered_peaks.Count > maximumNumberOfPeaks)
            {
                filtered_peaks.Sort(Ion.DescendingIntensityComparison);
                filtered_peaks.RemoveRange(maximumNumberOfPeaks, filtered_peaks.Count - maximumNumberOfPeaks);
            }

            return filtered_peaks;
        }
    }

    internal class Ion
    {
        public double MZ { get; set; }
        public double Intensity { get; set; }
        public int Charge { get; private set; }

        public Ion() { }
        public Ion(double intensity) { Intensity = intensity; }
        public Ion(double mz, double intensity)
        {
            MZ = mz;
            Intensity = intensity;
        }

        public Ion(double mz, double intensity, int charge)
        {
            MZ = mz;
            Intensity = intensity;
            Charge = charge;
        }

        public static int DescendingIntensityComparison(Ion left, Ion right)
        {
            return -(left.Intensity.CompareTo(right.Intensity));
        }
    }
}
