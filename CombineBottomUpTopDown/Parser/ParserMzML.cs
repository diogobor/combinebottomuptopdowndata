﻿/**
 * Program:     ProteoCombiner - Integrating bottom-up & top-down proteomics data
 * Author:      Diogo Borges Lima
 * Created:     5//2020
 * Update by:   Diogo Borges Lima
 * Description: Class responsable for reading mzML files
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSMSL.IO.MzML;
using CSMSL.Spectral;
using CombineBottomUpTopDown.Model;

namespace CombineBottomUpTopDown.Parser
{
    public class ParserMzML
    {
        private const double HYDROGEN_MASS = 1.00782503214;

        public ParserMzML() { }

        /// <summary>
        /// Method responsible for parsing mzML File.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public List<TandemMassSpectrum> ParseFile(String fileName, List<int> ScanNumbers = null, int MsnLevel = 2)
        {
            List<TandemMassSpectrum> myMassSpectra = new List<TandemMassSpectrum>();

            try
            {
                //Call CSMSL module for reading mzML file
                Mzml mm = new Mzml(fileName);
                mm.Open();
                Model.ActivationType activationType = ActivationType.CID;

                int spectra_processed = 0;
                int old_progress = 0;
                double lengthFile = mm.Where(scan => scan.MsnOrder > 1).Count();

                //Convert MSDataScan to TandemMassSpectrum
                foreach (MSDataScan scan in mm.Where(scan => scan.MsnOrder == MsnLevel))
                {
                    int new_progress = (int)((double)spectra_processed / (lengthFile) * 100);
                    if (new_progress > old_progress)
                    {
                        old_progress = new_progress;
                        Console.Write(" Reading mzML File: " + old_progress + "%");
                    }

                    if (ScanNumbers != null && !ScanNumbers.Contains(scan.SpectrumNumber))
                    {
                        spectra_processed++;
                        continue;
                    }

                    MZSpectrum ms = scan.MassSpectrum;
                    Spectrum<MZPeak, MZSpectrum> spec = ms;
                    double[] masses = spec.GetMasses();
                    double[] intensities = spec.GetIntensities();
                    List<Ion> ionsList = new List<Ion>();
                    for (int i = 0; i < masses.Count(); i++)
                    {
                        ionsList.Add(new Ion(masses[i], intensities[i]));
                    }
                    if (mm.GetDissociationType(scan.SpectrumNumber) == CSMSL.Proteomics.DissociationType.CID)
                        activationType = ActivationType.CID;
                    else if (mm.GetDissociationType(scan.SpectrumNumber) == CSMSL.Proteomics.DissociationType.ETD)
                        activationType = ActivationType.ETD;
                    else if (mm.GetDissociationType(scan.SpectrumNumber) == CSMSL.Proteomics.DissociationType.ECD)
                        activationType = ActivationType.ECD;
                    else if (mm.GetDissociationType(scan.SpectrumNumber) == CSMSL.Proteomics.DissociationType.HCD)
                        activationType = ActivationType.HCD;
                    else if (mm.GetDissociationType(scan.SpectrumNumber) == CSMSL.Proteomics.DissociationType.MPD)
                        activationType = ActivationType.MPD;
                    else if (mm.GetDissociationType(scan.SpectrumNumber) == CSMSL.Proteomics.DissociationType.NPTR)
                        activationType = ActivationType.NF;
                    else if (mm.GetDissociationType(scan.SpectrumNumber) == CSMSL.Proteomics.DissociationType.PQD)
                        activationType = ActivationType.PQD;

                    (int, double) precursor = (0, 0);
                    int precursorCharge = mm.GetPrecusorCharge(scan.SpectrumNumber);
                    double precursorMass = (mm.GetPrecusorMz(scan.SpectrumNumber) * precursorCharge) - ((precursorCharge - 1) * HYDROGEN_MASS);
                    precursor = (precursorCharge, precursorMass);
                    TandemMassSpectrum experimentalMZML = new TandemMassSpectrum(
                                        scan.SpectrumNumber,
                                        scan.RetentionTime,
                                        activationType,
                                        precursor.ToTuple(),
                                        masses.ToList(),
                                        intensities.ToList(),
                                        fileName,
                                        precursorCharge,
                                        0
                                        );

                    myMassSpectra.Add(experimentalMZML);
                    spectra_processed++;
                }
                myMassSpectra.RemoveAll(a => a.MassesFragIons.Count == 0);

            }
            catch (Exception ex)
            {
                Console.WriteLine(" ERROR: It's not possible to read raw file.");
            }
            return myMassSpectra;
        }
    }
}
