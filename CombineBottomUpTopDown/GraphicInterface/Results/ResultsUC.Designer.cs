﻿namespace CombineBottomUpTopDown.GraphicInterface.Results
{
    partial class ResultsUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ResultsUC));
            this.groupBoxFilter = new System.Windows.Forms.GroupBox();
            this.checkBoxPeptideUnique = new System.Windows.Forms.CheckBox();
            this.numericUpDownPeptideCount = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.buttonReturnWindow = new System.Windows.Forms.Button();
            this.numericUpDownSpectralCount = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.labelVersionValue = new System.Windows.Forms.Label();
            this.labelFileVersionText = new System.Windows.Forms.Label();
            this.buttonResetFilter = new System.Windows.Forms.Button();
            this.buttonFilterResult = new System.Windows.Forms.Button();
            this.textBoxSearchSeqFilter = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.numericUpDownCombScore = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControlResults = new System.Windows.Forms.TabControl();
            this.tabPageCombineResults = new System.Windows.Forms.TabPage();
            this.dataGridViewCombineResultsPeptide = new System.Windows.Forms.DataGridView();
            this.dataGridViewCombineResultsProteoform = new System.Windows.Forms.DataGridView();
            this.dataGridViewCombineResults = new System.Windows.Forms.DataGridView();
            this.tabPageBottomUp = new System.Windows.Forms.TabPage();
            this.dataGridViewBottomUpSpec = new System.Windows.Forms.DataGridView();
            this.dataGridViewBottomUpPept = new System.Windows.Forms.DataGridView();
            this.dataGridViewBottomUpPG = new System.Windows.Forms.DataGridView();
            this.tabPageTopDown = new System.Windows.Forms.TabPage();
            this.dataGridViewTopDown = new System.Windows.Forms.DataGridView();
            this.tabPageInformation = new System.Windows.Forms.TabPage();
            this.dataGridViewInformation = new System.Windows.Forms.DataGridView();
            this.dataGridViewGrouperProteoform = new Subro.Controls.DataGridViewGrouper(this.components);
            this.timerUpdateCombineResults = new System.Windows.Forms.Timer(this.components);
            this.groupBoxFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPeptideCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpectralCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCombScore)).BeginInit();
            this.tabControlResults.SuspendLayout();
            this.tabPageCombineResults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCombineResultsPeptide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCombineResultsProteoform)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCombineResults)).BeginInit();
            this.tabPageBottomUp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBottomUpSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBottomUpPept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBottomUpPG)).BeginInit();
            this.tabPageTopDown.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTopDown)).BeginInit();
            this.tabPageInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewInformation)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxFilter
            // 
            this.groupBoxFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxFilter.Controls.Add(this.checkBoxPeptideUnique);
            this.groupBoxFilter.Controls.Add(this.numericUpDownPeptideCount);
            this.groupBoxFilter.Controls.Add(this.label7);
            this.groupBoxFilter.Controls.Add(this.buttonReturnWindow);
            this.groupBoxFilter.Controls.Add(this.numericUpDownSpectralCount);
            this.groupBoxFilter.Controls.Add(this.label8);
            this.groupBoxFilter.Controls.Add(this.labelVersionValue);
            this.groupBoxFilter.Controls.Add(this.labelFileVersionText);
            this.groupBoxFilter.Controls.Add(this.buttonResetFilter);
            this.groupBoxFilter.Controls.Add(this.buttonFilterResult);
            this.groupBoxFilter.Controls.Add(this.textBoxSearchSeqFilter);
            this.groupBoxFilter.Controls.Add(this.label5);
            this.groupBoxFilter.Controls.Add(this.numericUpDownCombScore);
            this.groupBoxFilter.Controls.Add(this.label3);
            this.groupBoxFilter.Enabled = false;
            this.groupBoxFilter.Location = new System.Drawing.Point(3, 3);
            this.groupBoxFilter.Name = "groupBoxFilter";
            this.groupBoxFilter.Size = new System.Drawing.Size(867, 103);
            this.groupBoxFilter.TabIndex = 1;
            this.groupBoxFilter.TabStop = false;
            // 
            // checkBoxPeptideUnique
            // 
            this.checkBoxPeptideUnique.AutoSize = true;
            this.checkBoxPeptideUnique.Location = new System.Drawing.Point(369, 17);
            this.checkBoxPeptideUnique.Name = "checkBoxPeptideUnique";
            this.checkBoxPeptideUnique.Size = new System.Drawing.Size(98, 17);
            this.checkBoxPeptideUnique.TabIndex = 76;
            this.checkBoxPeptideUnique.Text = "Unique peptide";
            this.checkBoxPeptideUnique.UseVisualStyleBackColor = true;
            // 
            // numericUpDownPeptideCount
            // 
            this.numericUpDownPeptideCount.Location = new System.Drawing.Point(220, 14);
            this.numericUpDownPeptideCount.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDownPeptideCount.Name = "numericUpDownPeptideCount";
            this.numericUpDownPeptideCount.Size = new System.Drawing.Size(35, 20);
            this.numericUpDownPeptideCount.TabIndex = 74;
            this.numericUpDownPeptideCount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numericUpDownPeptideCount_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(141, 18);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 75;
            this.label7.Text = "Peptide Count:";
            // 
            // buttonReturnWindow
            // 
            this.buttonReturnWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReturnWindow.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonReturnWindow.Image = ((System.Drawing.Image)(resources.GetObject("buttonReturnWindow.Image")));
            this.buttonReturnWindow.Location = new System.Drawing.Point(787, 44);
            this.buttonReturnWindow.Name = "buttonReturnWindow";
            this.buttonReturnWindow.Size = new System.Drawing.Size(75, 27);
            this.buttonReturnWindow.TabIndex = 16;
            this.buttonReturnWindow.Text = "Return";
            this.buttonReturnWindow.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonReturnWindow.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonReturnWindow.UseVisualStyleBackColor = false;
            this.buttonReturnWindow.Click += new System.EventHandler(this.buttonReturnWindow_Click);
            // 
            // numericUpDownSpectralCount
            // 
            this.numericUpDownSpectralCount.Location = new System.Drawing.Point(327, 14);
            this.numericUpDownSpectralCount.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDownSpectralCount.Name = "numericUpDownSpectralCount";
            this.numericUpDownSpectralCount.Size = new System.Drawing.Size(35, 20);
            this.numericUpDownSpectralCount.TabIndex = 5;
            this.numericUpDownSpectralCount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numericUpDownSpectralCount_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(258, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "Spec Count:";
            // 
            // labelVersionValue
            // 
            this.labelVersionValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVersionValue.AutoSize = true;
            this.labelVersionValue.Location = new System.Drawing.Point(788, 78);
            this.labelVersionValue.Name = "labelVersionValue";
            this.labelVersionValue.Size = new System.Drawing.Size(13, 13);
            this.labelVersionValue.TabIndex = 13;
            this.labelVersionValue.Text = "?";
            // 
            // labelFileVersionText
            // 
            this.labelFileVersionText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelFileVersionText.AutoSize = true;
            this.labelFileVersionText.Location = new System.Drawing.Point(719, 78);
            this.labelFileVersionText.Name = "labelFileVersionText";
            this.labelFileVersionText.Size = new System.Drawing.Size(63, 13);
            this.labelFileVersionText.TabIndex = 11;
            this.labelFileVersionText.Text = "File version:";
            // 
            // buttonResetFilter
            // 
            this.buttonResetFilter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonResetFilter.Location = new System.Drawing.Point(9, 44);
            this.buttonResetFilter.Name = "buttonResetFilter";
            this.buttonResetFilter.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.buttonResetFilter.Size = new System.Drawing.Size(72, 27);
            this.buttonResetFilter.TabIndex = 14;
            this.buttonResetFilter.Text = "Reset";
            this.buttonResetFilter.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.buttonResetFilter.UseVisualStyleBackColor = false;
            this.buttonResetFilter.Click += new System.EventHandler(this.buttonResetFilter_Click);
            // 
            // buttonFilterResult
            // 
            this.buttonFilterResult.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonFilterResult.Image = ((System.Drawing.Image)(resources.GetObject("buttonFilterResult.Image")));
            this.buttonFilterResult.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonFilterResult.Location = new System.Drawing.Point(87, 44);
            this.buttonFilterResult.Name = "buttonFilterResult";
            this.buttonFilterResult.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.buttonFilterResult.Size = new System.Drawing.Size(694, 27);
            this.buttonFilterResult.TabIndex = 15;
            this.buttonFilterResult.Text = "Filter";
            this.buttonFilterResult.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonFilterResult.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.buttonFilterResult.UseVisualStyleBackColor = false;
            this.buttonFilterResult.Click += new System.EventHandler(this.buttonFilterResult_Click);
            // 
            // textBoxSearchSeqFilter
            // 
            this.textBoxSearchSeqFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSearchSeqFilter.Location = new System.Drawing.Point(519, 15);
            this.textBoxSearchSeqFilter.Name = "textBoxSearchSeqFilter";
            this.textBoxSearchSeqFilter.Size = new System.Drawing.Size(342, 20);
            this.textBoxSearchSeqFilter.TabIndex = 6;
            this.textBoxSearchSeqFilter.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxSearchSeqFilter_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(469, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Search:";
            // 
            // numericUpDownCombScore
            // 
            this.numericUpDownCombScore.DecimalPlaces = 3;
            this.numericUpDownCombScore.Location = new System.Drawing.Point(75, 14);
            this.numericUpDownCombScore.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownCombScore.Name = "numericUpDownCombScore";
            this.numericUpDownCombScore.Size = new System.Drawing.Size(61, 20);
            this.numericUpDownCombScore.TabIndex = 2;
            this.numericUpDownCombScore.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numericUpDownScoreFilterBU_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "CombScore:";
            // 
            // tabControlResults
            // 
            this.tabControlResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlResults.Controls.Add(this.tabPageCombineResults);
            this.tabControlResults.Controls.Add(this.tabPageBottomUp);
            this.tabControlResults.Controls.Add(this.tabPageTopDown);
            this.tabControlResults.Controls.Add(this.tabPageInformation);
            this.tabControlResults.Location = new System.Drawing.Point(3, 112);
            this.tabControlResults.Name = "tabControlResults";
            this.tabControlResults.SelectedIndex = 0;
            this.tabControlResults.Size = new System.Drawing.Size(867, 532);
            this.tabControlResults.TabIndex = 2;
            // 
            // tabPageCombineResults
            // 
            this.tabPageCombineResults.Controls.Add(this.dataGridViewCombineResultsPeptide);
            this.tabPageCombineResults.Controls.Add(this.dataGridViewCombineResultsProteoform);
            this.tabPageCombineResults.Controls.Add(this.dataGridViewCombineResults);
            this.tabPageCombineResults.Location = new System.Drawing.Point(4, 22);
            this.tabPageCombineResults.Name = "tabPageCombineResults";
            this.tabPageCombineResults.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCombineResults.Size = new System.Drawing.Size(859, 506);
            this.tabPageCombineResults.TabIndex = 0;
            this.tabPageCombineResults.Text = "Combine results";
            this.tabPageCombineResults.UseVisualStyleBackColor = true;
            // 
            // dataGridViewCombineResultsPeptide
            // 
            this.dataGridViewCombineResultsPeptide.AllowUserToAddRows = false;
            this.dataGridViewCombineResultsPeptide.AllowUserToDeleteRows = false;
            this.dataGridViewCombineResultsPeptide.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewCombineResultsPeptide.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCombineResultsPeptide.Location = new System.Drawing.Point(3, 353);
            this.dataGridViewCombineResultsPeptide.MultiSelect = false;
            this.dataGridViewCombineResultsPeptide.Name = "dataGridViewCombineResultsPeptide";
            this.dataGridViewCombineResultsPeptide.ReadOnly = true;
            this.dataGridViewCombineResultsPeptide.RowHeadersVisible = false;
            this.dataGridViewCombineResultsPeptide.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewCombineResultsPeptide.Size = new System.Drawing.Size(853, 147);
            this.dataGridViewCombineResultsPeptide.TabIndex = 3;
            this.dataGridViewCombineResultsPeptide.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewCombineResultsPeptide_CellDoubleClick);
            this.dataGridViewCombineResultsPeptide.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dataGridViewCombineResultsPeptide_CellPainting);
            // 
            // dataGridViewCombineResultsProteoform
            // 
            this.dataGridViewCombineResultsProteoform.AllowUserToAddRows = false;
            this.dataGridViewCombineResultsProteoform.AllowUserToDeleteRows = false;
            this.dataGridViewCombineResultsProteoform.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewCombineResultsProteoform.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCombineResultsProteoform.Location = new System.Drawing.Point(3, 205);
            this.dataGridViewCombineResultsProteoform.MultiSelect = false;
            this.dataGridViewCombineResultsProteoform.Name = "dataGridViewCombineResultsProteoform";
            this.dataGridViewCombineResultsProteoform.ReadOnly = true;
            this.dataGridViewCombineResultsProteoform.RowHeadersVisible = false;
            this.dataGridViewCombineResultsProteoform.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewCombineResultsProteoform.Size = new System.Drawing.Size(853, 146);
            this.dataGridViewCombineResultsProteoform.TabIndex = 2;
            this.dataGridViewCombineResultsProteoform.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewCombineResultsProteoform_CellClick);
            this.dataGridViewCombineResultsProteoform.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewCombineResultsProteoform_CellContentClick);
            this.dataGridViewCombineResultsProteoform.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewCombineResultsProteoform_CellDoubleClick);
            this.dataGridViewCombineResultsProteoform.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dataGridViewCombineResultsProteoform_CellPainting);
            this.dataGridViewCombineResultsProteoform.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewCombineResultsProteoform_RowEnter);
            // 
            // dataGridViewCombineResults
            // 
            this.dataGridViewCombineResults.AllowUserToAddRows = false;
            this.dataGridViewCombineResults.AllowUserToDeleteRows = false;
            this.dataGridViewCombineResults.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewCombineResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCombineResults.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewCombineResults.MultiSelect = false;
            this.dataGridViewCombineResults.Name = "dataGridViewCombineResults";
            this.dataGridViewCombineResults.ReadOnly = true;
            this.dataGridViewCombineResults.RowHeadersVisible = false;
            this.dataGridViewCombineResults.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewCombineResults.Size = new System.Drawing.Size(853, 199);
            this.dataGridViewCombineResults.TabIndex = 0;
            this.dataGridViewCombineResults.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewCombineResults_CellClick);
            this.dataGridViewCombineResults.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewCombineResults_CellMouseDoubleClick);
            this.dataGridViewCombineResults.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dataGridViewCombineResults_CellPainting);
            this.dataGridViewCombineResults.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewCombineResults_RowEnter);
            // 
            // tabPageBottomUp
            // 
            this.tabPageBottomUp.Controls.Add(this.dataGridViewBottomUpSpec);
            this.tabPageBottomUp.Controls.Add(this.dataGridViewBottomUpPept);
            this.tabPageBottomUp.Controls.Add(this.dataGridViewBottomUpPG);
            this.tabPageBottomUp.Location = new System.Drawing.Point(4, 22);
            this.tabPageBottomUp.Name = "tabPageBottomUp";
            this.tabPageBottomUp.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageBottomUp.Size = new System.Drawing.Size(859, 506);
            this.tabPageBottomUp.TabIndex = 1;
            this.tabPageBottomUp.Text = "Bottom-up results";
            this.tabPageBottomUp.UseVisualStyleBackColor = true;
            // 
            // dataGridViewBottomUpSpec
            // 
            this.dataGridViewBottomUpSpec.AllowUserToAddRows = false;
            this.dataGridViewBottomUpSpec.AllowUserToDeleteRows = false;
            this.dataGridViewBottomUpSpec.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewBottomUpSpec.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBottomUpSpec.Location = new System.Drawing.Point(3, 398);
            this.dataGridViewBottomUpSpec.MultiSelect = false;
            this.dataGridViewBottomUpSpec.Name = "dataGridViewBottomUpSpec";
            this.dataGridViewBottomUpSpec.ReadOnly = true;
            this.dataGridViewBottomUpSpec.RowHeadersVisible = false;
            this.dataGridViewBottomUpSpec.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewBottomUpSpec.Size = new System.Drawing.Size(853, 193);
            this.dataGridViewBottomUpSpec.TabIndex = 2;
            this.dataGridViewBottomUpSpec.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewBottomUpSpec_CellMouseDoubleClick);
            this.dataGridViewBottomUpSpec.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dataGridViewBottomUpSpec_CellPainting);
            // 
            // dataGridViewBottomUpPept
            // 
            this.dataGridViewBottomUpPept.AllowUserToAddRows = false;
            this.dataGridViewBottomUpPept.AllowUserToDeleteRows = false;
            this.dataGridViewBottomUpPept.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewBottomUpPept.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBottomUpPept.Location = new System.Drawing.Point(3, 200);
            this.dataGridViewBottomUpPept.MultiSelect = false;
            this.dataGridViewBottomUpPept.Name = "dataGridViewBottomUpPept";
            this.dataGridViewBottomUpPept.ReadOnly = true;
            this.dataGridViewBottomUpPept.RowHeadersVisible = false;
            this.dataGridViewBottomUpPept.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewBottomUpPept.Size = new System.Drawing.Size(853, 193);
            this.dataGridViewBottomUpPept.TabIndex = 1;
            this.dataGridViewBottomUpPept.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewBottomUpPept_CellClick);
            this.dataGridViewBottomUpPept.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dataGridViewBottomUpPept_CellPainting);
            this.dataGridViewBottomUpPept.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewBottomUpPept_RowEnter);
            // 
            // dataGridViewBottomUpPG
            // 
            this.dataGridViewBottomUpPG.AllowUserToAddRows = false;
            this.dataGridViewBottomUpPG.AllowUserToDeleteRows = false;
            this.dataGridViewBottomUpPG.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewBottomUpPG.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBottomUpPG.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewBottomUpPG.MultiSelect = false;
            this.dataGridViewBottomUpPG.Name = "dataGridViewBottomUpPG";
            this.dataGridViewBottomUpPG.ReadOnly = true;
            this.dataGridViewBottomUpPG.RowHeadersVisible = false;
            this.dataGridViewBottomUpPG.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewBottomUpPG.Size = new System.Drawing.Size(853, 193);
            this.dataGridViewBottomUpPG.TabIndex = 0;
            this.dataGridViewBottomUpPG.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewBottomUpPG_CellClick);
            this.dataGridViewBottomUpPG.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewBottomUpPG_CellMouseDoubleClick);
            this.dataGridViewBottomUpPG.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dataGridViewBottomUpPG_CellPainting);
            this.dataGridViewBottomUpPG.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewBottomUpPG_RowEnter);
            // 
            // tabPageTopDown
            // 
            this.tabPageTopDown.Controls.Add(this.dataGridViewTopDown);
            this.tabPageTopDown.Location = new System.Drawing.Point(4, 22);
            this.tabPageTopDown.Name = "tabPageTopDown";
            this.tabPageTopDown.Size = new System.Drawing.Size(859, 506);
            this.tabPageTopDown.TabIndex = 2;
            this.tabPageTopDown.Text = "Top-down results";
            this.tabPageTopDown.UseVisualStyleBackColor = true;
            // 
            // dataGridViewTopDown
            // 
            this.dataGridViewTopDown.AllowUserToAddRows = false;
            this.dataGridViewTopDown.AllowUserToDeleteRows = false;
            this.dataGridViewTopDown.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTopDown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewTopDown.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewTopDown.MultiSelect = false;
            this.dataGridViewTopDown.Name = "dataGridViewTopDown";
            this.dataGridViewTopDown.ReadOnly = true;
            this.dataGridViewTopDown.RowHeadersVisible = false;
            this.dataGridViewTopDown.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewTopDown.Size = new System.Drawing.Size(859, 506);
            this.dataGridViewTopDown.TabIndex = 0;
            this.dataGridViewTopDown.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewTopDown_CellMouseDoubleClick);
            this.dataGridViewTopDown.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dataGridViewTopDown_CellPainting);
            // 
            // tabPageInformation
            // 
            this.tabPageInformation.Controls.Add(this.dataGridViewInformation);
            this.tabPageInformation.Location = new System.Drawing.Point(4, 22);
            this.tabPageInformation.Name = "tabPageInformation";
            this.tabPageInformation.Size = new System.Drawing.Size(859, 506);
            this.tabPageInformation.TabIndex = 3;
            this.tabPageInformation.Text = "Information";
            this.tabPageInformation.UseVisualStyleBackColor = true;
            // 
            // dataGridViewInformation
            // 
            this.dataGridViewInformation.AllowUserToAddRows = false;
            this.dataGridViewInformation.AllowUserToDeleteRows = false;
            this.dataGridViewInformation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewInformation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewInformation.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewInformation.MultiSelect = false;
            this.dataGridViewInformation.Name = "dataGridViewInformation";
            this.dataGridViewInformation.ReadOnly = true;
            this.dataGridViewInformation.RowHeadersVisible = false;
            this.dataGridViewInformation.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewInformation.Size = new System.Drawing.Size(859, 506);
            this.dataGridViewInformation.TabIndex = 1;
            // 
            // dataGridViewGrouperProteoform
            // 
            this.dataGridViewGrouperProteoform.DataGridView = this.dataGridViewTopDown;
            this.dataGridViewGrouperProteoform.Options = ((Subro.Controls.GroupingOptions)(resources.GetObject("dataGridViewGrouperProteoform.Options")));
            // 
            // timerUpdateCombineResults
            // 
            this.timerUpdateCombineResults.Tick += new System.EventHandler(this.timerUpdateCombineResults_Tick);
            // 
            // ResultsUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControlResults);
            this.Controls.Add(this.groupBoxFilter);
            this.Name = "ResultsUC";
            this.Size = new System.Drawing.Size(873, 647);
            this.groupBoxFilter.ResumeLayout(false);
            this.groupBoxFilter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPeptideCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpectralCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCombScore)).EndInit();
            this.tabControlResults.ResumeLayout(false);
            this.tabPageCombineResults.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCombineResultsPeptide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCombineResultsProteoform)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCombineResults)).EndInit();
            this.tabPageBottomUp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBottomUpSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBottomUpPept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBottomUpPG)).EndInit();
            this.tabPageTopDown.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTopDown)).EndInit();
            this.tabPageInformation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewInformation)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxFilter;
        private System.Windows.Forms.Button buttonReturnWindow;
        private System.Windows.Forms.NumericUpDown numericUpDownSpectralCount;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelVersionValue;
        private System.Windows.Forms.Label labelFileVersionText;
        private System.Windows.Forms.Button buttonResetFilter;
        private System.Windows.Forms.Button buttonFilterResult;
        private System.Windows.Forms.TextBox textBoxSearchSeqFilter;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numericUpDownCombScore;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabControl tabControlResults;
        private System.Windows.Forms.TabPage tabPageCombineResults;
        private System.Windows.Forms.TabPage tabPageBottomUp;
        private System.Windows.Forms.TabPage tabPageTopDown;
        private System.Windows.Forms.DataGridView dataGridViewCombineResults;
        private System.Windows.Forms.DataGridView dataGridViewBottomUpPG;
        private System.Windows.Forms.DataGridView dataGridViewTopDown;
        private System.Windows.Forms.DataGridView dataGridViewBottomUpPept;
        private System.Windows.Forms.DataGridView dataGridViewBottomUpSpec;
        private System.Windows.Forms.CheckBox checkBoxPeptideUnique;
        private System.Windows.Forms.NumericUpDown numericUpDownPeptideCount;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dataGridViewCombineResultsProteoform;
        private Subro.Controls.DataGridViewGrouper dataGridViewGrouperProteoform;
        private System.Windows.Forms.DataGridView dataGridViewCombineResultsPeptide;
        private System.Windows.Forms.TabPage tabPageInformation;
        private System.Windows.Forms.DataGridView dataGridViewInformation;
        private System.Windows.Forms.Timer timerUpdateCombineResults;
    }
}
