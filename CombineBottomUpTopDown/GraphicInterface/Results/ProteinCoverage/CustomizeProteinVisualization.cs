﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CombineBottomUpTopDown.GraphicInterface.Results.ProteinCoverage
{
    public partial class CustomizeProteinVisualization : Form
    {
        public bool IsMultiLineProteinSequence { get; set; }
        public bool SortPeptidesByStartPosition { get; set; }

        public CustomizeProteinVisualization()
        {
            InitializeComponent();
            this.FormClosing += CustomizeProteinVisualization_FormClosing;
        }

        internal void Setup(bool isMultilineProteinSequence, bool sortPeptByStartPos)
        {
            this.customizeProteinVisualizationUC1.Setup(isMultilineProteinSequence, sortPeptByStartPos);
        }

        private void CustomizeProteinVisualization_FormClosing(object sender, FormClosingEventArgs e)
        {
            IsMultiLineProteinSequence = this.customizeProteinVisualizationUC1.IsMultilineProteinSequence;
            SortPeptidesByStartPosition = this.customizeProteinVisualizationUC1.SortPeptidesByStartPosition;
        }
    }
}
