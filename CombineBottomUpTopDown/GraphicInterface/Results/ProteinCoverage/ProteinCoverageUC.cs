﻿/**
 * Program:     ProteoCombiner - Integrating bottom-up & top-down proteomics data
 * Author:      Diogo Borges Lima
 * Created:     4/3/2019
 * Update by:   Diogo Borges Lima
 * Description: Protein Coverage User control
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CombineBottomUpTopDown.Model;
using System.Text.RegularExpressions;
using CombineBottomUpTopDown.Utils;
using CombineBottomUpTopDown.Controller;

namespace CombineBottomUpTopDown.GraphicInterface.Results.ProteinCoverage
{
    public partial class ProteinCoverageUC : UserControl
    {
        private List<Proteoform> MyProteoformList { get; set; }
        private ResidueMass MyResidueMass;
        private System.Drawing.Color[] PTMColorList { get; set; }
        //position in color array of used ptms colors
        private string[] PTMColorArrayTDPList { get; set; }
        private List<(Proteoform, List<(Peptide, List<TandemMassSpectrum>)>)> ValidProteoformAndPeptides { get; set; }

        //ProteinVisualization
        public bool IsMultilineProteinSequence { get; set; }

        //List<proteoform sequence, score, tool, isValid, theoretical mass>
        public List<(string, List<(double, string, bool, double)>)> AnnotationProteoforms { get; set; }

        public ProteinCoverageUC()
        {
            InitializeComponent();

            #region Initialize Color array
            PTMColorList = new Color[51];
            PTMColorList[0] = System.Drawing.Color.Red;
            PTMColorList[1] = System.Drawing.Color.Purple;
            PTMColorList[2] = System.Drawing.Color.Green;
            PTMColorList[3] = System.Drawing.Color.DarkKhaki;
            PTMColorList[4] = System.Drawing.Color.MediumTurquoise;
            PTMColorList[5] = System.Drawing.Color.Brown;
            PTMColorList[6] = System.Drawing.Color.Salmon;
            PTMColorList[7] = System.Drawing.Color.DarkViolet;
            PTMColorList[8] = System.Drawing.Color.Navy;
            PTMColorList[9] = System.Drawing.Color.Gray;
            PTMColorList[10] = System.Drawing.Color.Maroon;
            PTMColorList[11] = System.Drawing.Color.Magenta;
            PTMColorList[12] = System.Drawing.Color.MediumAquamarine;
            PTMColorList[13] = System.Drawing.Color.LightSteelBlue;
            PTMColorList[14] = System.Drawing.Color.BurlyWood;
            PTMColorList[15] = System.Drawing.Color.Beige;
            PTMColorList[16] = System.Drawing.Color.Aqua;
            PTMColorList[17] = System.Drawing.Color.OliveDrab;
            PTMColorList[18] = System.Drawing.Color.OrangeRed;
            PTMColorList[19] = System.Drawing.Color.PaleGreen;
            PTMColorList[20] = System.Drawing.Color.PapayaWhip;
            PTMColorList[21] = System.Drawing.Color.Peru;
            PTMColorList[22] = System.Drawing.Color.Silver;
            PTMColorList[23] = System.Drawing.Color.SeaShell;
            PTMColorList[24] = System.Drawing.Color.SkyBlue;
            PTMColorList[25] = System.Drawing.Color.Turquoise;
            PTMColorList[26] = System.Drawing.Color.Tan;
            PTMColorList[27] = System.Drawing.Color.Teal;
            PTMColorList[28] = System.Drawing.Color.Thistle;
            PTMColorList[29] = System.Drawing.Color.Tomato;
            PTMColorList[30] = System.Drawing.Color.Transparent;
            PTMColorList[31] = System.Drawing.Color.Wheat;
            PTMColorList[32] = System.Drawing.Color.White;
            PTMColorList[33] = System.Drawing.Color.WhiteSmoke;
            PTMColorList[34] = System.Drawing.Color.Yellow;
            PTMColorList[35] = System.Drawing.Color.YellowGreen;
            PTMColorList[36] = System.Drawing.Color.AliceBlue;
            PTMColorList[37] = System.Drawing.Color.AntiqueWhite;
            PTMColorList[38] = System.Drawing.Color.Aqua;
            PTMColorList[39] = System.Drawing.Color.Aquamarine;
            PTMColorList[40] = System.Drawing.Color.Azure;
            PTMColorList[41] = System.Drawing.Color.Beige;
            PTMColorList[42] = System.Drawing.Color.Bisque;
            PTMColorList[43] = System.Drawing.Color.Black;
            PTMColorList[44] = System.Drawing.Color.BlanchedAlmond;
            PTMColorList[45] = System.Drawing.Color.Blue;
            PTMColorList[46] = System.Drawing.Color.BlueViolet;
            PTMColorList[47] = System.Drawing.Color.Brown;
            PTMColorList[48] = System.Drawing.Color.BurlyWood;
            PTMColorList[49] = System.Drawing.Color.CadetBlue;
            PTMColorList[50] = System.Drawing.Color.Chartreuse;
            #endregion
        }

        private void FillCoverageBox(bool isBUP, bool isTDP, List<string> ptms = null)
        {
            //if (ptms.Count > 49)
            //{
            //    Console.WriteLine("ERROR: PTM array is greater than color array");
            //    return;
            //}

            PTMColorArrayTDPList = new string[ptms.Count];

            //add single column
            //-2 => autosize
            listViewCoverage.FullRowSelect = true;
            listViewCoverage.GridLines = true;
            listViewCoverage.View = System.Windows.Forms.View.List;
            listViewCoverage.LabelWrap = true;
            listViewCoverage.AutoArrange = false;
            Font _font = new Font("Microsoft Sans Serif", 8.0f, FontStyle.Bold);

            #region Approaches
            int approachCount = 1;
            if (isBUP)
            {
                ListViewItem lvi = new ListViewItem(approachCount + ". Bottom-up / middle-down");
                lvi.Font = _font;
                lvi.ForeColor = System.Drawing.Color.Blue;
                listViewCoverage.Items.Add(lvi);
                lvi = new ListViewItem("proteomics");
                lvi.Font = _font;
                lvi.ForeColor = System.Drawing.Color.Blue;
                listViewCoverage.Items.Add(lvi);
                approachCount++;
            }

            if (isTDP)
            {
                //Identified modes: 3 - intact proteoform; 6 - Truncated proteoform ; 9- Tagged Proteoform
                ListViewItem lvi2 = new ListViewItem(approachCount + ". Top-down proteomics");
                lvi2.Font = _font;
                lvi2.ForeColor = System.Drawing.Color.DarkOrange;
                listViewCoverage.Items.Add(lvi2);

                ListViewItem lvi3 = new ListViewItem(" " + approachCount + ".1 Expected proteoforms");
                lvi3.Font = _font;
                lvi3.ForeColor = System.Drawing.Color.DarkOrange;
                listViewCoverage.Items.Add(lvi3);

                ListViewItem lvi4 = new ListViewItem(" " + approachCount + ".2 Non-expected proteoforms");
                lvi4.Font = _font;
                lvi4.ForeColor = System.Drawing.Color.DarkCyan;
                listViewCoverage.Items.Add(lvi4);

                ListViewItem lvi5 = new ListViewItem(" " + approachCount + ".3 Tagged proteoforms");
                lvi5.Font = _font;
                lvi5.ForeColor = System.Drawing.Color.Red;
                listViewCoverage.Items.Add(lvi5);
            }
            #endregion

            #region PTMs
            if (ptms != null && ptms.Count > 0)
            {
                listViewCoverage.Items.Add("");
                ListViewItem lviPtms = new ListViewItem("PTMs");
                lviPtms.Font = _font;
                lviPtms.ForeColor = System.Drawing.Color.Black;
                listViewCoverage.Items.Add(lviPtms);

                const int COLUMN_LENGTH = 26;
                int ptmCount = 1;

                foreach (string ptm in ptms)
                {
                    PTMColorArrayTDPList[ptmCount - 1] = ptm;
                    if (ptm.Length < COLUMN_LENGTH)
                    {
                        lviPtms = new ListViewItem(ptmCount + ". " + ptm);
                        lviPtms.Font = _font;
                        lviPtms.ForeColor = PTMColorList[(ptmCount - 1) % 50];
                        listViewCoverage.Items.Add(lviPtms);
                    }
                    else//Split in two
                    {
                        int finalIndexOfSpace = 0;
                        int indexofspace = 0;
                        indexofspace = ptm.IndexOf(' ');
                        if (indexofspace != -1)
                        {
                            while (finalIndexOfSpace < COLUMN_LENGTH)
                            {
                                finalIndexOfSpace = indexofspace;
                                indexofspace = ptm.IndexOf(' ', finalIndexOfSpace);
                                if (finalIndexOfSpace == indexofspace) break;
                            }

                            string newPTMLabel = ptm.Substring(0, finalIndexOfSpace).Trim();
                            lviPtms = new ListViewItem(ptmCount + ". " + newPTMLabel);
                            lviPtms.Font = _font;
                            lviPtms.ForeColor = PTMColorList[(ptmCount - 1) % 50];
                            listViewCoverage.Items.Add(lviPtms);

                            newPTMLabel = ptm.Substring(finalIndexOfSpace, ptm.Length - finalIndexOfSpace).Trim();
                            lviPtms = new ListViewItem(newPTMLabel);
                            lviPtms.Font = _font;
                            lviPtms.ForeColor = PTMColorList[(ptmCount - 1) % 50];
                            listViewCoverage.Items.Add(lviPtms);
                        }
                        else
                        {
                            string newPTMLabel = ptm.Substring(0, COLUMN_LENGTH).Trim();
                            lviPtms = new ListViewItem(ptmCount + ". " + newPTMLabel);
                            lviPtms.Font = _font;
                            lviPtms.ForeColor = PTMColorList[(ptmCount - 1) % 50];
                            listViewCoverage.Items.Add(lviPtms);

                            newPTMLabel = ptm.Substring(COLUMN_LENGTH, ptm.Length - COLUMN_LENGTH).Trim();
                            lviPtms = new ListViewItem(newPTMLabel);
                            lviPtms.Font = _font;
                            lviPtms.ForeColor = PTMColorList[(ptmCount - 1) % 50];
                            listViewCoverage.Items.Add(lviPtms);
                        }
                    }
                    ptmCount++;
                }
            }
            #endregion

        }

        internal void Setup(bool isMultilineProteinSequence, List<ModificationItem> ptmsUniprot, ResidueMass myResiduesMass, List<(Proteoform, List<(Peptide, List<TandemMassSpectrum>)>)> validProteoformAndPeptides, FastaItem fastaItem)
        {
            ValidProteoformAndPeptides = validProteoformAndPeptides;
            IsMultilineProteinSequence = isMultilineProteinSequence;
            MyResidueMass = myResiduesMass;
            if (MyResidueMass == null) return;

            listBoxProteinDescription.Items.Clear();
            listBoxProteinDescription.Items.Add(fastaItem.Description);

            #region Theoretical proteoforms and PTMs
            //Tuple<proteinSequence, List<chain(or signal peptide), start position, end position>, List<modification-> ( description, position )>>
            (string, List<(string, int, int)>, List<(string, int)>) protein =
                (fastaItem.Sequence,
                (from chain in fastaItem.Chains
                 select chain.ToValueTuple()).ToList(),
                (from ptms in fastaItem.TheoreticalPTMs
                 select ptms.ToValueTuple()).ToList());
            #endregion

            if (validProteoformAndPeptides != null && validProteoformAndPeptides.Count > 0)
            {
                #region Check PTMs
                List<ModificationItem> presentPTMs = new List<ModificationItem>();
                List<string> modifications = (from protfm in validProteoformAndPeptides
                                              from mods in protfm.Item1.PrSMs[0].Modifications
                                              select mods.Item3).Distinct().ToList();
                string sequenceMM = validProteoformAndPeptides[0].Item1.Sequence;
                string sequenceMA = validProteoformAndPeptides[0].Item1.Sequence;

                var proteoformAmount = (from prot in validProteoformAndPeptides
                                        select prot.Item1.Sequence).Distinct().ToList();
                if (proteoformAmount.Count != 1)
                    mwLabel.Text = MyResidueMass.CalculatorMonoisotopicMass(fastaItem.SequenceInBytes, true, true).ToString("0.00") + " Da / " + MyResidueMass.CalculatorAverageMass(fastaItem.SequenceInBytes, true, true).ToString("0.00") + " Da";
                else
                {
                    if (modifications != null && modifications.Count > 0)
                    {
                        foreach (string mod in modifications)
                        {
                            foreach (ModificationItem ptmUniprot in ptmsUniprot)
                            {
                                if (ptmUniprot.Description.Contains(mod))
                                {
                                    presentPTMs.Add(ptmUniprot);
                                    break;
                                }
                                else
                                {
                                    string[] cols = Regex.Split(mod, "[-|' ']");
                                    string newPTM = string.Empty;
                                    bool isFound = false;
                                    int countPartPTMsFound = 0;
                                    for (int i = 0; i < cols.Length; i++)
                                    {
                                        newPTM = cols[i];
                                        if (ptmUniprot.Description.ToLower().Contains(newPTM.ToLower()))
                                        {
                                            if (newPTM.ToArray().Length > 1)
                                                countPartPTMsFound++;
                                        }
                                        else if (i == 0) break;
                                        if (countPartPTMsFound > 1)
                                        {
                                            presentPTMs.Add(ptmUniprot);
                                            isFound = true;
                                            break;
                                        }
                                    }
                                    if (isFound) break;
                                }
                            }
                        }

                        StringBuilder sbPTMs = new StringBuilder();
                        foreach (double mass in presentPTMs.Select(item => item.MonoIsotopicMass))
                        {
                            sbPTMs.Append("(");
                            sbPTMs.Append(mass);
                            sbPTMs.Append(")");
                        }
                        sequenceMM = sbPTMs.ToString() + validProteoformAndPeptides[0].Item1.Sequence;

                        sbPTMs = new StringBuilder();
                        foreach (double mass in presentPTMs.Select(item => item.AverageMass))
                        {
                            sbPTMs.Append("(");
                            sbPTMs.Append(mass);
                            sbPTMs.Append(")");
                        }
                        sequenceMA = sbPTMs.ToString() + validProteoformAndPeptides[0].Item1.Sequence;
                    }

                    byte[] sequenceByteMM = System.Text.ASCIIEncoding.Default.GetBytes(sequenceMM);
                    byte[] sequenceByteMA = System.Text.ASCIIEncoding.Default.GetBytes(sequenceMA);
                    mwLabel.Text = MyResidueMass.CalculatorMonoisotopicMass(sequenceByteMM, true, true).ToString("0.00") + " Da / " + MyResidueMass.CalculatorAverageMass(sequenceByteMA, true, true).ToString("0.00") + " Da";

                }
                #endregion
            }
            else
                mwLabel.Text = MyResidueMass.CalculatorMonoisotopicMass(fastaItem.SequenceInBytes, true, true).ToString("0.00") + " Da / " + MyResidueMass.CalculatorAverageMass(fastaItem.SequenceInBytes, true, true).ToString("0.00") + " Da";

            List<string> PtmsDescription = new List<string>();

            //Tuple<proteoform sequence, score, tool, isValid, theoretical mass, modifications( List<Tuple<aminoacid, position, PTMdescription>>), identified node >
            List<(string, List<(double, string, bool, double, List<(string, int, string)>)>, int)> Proteoforms = new List<(string, List<(double, string, bool, double, List<(string, int, string)>)>, int)>();

            //Tuple<peptide, isUnique, List<score, tool>, List<(aminoacid, position, PTMdescription)>>
            List<(string, bool, List<(double, string)>, List<string>, List<string>, List<(string, int, string)>)> Peptides = new List<(string, bool, List<(double, string)>, List<string>, List<string>, List<(string, int, string)>)>();

            if (validProteoformAndPeptides != null && validProteoformAndPeptides.Count > 0)
            {
                validProteoformAndPeptides.ForEach(proteoformPept =>
                    {
                        proteoformPept.Item1.PrSMs = proteoformPept.Item1.PrSMs.Distinct(new SelectBestParamsForEachTool()).ToList();

                        List<(double, string, bool, double, List<(string, int, string)>)> proteoParams = new List<(double, string, bool, double, List<(string, int, string)>)>();
                        int offset = fastaItem.Sequence.IndexOf(proteoformPept.Item1.Sequence);

                        foreach (ProteoformParams param in proteoformPept.Item1.PrSMs)
                        {
                            #region Update ptm position according to the beginning in protein sequence
                            if (offset != -1 && param.Modifications.Count > 0)
                            {
                                var protParamMods = (from mod in param.Modifications
                                                     select (mod.Item1, (mod.Item2 + offset), mod.Item3)).ToList();
                                proteoParams.Add((param.FinalScore, param.SearchEngine, param.IsValid, param.TheoreticalMass, protParamMods));
                            }
                            else
                                proteoParams.Add((param.FinalScore, param.SearchEngine, param.IsValid, param.TheoreticalMass, new List<(string, int, string)>()));
                            #endregion
                        }

                        Proteoforms.Add((proteoformPept.Item1.AnnotatedSequence, proteoParams, proteoformPept.Item1.PrSMs[0].IdentifiedNode));
                        PtmsDescription.AddRange(proteoformPept.Item1.PrSMs.SelectMany(b => b.Modifications.Select(mod => mod.Item3).ToList()));

                        proteoformPept.Item2.ForEach(peptidesTMS =>
                        {
                            if (peptidesTMS.Item2.Count > 0)
                            {
                                peptidesTMS.Item2.ForEach(tms =>
                                {
                                    //Tuple<peptide, isUnique, List<(score, tool>), List<proteoformWithScore>, List<Protein>, List<(aminoacid, position, description)>>
                                    int id = Peptides.FindIndex(item => item.Item1.Equals(tms.ModifiedSequence.Replace("_", "")));
                                    if (id == -1)
                                        Peptides.Add((tms.ModifiedSequence.Replace("_", ""), peptidesTMS.Item1.PSMs[0].IsUnique, (from pept_score in peptidesTMS.Item1.PSMs select (pept_score.NormalizedPeptideScore, pept_score.SearchEngine)).ToList(), new List<string>() { proteoformPept.Item1.AnnotatedSequence + proteoformPept.Item1.PrSMs[0].FinalScore }, proteoformPept.Item2.SelectMany(item => item.Item1.Proteins).Distinct().ToList(), (from spectrum in tms.Modifications select (spectrum.Item1, spectrum.Item2, spectrum.Item3)).ToList()));
                                    else
                                    {
                                        List<string> proteoforms = Peptides[id].Item4;
                                        proteoforms.Add(proteoformPept.Item1.AnnotatedSequence + proteoformPept.Item1.PrSMs[0].FinalScore);
                                        Peptides.RemoveAt(id);
                                        Peptides.Add((tms.ModifiedSequence.Replace("_", ""), peptidesTMS.Item1.PSMs[0].IsUnique, (from pept_score in peptidesTMS.Item1.PSMs select (pept_score.NormalizedPeptideScore, pept_score.SearchEngine)).ToList(), proteoforms, proteoformPept.Item2.SelectMany(item => item.Item1.Proteins).Distinct().ToList(), (from spectrum in tms.Modifications select (spectrum.Item1, spectrum.Item2, spectrum.Item3)).ToList()));
                                    }
                                    PtmsDescription.AddRange(tms.Modifications.Select(desc => desc.Item3).ToList());
                                });
                            }
                            else
                            {
                                Peptides.Add((peptidesTMS.Item1.Sequence, peptidesTMS.Item1.PSMs[0].IsUnique, (from pept_score in peptidesTMS.Item1.PSMs select (pept_score.NormalizedPeptideScore, pept_score.SearchEngine)).ToList(), new List<string>() { proteoformPept.Item1.AnnotatedSequence + proteoformPept.Item1.PrSMs[0].FinalScore }, new List<string>() { proteoformPept.Item1.ProteinAccession }, new List<(string, int, string)>() { ("", -1, "") }));
                            }
                        });
                    });
            }

            Proteoforms = Proteoforms.Distinct().ToList();
            PtmsDescription = PtmsDescription.Distinct().ToList();
            Peptides = Peptides.Distinct().ToList();

            FillCoverageBox(Peptides.Count > 0, Proteoforms != null && Proteoforms.Count > 0, PtmsDescription);
            this.proteinAnnotatorUC1.DrawProteoformsPeptides(!IsMultilineProteinSequence, protein, Proteoforms, Peptides, PTMColorArrayTDPList);
            ProteinCoverageRate(fastaItem.Sequence, Proteoforms, Peptides);

        }

        internal void Setup(bool isMultilineProteinSequence, List<ModificationItem> ptmsUniprot, ResidueMass myResiduesMass, List<Peptide> selectedPeptides, List<Proteoform> proteoformList, FastaItem fastaItem, List<TandemMassSpectrum> selectedMSMS = null)
        {
            #region Clone proteoformList
            if (proteoformList != null && proteoformList.Count > 0)
            {
                MyProteoformList = new List<Proteoform>(proteoformList.Count);
                proteoformList.ForEach(item =>
                {
                    Proteoform proteoform = new Proteoform(item.Sequence, item.ProteinAccession, item.ProteinDescription, item.AnnotatedSequence, item.SequenceCoverage, item.PrSMs);
                    MyProteoformList.Add(proteoform);
                });
            }
            else
                MyProteoformList = new List<Proteoform>(0);
            #endregion

            IsMultilineProteinSequence = isMultilineProteinSequence;
            MyResidueMass = myResiduesMass;
            if (MyResidueMass == null) return;

            listBoxProteinDescription.Items.Clear();
            listBoxProteinDescription.Items.Add(fastaItem.Description);

            #region Theoretical proteoforms and PTMs
            //Tuple<proteinSequence, List<chain(or signal peptide), start position, end position>, List<modification-> ( description, position )>>
            Tuple<string, List<Tuple<string, int, int>>, List<Tuple<string, int>>> protein = Tuple.Create(fastaItem.Sequence, fastaItem.Chains, fastaItem.TheoreticalPTMs);
            #endregion

            if (MyProteoformList != null && MyProteoformList.Count > 0)
            {
                #region Check PTMs
                List<ModificationItem> presentPTMs = new List<ModificationItem>();
                List<string> modifications = (from protfm in MyProteoformList
                                              from mods in protfm.PrSMs[0].Modifications
                                              select mods.Item3).Distinct().ToList();
                string sequenceMM = MyProteoformList[0].Sequence;
                string sequenceMA = MyProteoformList[0].Sequence;

                var proteoformAmount = (from prot in MyProteoformList
                                        select prot.Sequence).Distinct().ToList();
                if (proteoformAmount.Count != 1)
                    mwLabel.Text = MyResidueMass.CalculatorMonoisotopicMass(fastaItem.SequenceInBytes, true, true).ToString("0.00") + " Da / " + MyResidueMass.CalculatorAverageMass(fastaItem.SequenceInBytes, true, true).ToString("0.00") + " Da";
                else
                {
                    if (modifications != null && modifications.Count > 0)
                    {
                        foreach (string mod in modifications)
                        {
                            foreach (ModificationItem ptmUniprot in ptmsUniprot)
                            {
                                if (ptmUniprot.Description.Contains(mod))
                                {
                                    presentPTMs.Add(ptmUniprot);
                                    break;
                                }
                                else
                                {
                                    string[] cols = Regex.Split(mod, "[-|' ']");
                                    string newPTM = string.Empty;
                                    bool isFound = false;
                                    int countPartPTMsFound = 0;
                                    for (int i = 0; i < cols.Length; i++)
                                    {
                                        newPTM = cols[i];
                                        if (ptmUniprot.Description.ToLower().Contains(newPTM.ToLower()))
                                        {
                                            if (newPTM.ToArray().Length > 1)
                                                countPartPTMsFound++;
                                        }
                                        else if (i == 0) break;
                                        if (countPartPTMsFound > 1)
                                        {
                                            presentPTMs.Add(ptmUniprot);
                                            isFound = true;
                                            break;
                                        }
                                    }
                                    if (isFound) break;
                                }
                            }
                        }

                        StringBuilder sbPTMs = new StringBuilder();
                        foreach (double mass in presentPTMs.Select(item => item.MonoIsotopicMass))
                        {
                            sbPTMs.Append("(");
                            sbPTMs.Append(mass);
                            sbPTMs.Append(")");
                        }
                        sequenceMM = sbPTMs.ToString() + MyProteoformList[0].Sequence;

                        sbPTMs = new StringBuilder();
                        foreach (double mass in presentPTMs.Select(item => item.AverageMass))
                        {
                            sbPTMs.Append("(");
                            sbPTMs.Append(mass);
                            sbPTMs.Append(")");
                        }
                        sequenceMA = sbPTMs.ToString() + MyProteoformList[0].Sequence;
                    }

                    byte[] sequenceByteMM = System.Text.ASCIIEncoding.Default.GetBytes(sequenceMM);
                    byte[] sequenceByteMA = System.Text.ASCIIEncoding.Default.GetBytes(sequenceMA);
                    mwLabel.Text = MyResidueMass.CalculatorMonoisotopicMass(sequenceByteMM, true, true).ToString("0.00") + " Da / " + MyResidueMass.CalculatorAverageMass(sequenceByteMA, true, true).ToString("0.00") + " Da";

                }
                #endregion
            }
            else
                mwLabel.Text = MyResidueMass.CalculatorMonoisotopicMass(fastaItem.SequenceInBytes, true, true).ToString("0.00") + " Da / " + MyResidueMass.CalculatorAverageMass(fastaItem.SequenceInBytes, true, true).ToString("0.00") + " Da";
            List<string> PtmsDescription = new List<string>();

            //Tuple<proteoform, score, tool, isValid, theoretical mass, modifications( List<Tuple<aminoacid, position, PTMdescription>, identified node number>) >
            List<Tuple<string, List<Tuple<double, string, bool, double, List<Tuple<string, int, string>>>>, int>> Proteoforms = new List<Tuple<string, List<Tuple<double, string, bool, double, List<Tuple<string, int, string>>>>, int>>();

            //Tuple<peptide, isUnique, score, tool, List<proteoformWithScore>, List<protein>, List<aminoacid, position, PTMdescription>>
            List<Tuple<string, bool, List<Tuple<double, string>>, List<string>, List<string>, List<Tuple<string, int, string>>>> Peptides = new List<Tuple<string, bool, List<Tuple<double, string>>, List<string>, List<string>, List<Tuple<string, int, string>>>>();

            if (MyProteoformList != null && MyProteoformList.Count > 0)
            {
                MyProteoformList.ForEach(item =>
                {
                    item.PrSMs = item.PrSMs.Distinct(new SelectBestParamsForEachTool()).ToList();

                    List<Tuple<double, string, bool, double, List<Tuple<string, int, string>>>> proteoParams = new List<Tuple<double, string, bool, double, List<Tuple<string, int, string>>>>();
                    int offset = fastaItem.Sequence.IndexOf(item.Sequence);

                    foreach (ProteoformParams param in item.PrSMs)
                    {
                        #region Update ptm position according to the beginning in protein sequence
                        if (offset != -1 && param.Modifications.Count > 0)
                        {
                            var protParamMods = (from mod in param.Modifications
                                                 select Tuple.Create(mod.Item1, (mod.Item2 + offset), mod.Item3)).ToList();
                            proteoParams.Add(Tuple.Create(param.FinalScore, param.SearchEngine, param.IsValid, param.TheoreticalMass, protParamMods));
                        }
                        else
                            proteoParams.Add(Tuple.Create(param.FinalScore, param.SearchEngine, param.IsValid, param.TheoreticalMass, new List<Tuple<string, int, string>>()));
                        #endregion
                    }

                    Proteoforms.Add(Tuple.Create(item.AnnotatedSequence, proteoParams, item.PrSMs[0].IdentifiedNode));
                    PtmsDescription.AddRange(item.PrSMs.SelectMany(b => b.Modifications.Select(mod => mod.Item3).ToList()));
                });
            }

            if (selectedPeptides != null)
            {
                if (MyProteoformList != null && MyProteoformList.Count > 0)
                {

                    List<(Proteoform, List<(Peptide, List<TandemMassSpectrum>)>)> fitProteoformsAndPeptides = Core.FitProteoformsAndPeptides(fastaItem.Sequence, MyProteoformList, selectedPeptides, selectedMSMS);
                    fitProteoformsAndPeptides.ForEach(proteoformPept =>
                    {
                        proteoformPept.Item2.ForEach(peptidesTMS =>
                        {
                            if (peptidesTMS.Item2.Count > 0)
                            {
                                peptidesTMS.Item2.ForEach(tms =>
                                {
                                    //Tuple<peptide, isUnique, List<(score, tool>), List<proteoformWithScore>, List<Protein>, List<(aminoacid, position, description)>>
                                    int id = Peptides.FindIndex(item => item.Item1.Equals(tms.ModifiedSequence.Replace("_", "")));
                                    if (id == -1)
                                        Peptides.Add(Tuple.Create(tms.ModifiedSequence.Replace("_", ""), peptidesTMS.Item1.PSMs[0].IsUnique, (from pept_score in peptidesTMS.Item1.PSMs select Tuple.Create(pept_score.NormalizedPeptideScore, pept_score.SearchEngine)).ToList(), new List<string>() { proteoformPept.Item1.AnnotatedSequence + proteoformPept.Item1.PrSMs[0].FinalScore }, proteoformPept.Item2.SelectMany(item => item.Item1.Proteins).Distinct().ToList(), (from spectrum in tms.Modifications select Tuple.Create(spectrum.Item1, spectrum.Item2, spectrum.Item3)).ToList()));
                                    else
                                    {
                                        List<string> proteoforms = Peptides[id].Item4;
                                        proteoforms.Add(proteoformPept.Item1.AnnotatedSequence + proteoformPept.Item1.PrSMs[0].FinalScore);
                                        Peptides.RemoveAt(id);
                                        Peptides.Add(Tuple.Create(tms.ModifiedSequence.Replace("_", ""), peptidesTMS.Item1.PSMs[0].IsUnique, (from pept_score in peptidesTMS.Item1.PSMs select Tuple.Create(pept_score.NormalizedPeptideScore, pept_score.SearchEngine)).ToList(), proteoforms, proteoformPept.Item2.SelectMany(item => item.Item1.Proteins).Distinct().ToList(), (from spectrum in tms.Modifications select Tuple.Create(spectrum.Item1, spectrum.Item2, spectrum.Item3)).ToList()));
                                    }

                                    PtmsDescription.AddRange(tms.Modifications.Select(desc => desc.Item3).ToList());
                                });
                            }
                            else
                            {
                                int id = Peptides.FindIndex(item => item.Item1.Equals((peptidesTMS.Item1.Sequence)));
                                if (id == -1)
                                    Peptides.Add(Tuple.Create(peptidesTMS.Item1.Sequence, peptidesTMS.Item1.PSMs[0].IsUnique, (from pept_score in peptidesTMS.Item1.PSMs select Tuple.Create(pept_score.NormalizedPeptideScore, pept_score.SearchEngine)).ToList(), new List<string>() { proteoformPept.Item1.AnnotatedSequence + proteoformPept.Item1.PrSMs[0].FinalScore }, new List<string>() { proteoformPept.Item1.ProteinAccession }, new List<Tuple<string, int, string>>() { Tuple.Create("", -1, "") }));
                                else
                                {
                                    List<string> proteoforms = Peptides[id].Item4;
                                    proteoforms.Add(proteoformPept.Item1.AnnotatedSequence + proteoformPept.Item1.PrSMs[0].FinalScore);
                                    Peptides.RemoveAt(id);
                                    Peptides.Add(Tuple.Create(peptidesTMS.Item1.Sequence, peptidesTMS.Item1.PSMs[0].IsUnique, (from pept_score in peptidesTMS.Item1.PSMs select Tuple.Create(pept_score.NormalizedPeptideScore, pept_score.SearchEngine)).ToList(), proteoforms, new List<string>() { proteoformPept.Item1.ProteinAccession }, new List<Tuple<string, int, string>>() { Tuple.Create("", -1, "") }));
                                }
                            }
                        });

                    });

                }
                else
                {
                    selectedPeptides.ForEach(peptide =>
                    {
                        List<TandemMassSpectrum> currentSelectedMSMS = (from massMQ in selectedMSMS.AsParallel()
                                                                        where peptide.PSMs.SelectMany(item => item.MSMSIds).Contains(massMQ.MSMSID)
                                                                        select massMQ).ToList();

                        if (currentSelectedMSMS.Count > 0)
                        {
                            currentSelectedMSMS.ForEach(tms =>
                            {
                                if (tms.Modifications.Count > 0)
                                {
                                    #region Update ptm position according to the beginning in protein sequence
                                    int offset = fastaItem.Sequence.IndexOf(tms.Sequence);
                                    if (offset != -1)
                                    {
                                        //Tuple<peptide, isUnique, List<(score, tool>), List<proteoformWithScore>, List<Protein>, List<(aminoacid, position, description)>>
                                        Peptides.Add(Tuple.Create(tms.ModifiedSequence.Replace("_", ""), peptide.PSMs[0].IsUnique, (from pept_score in peptide.PSMs select Tuple.Create(pept_score.NormalizedPeptideScore, pept_score.SearchEngine)).ToList(), new List<string>(), selectedPeptides.SelectMany(item => item.Proteins).Distinct().ToList(), (from spectrum in tms.Modifications select Tuple.Create(spectrum.Item1, spectrum.Item2 + offset, spectrum.Item3)).ToList()));
                                        PtmsDescription.AddRange(tms.Modifications.Select(desc => desc.Item3).ToList());
                                    }
                                    #endregion
                                }
                                else
                                    Peptides.Add(Tuple.Create(peptide.Sequence, peptide.PSMs[0].IsUnique, (from pept_score in peptide.PSMs select Tuple.Create(pept_score.NormalizedPeptideScore, pept_score.SearchEngine)).ToList(), new List<string>(), selectedPeptides.SelectMany(item => item.Proteins).Distinct().ToList(), new List<Tuple<string, int, string>>() { Tuple.Create("", -1, "") }));
                            });
                        }
                        else
                            Peptides.Add(Tuple.Create(peptide.Sequence, peptide.PSMs[0].IsUnique, (from pept_score in peptide.PSMs select Tuple.Create(pept_score.NormalizedPeptideScore, pept_score.SearchEngine)).ToList(), new List<string>(), selectedPeptides.SelectMany(item => item.Proteins).Distinct().ToList(), new List<Tuple<string, int, string>>() { Tuple.Create("", -1, "") }));
                    });
                }
            }

            Proteoforms = Proteoforms.Distinct().ToList();
            PtmsDescription = PtmsDescription.Distinct().ToList();
            Peptides = Peptides.Distinct(new PeptideTupleComparer()).ToList();

            FillCoverageBox(selectedPeptides != null && selectedPeptides.Count > 0, Proteoforms != null && Proteoforms.Count > 0, PtmsDescription);
            this.proteinAnnotatorUC1.DrawProteoformsPeptides(!IsMultilineProteinSequence, protein, Proteoforms, Peptides, PTMColorArrayTDPList);
            ProteinCoverageRate(fastaItem.Sequence, Proteoforms, Peptides);
        }

        private void ProteinCoverageRate(string proteinSequence, List<(string, List<(double, string, bool, double, List<(string, int, string)>)>, int)> Proteoforms = null, List<(string, bool, List<(double, string)>, List<string>, List<string>, List<(string, int, string)>)> Peptides = null)
        {
            List<int> matchLocations = new List<int>(new int[(int)proteinSequence.Length]);
            if (Proteoforms != null && Proteoforms.Count > 0)
            {
                foreach ((string, List<(double, string, bool, double, List<(string, int, string)>)>, int) proteoform in Proteoforms)
                {
                    Match location = Regex.Match(proteinSequence.ToUpper(), proteoform.Item1.ToUpper());
                    for (int i = location.Index; i < location.Index + location.Length; i++)
                    {
                        matchLocations[i] = 1;
                    }
                }
            }

            if (Peptides != null && Peptides.Count > 0)
            {
                foreach (string peptide in Peptides.Select(item => item.Item1).Distinct())
                {
                    string cleanedPeptide = Util.CleanPeptide(peptide);
                    Match locationPept = Regex.Match(proteinSequence, cleanedPeptide);
                    for (int i = locationPept.Index; i < locationPept.Index + locationPept.Length; i++)
                    {
                        matchLocations[i] = 1;
                    }
                }
            }

            int matchCounter = matchLocations.FindAll(a => a == 1).Count;
            seqCovLabel.Text = matchCounter + " / " + proteinSequence.Length + " = " + (Math.Round((double)matchCounter / (double)proteinSequence.Length, 3) * 100) + "%";

        }

        private void ProteinCoverageRate(string proteinSequence, List<Tuple<string, List<Tuple<double, string, bool, double, List<Tuple<string, int, string>>>>, int>> Proteoforms = null, List<Tuple<string, bool, List<Tuple<double, string>>, List<string>, List<string>, List<Tuple<string, int, string>>>> Peptides = null)
        {
            List<int> matchLocations = new List<int>(new int[(int)proteinSequence.Length]);
            if (Proteoforms != null && Proteoforms.Count > 0)
            {
                foreach (Tuple<string, List<Tuple<double, string, bool, double, List<Tuple<string, int, string>>>>, int> proteoform in Proteoforms)
                {
                    Match location = Regex.Match(proteinSequence.ToLower(), proteoform.Item1.ToLower());
                    for (int i = location.Index; i < location.Index + location.Length; i++)
                    {
                        matchLocations[i] = 1;
                    }
                }
            }

            if (Peptides != null && Peptides.Count > 0)
            {
                foreach (string peptide in Peptides.Select(item => item.Item1).Distinct())
                {
                    string cleanedPeptide = Util.CleanPeptide(peptide);
                    Match locationPept = Regex.Match(proteinSequence.ToLower(), cleanedPeptide.ToLower());
                    for (int i = locationPept.Index; i < locationPept.Index + locationPept.Length; i++)
                    {
                        matchLocations[i] = 1;
                    }
                }
            }

            int matchCounter = matchLocations.FindAll(a => a == 1).Count;
            seqCovLabel.Text = matchCounter + " / " + proteinSequence.Length + " = " + (Math.Round((double)matchCounter / (double)proteinSequence.Length, 3) * 100) + "%";

        }

        private void buttonReturnWindow_Click(object sender, EventArgs e)
        {
            ((Form)this.TopLevelControl).Close();
        }

        private void listViewCoverage_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listViewCoverage.SelectedItems.Count > 0)
            {
                string modification = listViewCoverage.SelectedItems[0].Text;
                if (String.IsNullOrEmpty(modification) || modification.Equals("PTMs"))
                {
                    this.proteinAnnotatorUC1.HighLightProteoformOrPeptides();
                    return;
                }
                else if (modification.Contains("Top-down"))
                {
                    this.proteinAnnotatorUC1.HighLightProteoformOrPeptides("topDown");
                    return;
                }
                else if (modification.Contains("Bottom-up") || modification.Contains("proteomics"))
                {
                    this.proteinAnnotatorUC1.HighLightProteoformOrPeptides("bottomUp");
                    return;
                }
                else if (modification.Contains("Expected"))
                {
                    this.proteinAnnotatorUC1.HighLightProteoformOrPeptides("intact");
                    return;
                }
                else if (modification.Contains("Tagged"))
                {
                    this.proteinAnnotatorUC1.HighLightProteoformOrPeptides("biomarkers");
                    return;
                }
                else if (modification.Contains("Non-expected"))
                {
                    this.proteinAnnotatorUC1.HighLightProteoformOrPeptides("truncated");
                    return;
                }


                var index = listViewCoverage.Items.IndexOf(listViewCoverage.SelectedItems[0]);


                string[] cols = Regex.Split(modification, "\\.");
                if (cols.Length > 1)
                {
                    modification = cols[1].Trim();
                    //chech if there is a complement in the follow listView item
                    if (index + 1 < listViewCoverage.Items.Count)
                    {
                        ListViewItem mod = listViewCoverage.Items[index + 1];
                        cols = Regex.Split(mod.Text, "\\.");
                        if (cols.Length == 1)
                            modification = modification + " " + cols[0].Trim();
                    }
                }
                else//There is a complement in the follow listView item
                {
                    if (index - 1 > 0)
                    {
                        ListViewItem mod = listViewCoverage.Items[index - 1];
                        cols = Regex.Split(mod.Text, "\\.");
                        modification = cols[1].Trim() + " " + modification;
                    }
                    else
                        modification = cols[1].Trim();
                }
                this.proteinAnnotatorUC1.HighLightProteoformOrPeptides(modification);
            }
        }

        private void ProteinCoverageUC_Load(object sender, EventArgs e)
        {
            ((Form)this.Parent).Closing += new CancelEventHandler(ParentClosing);
        }

        private void ParentClosing(object sender, CancelEventArgs e)
        {
            #region Update AnnotationProteoform isValid property
            AnnotationProteoforms = this.proteinAnnotatorUC1.AnnotationProteoforms;
            #endregion
        }
    }
}
