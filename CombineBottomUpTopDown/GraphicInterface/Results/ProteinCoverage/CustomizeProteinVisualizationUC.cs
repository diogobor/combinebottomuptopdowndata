﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CombineBottomUpTopDown.GraphicInterface.Results.ProteinCoverage
{
    public partial class CustomizeProteinVisualizationUC : UserControl
    {
        public bool IsMultilineProteinSequence { get; set; }
        public bool SortPeptidesByStartPosition { get; set; }

        public CustomizeProteinVisualizationUC()
        {
            InitializeComponent();
        }

        private void okCustomPtnVisualization_Click(object sender, EventArgs e)
        {
            IsMultilineProteinSequence = checkBoxMultilinePtnSeq.Checked;
            SortPeptidesByStartPosition = checkBoxSortPeptStartPos.Checked;
            ((Form)this.TopLevelControl).Close();
        }

        internal void Setup(bool isMultilineProteinSequence, bool sortPeptByStartPos)
        {
            IsMultilineProteinSequence = isMultilineProteinSequence;
            checkBoxMultilinePtnSeq.Checked = IsMultilineProteinSequence;

            SortPeptidesByStartPosition = sortPeptByStartPos;
            checkBoxSortPeptStartPos.Checked = SortPeptidesByStartPosition;
        }
    }
}
