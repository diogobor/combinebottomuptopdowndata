﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CombineBottomUpTopDown.Model;

namespace CombineBottomUpTopDown.GraphicInterface.Results.ProteinCoverage
{
    public partial class ProteinCoverageForm : Form
    {
        public List<(string, List<(double, string, bool, double)>)> AnnotationProteoforms { get; set; }

        public ProteinCoverageForm()
        {
            InitializeComponent();
        }

        internal void Setup(bool isMultilineProteinSequence, List<ModificationItem> ptmsUniprot, ResidueMass residueMass, List<(Proteoform, List<(Peptide, List<TandemMassSpectrum>)>)> validProteoformAndPeptides, FastaItem fastaItem)
        {
            this.proteinCoverageUC1.Setup(isMultilineProteinSequence, ptmsUniprot, residueMass, validProteoformAndPeptides, fastaItem);
        }

        internal void Setup(bool isMultilineProteinSequence, List<ModificationItem> ptmsUniprot, ResidueMass residueMass, List<Peptide> selectedPeptides, List<Proteoform> proteoforms, FastaItem fastaItem, List<TandemMassSpectrum> selectedMSMS = null)
        {
            this.proteinCoverageUC1.Setup(isMultilineProteinSequence, ptmsUniprot, residueMass, selectedPeptides, proteoforms, fastaItem, selectedMSMS);
        }

        private void ProteinCoverageForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            AnnotationProteoforms = this.proteinCoverageUC1.AnnotationProteoforms;
        }
    }
}
