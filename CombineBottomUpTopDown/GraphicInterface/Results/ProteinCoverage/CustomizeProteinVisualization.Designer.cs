﻿namespace CombineBottomUpTopDown.GraphicInterface.Results.ProteinCoverage
{
    partial class CustomizeProteinVisualization
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.customizeProteinVisualizationUC1 = new CombineBottomUpTopDown.GraphicInterface.Results.ProteinCoverage.CustomizeProteinVisualizationUC();
            this.SuspendLayout();
            // 
            // customizeProteinVisualizationUC1
            // 
            this.customizeProteinVisualizationUC1.IsMultilineProteinSequence = false;
            this.customizeProteinVisualizationUC1.Location = new System.Drawing.Point(1, -2);
            this.customizeProteinVisualizationUC1.Name = "customizeProteinVisualizationUC1";
            this.customizeProteinVisualizationUC1.Size = new System.Drawing.Size(448, 254);
            this.customizeProteinVisualizationUC1.TabIndex = 0;
            // 
            // CustomizeProteinVisualization
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(450, 254);
            this.Controls.Add(this.customizeProteinVisualizationUC1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CustomizeProteinVisualization";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ProteoCombiner - Customize Visualization";
            this.ResumeLayout(false);

        }

        #endregion

        private CustomizeProteinVisualizationUC customizeProteinVisualizationUC1;
    }
}