﻿namespace CombineBottomUpTopDown.GraphicInterface.Results.ProteinCoverage
{
    partial class CustomizeProteinVisualizationUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomizeProteinVisualizationUC));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.okCustomPtnVisualization = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.checkBoxMultilinePtnSeq = new System.Windows.Forms.CheckBox();
            this.checkBoxSortPeptStartPos = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBoxSortPeptStartPos);
            this.groupBox1.Controls.Add(this.okCustomPtnVisualization);
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Controls.Add(this.checkBoxMultilinePtnSeq);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(442, 248);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // okCustomPtnVisualization
            // 
            this.okCustomPtnVisualization.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.okCustomPtnVisualization.Image = ((System.Drawing.Image)(resources.GetObject("okCustomPtnVisualization.Image")));
            this.okCustomPtnVisualization.Location = new System.Drawing.Point(99, 208);
            this.okCustomPtnVisualization.Name = "okCustomPtnVisualization";
            this.okCustomPtnVisualization.Size = new System.Drawing.Size(244, 32);
            this.okCustomPtnVisualization.TabIndex = 2;
            this.okCustomPtnVisualization.Text = "OK";
            this.okCustomPtnVisualization.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.okCustomPtnVisualization.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.okCustomPtnVisualization.UseVisualStyleBackColor = false;
            this.okCustomPtnVisualization.Click += new System.EventHandler(this.okCustomPtnVisualization_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(6, 68);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(430, 130);
            this.dataGridView1.TabIndex = 1;
            // 
            // checkBoxMultilinePtnSeq
            // 
            this.checkBoxMultilinePtnSeq.AutoSize = true;
            this.checkBoxMultilinePtnSeq.Location = new System.Drawing.Point(6, 19);
            this.checkBoxMultilinePtnSeq.Name = "checkBoxMultilinePtnSeq";
            this.checkBoxMultilinePtnSeq.Size = new System.Drawing.Size(152, 17);
            this.checkBoxMultilinePtnSeq.TabIndex = 0;
            this.checkBoxMultilinePtnSeq.Text = "Multi line protein sequence";
            this.checkBoxMultilinePtnSeq.UseVisualStyleBackColor = true;
            // 
            // checkBoxSortPeptStartPos
            // 
            this.checkBoxSortPeptStartPos.AutoSize = true;
            this.checkBoxSortPeptStartPos.Location = new System.Drawing.Point(6, 42);
            this.checkBoxSortPeptStartPos.Name = "checkBoxSortPeptStartPos";
            this.checkBoxSortPeptStartPos.Size = new System.Drawing.Size(211, 17);
            this.checkBoxSortPeptStartPos.TabIndex = 1;
            this.checkBoxSortPeptStartPos.Text = "Display peptides sorted by start position";
            this.checkBoxSortPeptStartPos.UseVisualStyleBackColor = true;
            // 
            // CustomizeProteinVisualizationUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "CustomizeProteinVisualizationUC";
            this.Size = new System.Drawing.Size(448, 254);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.CheckBox checkBoxMultilinePtnSeq;
        private System.Windows.Forms.Button okCustomPtnVisualization;
        private System.Windows.Forms.CheckBox checkBoxSortPeptStartPos;
    }
}
