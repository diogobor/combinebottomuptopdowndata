﻿namespace CombineBottomUpTopDown.GraphicInterface.Results.ProteinCoverage
{
    partial class ProteinCoverageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProteinCoverageForm));
            this.proteinCoverageUC1 = new CombineBottomUpTopDown.GraphicInterface.Results.ProteinCoverage.ProteinCoverageUC();
            this.SuspendLayout();
            // 
            // proteinCoverageUC1
            // 
            this.proteinCoverageUC1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.proteinCoverageUC1.AnnotationProteoforms = null;
            this.proteinCoverageUC1.IsMultilineProteinSequence = false;
            this.proteinCoverageUC1.Location = new System.Drawing.Point(2, 1);
            this.proteinCoverageUC1.Name = "proteinCoverageUC1";
            this.proteinCoverageUC1.Size = new System.Drawing.Size(854, 447);
            this.proteinCoverageUC1.TabIndex = 0;
            // 
            // ProteinCoverageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(855, 450);
            this.Controls.Add(this.proteinCoverageUC1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ProteinCoverageForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ProteoCombiner - ProteinCoverage";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ProteinCoverageForm_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private ProteinCoverageUC proteinCoverageUC1;
    }
}