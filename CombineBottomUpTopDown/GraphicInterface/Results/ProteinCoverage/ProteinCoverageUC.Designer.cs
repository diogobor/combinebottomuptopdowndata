﻿namespace CombineBottomUpTopDown.GraphicInterface.Results.ProteinCoverage
{
    partial class ProteinCoverageUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProteinCoverageUC));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.listBoxProteinDescription = new System.Windows.Forms.ListBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.listViewCoverage = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label4 = new System.Windows.Forms.Label();
            this.buttonReturnWindow = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.proteinAnnotator1 = new System.Windows.Forms.Integration.ElementHost();
            this.proteinAnnotatorUC1 = new ProteinAnnotation.ProteinAnnotatorUC();
            this.mwLabel = new System.Windows.Forms.Label();
            this.seqCovLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(719, 319);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.listBoxProteinDescription);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.buttonReturnWindow);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.mwLabel);
            this.tabPage1.Controls.Add(this.seqCovLabel);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(711, 293);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Sequence";
            // 
            // listBoxProteinDescription
            // 
            this.listBoxProteinDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxProteinDescription.BackColor = System.Drawing.SystemColors.Control;
            this.listBoxProteinDescription.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBoxProteinDescription.FormattingEnabled = true;
            this.listBoxProteinDescription.Items.AddRange(new object[] {
            "?"});
            this.listBoxProteinDescription.Location = new System.Drawing.Point(190, 8);
            this.listBoxProteinDescription.Name = "listBoxProteinDescription";
            this.listBoxProteinDescription.Size = new System.Drawing.Size(491, 13);
            this.listBoxProteinDescription.TabIndex = 18;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.listViewCoverage);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Location = new System.Drawing.Point(9, 78);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(192, 209);
            this.panel2.TabIndex = 14;
            // 
            // listViewCoverage
            // 
            this.listViewCoverage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listViewCoverage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listViewCoverage.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.listViewCoverage.Location = new System.Drawing.Point(6, 23);
            this.listViewCoverage.MultiSelect = false;
            this.listViewCoverage.Name = "listViewCoverage";
            this.listViewCoverage.Scrollable = false;
            this.listViewCoverage.ShowGroups = false;
            this.listViewCoverage.Size = new System.Drawing.Size(181, 181);
            this.listViewCoverage.TabIndex = 18;
            this.listViewCoverage.UseCompatibleStateImageBehavior = false;
            this.listViewCoverage.VirtualListSize = 10;
            this.listViewCoverage.SelectedIndexChanged += new System.EventHandler(this.listViewCoverage_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "text column";
            this.columnHeader1.Width = 600;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "ColumnHeader2";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "ColumnHeader3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Approach(es):";
            // 
            // buttonReturnWindow
            // 
            this.buttonReturnWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReturnWindow.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonReturnWindow.Image = ((System.Drawing.Image)(resources.GetObject("buttonReturnWindow.Image")));
            this.buttonReturnWindow.Location = new System.Drawing.Point(630, 45);
            this.buttonReturnWindow.Name = "buttonReturnWindow";
            this.buttonReturnWindow.Size = new System.Drawing.Size(75, 27);
            this.buttonReturnWindow.TabIndex = 17;
            this.buttonReturnWindow.Text = "Return";
            this.buttonReturnWindow.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonReturnWindow.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonReturnWindow.UseVisualStyleBackColor = false;
            this.buttonReturnWindow.Click += new System.EventHandler(this.buttonReturnWindow_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.proteinAnnotator1);
            this.panel1.Location = new System.Drawing.Point(207, 78);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(498, 209);
            this.panel1.TabIndex = 13;
            // 
            // proteinAnnotator1
            // 
            this.proteinAnnotator1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.proteinAnnotator1.Location = new System.Drawing.Point(3, 3);
            this.proteinAnnotator1.Name = "proteinAnnotator1";
            this.proteinAnnotator1.Size = new System.Drawing.Size(490, 201);
            this.proteinAnnotator1.TabIndex = 0;
            this.proteinAnnotator1.Text = "elementHost1";
            this.proteinAnnotator1.Child = this.proteinAnnotatorUC1;
            // 
            // mwLabel
            // 
            this.mwLabel.AutoSize = true;
            this.mwLabel.Location = new System.Drawing.Point(188, 30);
            this.mwLabel.Name = "mwLabel";
            this.mwLabel.Size = new System.Drawing.Size(13, 13);
            this.mwLabel.TabIndex = 12;
            this.mwLabel.Text = "?";
            // 
            // seqCovLabel
            // 
            this.seqCovLabel.AutoSize = true;
            this.seqCovLabel.Location = new System.Drawing.Point(188, 52);
            this.seqCovLabel.Name = "seqCovLabel";
            this.seqCovLabel.Size = new System.Drawing.Size(13, 13);
            this.seqCovLabel.TabIndex = 11;
            this.seqCovLabel.Text = "?";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Sequence Coverage:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(180, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Monoisotopic / Average Mass:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Protein:";
            // 
            // ProteinCoverageUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Name = "ProteinCoverageUC";
            this.Size = new System.Drawing.Size(725, 325);
            this.Load += new System.EventHandler(this.ProteinCoverageUC_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label mwLabel;
        private System.Windows.Forms.Label seqCovLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonReturnWindow;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListView listViewCoverage;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ListBox listBoxProteinDescription;
        private System.Windows.Forms.Integration.ElementHost proteinAnnotator1;
        private ProteinAnnotation.ProteinAnnotatorUC proteinAnnotatorUC1;
    }
}
