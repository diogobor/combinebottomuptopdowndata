﻿/**
 * Program:     ProteoCombiner - Integrating bottom-up & top-down proteomics data
 * Author:      Diogo Borges Lima
 * Update:      4/3/2019
 * Update by:   Diogo Borges Lima
 * Description: Results window
 */
using CombineBottomUpTopDown.Controller;
using CombineBottomUpTopDown.GraphicInterface.Help;
using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CombineBottomUpTopDown.GraphicInterface.Results
{
    public partial class ResultsForm : Form
    {
        public bool ReturnSucessful { get; set; } = false;

        private Thread loadThread;
        private Thread saveThread;

        private GUI MygUI { get; set; }

        public ResultsForm()
        {
            InitializeComponent();
        }

        public void SetGUI(GUI gUI = null)
        {
            MygUI = gUI;
        }

        public void Setup(Core core)
        {
            this.resultsUC1.Setup(core, true);
        }

        public bool LoadResultsFromScreen(string fileName)
        {
            bool returnSuccess = this.resultsUC1.LoadResultsFromScreen(fileName);
            if (returnSuccess)
            {
                this.Text = "ProteoCombiner - " + this.resultsUC1.FileName;
            }
            return returnSuccess;
        }

        public bool LoadResults(string fileName = null)
        {
            if (String.IsNullOrEmpty(fileName))
            {
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                dlg.FileName = ""; // Default file name
                dlg.Filter = "ProteoCombiner results (*.pcmb)|*.pcmb"; // Filter files by extension
                dlg.Title = "Load results";

                // Show open file dialog box
                Nullable<bool> result = dlg.ShowDialog();
                if (result == true)
                {
                    #region Header
                    string version = "";
                    try
                    {
                        version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                    }
                    catch (Exception e1)
                    {
                        //Unable to retrieve version number
                        Console.WriteLine("", e1);
                        version = "";
                    }

                    Console.WriteLine("############################################################################");
                    Console.WriteLine("                                                              ProteoCombiner - v. " + version + "\n");
                    Console.WriteLine("                         Engineered by Mass Spectrometry for Biology Unit - Institut Pasteur             \n");
                    Console.WriteLine("############################################################################");
                    #endregion

                    this.resultsUC1.FileName = dlg.FileName;
                    loadThread = new Thread(new ThreadStart(this.resultsUC1.LoadResults));
                    loadThread.Start();
                    timerResults.Enabled = true;

                    return false;
                }
                else
                    return true;
            }
            else
            {
                #region Header
                string version = "";
                try
                {
                    version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                }
                catch (Exception e1)
                {
                    //Unable to retrieve version number
                    Console.WriteLine("", e1);
                    version = "";
                }

                Console.WriteLine("############################################################################");
                Console.WriteLine("                                                              ProteoCombiner - v. " + version + "\n");
                Console.WriteLine("                         Engineered by Mass Spectrometry for Biology Unit - Institut Pasteur             \n");
                Console.WriteLine("############################################################################");
                #endregion

                this.resultsUC1.FileName = fileName;
                loadThread = new Thread(new ThreadStart(this.resultsUC1.LoadResults));
                loadThread.Start();
                timerResults.Enabled = true;

                return false;
            }
        }

        public void ResizeDataGridViews(bool isNewResult = false)
        {
            this.resultsUC1.MeasureDataGridViews(isNewResult);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About aboutScreen = new About();
            aboutScreen.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pDFFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!this.resultsUC1.HasAvailableData())
            {
                MessageBox.Show("There is no data to be processed!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            this.resultsUC1.ExportResultsToPDF();
        }

        private void excelFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!this.resultsUC1.HasAvailableData())
            {
                MessageBox.Show("There is no data to be processed!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            this.resultsUC1.ExportResultsToExcel();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!this.resultsUC1.HasAvailableData())
            {
                MessageBox.Show("There is no data to be processed!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = ""; // Default file name
            dlg.Filter = "ProteoCombiner results (*.pcmb)|*.pcmb"; // Filter files by extension
            dlg.Title = "Save results";

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                string fileName = dlg.FileName;
                try
                {
                    #region Header
                    string version = "";
                    try
                    {
                        version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                    }
                    catch (Exception e1)
                    {
                        //Unable to retrieve version number
                        Console.WriteLine("", e1);
                        version = "";
                    }

                    Console.WriteLine("############################################################################");
                    Console.WriteLine("                                                              ProteoCombiner - v. " + version + "\n");
                    Console.WriteLine("                         Engineered by Mass Spectrometry for Biology Unit - Institut Pasteur             \n");
                    Console.WriteLine("############################################################################");
                    #endregion

                    this.resultsUC1.FileName = dlg.FileName;
                    saveThread = new Thread(new ThreadStart(this.resultsUC1.SaveResults));
                    saveThread.Start();
                    timerResults.Enabled = true;
                    this.Hide();
                    MygUI.Show();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to save file!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = ""; // Default file name
            dlg.Filter = "ProteoCombiner results (*.pcmb)|*.pcmb"; // Filter files by extension
            dlg.Title = "Load results";

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                ReturnSucessful = false;
                this.Close();
                MygUI.LoadResultsFromWinResults(dlg.FileName);

            }
        }

        private void customProteinVisualizationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.resultsUC1.CustomProteinVisualization();
        }

        private void ResultsForm_Resize(object sender, EventArgs e)
        {
            this.ResizeDataGridViews();
        }

        private void ResultsForm_Load(object sender, EventArgs e)
        {
            this.ResizeDataGridViews(true);
        }

        private void readMeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HelpForm help = new HelpForm();
            help.ShowDialog();
        }

        private void timerResults_Tick(object sender, EventArgs e)
        {
            if(loadThread != null && this.resultsUC1.ReturnSucess)
            {
                timerResults.Enabled = false;
                loadThread = null;
                
                this.ReturnSucessful = this.resultsUC1.ReturnSucess;
                if (this.ReturnSucessful)
                {
                    this.Text = "ProteoCombiner - " + this.resultsUC1.FileName;
                }
            }

            if(saveThread != null && this.resultsUC1.ReturnSucess)
            {
                timerResults.Enabled = false;
                loadThread = null;

                MessageBox.Show("Results stored successfully!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.ShowDialog();
            }
        }
    }
}
