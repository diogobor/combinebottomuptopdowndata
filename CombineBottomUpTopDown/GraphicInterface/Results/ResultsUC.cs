﻿/**
 * Program:     ProteoCombiner - Integrating bottom-up & top-down proteomics data
 * Author:      Diogo Borges Lima
 * Created:      4/3/2019
 * Update by:   Diogo Borges Lima
 * Description: Results User control
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CombineBottomUpTopDown.Controller;
using CombineBottomUpTopDown.Model;
using CombineBottomUpTopDown.GraphicInterface.Results.ProteinCoverage;
using System.Text.RegularExpressions;
using Subro.Controls;
using PatternTools.MSParserLight;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;
using Row = DocumentFormat.OpenXml.Spreadsheet.Row;
using CombineBottomUpTopDown.Utils;
using System.Threading;
using MathNet.Numerics.Distributions;

namespace CombineBottomUpTopDown.GraphicInterface.Results
{
    public partial class ResultsUC : UserControl
    {
        /// <summary>
        /// Local variables
        /// </summary>
        private Core MyOriginalCore { get; set; }
        private List<Peptide> SelectedPeptides { get; set; }
        private List<TandemMassSpectrum> SelectedMSMS { get; set; }
        private Core Core { get; set; }
        private bool IsNewResult { get; set; }
        //Tuple<proteinID, sequence (proteoform or peptide), modification, theoretical mass, peptideList >
        private List<Tuple<string, string, string, double, List<Peptide>>> CombineResultsProteoformPeptides { get; set; }

        //Responsible for storing current Proteoforms from selected protein
        private List<Proteoform> ProteoformGroupedList { get; set; }

        //ProteinVisualization
        private bool IsMultilineProteinSequence { get; set; }
        private bool SortPeptidesByStartPosition { get; set; }

        public string FileName { get; set; }
        public bool ReturnSucess { get; set; } = false;


        public ResultsUC()
        {
            InitializeComponent();
            CombineResultsProteoformPeptides = new List<Tuple<string, string, string, double, List<Peptide>>>();
            this.dataGridViewGrouperProteoform.DisplayGroup += grouper_DisplayGroup;
        }

        internal void grouper_DisplayGroup(object sender, GroupDisplayEventArgs e)
        {
            e.BackColor = System.Drawing.Color.LightGray;
            e.Header = e.Header.Replace("TheoreticalMass", "Theoretical Mass: ");
            e.DisplayValue = Convert.ToDouble(e.DisplayValue).ToString("0.00") + " Da";
            e.Summary = "";
        }

        public void ExportResultsToExcel()
        {
            byte returnAnswer = this.ExportResultsToExcelfile();
            if (returnAnswer == 0)
                System.Windows.Forms.MessageBox.Show("Data exported successfully!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else if (returnAnswer == 1)
                System.Windows.Forms.MessageBox.Show("Export data failed!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private byte ExportResultsToExcelfile()
        {
            byte returnOK = 2;//0 -> ok, 1 -> failed, 2 -> cancel

            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "results.xlsx"; // Default file name
            dlg.Filter = "Excel file (*.xlsx)|*.xlsx"; // Filter files by extension
            dlg.Title = "Save results";

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                string fileName = dlg.FileName;
                try
                {
                    CreateSpreadsheetWorkbook(fileName);
                    returnOK = 0;//0 -> ok, 1 -> failed, 2 -> cancel
                }
                catch (Exception ex)
                {
                    returnOK = 1;//0 -> ok, 1 -> failed, 2 -> cancel
                }
            }
            return returnOK;
        }

        private void CreateSpreadsheetWorkbook(string filepath)
        {
            // Create a spreadsheet document by supplying the filepath.
            // By default, AutoSave = true, Editable = true, and Type = xlsx.
            using (SpreadsheetDocument document = SpreadsheetDocument.Create(filepath, SpreadsheetDocumentType.Workbook))
            {
                // Add a WorkbookPart to the document.
                WorkbookPart workbookPart = document.AddWorkbookPart();
                workbookPart.Workbook = new Workbook();

                // Add a WorksheetPart to the WorkbookPart.
                WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                worksheetPart.Worksheet = new Worksheet();

                // Adding style
                WorkbookStylesPart stylePart = workbookPart.AddNewPart<WorkbookStylesPart>();
                stylePart.Stylesheet = GenerateStylesheet();
                stylePart.Stylesheet.Save();

                // Setting up columns
                Columns columns = new Columns(
                        new Column // Id column
                        {
                            Min = 1,
                            Max = 1,
                            Width = 20.5,
                            CustomWidth = true
                        },
                        new Column // Id column
                        {
                            Min = 2,
                            Max = 2,
                            Width = 27,
                            CustomWidth = true
                        },
                        new Column // Id column
                        {
                            Min = 3,
                            Max = 3,
                            Width = 43,
                            CustomWidth = true
                        },
                        new Column // Id column
                        {
                            Min = 4,
                            Max = 4,
                            Width = 19,
                            CustomWidth = true
                        },
                        new Column // Id column
                        {
                            Min = 5,
                            Max = 5,
                            Width = 10,
                            CustomWidth = true
                        },
                        new Column // Id column
                        {
                            Min = 6,
                            Max = 6,
                            Width = 10,
                            CustomWidth = true
                        },
                        new Column // Id column
                        {
                            Min = 7,
                            Max = 7,
                            Width = 10,
                            CustomWidth = true
                        },
                        new Column // Id column
                        {
                            Min = 8,
                            Max = 8,
                            Width = 13,
                            CustomWidth = true
                        });

                worksheetPart.Worksheet.AppendChild(columns);

                // Add Sheets to the Workbook.
                Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());
                Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Results" };
                sheets.Append(sheet);
                workbookPart.Workbook.Save();

                SheetData sheetData = worksheetPart.Worksheet.AppendChild(new SheetData());
                // Constructing header
                Row row = new Row();
                row.Append(ConstructCell("ProteoCombiner: Integrating Top-down, Bottom-up and Middle-down Proteomics Data", CellValues.String, 1));
                // Insert the header row to the Sheet Data
                sheetData.AppendChild(row);

                //Insert a blank line
                InsertBlankLineSheet(row, sheetData);


                #region Protein
                foreach (Protein protein in Core.MyResults.Proteins)
                {
                    row = new Row();
                    row.Append(ConstructCell("Protein", CellValues.String, 2),
                            ConstructCell("Description", CellValues.String, 2),
                            ConstructCell("Sequence Coverage (%)", CellValues.String, 2),
                            ConstructCell("Score", CellValues.String, 2),
                            ConstructCell("", CellValues.String, 2), ConstructCell("", CellValues.String, 2),
                            ConstructCell("", CellValues.String, 2), ConstructCell("", CellValues.String, 2));

                    // Insert the header row to the Sheet Data
                    sheetData.AppendChild(row);

                    row = new Row();
                    row.Append(ConstructCell(protein.AccessionNumber, CellValues.String),
                                ConstructCell(protein.ProteinDescription, CellValues.String),
                                ConstructCell((Math.Truncate(100 * protein.SequenceCoverage) / 100).ToString(), CellValues.String),
                                ConstructCell((Math.Round(100000 * protein.Score) / 100000).ToString(), CellValues.String)
                        );
                    sheetData.AppendChild(row);

                    #region Proteoform and Peptides
                    List<Proteoform> proteoformGroupedList = new List<Proteoform>();
                    if (protein.Proteoforms != null)
                    {
                        var proteoformGroupedListObject = (from protm in protein.Proteoforms
                                                           group protm by protm.PrSMs[0].TheoreticalMass into newGroup
                                                           orderby newGroup.Key
                                                           select newGroup).ToList();

                        foreach (var proteoformGroup in proteoformGroupedListObject)
                        {
                            foreach (var proteoform in proteoformGroup)
                            {
                                proteoformGroupedList.Add(proteoform);
                                break;
                            }
                        }
                        proteoformGroupedList.Sort((a, b) => b.PrSMs[0].FinalScore.CompareTo(a.PrSMs[0].FinalScore));

                    }

                    List<TandemMassSpectrum> selectedMSMS = new List<TandemMassSpectrum>(0);
                    if (protein.Peptides != null)
                    {
                        ProteinGroup proteinGroup = null;
                        string[] cols = Regex.Split(protein.AccessionNumber, ";");
                        if (cols.Length == 1)
                            proteinGroup = Core.MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(cols[0])).FirstOrDefault();
                        else
                            proteinGroup = Core.MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(protein.AccessionNumber)).FirstOrDefault();

                        if (proteinGroup == null)
                        {
                            cols = Regex.Split(protein.AccessionNumber, "\\|");
                            if (cols.Length > 1)
                            {
                                foreach (string accession in cols)
                                {
                                    if (accession.Equals("sp") || accession.Equals("tr")) continue;
                                    proteinGroup = Core.MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(accession)).FirstOrDefault();
                                    if (proteinGroup != null)
                                        break;
                                }
                            }
                        }

                    }

                    //Insert a blank line
                    InsertBlankLineSheet(row, sheetData, 0, true);

                    List<(Proteoform, List<(Peptide, List<TandemMassSpectrum>)>)> validProteoformAndPeptides = Core.FitProteoformsAndPeptides(protein.Sequence, proteoformGroupedList, protein.Peptides, selectedMSMS);

                    if (validProteoformAndPeptides.Count > 0)
                    {
                        validProteoformAndPeptides.ForEach(proteoformPeptides =>
                        {
                            row = new Row();
                            row.SetAttribute(new OpenXmlAttribute("outlineLevel", string.Empty, "1"));
                            row.Append(ConstructCell("", CellValues.String, 3),
                                        ConstructCell("Proteoform Sequence", CellValues.String, 3),
                                        ConstructCell("Modifications", CellValues.String, 3),
                                        ConstructCell("Theoretical Mass (Da)", CellValues.String, 3),
                                        ConstructCell("Score", CellValues.String, 3),
                                        ConstructCell("Is Valid", CellValues.String, 3), ConstructCell("", CellValues.String, 3),
                                        ConstructCell("", CellValues.String, 3));
                            sheetData.AppendChild(row);

                            row = new Row();
                            row.SetAttribute(new OpenXmlAttribute("outlineLevel", string.Empty, "1"));
                            row.Append(ConstructCell("", CellValues.String),
                                        ConstructCell(proteoformPeptides.Item1.Sequence, CellValues.String),
                                        ConstructCell(String.Join(" ; ", proteoformPeptides.Item1.PrSMs[0].Modifications.Select(item => item.Item1 + item.Item2 + "(" + item.Item3 + ")")).Replace("}1", "N-Term").Replace("}2", "N-Term").Replace("}3", "N-Term").Replace("}4", "N-Term").Replace("}5", "N-Term"), CellValues.String),
                                        ConstructCell((Math.Truncate(100 * proteoformPeptides.Item1.PrSMs[0].TheoreticalMass) / 100).ToString(), CellValues.String),
                                        ConstructCell((Math.Round(100000 * proteoformPeptides.Item1.PrSMs[0].FinalScore) / 100000).ToString(), CellValues.String),
                                        ConstructCell(proteoformPeptides.Item1.PrSMs[0].IsValid.ToString(), CellValues.String));
                            sheetData.AppendChild(row);

                            //Insert a blank line
                            row = new Row();
                            row.SetAttributes(new List<OpenXmlAttribute>(){
                                new OpenXmlAttribute("outlineLevel", string.Empty, "1"),
                                new OpenXmlAttribute("outlineLevel", string.Empty, "2")});
                            sheetData.AppendChild(row);

                            #region Peptides

                            if (proteoformPeptides.Item2.Count > 0)
                            {
                                row = new Row();
                                row.SetAttributes(new List<OpenXmlAttribute>(){
                                new OpenXmlAttribute("outlineLevel", string.Empty, "1"),
                                new OpenXmlAttribute("outlineLevel", string.Empty, "2")});
                                row.Append(ConstructCell("", CellValues.String, 4),
                                        ConstructCell("", CellValues.String, 4),
                                        ConstructCell("Peptide Sequence", CellValues.String, 4),
                                        ConstructCell("Theoretical Mass (Da)", CellValues.String, 4),
                                        ConstructCell("IsUnique", CellValues.String, 4),
                                        ConstructCell("Charges", CellValues.String, 4),
                                        ConstructCell("Score", CellValues.String, 4),
                                        ConstructCell("MS/MS Count", CellValues.String, 4));
                                sheetData.AppendChild(row);

                                proteoformPeptides.Item2.ForEach(peptide =>
                                {
                                    row = new Row();
                                    row.SetAttributes(new List<OpenXmlAttribute>(){
                                new OpenXmlAttribute("outlineLevel", string.Empty, "1"),
                                new OpenXmlAttribute("outlineLevel", string.Empty, "2")});
                                    row.Append(ConstructCell("", CellValues.String),
                                        ConstructCell("", CellValues.String),
                                        ConstructCell(peptide.Item1.Sequence, CellValues.String),
                                        ConstructCell(peptide.Item1.PSMs[0].Mass.ToString(), CellValues.String),
                                        ConstructCell(peptide.Item1.PSMs[0].IsUnique.ToString(), CellValues.String),
                                        ConstructCell(String.Join(", ", peptide.Item1.Charges), CellValues.String),
                                        ConstructCell(peptide.Item1.PSMs[0].PeptideScore.ToString(), CellValues.String),
                                        ConstructCell(peptide.Item1.PSMs[0].MSMSCount.ToString(), CellValues.String));
                                    sheetData.AppendChild(row);
                                });

                                //Insert a blank line
                                row = new Row();
                                row.SetAttributes(new List<OpenXmlAttribute>(){
                                new OpenXmlAttribute("outlineLevel", string.Empty, "1"),
                                new OpenXmlAttribute("outlineLevel", string.Empty, "2")});
                                sheetData.AppendChild(row);
                            }
                            #endregion
                        });
                    }
                    #endregion
                }
                #endregion

                worksheetPart.Worksheet.Save();

                // Close the document.
                document.Close();
            }
        }

        private void InsertBlankLineSheet(Row row, SheetData sheetData, uint styleIndex = 0, bool collapse = false, string collapseQualifiedName = "outlineLevel", string collapseValue = "1")
        {
            //Insert a blank line
            row = new Row();
            if (collapse)
                row.SetAttribute(new OpenXmlAttribute(collapseQualifiedName, string.Empty, collapseValue));
            row.Append(ConstructCell("", CellValues.String, styleIndex));
            sheetData.AppendChild(row);
        }

        private DocumentFormat.OpenXml.Spreadsheet.Cell ConstructCell(string value, DocumentFormat.OpenXml.Spreadsheet.CellValues dataType, uint styleIndex = 0)
        {
            return new DocumentFormat.OpenXml.Spreadsheet.Cell()
            {
                CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(value),
                DataType = new EnumValue<CellValues>(dataType),
                StyleIndex = styleIndex
            };
        }

        private Stylesheet GenerateStylesheet()
        {
            Stylesheet styleSheet = null;

            Fonts fonts = new Fonts(
                new DocumentFormat.OpenXml.Spreadsheet.Font( // Index 0 - default
                    new FontSize() { Val = 12 }

                ),
                new DocumentFormat.OpenXml.Spreadsheet.Font( // Index 1 - header
                    new FontSize() { Val = 16 },
                    new Bold()
                ),
                new DocumentFormat.OpenXml.Spreadsheet.Font( // Index 2 - protein header
                    new FontSize() { Val = 12 },
                    new Bold()
                ));

            Fills fills = new Fills(
                    new Fill(new PatternFill() { PatternType = PatternValues.None }), // Index 0 - default
                    new Fill(new PatternFill() { PatternType = PatternValues.Gray125 }), // Index 1 - default
                    new Fill(new PatternFill(new ForegroundColor { Rgb = "a6a6a6" })
                    { PatternType = PatternValues.Solid }), // Index 2 - protein header
                    new Fill(new PatternFill(new ForegroundColor { Rgb = "d9d8d9" })
                    { PatternType = PatternValues.Solid }), // Index 2 - proteoform header
                    new Fill(new PatternFill(new ForegroundColor { Rgb = "f2f2f1" })
                    { PatternType = PatternValues.Solid }) // Index 2 - peptide header
                );

            Borders borders = new Borders(
                    new Border(), // index 0 default
                    new Border( // index 1 black border
                        new LeftBorder(new DocumentFormat.OpenXml.Office2010.Excel.Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new RightBorder(new DocumentFormat.OpenXml.Office2010.Excel.Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new TopBorder(new DocumentFormat.OpenXml.Office2010.Excel.Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new BottomBorder(new DocumentFormat.OpenXml.Office2010.Excel.Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new DiagonalBorder())
                );

            CellFormats cellFormats = new CellFormats(
                    new CellFormat(), // default
                    new CellFormat { FontId = 1, FillId = 0, BorderId = 0, ApplyBorder = false }, // title
                    new CellFormat { FontId = 2, FillId = 2, BorderId = 1, ApplyFill = true }, // protein Header
                    new CellFormat { FontId = 2, FillId = 3, BorderId = 1, ApplyFill = true },// proteoform Header
                    new CellFormat { FontId = 2, FillId = 4, BorderId = 1, ApplyFill = true } // peptide Header
                );

            styleSheet = new Stylesheet(fonts, fills, borders, cellFormats);

            return styleSheet;
        }

        public void ExportResultsToPDF()
        {
            byte returnAnswer = this.ExportResultsToPDFfile();
            if (returnAnswer == 0)
                System.Windows.Forms.MessageBox.Show("Data exported successfully!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else if (returnAnswer == 1)
                System.Windows.Forms.MessageBox.Show("Export data failed!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private byte ExportResultsToPDFfile()
        {
            byte returnOK = 2;//0 -> ok, 1 -> failed, 2 -> cancel

            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "results.pdf"; // Default file name
            dlg.Filter = "PDF file (*.pdf)|*.pdf"; // Filter files by extension
            dlg.Title = "Save results";

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                Document doc = new Document();
                float updateWidth = 0;
                float updateHeight = 0;

                doc.SetPageSize(new iTextSharp.text.Rectangle(0, 0, 842 + updateWidth, 595 + updateHeight));
                iTextSharp.text.Rectangle rectangle = new iTextSharp.text.Rectangle(doc.PageSize);

                string fileName = dlg.FileName;
                try
                {
                    PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(fileName, FileMode.Create));
                    doc.Open();


                    iTextSharp.text.Font customFont = FontFactory.GetFont("Times New Roman", 16, iTextSharp.text.Font.BOLD);
                    //iTextSharp.text.Paragraph paragraph = new iTextSharp.text.Paragraph("  SIM-XL score >= " + ThresholdScoreInter, customFont);
                    iTextSharp.text.Paragraph paragraph = new iTextSharp.text.Paragraph("  Combine Bottom-up & Top-down proteomics data", customFont);
                    doc.Add(paragraph);

                    PdfContentByte content = writer.DirectContent;

                    rectangle.Left += doc.LeftMargin;
                    rectangle.Right -= doc.RightMargin;
                    rectangle.Top -= doc.TopMargin;
                    rectangle.Bottom += doc.BottomMargin;
                    content.SetColorStroke(iTextSharp.text.Color.BLACK);
                    content.Rectangle(rectangle.Left, rectangle.Bottom, rectangle.Width, rectangle.Height);
                    content.Stroke();

                    returnOK = 0;//0 -> ok, 1 -> failed, 2 -> cancel
                }
                catch (Exception)
                {
                    returnOK = 1;//0 -> ok, 1 -> failed, 2 -> cancel
                }
                finally
                {
                    doc.Close();
                }
            }
            return returnOK;
        }

        public void SaveResults()
        {
            ReturnSucess = false;
            MyOriginalCore.SerializeResults(FileName);
            ReturnSucess = true;
        }

        public void LoadResults()
        {
            if (this.LoadResults(this.FileName))
                ReturnSucess = true;
        }

        public bool LoadResultsFromScreen(string fileName)
        {
            if (this.LoadResults(fileName))
                return true;
            return false;
        }

        private bool LoadResults(string fileName)
        {
            bool result = false;

            try
            {
                MyOriginalCore = new Core();
                MyOriginalCore.LoadResults(fileName);

                //Updating norm score
                double[] maxScores = null;
                double[] minScores = null;
                Thread scoreThread = new Thread(() => MyOriginalCore.SelectScores(out maxScores, out minScores));
                scoreThread.Start();

                this.Setup(MyOriginalCore, true);
                FileInfo finfo = new FileInfo(fileName);
                this.FileName = finfo.Name;
                scoreThread.Join();
                MyOriginalCore.MaxScores = maxScores;
                MyOriginalCore.MinScores = minScores;
                result = true;
            }
            catch (Exception)
            {
                MessageBox.Show("Failed to load file!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                result = false;
            }
            return result;
        }

        public void CustomProteinVisualization()
        {
            CustomizeProteinVisualization window = new CustomizeProteinVisualization();
            window.Setup(IsMultilineProteinSequence, SortPeptidesByStartPosition);
            window.ShowDialog();
            IsMultilineProteinSequence = window.IsMultiLineProteinSequence;
            SortPeptidesByStartPosition = window.SortPeptidesByStartPosition;
        }

        /// <summary>
        /// Check whether there is result
        /// </summary>
        /// <returns></returns>
        public bool HasAvailableData()
        {
            return groupBoxFilter.Enabled;
        }

        private void buttonReturnWindow_Click(object sender, EventArgs e)
        {
            ((Form)this.TopLevelControl).Close();
        }

        internal void Setup(Core core, bool isNewResult)
        {
            IsMultilineProteinSequence = false;//The protein will be shown in a single line
            MyOriginalCore = core;
            Core = new Core();
            labelVersionValue.Text = System.Text.ASCIIEncoding.Default.GetString(MyOriginalCore.MyResults.ProgramParams.ProgramVersion);
            Console.WriteLine(" Preparing results...");
            PrepareFilterResults(isNewResult);
            this.groupBoxFilter.Enabled = true;
            this.tabControlResults.SelectedIndex = 0;
        }

        private void FillDataGridInformation()
        {
            //Verify if can clear datagridview
            try
            {
                //Clear DataGridView
                dataGridViewInformation.DataSource = null;
                dataGridViewInformation.Refresh();
            }
            catch (Exception) { }

            try
            {
                DataSet dataSetQueriesInformation = new DataSet();
                DataTable dtQueriesInformation = new DataTable("Queries");
                dataSetQueriesInformation.Tables.Add(dtQueriesInformation);
                dtQueriesInformation.Columns.Add("Data", typeof(string));
                dtQueriesInformation.Columns.Add("Information", typeof(string));

                DataRow newRow = dtQueriesInformation.NewRow();
                newRow["Information"] = "# Combined proteins:";
                newRow["Data"] = Core.MyResults.Proteins.Count();
                dtQueriesInformation.Rows.Add(newRow);

                newRow = dtQueriesInformation.NewRow();
                newRow["Information"] = "# Identified proteins - Top-down:";
                newRow["Data"] = (from item in Core.MyResults.Proteoforms.AsParallel()
                                  select item.ProteinAccession).Distinct().ToList().Count();
                dtQueriesInformation.Rows.Add(newRow);

                newRow = dtQueriesInformation.NewRow();
                newRow["Information"] = "   # Identified proteoforms:";
                newRow["Data"] = Core.MyResults.Proteoforms.Count;
                dtQueriesInformation.Rows.Add(newRow);

                newRow = dtQueriesInformation.NewRow();
                newRow["Information"] = "   # PrSMs:";
                int totalPrsm = 0;
                Core.MyResults.Proteoforms.ForEach(item => { totalPrsm += item.PrSMs.Count; });
                newRow["Data"] = totalPrsm;
                dtQueriesInformation.Rows.Add(newRow);

                newRow = dtQueriesInformation.NewRow();
                newRow["Information"] = "# Identified proteins - Bottom-Up:";
                newRow["Data"] = Core.MyResults.ProteinGroupList.Count();
                dtQueriesInformation.Rows.Add(newRow);

                newRow = dtQueriesInformation.NewRow();
                newRow["Information"] = "   # Identified peptides:";
                newRow["Data"] = Core.MyResults.PeptideList.Count();
                dtQueriesInformation.Rows.Add(newRow);

                newRow = dtQueriesInformation.NewRow();
                newRow["Information"] = "   # PSMs:";
                int totalPsm = 0;
                Core.MyResults.PeptideList.ForEach(item => { totalPsm += item.PSMs.Where(a => a.MSMSIds != null).Sum(item2 => item2.MSMSIds.Count); });
                newRow["Data"] = totalPsm;
                dtQueriesInformation.Rows.Add(newRow);

                dtQueriesInformation.AcceptChanges();
                this.dataGridViewInformation.Columns.Clear();

                DataGridViewTextBoxColumn colInformation = new DataGridViewTextBoxColumn();
                colInformation.DataPropertyName = "Information";
                colInformation.Name = "Information";
                colInformation.HeaderText = "Information";
                colInformation.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colInformation.SortMode = DataGridViewColumnSortMode.NotSortable;
                colInformation.FillWeight = 50;
                colInformation.ReadOnly = true;
                colInformation.Frozen = false;
                this.dataGridViewInformation.Columns.Add(colInformation);

                colInformation = new DataGridViewTextBoxColumn();
                colInformation.DataPropertyName = "Data";
                colInformation.Name = "Data";
                colInformation.HeaderText = "Value";
                colInformation.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colInformation.SortMode = DataGridViewColumnSortMode.NotSortable;
                colInformation.FillWeight = 360;
                colInformation.ReadOnly = true;
                colInformation.Frozen = false;
                this.dataGridViewInformation.Columns.Add(colInformation);

                dataGridViewInformation.EndEdit();
                this.dataGridViewInformation.DataSource = dataSetQueriesInformation.Tables["Queries"];
            }
            catch (Exception) { }
        }

        private void FillDataGridCombineResults()
        {
            //Verify if can clear datagridview
            try
            {
                //Clear DataGridView
                dataGridViewCombineResults.DataSource = null;
                dataGridViewCombineResults.Refresh();
            }
            catch (Exception) { }

            try
            {
                DataSet dataSetQueriesCombineData = new DataSet();
                DataTable dtQueriesCombineData = new DataTable("Queries");
                dataSetQueriesCombineData.Tables.Add(dtQueriesCombineData);
                dtQueriesCombineData.Columns.Add("AccessionNumber", typeof(string));
                dtQueriesCombineData.Columns.Add("ProteinDescription", typeof(string));
                dtQueriesCombineData.Columns.Add("SequenceCoverage", typeof(double));
                dtQueriesCombineData.Columns.Add("Score", typeof(double));

                Core.MyResults.Proteins.Sort((a, b) => b.Score.CompareTo(a.Score));
                Core.MyResults.Proteins = Core.MyResults.Proteins.OrderByDescending(o => o.Score).ThenByDescending(o => o.SequenceCoverage).ToList();

                foreach (Protein protein in Core.MyResults.Proteins)
                {
                    DataRow newRow = dtQueriesCombineData.NewRow();
                    newRow["AccessionNumber"] = protein.AccessionNumber;
                    newRow["ProteinDescription"] = protein.ProteinDescription;
                    newRow["Score"] = protein.Score;
                    //newRow["Score"] = Math.Round(100000 * protein.Score) / 100000;
                    newRow["SequenceCoverage"] = Math.Truncate(100 * protein.SequenceCoverage) / 100;
                    dtQueriesCombineData.Rows.Add(newRow);
                }

                dtQueriesCombineData.AcceptChanges();
                this.dataGridViewCombineResults.Columns.Clear();

                DataGridViewTextBoxColumn colCombineResults = new DataGridViewTextBoxColumn();
                colCombineResults.DataPropertyName = "AccessionNumber";
                colCombineResults.Name = "AccessionNumber";
                colCombineResults.HeaderText = "Protein ID";
                colCombineResults.Width = 120;
                colCombineResults.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                colCombineResults.Frozen = true;
                colCombineResults.ReadOnly = true;
                this.dataGridViewCombineResults.Columns.Add(colCombineResults);

                colCombineResults = new DataGridViewTextBoxColumn();
                colCombineResults.DataPropertyName = "ProteinDescription";
                colCombineResults.Name = "ProteinDescription";
                colCombineResults.HeaderText = "Protein Description";
                colCombineResults.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colCombineResults.ReadOnly = true;
                this.dataGridViewCombineResults.Columns.Add(colCombineResults);

                colCombineResults = new DataGridViewTextBoxColumn();
                colCombineResults.DataPropertyName = "Score";
                colCombineResults.Name = "Score";
                colCombineResults.HeaderText = "Score";
                colCombineResults.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                colCombineResults.ReadOnly = true;
                this.dataGridViewCombineResults.Columns.Add(colCombineResults);

                colCombineResults = new DataGridViewTextBoxColumn();
                colCombineResults.DataPropertyName = "SequenceCoverage";
                colCombineResults.Name = "SequenceCoverage";
                colCombineResults.HeaderText = "Sequence Coverage (%)";
                colCombineResults.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                colCombineResults.ReadOnly = true;
                this.dataGridViewCombineResults.Columns.Add(colCombineResults);

                dataGridViewCombineResults.EndEdit();
                this.dataGridViewCombineResults.DataSource = dataSetQueriesCombineData.Tables["Queries"];
            }
            catch (Exception) { }
        }

        private void FillDataGridCombineResultsProteoform(string proteinAccession)
        {
            bool isValidProteoform = false;

            List<Protein> proteinList = null;
            CombineResultsProteoformPeptides = new List<Tuple<string, string, string, double, List<Peptide>>>();

            if (Core.MyResults.Proteins.Count > 0)
            {
                proteinList = Core.MyResults.Proteins.Where(item => item.AccessionNumber.Contains(proteinAccession)).ToList();
                if (proteinList == null || proteinList.Count == 0)
                {
                    string[] cols = Regex.Split(proteinAccession, ";|\\|");
                    if (cols.Length == 1)
                    {
                        proteinList = Core.MyResults.Proteins.Where(item => item.AccessionNumber.Contains(cols[0])).ToList();
                        if (proteinList != null && proteinList.Count > 0)
                            isValidProteoform = true;
                    }
                    else
                    {
                        foreach (string accession in cols)
                        {
                            if (String.IsNullOrEmpty(accession) || accession.Equals("sp") || accession.Equals("tr")) continue;
                            proteinList.AddRange(Core.MyResults.Proteins.Where(item => item.AccessionNumber.Contains(accession)).ToList());
                            if (proteinList != null && proteinList.Count > 0)
                            {
                                isValidProteoform = true;
                                break;
                            }
                        }
                    }
                }
                else
                    isValidProteoform = true;
            }

            if (isValidProteoform)
            {
                ProteoformGroupedList = new List<Proteoform>();
                if (proteinList[0].Proteoforms != null)
                {
                    var proteoformGroupedListObject = (from protm in proteinList[0].Proteoforms
                                                       group protm by protm.PrSMs[0].TheoreticalMass into newGroup
                                                       orderby newGroup.Key
                                                       select newGroup).ToList();

                    foreach (var proteoformGroup in proteoformGroupedListObject)
                    {
                        foreach (var proteoform in proteoformGroup)
                        {
                            ProteoformGroupedList.Add(proteoform);
                            break;
                        }
                    }
                    ProteoformGroupedList.Sort((a, b) => b.PrSMs[0].FinalScore.CompareTo(a.PrSMs[0].FinalScore));
                }

                //Verify if can clear datagridview
                try
                {
                    //Clear DataGridView
                    dataGridViewCombineResultsProteoform.DataSource = null;
                    dataGridViewCombineResultsProteoform.Refresh();
                }
                catch (Exception) { }
                this.dataGridViewCombineResultsProteoform.Visible = true;

                try
                {

                    DataSet dataSetQueriesCombineResultsProteoform = new DataSet();
                    DataTable dtQueriesCombineResultsProteoform = new DataTable("Queries");
                    dataSetQueriesCombineResultsProteoform.Tables.Add(dtQueriesCombineResultsProteoform);
                    dtQueriesCombineResultsProteoform.Columns.Add("IsValid", typeof(bool));
                    dtQueriesCombineResultsProteoform.Columns.Add("ProteinAccession", typeof(string));
                    dtQueriesCombineResultsProteoform.Columns.Add("Sequence", typeof(string));
                    dtQueriesCombineResultsProteoform.Columns.Add("Modifications", typeof(string));
                    dtQueriesCombineResultsProteoform.Columns.Add("Score", typeof(double));
                    dtQueriesCombineResultsProteoform.Columns.Add("TheoreticalMass", typeof(double));

                    foreach (Proteoform proteoform in ProteoformGroupedList)
                    {
                        DataRow newRow = dtQueriesCombineResultsProteoform.NewRow();
                        newRow["Sequence"] = proteoform.Sequence;
                        newRow["Modifications"] = String.Join(" ; ", proteoform.PrSMs[0].Modifications.Select(item => item.Item1.Equals("}") ? "N-Term" + "(" + item.Item3 + ")" : item.Item1 + item.Item2 + "(" + item.Item3 + ")"));
                        newRow["ProteinAccession"] = proteoform.ProteinAccession;
                        newRow["TheoreticalMass"] = Math.Truncate(100 * proteoform.PrSMs[0].TheoreticalMass) / 100;
                        newRow["Score"] = proteoform.PrSMs[0].FinalScore;
                        //newRow["Score"] = Math.Round(100000 * proteoform.PrSMs[0].FinalScore) / 100000;
                        newRow["IsValid"] = proteoform.PrSMs[0].IsValid;

                        dtQueriesCombineResultsProteoform.Rows.Add(newRow);
                    }

                    dtQueriesCombineResultsProteoform.AcceptChanges();
                    this.dataGridViewCombineResultsProteoform.Columns.Clear();

                    DataGridViewCheckBoxColumn colCombResultsProteoformCheckBox = new DataGridViewCheckBoxColumn();
                    colCombResultsProteoformCheckBox.DataPropertyName = "IsValid";
                    colCombResultsProteoformCheckBox.Name = "IsValid";
                    colCombResultsProteoformCheckBox.HeaderText = "Is Valid";
                    colCombResultsProteoformCheckBox.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                    colCombResultsProteoformCheckBox.ReadOnly = true;
                    this.dataGridViewCombineResultsProteoform.Columns.Add(colCombResultsProteoformCheckBox);

                    DataGridViewTextBoxColumn colCombResultsProteoform = new DataGridViewTextBoxColumn();
                    colCombResultsProteoform.DataPropertyName = "ProteinAccession";
                    colCombResultsProteoform.Name = "ProteinAccession";
                    colCombResultsProteoform.HeaderText = "Protein Accession";
                    colCombResultsProteoform.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                    colCombResultsProteoform.ReadOnly = true;
                    this.dataGridViewCombineResultsProteoform.Columns.Add(colCombResultsProteoform);

                    colCombResultsProteoform = new DataGridViewTextBoxColumn();
                    colCombResultsProteoform.DataPropertyName = "Score";
                    colCombResultsProteoform.Name = "Score";
                    colCombResultsProteoform.HeaderText = "Score";
                    colCombResultsProteoform.ReadOnly = true;
                    colCombResultsProteoform.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                    this.dataGridViewCombineResultsProteoform.Columns.Add(colCombResultsProteoform);

                    colCombResultsProteoform = new DataGridViewTextBoxColumn();
                    colCombResultsProteoform.DataPropertyName = "Sequence";
                    colCombResultsProteoform.Name = "Sequence";
                    colCombResultsProteoform.HeaderText = "Proteoform Sequence";
                    colCombResultsProteoform.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                    colCombResultsProteoform.Width = 450;
                    colCombResultsProteoform.ReadOnly = true;
                    this.dataGridViewCombineResultsProteoform.Columns.Add(colCombResultsProteoform);

                    colCombResultsProteoform = new DataGridViewTextBoxColumn();
                    colCombResultsProteoform.DataPropertyName = "Modifications";
                    colCombResultsProteoform.Name = "Modifications";
                    colCombResultsProteoform.HeaderText = "Modifications";
                    colCombResultsProteoform.ReadOnly = true;
                    colCombResultsProteoform.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    this.dataGridViewCombineResultsProteoform.Columns.Add(colCombResultsProteoform);

                    colCombResultsProteoform = new DataGridViewTextBoxColumn();
                    colCombResultsProteoform.DataPropertyName = "TheoreticalMass";
                    colCombResultsProteoform.Name = "TheoreticalMass";
                    colCombResultsProteoform.HeaderText = "Theoretical Mass";
                    colCombResultsProteoform.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                    colCombResultsProteoform.ReadOnly = true;
                    this.dataGridViewCombineResultsProteoform.Columns.Add(colCombResultsProteoform);

                    dataGridViewCombineResultsProteoform.EndEdit();
                    this.dataGridViewCombineResultsProteoform.DataSource = dataSetQueriesCombineResultsProteoform.Tables["Queries"];

                }
                catch (Exception) { }
            }
            else
            {
                #region Peptides
                ProteinGroup proteinGroup = null;
                string[] colsPept = Regex.Split(proteinAccession, ";");
                if (colsPept.Length == 1)
                {
                    proteinGroup = Core.MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(colsPept[0])).FirstOrDefault();
                }
                else
                {
                    foreach (string accession in colsPept)
                    {
                        proteinGroup = Core.MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(accession)).FirstOrDefault();
                        if (proteinGroup != null) break;
                    }
                }

                if (proteinGroup == null)
                {
                    colsPept = Regex.Split(proteinAccession, "\\|");
                    if (colsPept.Length > 1)
                    {
                        foreach (string accession in colsPept)
                        {
                            if (accession.Equals("sp") || accession.Equals("tr")) continue;
                            proteinGroup = Core.MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(accession)).FirstOrDefault();
                            if (proteinGroup != null)
                                break;
                        }
                    }
                }

                if (proteinGroup != null)
                {
                    SelectedPeptides = new List<Peptide>();
                    foreach (ProteinGroupParams ptnGroupParam in proteinGroup.Params)
                    {
                        List<ulong> ids = ptnGroupParam.PeptideIds.Distinct().ToList();
                        List<Peptide> pepts = (from pepti in Core.MyResults.PeptideList.AsParallel()
                                               where pepti.PSMs.Any(item => ids.Contains(item.PeptideID) && pepti.PSMs.Any(item1 => item1.ProteinGroupIDs.Contains(ptnGroupParam.ProteinGroupID)) && item.SearchEngine.Equals(ptnGroupParam.SearchEngine))
                                               select pepti).ToList();

                        SelectedPeptides.AddRange(pepts);
                    }

                    SelectedPeptides = SelectedPeptides.Distinct(new PeptideComparer()).ToList();
                }

                #endregion

                //Verify if can clear datagridview
                try
                {
                    //Clear DataGridView
                    dataGridViewCombineResultsProteoform.DataSource = null;
                    dataGridViewCombineResultsProteoform.Refresh();
                }
                catch (Exception) { }

                if (SelectedPeptides != null && SelectedPeptides.Count > 0)//There is no proteoform, but there are peptides
                {
                    this.dataGridViewCombineResultsProteoform.Visible = false;
                    FillDataGridCombineResultsPeptide(SelectedPeptides, proteinAccession, string.Empty, 0);
                }
            }

            MeasureDataGridViews();
        }

        private void FillDataGridCombineResultsPeptide(List<Peptide> selectedPeptides, string proteinAccession = "", string sequence = "", double theoreticalMass = 0.0)
        {
            List<Proteoform> proteoformList = new List<Proteoform>();
            List<(Proteoform, List<(Peptide, List<TandemMassSpectrum>)>)> validProteoformAndPeptides = null;

            if (selectedPeptides == null || (selectedPeptides != null && selectedPeptides.Count == 0))
            {
                #region Proteoform
                if (Core.MyResults.Proteoforms.Count > 0)
                {
                    proteoformList = Core.MyResults.Proteoforms.Where(item => (Math.Truncate(100 * item.PrSMs[0].TheoreticalMass) / 100) == theoreticalMass && item.ProteinAccession.Contains(proteinAccession) && item.Sequence.Equals(sequence)).ToList();
                    proteoformList.Sort((a, b) => b.PrSMs[0].LogPScore.CompareTo(a.PrSMs[0].LogPScore));
                }

                FastaItem fastaItem = null;
                if (proteoformList != null && proteoformList.Count > 0)
                {
                    fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(proteinAccession)).FirstOrDefault();
                    if (fastaItem == null)
                    {
                        fastaItem = SelectFastaItem(proteoformList, fastaItem, proteinAccession, sequence);
                    }
                }
                else
                    fastaItem = SelectFastaItem(proteoformList, fastaItem, proteinAccession, sequence);

                #endregion

                #region Peptides
                ProteinGroup proteinGroup = null;
                string[] cols = Regex.Split(proteinAccession, ";");
                if (cols.Length == 1)
                {
                    proteinGroup = Core.MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(cols[0])).FirstOrDefault();
                    if (proteinGroup != null)
                    {
                        if (fastaItem == null) fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(proteinAccession)).FirstOrDefault();
                    }
                }
                else
                {
                    proteinGroup = Core.MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(proteinAccession)).FirstOrDefault();
                    foreach (string accession in cols)
                    {
                        if (proteinGroup != null)
                        {
                            if (fastaItem == null) fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(accession)).FirstOrDefault();
                            if (fastaItem != null) break;
                        }
                    }

                }

                if (proteinGroup == null)
                {
                    cols = Regex.Split(proteinAccession, "\\|");
                    if (cols.Length > 1)
                    {
                        foreach (string accession in cols)
                        {
                            if (accession.Equals("sp") || accession.Equals("tr")) continue;
                            proteinGroup = Core.MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(accession)).FirstOrDefault();
                            if (proteinGroup != null)
                                break;
                        }
                    }
                }

                if (proteinGroup != null)
                {
                    SelectedPeptides = new List<Peptide>();
                    foreach (ProteinGroupParams ptnGroupParam in proteinGroup.Params)
                    {
                        List<ulong> ids = ptnGroupParam.PeptideIds.Distinct().ToList();
                        List<Peptide> pepts = (from pepti in Core.MyResults.PeptideList
                                               where pepti.PSMs.Any(item => ids.Contains(item.PeptideID) && pepti.PSMs.Any(item1 => item1.ProteinGroupIDs.Contains(ptnGroupParam.ProteinGroupID)) && item.SearchEngine.Equals(ptnGroupParam.SearchEngine))
                                               select pepti).ToList();
                        SelectedPeptides.AddRange(pepts);
                    }

                    SelectedPeptides = SelectedPeptides.Distinct(new PeptideComparer()).ToList();
                }
                else
                    SelectedPeptides = null;
                #endregion


                List<TandemMassSpectrum> selectedMSMS = null;
                if (SelectedPeptides != null)
                {
                    List<ulong> msmsIds = new List<ulong>();
                    foreach (Peptide peptide in SelectedPeptides)
                        msmsIds.AddRange(peptide.PSMs.Select(item => item.BestMSMS));
                    msmsIds = msmsIds.Distinct().ToList();
                    selectedMSMS = (from tms in Core.MyResults.TandemMassSpectraMQList.AsParallel()
                                    where msmsIds.Contains(tms.MSMSID) &&
                                    proteinGroup.Params.Select(item => item.ProteinGroupID).Intersect(tms.ProteinGroupID).Count() > 0
                                    select tms).ToList();

                    selectedMSMS.Sort((a, b) => b.Score.CompareTo(a.Score));
                }


                if (proteoformList.Count > 0 && fastaItem != null)//There is no identified proteoform
                    validProteoformAndPeptides = Core.FitProteoformsAndPeptides(fastaItem.Sequence, new List<Proteoform>() { proteoformList[0] }, SelectedPeptides, selectedMSMS);
            }

            //Verify if can clear datagridview
            try
            {
                //Clear DataGridView
                dataGridViewCombineResultsPeptide.DataSource = null;
                dataGridViewCombineResultsPeptide.Refresh();
            }
            catch (Exception) { }

            try
            {
                DataSet dataSetQueriesCombineResultsPeptide = new DataSet();
                DataTable dtQueriesCombineResultsPeptide = new DataTable("Queries");
                dataSetQueriesCombineResultsPeptide.Tables.Add(dtQueriesCombineResultsPeptide);
                dtQueriesCombineResultsPeptide.Columns.Add("PeptideSequence", typeof(string));
                dtQueriesCombineResultsPeptide.Columns.Add("Mass", typeof(double));
                dtQueriesCombineResultsPeptide.Columns.Add("IsUnique", typeof(string));
                dtQueriesCombineResultsPeptide.Columns.Add("Charges", typeof(string));
                dtQueriesCombineResultsPeptide.Columns.Add("PeptideScore", typeof(string));
                dtQueriesCombineResultsPeptide.Columns.Add("SearchEngine", typeof(string));
                dtQueriesCombineResultsPeptide.Columns.Add("MSMSCount", typeof(ulong));

                if (proteoformList.Count > 0)//There is no identified proteoform
                {
                    foreach ((Proteoform, List<(Peptide, List<TandemMassSpectrum>)>) proteoform in validProteoformAndPeptides)
                    {
                        foreach ((Peptide, List<TandemMassSpectrum>) peptide in proteoform.Item2)
                        {
                            if (peptide.Item2.Count > 0)//Petide contains modification
                            {
                                foreach (TandemMassSpectrum spectrum in peptide.Item2)
                                {
                                    DataRow newRow = dtQueriesCombineResultsPeptide.NewRow();
                                    newRow["PeptideSequence"] = spectrum.ModifiedSequence.Replace("_", "");
                                    newRow["Mass"] = Math.Round(100000 * peptide.Item1.PSMs[0].Mass) / 100000;
                                    newRow["IsUnique"] = peptide.Item1.PSMs[0].IsUnique.ToString();
                                    newRow["Charges"] = String.Join(", ", peptide.Item1.Charges);
                                    newRow["PeptideScore"] = String.Join(", ", peptide.Item1.PSMs.Select(item => item.PeptideScore.ToString("0.0000")));
                                    newRow["SearchEngine"] = String.Join(", ", peptide.Item1.PSMs.Select(item => item.SearchEngine));
                                    newRow["MSMSCount"] = peptide.Item1.PSMs.Sum(item => item.MSMSIds.Count);
                                    dtQueriesCombineResultsPeptide.Rows.Add(newRow);
                                }
                            }
                            else
                            {
                                DataRow newRow = dtQueriesCombineResultsPeptide.NewRow();
                                newRow["PeptideSequence"] = peptide.Item1.Sequence;
                                newRow["Mass"] = peptide.Item1.PSMs[0].Mass.ToString("0.0000");
                                newRow["IsUnique"] = peptide.Item1.PSMs[0].IsUnique.ToString();
                                newRow["Charges"] = String.Join(", ", peptide.Item1.Charges);
                                newRow["PeptideScore"] = String.Join(", ", peptide.Item1.PSMs.Select(item => item.PeptideScore.ToString("0.0000")));
                                newRow["SearchEngine"] = String.Join(", ", peptide.Item1.PSMs.Select(item => item.SearchEngine));
                                newRow["MSMSCount"] = peptide.Item1.PSMs.Sum(item => item.MSMSIds.Count);
                                dtQueriesCombineResultsPeptide.Rows.Add(newRow);
                            }
                        }
                    }
                }
                else // There is no identified proteoform, but there are peptides
                {
                    if (SelectedPeptides != null)
                    {
                        foreach (Peptide peptide in SelectedPeptides)
                        {
                            DataRow newRow = dtQueriesCombineResultsPeptide.NewRow();
                            newRow["PeptideSequence"] = peptide.Sequence;
                            newRow["Mass"] = peptide.PSMs[0].Mass.ToString("0.0000");
                            newRow["IsUnique"] = peptide.PSMs[0].IsUnique.ToString();
                            newRow["Charges"] = String.Join(", ", peptide.Charges);
                            newRow["PeptideScore"] = String.Join(", ", peptide.PSMs.Select(item => item.PeptideScore.ToString("0.0000")));
                            newRow["SearchEngine"] = String.Join(", ", peptide.PSMs.Select(item => item.SearchEngine));
                            newRow["MSMSCount"] = peptide.PSMs.Sum(item => item.MSMSIds.Count);
                            dtQueriesCombineResultsPeptide.Rows.Add(newRow);
                        }
                    }
                }

                dtQueriesCombineResultsPeptide.AcceptChanges();
                this.dataGridViewCombineResultsPeptide.Columns.Clear();

                DataGridViewTextBoxColumn colCombResultsPeptide = new DataGridViewTextBoxColumn();
                colCombResultsPeptide.DataPropertyName = "PeptideSequence";
                colCombResultsPeptide.Name = "PeptideSequence";
                colCombResultsPeptide.HeaderText = "Peptide Sequence";
                colCombResultsPeptide.Width = 100;
                colCombResultsPeptide.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colCombResultsPeptide.ReadOnly = true;
                this.dataGridViewCombineResultsPeptide.Columns.Add(colCombResultsPeptide);

                colCombResultsPeptide = new DataGridViewTextBoxColumn();
                colCombResultsPeptide.DataPropertyName = "Mass";
                colCombResultsPeptide.Name = "Mass";
                colCombResultsPeptide.HeaderText = "Mass";
                colCombResultsPeptide.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colCombResultsPeptide.ReadOnly = true;
                this.dataGridViewCombineResultsPeptide.Columns.Add(colCombResultsPeptide);

                colCombResultsPeptide = new DataGridViewTextBoxColumn();
                colCombResultsPeptide.DataPropertyName = "IsUnique";
                colCombResultsPeptide.Name = "IsUnique";
                colCombResultsPeptide.HeaderText = "Unique";
                colCombResultsPeptide.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colCombResultsPeptide.ReadOnly = true;
                this.dataGridViewCombineResultsPeptide.Columns.Add(colCombResultsPeptide);

                colCombResultsPeptide = new DataGridViewTextBoxColumn();
                colCombResultsPeptide.DataPropertyName = "Charges";
                colCombResultsPeptide.Name = "Charges";
                colCombResultsPeptide.HeaderText = "Charges";
                colCombResultsPeptide.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colCombResultsPeptide.ReadOnly = true;
                this.dataGridViewCombineResultsPeptide.Columns.Add(colCombResultsPeptide);

                colCombResultsPeptide = new DataGridViewTextBoxColumn();
                colCombResultsPeptide.DataPropertyName = "PeptideScore";
                colCombResultsPeptide.Name = "PeptideScore";
                colCombResultsPeptide.HeaderText = "Peptide Score";
                colCombResultsPeptide.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colCombResultsPeptide.ReadOnly = true;
                this.dataGridViewCombineResultsPeptide.Columns.Add(colCombResultsPeptide);

                colCombResultsPeptide = new DataGridViewTextBoxColumn();
                colCombResultsPeptide.DataPropertyName = "MSMSCount";
                colCombResultsPeptide.Name = "MSMSCount";
                colCombResultsPeptide.HeaderText = "Spec Count";
                colCombResultsPeptide.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colCombResultsPeptide.ReadOnly = true;
                this.dataGridViewCombineResultsPeptide.Columns.Add(colCombResultsPeptide);

                dataGridViewCombineResultsPeptide.EndEdit();
                this.dataGridViewCombineResultsPeptide.DataSource = dataSetQueriesCombineResultsPeptide.Tables["Queries"];

            }
            catch (Exception) { }
        }

        private void FillDataGridTopDown()
        {
            //Verify if can clear datagridview
            try
            {
                //Clear DataGridView
                dataGridViewTopDown.DataSource = null;
                dataGridViewTopDown.Refresh();
            }
            catch (Exception) { }

            try
            {
                DataSet dataSetQueriesTopDown = new DataSet();
                DataTable dtQueriesTopDown = new DataTable("Queries");
                dataSetQueriesTopDown.Tables.Add(dtQueriesTopDown);
                dtQueriesTopDown.Columns.Add("ScanNumber", typeof(int));
                dtQueriesTopDown.Columns.Add("Sequence", typeof(string));
                dtQueriesTopDown.Columns.Add("Modifications", typeof(string));
                dtQueriesTopDown.Columns.Add("ProteinAccession", typeof(string));
                dtQueriesTopDown.Columns.Add("ProteinDescription", typeof(string));
                dtQueriesTopDown.Columns.Add("Charge", typeof(string));
                dtQueriesTopDown.Columns.Add("MZ", typeof(double));
                dtQueriesTopDown.Columns.Add("MHPlus", typeof(double));
                dtQueriesTopDown.Columns.Add("TheoreticalMass", typeof(double));
                dtQueriesTopDown.Columns.Add("DeltaMass", typeof(double));
                dtQueriesTopDown.Columns.Add("DeltaMZ", typeof(double));
                dtQueriesTopDown.Columns.Add("MatchedIons", typeof(int));
                dtQueriesTopDown.Columns.Add("TotalIons", typeof(int));
                dtQueriesTopDown.Columns.Add("Intensity", typeof(double));
                dtQueriesTopDown.Columns.Add("ActivationType", typeof(string));
                dtQueriesTopDown.Columns.Add("RetentionTime", typeof(string));
                dtQueriesTopDown.Columns.Add("SpectrumFile", typeof(string));
                dtQueriesTopDown.Columns.Add("LogPScore", typeof(double));
                dtQueriesTopDown.Columns.Add("LogEValue", typeof(double));

                foreach (Proteoform proteoform in Core.MyResults.Proteoforms)
                {
                    foreach (ProteoformParams param in proteoform.PrSMs)
                    {
                        DataRow newRow = dtQueriesTopDown.NewRow();
                        newRow["ScanNumber"] = param.ScanNumber;
                        newRow["Sequence"] = proteoform.Sequence;
                        newRow["Modifications"] = String.Join(" ; ", param.Modifications.Select(item => item.Item1 + item.Item2 + "(" + item.Item3 + ")")).Replace("}1", "N-Term");
                        newRow["ProteinAccession"] = proteoform.ProteinAccession;
                        newRow["ProteinDescription"] = proteoform.ProteinDescription;
                        newRow["Charge"] = param.Charge;
                        newRow["MZ"] = (Math.Truncate(10000 * param.MZ) / 10000);
                        newRow["MHPlus"] = (Math.Truncate(10000 * param.MHPlus) / 10000);
                        newRow["TheoreticalMass"] = (Math.Truncate(10000 * param.TheoreticalMass) / 10000);
                        newRow["DeltaMass"] = Math.Round(100000 * param.DeltaMass) / 100000;
                        newRow["DeltaMZ"] = Math.Round(100000 * param.DeltaMZ) / 100000;
                        newRow["MatchedIons"] = param.MatchedIons;
                        newRow["TotalIons"] = param.TotalIons;
                        newRow["Intensity"] = (Math.Truncate(10000 * param.Intensity) / 10000);
                        newRow["ActivationType"] = param.ActivationType;
                        newRow["RetentionTime"] = (Math.Truncate(10000 * param.RetentionTime) / 10000);
                        newRow["SpectrumFile"] = param.SpectrumFile;
                        newRow["LogPScore"] = (Math.Truncate(1000000 * param.LogPScore) / 1000000);
                        newRow["LogEValue"] = (Math.Truncate(1000000 * param.LogEValue) / 1000000);
                        dtQueriesTopDown.Rows.Add(newRow);
                    }
                }

                dtQueriesTopDown.AcceptChanges();
                this.dataGridViewTopDown.Columns.Clear();

                DataGridViewTextBoxColumn colTopDown = new DataGridViewTextBoxColumn();
                colTopDown.DataPropertyName = "ScanNumber";
                colTopDown.Name = "ScanNumber";
                colTopDown.HeaderText = "Scan Number";
                colTopDown.Width = 20;
                colTopDown.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                colTopDown.Frozen = true;
                colTopDown.ReadOnly = true;
                this.dataGridViewTopDown.Columns.Add(colTopDown);

                colTopDown = new DataGridViewTextBoxColumn();
                colTopDown.DataPropertyName = "Sequence";
                colTopDown.Name = "Sequence";
                colTopDown.HeaderText = "Proteoform Sequence";
                colTopDown.Width = 100;
                colTopDown.ReadOnly = true;
                this.dataGridViewTopDown.Columns.Add(colTopDown);

                colTopDown = new DataGridViewTextBoxColumn();
                colTopDown.DataPropertyName = "Modifications";
                colTopDown.Name = "Modifications";
                colTopDown.HeaderText = "Modifications";
                colTopDown.ReadOnly = true;
                this.dataGridViewTopDown.Columns.Add(colTopDown);

                colTopDown = new DataGridViewTextBoxColumn();
                colTopDown.DataPropertyName = "ProteinAccession";
                colTopDown.Name = "ProteinAccession";
                colTopDown.HeaderText = "Protein Accession";
                colTopDown.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                colTopDown.ReadOnly = true;
                this.dataGridViewTopDown.Columns.Add(colTopDown);

                colTopDown = new DataGridViewTextBoxColumn();
                colTopDown.DataPropertyName = "ProteinDescription";
                colTopDown.Name = "ProteinDescription";
                colTopDown.HeaderText = "Protein Description";
                colTopDown.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                colTopDown.ReadOnly = true;
                this.dataGridViewTopDown.Columns.Add(colTopDown);

                colTopDown = new DataGridViewTextBoxColumn();
                colTopDown.DataPropertyName = "Charge";
                colTopDown.Name = "Charge";
                colTopDown.HeaderText = "Charge";
                colTopDown.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                colTopDown.ReadOnly = true;
                this.dataGridViewTopDown.Columns.Add(colTopDown);

                colTopDown = new DataGridViewTextBoxColumn();
                colTopDown.DataPropertyName = "MZ";
                colTopDown.Name = "MZ";
                colTopDown.HeaderText = "m/z";
                colTopDown.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                colTopDown.ReadOnly = true;
                this.dataGridViewTopDown.Columns.Add(colTopDown);

                colTopDown = new DataGridViewTextBoxColumn();
                colTopDown.DataPropertyName = "MHPlus";
                colTopDown.Name = "MHPlus";
                colTopDown.HeaderText = "MH+";
                colTopDown.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                colTopDown.ReadOnly = true;
                this.dataGridViewTopDown.Columns.Add(colTopDown);

                colTopDown = new DataGridViewTextBoxColumn();
                colTopDown.DataPropertyName = "TheoreticalMass";
                colTopDown.Name = "TheoreticalMass";
                colTopDown.HeaderText = "Theoretical Mass";
                colTopDown.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                colTopDown.ReadOnly = true;
                this.dataGridViewTopDown.Columns.Add(colTopDown);

                colTopDown = new DataGridViewTextBoxColumn();
                colTopDown.DataPropertyName = "DeltaMass";
                colTopDown.Name = "DeltaMass";
                colTopDown.HeaderText = "Delta Mass";
                colTopDown.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                colTopDown.ReadOnly = true;
                this.dataGridViewTopDown.Columns.Add(colTopDown);

                colTopDown = new DataGridViewTextBoxColumn();
                colTopDown.DataPropertyName = "DeltaMZ";
                colTopDown.Name = "DeltaMZ";
                colTopDown.HeaderText = "Delta m/z";
                colTopDown.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                colTopDown.ReadOnly = true;
                this.dataGridViewTopDown.Columns.Add(colTopDown);

                colTopDown = new DataGridViewTextBoxColumn();
                colTopDown.DataPropertyName = "MatchedIons";
                colTopDown.Name = "MatchedIons";
                colTopDown.HeaderText = "Matched Ions";
                colTopDown.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                colTopDown.ReadOnly = true;
                this.dataGridViewTopDown.Columns.Add(colTopDown);

                colTopDown = new DataGridViewTextBoxColumn();
                colTopDown.DataPropertyName = "TotalIons";
                colTopDown.Name = "TotalIons";
                colTopDown.HeaderText = "Total Ions";
                colTopDown.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                colTopDown.ReadOnly = true;
                this.dataGridViewTopDown.Columns.Add(colTopDown);

                colTopDown = new DataGridViewTextBoxColumn();
                colTopDown.DataPropertyName = "Intensity";
                colTopDown.Name = "Intensity";
                colTopDown.HeaderText = "Intensity";
                colTopDown.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                colTopDown.ReadOnly = true;
                this.dataGridViewTopDown.Columns.Add(colTopDown);

                colTopDown = new DataGridViewTextBoxColumn();
                colTopDown.DataPropertyName = "ActivationType";
                colTopDown.Name = "ActivationType";
                colTopDown.HeaderText = "Activation Type";
                colTopDown.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                colTopDown.ReadOnly = true;
                this.dataGridViewTopDown.Columns.Add(colTopDown);

                colTopDown = new DataGridViewTextBoxColumn();
                colTopDown.DataPropertyName = "RetentionTime";
                colTopDown.Name = "RetentionTime";
                colTopDown.HeaderText = "Retention Time";
                colTopDown.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                colTopDown.FillWeight = 60;
                colTopDown.ReadOnly = true;
                this.dataGridViewTopDown.Columns.Add(colTopDown);

                colTopDown = new DataGridViewTextBoxColumn();
                colTopDown.DataPropertyName = "SpectrumFile";
                colTopDown.Name = "SpectrumFile";
                colTopDown.HeaderText = "Spectrum File";
                colTopDown.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                colTopDown.FillWeight = 60;
                colTopDown.ReadOnly = true;
                this.dataGridViewTopDown.Columns.Add(colTopDown);

                colTopDown = new DataGridViewTextBoxColumn();
                colTopDown.DataPropertyName = "LogPScore";
                colTopDown.Name = "LogPScore";
                colTopDown.HeaderText = "-Log p-score";
                colTopDown.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                colTopDown.ReadOnly = true;
                this.dataGridViewTopDown.Columns.Add(colTopDown);

                colTopDown = new DataGridViewTextBoxColumn();
                colTopDown.DataPropertyName = "LogEValue";
                colTopDown.Name = "LogEValue";
                colTopDown.HeaderText = "-Log e-value";
                colTopDown.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                colTopDown.ReadOnly = true;
                this.dataGridViewTopDown.Columns.Add(colTopDown);


                dataGridViewTopDown.EndEdit();
                this.dataGridViewTopDown.DataSource = dataSetQueriesTopDown.Tables["Queries"];

            }
            catch (Exception) { }

            this.dataGridViewGrouperProteoform.SetGroupOn("TheoreticalMass");
            this.dataGridViewGrouperProteoform.Options.ShowCount = true;
            this.dataGridViewGrouperProteoform.Options.ShowGroupName = true;
        }

        private void FillDataGridBottomUpProteinGroup()
        {
            //Verify if can clear datagridview
            try
            {
                //Clear DataGridView
                dataGridViewBottomUpPG.DataSource = null;
                dataGridViewBottomUpPG.Refresh();
            }
            catch (Exception) { }

            try
            {
                DataSet dataSetQueriesBottomUpPG = new DataSet();
                DataTable dtQueriesBottomUpPG = new DataTable("Queries");
                dataSetQueriesBottomUpPG.Tables.Add(dtQueriesBottomUpPG);
                dtQueriesBottomUpPG.Columns.Add("ProteinID", typeof(string));
                dtQueriesBottomUpPG.Columns.Add("SequenceLength", typeof(int));
                dtQueriesBottomUpPG.Columns.Add("ProteinScore", typeof(double));
                dtQueriesBottomUpPG.Columns.Add("MSMSCount", typeof(ulong));
                dtQueriesBottomUpPG.Columns.Add("PeptideCounts", typeof(ulong));
                dtQueriesBottomUpPG.Columns.Add("SearchEngine", typeof(string));

                foreach (ProteinGroup proteinGroup in Core.MyResults.ProteinGroupList)
                {
                    DataRow newRow = dtQueriesBottomUpPG.NewRow();
                    newRow["ProteinID"] = proteinGroup.ProteinID;
                    newRow["SequenceLength"] = proteinGroup.SequenceLength;
                    newRow["ProteinScore"] = Math.Round(100000 * proteinGroup.Params[0].ProteinScore) / 100000;
                    newRow["MSMSCount"] = proteinGroup.MSMSCount;
                    newRow["PeptideCounts"] = proteinGroup.Params.Sum(item => item.PeptideIds.Count);
                    newRow["SearchEngine"] = String.Join(", ", proteinGroup.Params.Select(item => item.SearchEngine).Distinct().ToList());
                    dtQueriesBottomUpPG.Rows.Add(newRow);
                }

                dtQueriesBottomUpPG.AcceptChanges();
                this.dataGridViewBottomUpPG.Columns.Clear();

                DataGridViewTextBoxColumn colBottomUpPG = new DataGridViewTextBoxColumn();
                colBottomUpPG.DataPropertyName = "ProteinID";
                colBottomUpPG.Name = "ProteinID";
                colBottomUpPG.HeaderText = "Protein ID";
                colBottomUpPG.Width = 200;
                colBottomUpPG.ReadOnly = true;
                this.dataGridViewBottomUpPG.Columns.Add(colBottomUpPG);

                colBottomUpPG = new DataGridViewTextBoxColumn();
                colBottomUpPG.DataPropertyName = "SequenceLength";
                colBottomUpPG.Name = "SequenceLength";
                colBottomUpPG.HeaderText = "Sequence Length";
                colBottomUpPG.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpPG.ReadOnly = true;
                this.dataGridViewBottomUpPG.Columns.Add(colBottomUpPG);

                colBottomUpPG = new DataGridViewTextBoxColumn();
                colBottomUpPG.DataPropertyName = "ProteinScore";
                colBottomUpPG.Name = "ProteinScore";
                colBottomUpPG.HeaderText = "Protein Score";
                colBottomUpPG.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpPG.ReadOnly = true;
                this.dataGridViewBottomUpPG.Columns.Add(colBottomUpPG);

                colBottomUpPG = new DataGridViewTextBoxColumn();
                colBottomUpPG.DataPropertyName = "MSMSCount";
                colBottomUpPG.Name = "MSMSCount";
                colBottomUpPG.HeaderText = "MS/MS Count";
                colBottomUpPG.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpPG.ReadOnly = true;
                this.dataGridViewBottomUpPG.Columns.Add(colBottomUpPG);

                colBottomUpPG = new DataGridViewTextBoxColumn();
                colBottomUpPG.DataPropertyName = "PeptideCounts";
                colBottomUpPG.Name = "PeptideCounts";
                colBottomUpPG.HeaderText = "Peptide Count";
                colBottomUpPG.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpPG.ReadOnly = true;
                this.dataGridViewBottomUpPG.Columns.Add(colBottomUpPG);

                colBottomUpPG = new DataGridViewTextBoxColumn();
                colBottomUpPG.DataPropertyName = "SearchEngine";
                colBottomUpPG.Name = "SearchEngine";
                colBottomUpPG.HeaderText = "Search Engine(s)";
                colBottomUpPG.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpPG.ReadOnly = true;
                this.dataGridViewBottomUpPG.Columns.Add(colBottomUpPG);

                dataGridViewBottomUpPG.EndEdit();
                this.dataGridViewBottomUpPG.DataSource = dataSetQueriesBottomUpPG.Tables["Queries"];
            }
            catch (Exception) { }
        }

        private void FillDataGridBottomUpPeptide(string proteinID)
        {
            ProteinGroup proteinGroup = null;
            string[] cols = Regex.Split(proteinID, ";");
            if (cols.Length == 1)
            {
                proteinGroup = Core.MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(cols[0])).FirstOrDefault();
            }
            else
            {
                foreach (string accession in cols)
                {
                    proteinGroup = Core.MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(accession)).FirstOrDefault();
                    if (proteinGroup != null)
                        break;
                }
            }

            if (proteinGroup == null)
            {
                cols = Regex.Split(proteinID, "\\|");
                if (cols.Length == 1) return;
                else
                {
                    foreach (string accession in cols)
                    {
                        if (accession.Equals("sp") || accession.Equals("tr")) continue;
                        proteinGroup = Core.MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(accession)).FirstOrDefault();
                        if (proteinGroup != null)
                            break;
                    }
                }
                if (proteinGroup == null) return;
            }

            SelectedPeptides = new List<Peptide>();
            foreach (ProteinGroupParams ptnGroupParam in proteinGroup.Params)
            {
                List<ulong> ids = ptnGroupParam.PeptideIds.Distinct().ToList();
                List<Peptide> pepts = (from pepti in Core.MyResults.PeptideList
                                       where pepti.PSMs.Any(item => ids.Contains(item.PeptideID) && pepti.PSMs.Any(item1 => item1.ProteinGroupIDs.Contains(ptnGroupParam.ProteinGroupID)) && item.SearchEngine.Equals(ptnGroupParam.SearchEngine))
                                       select pepti).ToList();

                List<Peptide> total = (from pepti in Core.MyResults.PeptideList
                                       where pepti.PSMs.Any(item => ids.Contains(item.PeptideID))
                                       select pepti).ToList();

                List<Peptide> exce = total.Except(pepts).ToList();

                SelectedPeptides.AddRange(pepts);
            }

            SelectedPeptides = SelectedPeptides.Distinct(new PeptideComparer()).ToList();

            //Verify if can clear datagridview
            try
            {
                //Clear DataGridView
                dataGridViewBottomUpPept.DataSource = null;
                dataGridViewBottomUpPept.Refresh();
            }
            catch (Exception) { }

            try
            {
                DataSet dataSetQueriesBottomUpPept = new DataSet();
                DataTable dtQueriesBottomUpPept = new DataTable("Queries");
                dataSetQueriesBottomUpPept.Tables.Add(dtQueriesBottomUpPept);
                dtQueriesBottomUpPept.Columns.Add("Sequence", typeof(string));
                dtQueriesBottomUpPept.Columns.Add("Mass", typeof(string));
                dtQueriesBottomUpPept.Columns.Add("Proteins", typeof(string));
                dtQueriesBottomUpPept.Columns.Add("IsUnique", typeof(string));
                dtQueriesBottomUpPept.Columns.Add("Charges", typeof(string));
                dtQueriesBottomUpPept.Columns.Add("SearchEngine", typeof(string));
                dtQueriesBottomUpPept.Columns.Add("PeptideScore", typeof(string));
                dtQueriesBottomUpPept.Columns.Add("NormalizedPeptideScore", typeof(string));
                dtQueriesBottomUpPept.Columns.Add("MSMSCount", typeof(ulong));

                foreach (Peptide peptide in SelectedPeptides)
                {
                    DataRow newRow = dtQueriesBottomUpPept.NewRow();
                    newRow["Sequence"] = peptide.Sequence;
                    newRow["Mass"] = String.Join(", ", peptide.PSMs.Select(item => item.Mass.ToString("0.0000")));
                    newRow["Proteins"] = String.Join(", ", peptide.Proteins);
                    newRow["IsUnique"] = peptide.PSMs[0].IsUnique.ToString();
                    newRow["Charges"] = String.Join(", ", peptide.Charges);
                    newRow["SearchEngine"] = String.Join(", ", peptide.PSMs.Select(item => item.SearchEngine));
                    newRow["PeptideScore"] = String.Join(", ", peptide.PSMs.Select(item => item.PeptideScore.ToString("0.0000")));
                    newRow["NormalizedPeptideScore"] = String.Join(", ", peptide.PSMs.Select(item => item.NormalizedPeptideScore.ToString("0.0000")));
                    newRow["MSMSCount"] = peptide.PSMs.Sum(item => item.MSMSIds.Count);
                    dtQueriesBottomUpPept.Rows.Add(newRow);
                }

                dtQueriesBottomUpPept.AcceptChanges();
                this.dataGridViewBottomUpPept.Columns.Clear();

                DataGridViewTextBoxColumn colBottomUpPeptide = new DataGridViewTextBoxColumn();
                colBottomUpPeptide.DataPropertyName = "Sequence";
                colBottomUpPeptide.Name = "Sequence";
                colBottomUpPeptide.HeaderText = "Sequence";
                colBottomUpPeptide.Width = 100;
                colBottomUpPeptide.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpPeptide.ReadOnly = true;
                this.dataGridViewBottomUpPept.Columns.Add(colBottomUpPeptide);

                colBottomUpPeptide = new DataGridViewTextBoxColumn();
                colBottomUpPeptide.DataPropertyName = "Mass";
                colBottomUpPeptide.Name = "Mass";
                colBottomUpPeptide.HeaderText = "Mass";
                colBottomUpPeptide.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpPeptide.ReadOnly = true;
                this.dataGridViewBottomUpPept.Columns.Add(colBottomUpPeptide);

                colBottomUpPeptide = new DataGridViewTextBoxColumn();
                colBottomUpPeptide.DataPropertyName = "Proteins";
                colBottomUpPeptide.Name = "Proteins";
                colBottomUpPeptide.HeaderText = "Proteins";
                colBottomUpPeptide.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpPeptide.ReadOnly = true;
                this.dataGridViewBottomUpPept.Columns.Add(colBottomUpPeptide);

                colBottomUpPeptide = new DataGridViewTextBoxColumn();
                colBottomUpPeptide.DataPropertyName = "IsUnique";
                colBottomUpPeptide.Name = "IsUnique";
                colBottomUpPeptide.HeaderText = "Unique";
                colBottomUpPeptide.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpPeptide.ReadOnly = true;
                this.dataGridViewBottomUpPept.Columns.Add(colBottomUpPeptide);

                colBottomUpPeptide = new DataGridViewTextBoxColumn();
                colBottomUpPeptide.DataPropertyName = "Charges";
                colBottomUpPeptide.Name = "Charges";
                colBottomUpPeptide.HeaderText = "Charges";
                colBottomUpPeptide.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpPeptide.ReadOnly = true;
                this.dataGridViewBottomUpPept.Columns.Add(colBottomUpPeptide);

                colBottomUpPeptide = new DataGridViewTextBoxColumn();
                colBottomUpPeptide.DataPropertyName = "SearchEngine";
                colBottomUpPeptide.Name = "SearchEngine";
                colBottomUpPeptide.HeaderText = "Search Engine(s)";
                colBottomUpPeptide.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpPeptide.ReadOnly = true;
                this.dataGridViewBottomUpPept.Columns.Add(colBottomUpPeptide);

                colBottomUpPeptide = new DataGridViewTextBoxColumn();
                colBottomUpPeptide.DataPropertyName = "PeptideScore";
                colBottomUpPeptide.Name = "PeptideScore";
                colBottomUpPeptide.HeaderText = "Peptide Score(s)";
                colBottomUpPeptide.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpPeptide.ReadOnly = true;
                this.dataGridViewBottomUpPept.Columns.Add(colBottomUpPeptide);

                colBottomUpPeptide = new DataGridViewTextBoxColumn();
                colBottomUpPeptide.DataPropertyName = "NormalizedPeptideScore";
                colBottomUpPeptide.Name = "NormalizedPeptideScore";
                colBottomUpPeptide.HeaderText = "Norm Peptide Score(s)";
                colBottomUpPeptide.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpPeptide.ReadOnly = true;
                this.dataGridViewBottomUpPept.Columns.Add(colBottomUpPeptide);

                colBottomUpPeptide = new DataGridViewTextBoxColumn();
                colBottomUpPeptide.DataPropertyName = "MSMSCount";
                colBottomUpPeptide.Name = "MSMSCount";
                colBottomUpPeptide.HeaderText = "Spec Count";
                colBottomUpPeptide.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpPeptide.ReadOnly = true;
                this.dataGridViewBottomUpPept.Columns.Add(colBottomUpPeptide);

                dataGridViewBottomUpPept.EndEdit();
                this.dataGridViewBottomUpPept.DataSource = dataSetQueriesBottomUpPept.Tables["Queries"];

            }
            catch (Exception) { }
        }

        private void FillDataGridBottomUpSpec(string peptideSequence)
        {
            if (SelectedPeptides == null || String.IsNullOrEmpty(peptideSequence)) return;
            Peptide peptide = SelectedPeptides.Where(item => item.Sequence.Equals(peptideSequence)).FirstOrDefault();
            if (peptide == null) return;
            List<ulong> msmsIds = peptide.PSMs.SelectMany(item => item.MSMSIds).Distinct().ToList();
            SelectedMSMS = (from tms in Core.MyResults.TandemMassSpectraMQList.AsParallel()
                            where msmsIds.Contains(tms.MSMSID) &&
                            peptide.PSMs.Select(item => item.PeptideID).Contains(tms.PeptideID)
                            select tms).ToList();

            List<TandemMassSpectrum> test2 = (from tms in Core.MyResults.TandemMassSpectraMQList.AsParallel()
                                              where msmsIds.Contains(tms.MSMSID)
                                              select tms).ToList();

            SelectedMSMS.Sort((a, b) => b.Score.CompareTo(a.Score));

            //Verify if can clear datagridview
            try
            {
                //Clear DataGridView
                dataGridViewBottomUpSpec.DataSource = null;
                dataGridViewBottomUpSpec.Refresh();
            }
            catch (Exception) { }

            try
            {
                DataSet dataSetQueriesBottomUpSpec = new DataSet();
                DataTable dtQueriesBottomUpSpec = new DataTable("Queries");
                dataSetQueriesBottomUpSpec.Tables.Add(dtQueriesBottomUpSpec);
                dtQueriesBottomUpSpec.Columns.Add("ScanNumber", typeof(int));
                dtQueriesBottomUpSpec.Columns.Add("Sequence", typeof(string));
                dtQueriesBottomUpSpec.Columns.Add("Modifications", typeof(string));
                dtQueriesBottomUpSpec.Columns.Add("Proteins", typeof(string));
                dtQueriesBottomUpSpec.Columns.Add("Charge", typeof(int));
                dtQueriesBottomUpSpec.Columns.Add("MZ", typeof(double));
                dtQueriesBottomUpSpec.Columns.Add("Mass", typeof(double));
                dtQueriesBottomUpSpec.Columns.Add("PPMError", typeof(double));
                dtQueriesBottomUpSpec.Columns.Add("RetentionTime", typeof(double));
                dtQueriesBottomUpSpec.Columns.Add("Score", typeof(double));
                dtQueriesBottomUpSpec.Columns.Add("NumberOfMatches", typeof(int));

                foreach (TandemMassSpectrum tandemMassSpectrum in SelectedMSMS)
                {
                    DataRow newRow = dtQueriesBottomUpSpec.NewRow();
                    newRow["ScanNumber"] = tandemMassSpectrum.ScanNumber;
                    newRow["Sequence"] = tandemMassSpectrum.ModifiedSequence.Replace("_", "");
                    newRow["Modifications"] = String.Join(" ; ", tandemMassSpectrum.Modifications.Select(item => item.Item1 + item.Item2 + "(" + item.Item3 + ")")).Replace("}1", "N-Term");
                    newRow["Proteins"] = String.Join(" ; ", tandemMassSpectrum.Proteins);
                    newRow["Charge"] = tandemMassSpectrum.Charge;
                    newRow["MZ"] = (Math.Truncate(10000 * tandemMassSpectrum.MZ) / 10000);
                    newRow["Mass"] = (Math.Truncate(10000 * tandemMassSpectrum.Mass) / 10000);
                    newRow["PPMError"] = (Math.Truncate(10000 * tandemMassSpectrum.PPMError) / 10000);
                    newRow["RetentionTime"] = (Math.Truncate(10000 * tandemMassSpectrum.RetentionTime) / 10000);
                    newRow["Score"] = (Math.Truncate(10000 * tandemMassSpectrum.Score) / 10000);
                    newRow["NumberOfMatches"] = tandemMassSpectrum.NumberOfMatches;

                    dtQueriesBottomUpSpec.Rows.Add(newRow);
                }

                dtQueriesBottomUpSpec.AcceptChanges();
                this.dataGridViewBottomUpSpec.Columns.Clear();

                DataGridViewTextBoxColumn colBottomUpSpec = new DataGridViewTextBoxColumn();
                colBottomUpSpec.DataPropertyName = "ScanNumber";
                colBottomUpSpec.Name = "ScanNumber";
                colBottomUpSpec.HeaderText = "Scan Number";
                colBottomUpSpec.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpSpec.ReadOnly = true;
                this.dataGridViewBottomUpSpec.Columns.Add(colBottomUpSpec);

                colBottomUpSpec = new DataGridViewTextBoxColumn();
                colBottomUpSpec.DataPropertyName = "Sequence";
                colBottomUpSpec.Name = "Sequence";
                colBottomUpSpec.HeaderText = "Sequence";
                colBottomUpSpec.Width = 100;
                colBottomUpSpec.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpSpec.ReadOnly = true;
                this.dataGridViewBottomUpSpec.Columns.Add(colBottomUpSpec);

                colBottomUpSpec = new DataGridViewTextBoxColumn();
                colBottomUpSpec.DataPropertyName = "Modifications";
                colBottomUpSpec.Name = "Modifications";
                colBottomUpSpec.HeaderText = "Modifications";
                colBottomUpSpec.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpSpec.ReadOnly = true;
                this.dataGridViewBottomUpSpec.Columns.Add(colBottomUpSpec);

                colBottomUpSpec = new DataGridViewTextBoxColumn();
                colBottomUpSpec.DataPropertyName = "Proteins";
                colBottomUpSpec.Name = "Proteins";
                colBottomUpSpec.HeaderText = "Proteins";
                colBottomUpSpec.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpSpec.ReadOnly = true;
                this.dataGridViewBottomUpSpec.Columns.Add(colBottomUpSpec);

                colBottomUpSpec = new DataGridViewTextBoxColumn();
                colBottomUpSpec.DataPropertyName = "Charge";
                colBottomUpSpec.Name = "Charge";
                colBottomUpSpec.HeaderText = "Z";
                colBottomUpSpec.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpSpec.ReadOnly = true;
                this.dataGridViewBottomUpSpec.Columns.Add(colBottomUpSpec);

                colBottomUpSpec = new DataGridViewTextBoxColumn();
                colBottomUpSpec.DataPropertyName = "MZ";
                colBottomUpSpec.Name = "MZ";
                colBottomUpSpec.HeaderText = "m/z";
                colBottomUpSpec.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpSpec.ReadOnly = true;
                this.dataGridViewBottomUpSpec.Columns.Add(colBottomUpSpec);

                colBottomUpSpec = new DataGridViewTextBoxColumn();
                colBottomUpSpec.DataPropertyName = "Mass";
                colBottomUpSpec.Name = "Mass";
                colBottomUpSpec.HeaderText = "Mass";
                colBottomUpSpec.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpSpec.ReadOnly = true;
                this.dataGridViewBottomUpSpec.Columns.Add(colBottomUpSpec);

                colBottomUpSpec = new DataGridViewTextBoxColumn();
                colBottomUpSpec.DataPropertyName = "PPMError";
                colBottomUpSpec.Name = "PPMError";
                colBottomUpSpec.HeaderText = "ppm";
                colBottomUpSpec.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpSpec.ReadOnly = true;
                this.dataGridViewBottomUpSpec.Columns.Add(colBottomUpSpec);

                colBottomUpSpec = new DataGridViewTextBoxColumn();
                colBottomUpSpec.DataPropertyName = "RetentionTime";
                colBottomUpSpec.Name = "RetentionTime";
                colBottomUpSpec.HeaderText = "Retention Time";
                colBottomUpSpec.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpSpec.ReadOnly = true;
                this.dataGridViewBottomUpSpec.Columns.Add(colBottomUpSpec);

                colBottomUpSpec = new DataGridViewTextBoxColumn();
                colBottomUpSpec.DataPropertyName = "Score";
                colBottomUpSpec.Name = "Score";
                colBottomUpSpec.HeaderText = "Score";
                colBottomUpSpec.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpSpec.ReadOnly = true;
                this.dataGridViewBottomUpSpec.Columns.Add(colBottomUpSpec);

                colBottomUpSpec = new DataGridViewTextBoxColumn();
                colBottomUpSpec.DataPropertyName = "NumberOfMatches";
                colBottomUpSpec.Name = "NumberOfMatches";
                colBottomUpSpec.HeaderText = "Number Of Matches";
                colBottomUpSpec.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                colBottomUpSpec.ReadOnly = true;
                this.dataGridViewBottomUpSpec.Columns.Add(colBottomUpSpec);

                dataGridViewBottomUpSpec.EndEdit();
                this.dataGridViewBottomUpSpec.DataSource = dataSetQueriesBottomUpSpec.Tables["Queries"];

            }
            catch (Exception) { }
        }

        private void CleanBUPDataGridViews(int selectDGV)
        {
            if (selectDGV == 0)//Clean all dgvs
            {
                //Verify if can clear datagridview
                try
                {
                    //Clear DataGridView
                    dataGridViewBottomUpPept.DataSource = null;
                    dataGridViewBottomUpPept.Refresh();
                }
                catch (Exception) { }

                //Verify if can clear datagridview
                try
                {
                    //Clear DataGridView
                    dataGridViewBottomUpSpec.DataSource = null;
                    dataGridViewBottomUpSpec.Refresh();
                }
                catch (Exception) { }
            }
            else
            {
                //Verify if can clear datagridview
                try
                {
                    //Clear DataGridView
                    dataGridViewBottomUpSpec.DataSource = null;
                    dataGridViewBottomUpSpec.Refresh();
                }
                catch (Exception) { }
            }
        }

        private void CleanCombineDataGridViews(int selectDGV)
        {
            if (selectDGV == -1)//Clean all dgvs
            {
                //Verify if can clear datagridview
                try
                {
                    //Clear DataGridView
                    dataGridViewCombineResultsProteoform.DataSource = null;
                    dataGridViewCombineResultsProteoform.Refresh();
                }
                catch (Exception) { }

                //Verify if can clear datagridview
                try
                {
                    //Clear DataGridView
                    dataGridViewCombineResultsPeptide.DataSource = null;
                    dataGridViewCombineResultsPeptide.Refresh();
                }
                catch (Exception) { }

                //Verify if can clear datagridview
                try
                {
                    //Clear DataGridView
                    dataGridViewInformation.DataSource = null;
                    dataGridViewInformation.Refresh();
                }
                catch (Exception) { }
            }
            else if (selectDGV == 0)//Clean all dgvs except Information Datagrid
            {
                //Verify if can clear datagridview
                try
                {
                    //Clear DataGridView
                    dataGridViewCombineResultsProteoform.DataSource = null;
                    dataGridViewCombineResultsProteoform.Refresh();
                }
                catch (Exception) { }

                //Verify if can clear datagridview
                try
                {
                    //Clear DataGridView
                    dataGridViewCombineResultsPeptide.DataSource = null;
                    dataGridViewCombineResultsPeptide.Refresh();
                }
                catch (Exception) { }
            }
            else
            {
                //Verify if can clear datagridview
                try
                {
                    //Clear DataGridView
                    dataGridViewCombineResultsPeptide.DataSource = null;
                    dataGridViewCombineResultsPeptide.Refresh();
                }
                catch (Exception) { }
            }
        }

        private void dataGridViewTopDown_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            //if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            //{
            //    DataGridViewRow currentRow = dataGridViewTopDown.Rows[e.RowIndex];
            //    if (currentRow.Cells[0].Value != null && !String.IsNullOrEmpty(currentRow.Cells[0].Value.ToString())
            //        && e.RowIndex % 2 == 0)
            //    {
            //        e.CellStyle.BackColor = Color.LightGray;
            //    }
            //}
        }

        private void dataGridViewBottomUpPG_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                DataGridViewRow currentRow = dataGridViewBottomUpPG.Rows[e.RowIndex];
                if (currentRow.Cells[0].Value != null && !String.IsNullOrEmpty(currentRow.Cells[0].Value.ToString())
                    && e.RowIndex % 2 == 0)
                {
                    e.CellStyle.BackColor = System.Drawing.Color.LightGray;
                }
            }
        }

        private void dataGridViewBottomUpPG_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewBottomUpPG.SelectedCells.Count == 0) return;
            int selectedCellCount = dataGridViewBottomUpPG.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = dataGridViewBottomUpPG.Rows[selectedCellCount];
            if (selectedRow.Cells["ProteinID"].Value != null)
            {
                string proteinID = selectedRow.Cells["ProteinID"].Value.ToString();
                FillDataGridBottomUpPeptide(proteinID);
            }
        }

        private void dataGridViewBottomUpPG_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            CleanBUPDataGridViews(0);
            dataGridViewBottomUpPG_CellClick(sender, e);
        }

        private void dataGridViewBottomUpPept_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                DataGridViewRow currentRow = dataGridViewBottomUpPept.Rows[e.RowIndex];
                if (currentRow.Cells[0].Value != null && !String.IsNullOrEmpty(currentRow.Cells[0].Value.ToString())
                    && e.RowIndex % 2 == 0)
                {
                    e.CellStyle.BackColor = System.Drawing.Color.LightGray;
                }
            }
        }

        private void dataGridViewBottomUpPept_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewBottomUpPept.SelectedCells.Count == 0) return;
            int selectedCellCount = dataGridViewBottomUpPept.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = dataGridViewBottomUpPept.Rows[selectedCellCount];
            if (selectedRow.Cells["Sequence"].Value != null)
            {
                string peptideSequence = selectedRow.Cells["Sequence"].Value.ToString();
                FillDataGridBottomUpSpec(peptideSequence);
            }
        }

        private void dataGridViewBottomUpSpec_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                DataGridViewRow currentRow = dataGridViewBottomUpSpec.Rows[e.RowIndex];
                if (currentRow.Cells[0].Value != null && !String.IsNullOrEmpty(currentRow.Cells[0].Value.ToString())
                    && e.RowIndex % 2 == 0)
                {
                    e.CellStyle.BackColor = System.Drawing.Color.LightGray;
                }
            }
        }

        private void dataGridViewBottomUpPept_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            CleanBUPDataGridViews(1);
            dataGridViewBottomUpPept_CellClick(sender, e);
        }

        private void dataGridViewBottomUpPG_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int selectedCellCount = dataGridViewBottomUpPG.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = dataGridViewBottomUpPG.Rows[selectedCellCount];
            if (selectedRow.Cells["ProteinID"].Value != null)
            {
                ProteinGroup proteinGroup = null;
                FastaItem fastaItem = null;
                string proteinID = selectedRow.Cells["ProteinID"].Value.ToString();
                string[] cols = Regex.Split(proteinID, ";");
                if (cols.Length == 1)
                {
                    proteinGroup = Core.MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(cols[0])).FirstOrDefault();
                    fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(proteinGroup.ProteinID)).FirstOrDefault();
                    if (fastaItem == null)
                        fastaItem = SelectFastaItem(null, fastaItem, proteinGroup.ProteinID);
                }
                else
                {
                    foreach (string accession in cols)
                    {
                        proteinGroup = Core.MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(accession)).FirstOrDefault();
                        if (proteinGroup != null)
                        {
                            if (fastaItem == null)
                                fastaItem = SelectFastaItem(null, fastaItem, accession);
                            //fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(accession)).FirstOrDefault();
                            if (fastaItem != null) break;
                        }
                    }
                }

                if (proteinGroup == null)
                {
                    cols = Regex.Split(proteinID, "\\|");
                    if (cols.Length == 1)
                    {
                        proteinGroup = Core.MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(cols[0])).FirstOrDefault();
                        if (proteinGroup != null)
                        {
                            if (fastaItem == null)
                                fastaItem = SelectFastaItem(null, fastaItem, proteinID);
                            //fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(proteinID)).FirstOrDefault();
                        }
                    }
                    else
                    {
                        foreach (string accession in cols)
                        {
                            if (accession.Equals("sp") || accession.Equals("tr")) continue;
                            proteinGroup = Core.MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(accession)).FirstOrDefault();
                            if (proteinGroup != null)
                            {
                                if (fastaItem == null)
                                    fastaItem = SelectFastaItem(null, fastaItem, accession);
                                //fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(accession)).FirstOrDefault();
                                if (fastaItem != null) break;
                            }
                        }
                    }
                }

                if (proteinGroup != null)
                {
                    SelectedPeptides = new List<Peptide>();
                    foreach (ProteinGroupParams ptnGroupParam in proteinGroup.Params)
                    {
                        List<ulong> ids = ptnGroupParam.PeptideIds.Distinct().ToList();
                        List<Peptide> pepts = (from pepti in Core.MyResults.PeptideList.AsParallel()
                                               where pepti.PSMs.Any(item => ids.Contains(item.PeptideID) && pepti.PSMs.Any(item1 => item1.ProteinGroupIDs.Contains(ptnGroupParam.ProteinGroupID)) && item.SearchEngine.Equals(ptnGroupParam.SearchEngine))
                                               select pepti).ToList();
                        SelectedPeptides.AddRange(pepts);
                    }

                    SelectedPeptides = SelectedPeptides.Distinct(new PeptideComparer()).ToList();
                }
                else
                    SelectedPeptides = null;
                if (fastaItem == null)
                {
                    MessageBox.Show(
                        "Protein sequence not found.",
                        "Warning",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return;
                }

                if (SelectedPeptides == null)
                {
                    MessageBox.Show(
                        "No peptide has been found.",
                        "Warning",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return;
                }

                if (SortPeptidesByStartPosition)//Sort peptides by start position
                {
                    SelectedPeptides.Sort((a, b) => a.StartPosition.CompareTo(b.StartPosition));
                }

                List<ulong> msmsIds = new List<ulong>();
                foreach (Peptide peptide in SelectedPeptides)
                    msmsIds.AddRange(peptide.PSMs.Select(item => item.BestMSMS));
                msmsIds = msmsIds.Distinct().ToList();
                List<TandemMassSpectrum> selectedMSMS = (from tms in Core.MyResults.TandemMassSpectraMQList.AsParallel()
                                                         where msmsIds.Contains(tms.MSMSID) &&
                                                         proteinGroup.Params.Select(item => item.ProteinGroupID).Intersect(tms.ProteinGroupID).Count() > 0
                                                         select tms).ToList();
                selectedMSMS.Sort((a, b) => b.Score.CompareTo(a.Score));



                int windowWidth = (fastaItem.Sequence.Length / 100) < 2 ? 1 : (fastaItem.Sequence.Length / 100) < 4 ? 2 : 3;
                ProteinCoverageForm proteinCoverageFrom = new ProteinCoverageForm();
                proteinCoverageFrom.Width = 1110 + 10 * (windowWidth - 1);
                proteinCoverageFrom.Setup(IsMultilineProteinSequence, MyOriginalCore.MyResults.PTMsUniprot, MyOriginalCore.MyResults.ResidueMass, SelectedPeptides, null, fastaItem, selectedMSMS);
                proteinCoverageFrom.ShowDialog();
            }
        }

        private void dataGridViewCombineResults_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                DataGridViewRow currentRow = dataGridViewCombineResults.Rows[e.RowIndex];
                if (currentRow.Cells[0].Value != null && !String.IsNullOrEmpty(currentRow.Cells[0].Value.ToString())
                    && e.RowIndex % 2 == 0)
                {
                    e.CellStyle.BackColor = System.Drawing.Color.LightGray;
                }
            }
        }

        private void dataGridViewTopDown_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {

            int selectedCellCount = dataGridViewTopDown.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = dataGridViewTopDown.Rows[selectedCellCount];
            DataGridViewColumn gridViewColumn = dataGridViewTopDown.Columns[e.ColumnIndex];
            List<Proteoform> proteoformList = new List<Proteoform>();
            int scanNumber = 0;
            string proteoformSequence = string.Empty;
            string proteinAccession = string.Empty;
            string specFile = string.Empty;
            double score = 0;
            double deltaMZ = 0;
            double deltaMass = 0;

            if (selectedRow.Cells["ProteinAccession"].Value != null)
            {
                scanNumber = Convert.ToInt32(selectedRow.Cells["ScanNumber"].Value.ToString());
                proteoformSequence = selectedRow.Cells["Sequence"].Value.ToString();
                proteinAccession = selectedRow.Cells["ProteinAccession"].Value.ToString();
                specFile = selectedRow.Cells["SpectrumFile"].Value.ToString();
                score = Convert.ToDouble(selectedRow.Cells["LogPScore"].Value.ToString());
                deltaMZ = Convert.ToDouble(selectedRow.Cells["DeltaMZ"].Value.ToString());
                deltaMass = Convert.ToDouble(selectedRow.Cells["DeltaMass"].Value.ToString());

                if (!String.IsNullOrEmpty(specFile) && deltaMZ != 0 && deltaMass != 0)//Prosight or pTop information
                    proteoformList = Core.MyResults.Proteoforms.Where(item => item.ProteinAccession.Contains(proteinAccession) && item.Sequence.Equals(proteoformSequence) && item.PrSMs.Exists(item1 => item1.ScanNumber == scanNumber) && item.PrSMs.Exists(item1 => item1.SpectrumFile.Equals(specFile)) && item.PrSMs.Exists(item1 => item1.LogPScore == score)).ToList();
                else //TopPIC
                    proteoformList = Core.MyResults.Proteoforms.Where(item => item.ProteinAccession.Contains(proteinAccession) && item.Sequence.Equals(proteoformSequence) && item.PrSMs.Exists(item1 => item1.ScanNumber == scanNumber) && (Math.Truncate(1000000 * item.PrSMs[0].LogPScore) / 1000000) == (Math.Truncate(1000000 * score) / 1000000)).ToList();

                if (proteoformList != null && proteoformList.Count > 0)
                {
                    if (gridViewColumn.HeaderText.Equals("Scan Number"))// Call Spec Viewer
                    {
                        Proteoform proteoform = proteoformList.FirstOrDefault();
                        if (proteoform.PrSMs[0].FragmentIons != null &&
                            proteoform.PrSMs[0].FragmentIons.Count > 0)
                        {
                            List<Tuple<float, float>> ions = (from peak in proteoform.PrSMs[0].FragmentIons
                                                              select System.Tuple.Create((float)peak.Item1, (float)peak.Item2)).ToList();

                            #region Get Activation Type
                            bool IonA = false;
                            bool IonB = false;
                            bool IonC = false;
                            bool IonX = false;
                            bool IonY = false;
                            bool IonZ = false;

                            switch (proteoform.PrSMs[0].ActivationType)
                            {
                                case ActivationType.EThCD:
                                    IonA = false;
                                    IonB = true;
                                    IonC = true;
                                    IonX = false;
                                    IonY = true;
                                    IonZ = true;
                                    break;
                                case ActivationType.CID:
                                    IonA = true;
                                    IonB = true;
                                    IonC = false;
                                    IonX = false;
                                    IonY = true;
                                    IonZ = false;
                                    break;
                                case ActivationType.ETD:
                                    IonA = false;
                                    IonB = false;
                                    IonC = true;
                                    IonX = false;
                                    IonY = false;
                                    IonZ = true;
                                    break;
                                case ActivationType.HCD:
                                    IonA = true;
                                    IonB = true;
                                    IonC = false;
                                    IonX = false;
                                    IonY = true;
                                    IonZ = false;
                                    break;
                                case ActivationType.ECD:
                                    IonA = false;
                                    IonB = false;
                                    IonC = true;
                                    IonX = false;
                                    IonY = false;
                                    IonZ = true;
                                    break;
                                case ActivationType.MPD:
                                    IonA = true;
                                    IonB = true;
                                    IonC = false;
                                    IonX = false;
                                    IonY = true;
                                    IonZ = false;
                                    break;
                                case ActivationType.PQD://Pulsed Q-dissociation -> similar CID
                                    IonA = true;
                                    IonB = true;
                                    IonC = false;
                                    IonX = false;
                                    IonY = true;
                                    IonZ = false;
                                    break;
                                default:
                                    IonA = true;
                                    IonB = true;
                                    IonC = false;
                                    IonX = false;
                                    IonY = true;
                                    IonZ = false;
                                    break;
                            }
                            #endregion

                            SpectrumViewer2.PeprideSpectrumViewer.MSViewerWindow svf = new SpectrumViewer2.PeprideSpectrumViewer.MSViewerWindow();
                            double ppm = 450;
                            svf.PlotSpectrum(ions, ppm, proteoform.Sequence, PatternTools.PTMMods.DefaultModifications.TheModifications, false, IonA, IonB, IonC, IonX, IonY, IonZ, proteoform.PrSMs[0].Charge);
                            svf.ShowDialog();
                        }
                        else
                        {
                            MessageBox.Show(
                                "Spectrum not found.",
                                "Warning",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                        }

                    }
                    else //Call Protein Coverage
                    {
                        #region Proteoform
                        FastaItem fastaItem = null;
                        fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(proteinAccession)).FirstOrDefault();
                        if (fastaItem == null)
                            fastaItem = SelectFastaItem(proteoformList, fastaItem, proteinAccession);

                        if (fastaItem == null && proteoformList.Count > 0 && !String.IsNullOrEmpty(proteoformList[0].Sequence))
                        {
                            fastaItem = new FastaItem();
                            fastaItem.Sequence = proteoformList[0].Sequence;
                            fastaItem.Description = string.Empty;
                            fastaItem.SequenceInBytes = System.Text.ASCIIEncoding.Default.GetBytes(fastaItem.Sequence);
                        }

                        #endregion

                        if (fastaItem == null)
                        {
                            MessageBox.Show(
                                "Protein sequence not found.",
                                "Warning",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                            return;
                        }

                        SelectedPeptides = null;
                        int windowWidth = (fastaItem.Sequence.Length / 100) < 2 ? 1 : (fastaItem.Sequence.Length / 100) < 4 ? 2 : 3;
                        ProteinCoverageForm proteinCoverageFrom = new ProteinCoverageForm();
                        proteinCoverageFrom.Width = 1110 + 10 * (windowWidth - 1);
                        proteinCoverageFrom.Setup(IsMultilineProteinSequence, MyOriginalCore.MyResults.PTMsUniprot, MyOriginalCore.MyResults.ResidueMass, SelectedPeptides, proteoformList, fastaItem);
                        proteinCoverageFrom.ShowDialog();
                    }
                }
                else
                {
                    MessageBox.Show(
                                "Proteoform not found.",
                                "Warning",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                }
            }
        }

        private List<Proteoform> SelectProteoforms(string proteinAccession, string proteoformSequence = null)
        {
            List<Proteoform> proteoformList = null;
            string[] cols = Regex.Split(proteinAccession, ",|;|\\|");
            if (cols.Length == 1)
            {
                if (proteoformList == null || proteoformList != null && proteoformList.Count == 0)
                {
                    if (!String.IsNullOrEmpty(proteoformSequence))
                        proteoformList = Core.MyResults.Proteoforms.Where(item => item.ProteinAccession.Contains(cols[0]) && item.Sequence.Equals(proteoformSequence)).ToList();
                    else
                        proteoformList = Core.MyResults.Proteoforms.Where(item => item.ProteinAccession.Contains(cols[0])).ToList();
                }
            }
            else
            {
                proteoformList = new List<Proteoform>();
                foreach (string accession in cols)
                {
                    if (String.IsNullOrEmpty(accession) || accession.Equals("sp") || accession.Equals("tr")) continue;
                    if (!String.IsNullOrEmpty(proteoformSequence))
                        proteoformList.AddRange(Core.MyResults.Proteoforms.Where(item => item.ProteinAccession.Contains(accession) && item.Sequence.Equals(proteoformSequence)).ToList());
                    else
                        proteoformList.AddRange(Core.MyResults.Proteoforms.Where(item => item.ProteinAccession.Contains(accession)).ToList());
                }
            }
            return proteoformList;
        }

        private FastaItem SelectFastaItem(List<Proteoform> proteoformList, FastaItem fastaItem, string proteinAccession, string proteoformSequence = null)
        {
            if (proteoformList == null || proteoformList != null && proteoformList.Count == 0)
                proteoformList = this.SelectProteoforms(proteinAccession, proteoformSequence);

            string[] cols = Regex.Split(proteinAccession, ",|;|\\|");
            if (cols.Length == 1)
            {
                if (proteoformList != null && proteoformList.Count > 0)
                {
                    fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Contains(proteoformList[0].ProteinAccession)).FirstOrDefault();
                }
                else // There is no proteoforms
                {
                    //Try to recovery fasta item based on exact fragmented accession number
                    fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(cols[0])).FirstOrDefault();
                    if (fastaItem == null)
                    {
                        //Try to recovery fasta item based on fragmented accession number
                        fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Contains(cols[0])).FirstOrDefault();
                    }
                }
            }
            else
            {
                foreach (string accession in cols)
                {
                    if (String.IsNullOrEmpty(accession) || accession.Equals("sp") || accession.Equals("tr")) continue;
                    if (proteoformList != null && proteoformList.Count > 0)
                    {
                        //Try to recovery fasta item based on exact fragmented accession number
                        fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(accession) && item.Description.Contains(proteoformList[0].ProteinDescription)).FirstOrDefault();
                        if (fastaItem != null) break;
                        else
                        {
                            //Try to recovery fasta item based on fragmented accession number
                            fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Contains(accession) && item.Description.Contains(proteoformList[0].ProteinDescription)).FirstOrDefault();
                            if (fastaItem != null) break;
                            else
                            {
                                //Try to compare proteoform description to fasta description
                                fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(proteinAccession) && item.Description.Contains(proteoformList[0].ProteinDescription)).FirstOrDefault();
                                if (fastaItem != null) break;
                                else
                                {
                                    //Try to compare fasta description to proteoform description
                                    fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(proteinAccession) && proteoformList[0].ProteinDescription.Contains(item.Description)).FirstOrDefault();
                                    if (fastaItem != null) break;
                                    else
                                    {
                                        fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Contains(proteoformList[0].ProteinAccession)).FirstOrDefault();
                                        if (fastaItem != null) break;
                                    }
                                }
                            }
                        }
                    }
                    else //There is no proteoforms
                    {
                        //Try to recovery fasta item based on exact fragmented accession number
                        fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(accession)).FirstOrDefault();
                        if (fastaItem != null) break;
                        else
                        {
                            //Try to recovery fasta item based on fragmented accession number
                            fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Contains(accession)).FirstOrDefault();
                            if (fastaItem != null) break;
                            else
                            {
                                //Try to compare proteinAccesion
                                fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(proteinAccession)).FirstOrDefault();
                                if (fastaItem != null) break;
                                else
                                {
                                    //Try to compare fasta description to proteoform description
                                    fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Contains(proteinAccession)).FirstOrDefault();
                                    if (fastaItem != null) break;
                                }
                            }
                        }
                    }
                }
            }

            return fastaItem;
        }

        private void dataGridViewCombineResults_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int selectedCellCount = dataGridViewCombineResults.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = dataGridViewCombineResults.Rows[selectedCellCount];
            if (selectedRow.Cells["AccessionNumber"].Value != null)
            {
                #region Proteoform

                FastaItem fastaItem = null;
                List<Proteoform> proteoformList = new List<Proteoform>();

                string proteinAccession = selectedRow.Cells["AccessionNumber"].Value.ToString();
                List<Proteoform> proteoformListSelected = this.SelectProteoforms(proteinAccession);
                if (proteoformListSelected != null && proteoformListSelected.Count > 0)
                {
                    fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(proteinAccession)).FirstOrDefault();
                    if (fastaItem == null)
                    {
                        fastaItem = SelectFastaItem(proteoformListSelected, fastaItem, proteinAccession);
                    }
                }
                else
                    fastaItem = SelectFastaItem(proteoformListSelected, fastaItem, proteinAccession);

                var proteoformGroupedList = (from protm in proteoformListSelected
                                             group protm by protm.PrSMs[0].TheoreticalMass into newGroup
                                             orderby newGroup.Key
                                             select newGroup).ToList();
                foreach (var proteoformGroup in proteoformGroupedList)
                {
                    foreach (var proteoform in proteoformGroup)
                    {
                        proteoformList.Add(proteoform);
                        break;
                    }
                }
                proteoformList.Sort((a, b) => b.PrSMs[0].FinalScore.CompareTo(a.PrSMs[0].FinalScore));
                #endregion

                #region Peptides
                ProteinGroup proteinGroup = null;
                string proteinID = selectedRow.Cells["AccessionNumber"].Value.ToString();
                string[] cols = Regex.Split(proteinID, ";");
                if (cols.Length == 1)
                {
                    proteinGroup = Core.MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(cols[0])).FirstOrDefault();
                    if (proteinGroup != null)
                    {
                        if (fastaItem == null)
                            fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(proteinAccession)).FirstOrDefault();
                    }
                }
                else
                {
                    foreach (string accession in cols)
                    {
                        proteinGroup = Core.MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(proteinID)).FirstOrDefault();
                        if (proteinGroup != null)
                        {
                            if (fastaItem == null)
                                fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(accession)).FirstOrDefault();
                            if (fastaItem != null) break;
                        }
                    }
                }

                if (proteinGroup == null)
                {
                    cols = Regex.Split(proteinID, "\\|");
                    if (cols.Length == 1)
                    {
                        proteinGroup = Core.MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(cols[0])).FirstOrDefault();
                        if (proteinGroup != null)
                        {
                            if (fastaItem == null)
                                fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(proteinAccession)).FirstOrDefault();
                        }
                    }
                    else
                    {
                        foreach (string accession in cols)
                        {
                            if (accession.Equals("sp") || accession.Equals("tr")) continue;
                            proteinGroup = Core.MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(accession)).FirstOrDefault();
                            if (proteinGroup != null)
                            {
                                if (fastaItem == null)
                                    fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(accession)).FirstOrDefault();
                                if (fastaItem != null) break;
                            }
                        }
                    }
                }

                if (proteinGroup != null)
                {
                    SelectedPeptides = new List<Peptide>();
                    foreach (ProteinGroupParams ptnGroupParam in proteinGroup.Params)
                    {
                        List<ulong> ids = ptnGroupParam.PeptideIds.Distinct().ToList();
                        List<Peptide> pepts = (from pepti in Core.MyResults.PeptideList.AsParallel()
                                               where pepti.PSMs.Any(item => ids.Contains(item.PeptideID) && pepti.PSMs.Any(item1 => item1.ProteinGroupIDs.Contains(ptnGroupParam.ProteinGroupID)) && item.SearchEngine.Equals(ptnGroupParam.SearchEngine))
                                               select pepti).ToList();
                        SelectedPeptides.AddRange(pepts);
                    }

                    SelectedPeptides = SelectedPeptides.Distinct(new PeptideComparer()).ToList();
                }
                else
                    SelectedPeptides = null;
                #endregion

                if (fastaItem == null)
                {
                    MessageBox.Show(
                        "Protein sequence not found.",
                        "Warning",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return;
                }

                List<TandemMassSpectrum> selectedMSMS = null;
                if (SelectedPeptides != null)
                {
                    List<ulong> msmsIds = new List<ulong>();
                    foreach (Peptide peptide in SelectedPeptides)
                        msmsIds.AddRange(peptide.PSMs.Select(item => item.BestMSMS));
                    msmsIds = msmsIds.Distinct().ToList();
                    selectedMSMS = (from tms in Core.MyResults.TandemMassSpectraMQList.AsParallel()
                                    where msmsIds.Contains(tms.MSMSID) &&
                                    proteinGroup.Params.Select(item => item.ProteinGroupID).Intersect(tms.ProteinGroupID).Count() > 0
                                    select tms).ToList();
                    selectedMSMS.Sort((a, b) => b.Score.CompareTo(a.Score));

                    if (SortPeptidesByStartPosition)//Sort peptides by start position
                    {
                        SelectedPeptides.Sort((a, b) => a.StartPosition.CompareTo(b.StartPosition));
                    }
                }

                int windowWidth = (fastaItem.Sequence.Length / 100) < 2 ? 1 : (fastaItem.Sequence.Length / 100) < 4 ? 2 : 3;
                ProteinCoverageForm proteinCoverageFrom = new ProteinCoverageForm();
                proteinCoverageFrom.Width = 1110 + 10 * (windowWidth - 1);
                proteinCoverageFrom.Setup(IsMultilineProteinSequence, MyOriginalCore.MyResults.PTMsUniprot, MyOriginalCore.MyResults.ResidueMass, SelectedPeptides, proteoformList, fastaItem, selectedMSMS);
                proteinCoverageFrom.ShowDialog();

                UpdateDataGridViewCombineResultsProteoform(proteinCoverageFrom.AnnotationProteoforms);
            }
        }

        private void buttonFilterResult_Click(object sender, EventArgs e)
        {
            PrepareFilterResults();
        }

        private void PrepareFilterResults(bool isNewResult = false)
        {
            if (MyOriginalCore == null) return;

            this.groupBoxFilter.Enabled = false;

            #region Cloning MyOriginalCore
            if (MyOriginalCore.MyResults.Proteins != null)
            {
                Console.WriteLine(" Cloning Proteins...");
                Core.MyResults.Proteins = new List<Protein>(MyOriginalCore.MyResults.Proteins.Count);
                MyOriginalCore.MyResults.Proteins.ForEach((item) =>
                {
                    Protein protein = new Protein();
                    protein.AccessionNumber = item.AccessionNumber;
                    protein.Peptides = item.Peptides;
                    protein.Sequence = item.Sequence;
                    protein.ProteinDescription = item.ProteinDescription;
                    protein.Proteoforms = item.Proteoforms;
                    protein.Score = item.Score;
                    protein.SequenceCoverage = item.SequenceCoverage;
                    Core.MyResults.Proteins.Add(protein);
                });
            }
            else
                Core.MyResults.Proteins = new List<Protein>();

            if (MyOriginalCore.MyResults.FastaItems != null)
            {
                Console.WriteLine(" Cloning Fasta items...");
                Core.MyResults.FastaItems = new List<FastaItem>(MyOriginalCore.MyResults.FastaItems.Count);
                MyOriginalCore.MyResults.FastaItems.ForEach((item) =>
                {
                    FastaItem fasta = new FastaItem();
                    fasta.Description = item.Description;
                    fasta.ExtraDouble = item.ExtraDouble;
                    fasta.Sequence = item.Sequence;
                    fasta.SequenceIdentifier = item.SequenceIdentifier;
                    fasta.SequenceInBytes = item.SequenceInBytes;
                    fasta.Chains = item.Chains;
                    fasta.TheoreticalPTMs = item.TheoreticalPTMs;
                    Core.MyResults.FastaItems.Add(fasta);
                });
            }
            else
                Core.MyResults.FastaItems = new List<FastaItem>();

            if (MyOriginalCore.MyResults.PeptideList != null)
            {
                Console.WriteLine(" Cloning Peptides...");
                Core.MyResults.PeptideList = new List<Peptide>(MyOriginalCore.MyResults.PeptideList.Count);
                MyOriginalCore.MyResults.PeptideList.ForEach((item) =>
                {
                    Peptide pept = new Peptide();
                    pept.Sequence = item.Sequence;
                    pept.Charges = item.Charges;
                    pept.Proteins = item.Proteins;
                    pept.StartPosition = item.StartPosition;
                    pept.EndPosition = item.EndPosition;
                    pept.PSMs = item.PSMs;
                    Core.MyResults.PeptideList.Add(pept);
                });
            }
            else
                Core.MyResults.PeptideList = new List<Peptide>();

            Core.MyResults.ProgramParams = new Utils.ProgramParams();
            Core.MyResults.ProgramParams.BottomUpDirectory = MyOriginalCore.MyResults.ProgramParams.BottomUpDirectory;
            Core.MyResults.ProgramParams.ProcessingTime = MyOriginalCore.MyResults.ProgramParams.ProcessingTime;
            Core.MyResults.ProgramParams.ProgramVersion = MyOriginalCore.MyResults.ProgramParams.ProgramVersion;
            Core.MyResults.ProgramParams.TopDownDirectory = MyOriginalCore.MyResults.ProgramParams.TopDownDirectory;

            Core.MyResults.ResidueMass = new ResidueMass();

            if (MyOriginalCore.MyResults.ProteinGroupList != null)
            {
                Console.WriteLine(" Cloning Protein groups...");
                Core.MyResults.ProteinGroupList = new List<ProteinGroup>(MyOriginalCore.MyResults.ProteinGroupList.Count);
                MyOriginalCore.MyResults.ProteinGroupList.ForEach((item) =>
                {
                    ProteinGroup protGroup = new ProteinGroup();
                    protGroup.FastaHeader = item.FastaHeader;
                    protGroup.MSMSCount = item.MSMSCount;
                    protGroup.ProteinID = item.ProteinID;
                    protGroup.SequenceLength = item.SequenceLength;
                    protGroup.Params = item.Params;
                    Core.MyResults.ProteinGroupList.Add(protGroup);
                });
            }
            else
                Core.MyResults.ProteinGroupList = new List<ProteinGroup>();

            if (MyOriginalCore.MyResults.Proteoforms != null)
            {
                Console.WriteLine(" Cloning Proteoforms...");
                Core.MyResults.Proteoforms = new List<Proteoform>(MyOriginalCore.MyResults.Proteoforms.Count);
                MyOriginalCore.MyResults.Proteoforms.ForEach(item =>
                {
                    Proteoform proteoform = new Proteoform(item.Sequence, item.ProteinAccession, item.ProteinDescription, item.AnnotatedSequence, item.SequenceCoverage, item.PrSMs);
                    Core.MyResults.Proteoforms.Add(proteoform);
                });
            }
            else
                Core.MyResults.Proteoforms = new List<Proteoform>();

            if (MyOriginalCore.MyResults.TandemMassSpectraMQList != null)
            {
                Console.WriteLine(" Cloning Tandem Mass Spectra...");
                Core.MyResults.TandemMassSpectraMQList = new List<TandemMassSpectrum>(MyOriginalCore.MyResults.TandemMassSpectraMQList.Count);
                MyOriginalCore.MyResults.TandemMassSpectraMQList.ForEach(item =>
                {
                    TandemMassSpectrum tandemMassSpectra = new TandemMassSpectrum(item.RAWfile, item.ScanNumber, item.Sequence, item.ModifiedSequence, item.Modifications, item.Proteins, item.Charge, item.MZ, item.Mass, item.PPMError, item.RetentionTime, item.Score, item.MatchedIons, "", "", item.NumberOfMatches, item.MSMSID, item.ProteinGroupID, item.PeptideID);
                    tandemMassSpectra.IntensitiesFragIons = item.IntensitiesFragIons;
                    tandemMassSpectra.MassesFragIons = item.MassesFragIons;
                    Core.MyResults.TandemMassSpectraMQList.Add(tandemMassSpectra);
                });
            }
            else
                Core.MyResults.TandemMassSpectraMQList = new List<TandemMassSpectrum>();
            #endregion

            double combScore = 0;
            string searchSequence = textBoxSearchSeqFilter.Text;
            ulong spectralCount = 0;
            ulong peptideCount = 0;
            bool onlyUniquePeptides = checkBoxPeptideUnique.Checked;

            if (!String.IsNullOrEmpty(((UpDownBase)numericUpDownCombScore).Text))
                combScore = Convert.ToDouble(numericUpDownCombScore.Value);
            else
            {
                ((UpDownBase)numericUpDownCombScore).Text = combScore.ToString();
                numericUpDownCombScore.Value = (decimal)combScore;
            }

            if (!String.IsNullOrEmpty(((UpDownBase)numericUpDownSpectralCount).Text))
                spectralCount = Convert.ToUInt64(numericUpDownSpectralCount.Value);
            else
            {
                ((UpDownBase)numericUpDownSpectralCount).Text = spectralCount.ToString();
                numericUpDownSpectralCount.Value = (decimal)spectralCount;
            }

            if (!String.IsNullOrEmpty(((UpDownBase)numericUpDownPeptideCount).Text))
                peptideCount = Convert.ToUInt64(numericUpDownPeptideCount.Value);
            else
            {
                ((UpDownBase)numericUpDownPeptideCount).Text = peptideCount.ToString();
                numericUpDownPeptideCount.Value = (decimal)peptideCount;
            }

            #region Storing filter
            string filterStored = "" + combScore + "###" + spectralCount + "###" + peptideCount;
            ProteoCombiner.Properties.Settings.Default.UserFilterStored = filterStored;
            ProteoCombiner.Properties.Settings.Default.Save();
            #endregion

            Console.WriteLine(" Filtering results...");
            this.FilterResults(isNewResult, combScore, searchSequence, spectralCount, peptideCount, onlyUniquePeptides);
            this.groupBoxFilter.Enabled = true;
        }

        private void FilterResults(bool isNewResult, double combScore, string searchSequence, ulong spectralCount, ulong peptideCount, bool onlyUniquePeptides)
        {
            if (!isNewResult)
            {
                #region Apply filter
                #region CombScore
                if (combScore > 0)
                {
                    Core.MyResults.Proteins = Core.MyResults.Proteins != null ? Core.MyResults.Proteins.Where(item => item.Score > combScore).AsParallel().ToList() : Core.MyResults.Proteins;
                }
                #endregion

                #region Spectral count
                if (spectralCount > 0 && peptideCount > 0)
                {
                    Core.MyResults.ProteinGroupList = Core.MyResults.ProteinGroupList.Where(item => item.MSMSCount > spectralCount && item.Params[0].PeptideIds.Count > (int)peptideCount).AsParallel().ToList();

                    List<ulong> peptideIds = Core.MyResults.ProteinGroupList.SelectMany(item => item.Params.SelectMany(item1 => item1.PeptideIds)).Distinct().ToList();
                    Core.MyResults.PeptideList = Core.MyResults.PeptideList.Where(item => item.PSMs.Any(item1 => peptideIds.Contains(item1.PeptideID))).AsParallel().Distinct(new PeptideComparer()).ToList();
                    Core.MyResults.Proteins = Core.MyResults.Proteins.Where(item => item.Peptides != null && item.Peptides.Count > (int)peptideCount).ToList();
                }
                else if (spectralCount > 0)
                {
                    #region bottom-up

                    List<ProteinGroup> exclusionProteinGroupMQ = Core.MyResults.ProteinGroupList.Where(item => item.MSMSCount < spectralCount).AsParallel().ToList();
                    Core.MyResults.ProteinGroupList = Core.MyResults.ProteinGroupList.Except(exclusionProteinGroupMQ).AsParallel().ToList();

                    List<Peptide> validPeptides = new List<Peptide>();
                    Core.MyResults.ProteinGroupList.ForEach(protein =>
                    {
                        validPeptides.AddRange(Core.MyResults.PeptideList.Where(item => item.PSMs[0].ProteinGroupIDs.Contains(protein.Params[0].ProteinGroupID)));
                    });
                    Core.MyResults.PeptideList = validPeptides;
                    #endregion
                }
                #endregion

                #region Peptide count
                else if (peptideCount > 0)
                {
                    #region bottom-up
                    List<ProteinGroup> exclusionProteinGroupMQ = Core.MyResults.ProteinGroupList.Where(item => item.Params[0].PeptideCounts[0] < peptideCount).AsParallel().ToList();
                    Core.MyResults.ProteinGroupList = Core.MyResults.ProteinGroupList.Except(exclusionProteinGroupMQ).AsParallel().ToList();

                    List<Peptide> validPeptides = new List<Peptide>();
                    Core.MyResults.ProteinGroupList.ForEach(protein =>
                    {
                        validPeptides.AddRange(Core.MyResults.PeptideList.Where(item => item.PSMs[0].ProteinGroupIDs.Contains(protein.Params[0].ProteinGroupID)));
                    });
                    Core.MyResults.PeptideList = validPeptides;
                    Core.MyResults.Proteins = Core.MyResults.Proteins.Where(item => item.Peptides != null && item.Peptides.Count > (int)peptideCount).ToList();
                    #endregion
                }
                #endregion

                #region Consider only unique peptides
                if (onlyUniquePeptides)
                {
                    Core.MyResults.PeptideList = Core.MyResults.PeptideList.Where(item => item.PSMs.Any(item1 => item1.IsUnique)).AsParallel().ToList();
                    List<ulong> proteingGroupIDs = Core.MyResults.PeptideList.SelectMany(item => item.PSMs.SelectMany(item1 => item1.ProteinGroupIDs)).Distinct().ToList();
                    Core.MyResults.ProteinGroupList = Core.MyResults.ProteinGroupList.Where(item => item.Params.Any(item1 => proteingGroupIDs.Contains(item1.ProteinGroupID))).AsParallel().Distinct(new ProteinGroupComparer()).ToList();

                    Core.MyResults.Proteins = (from ptn in Core.MyResults.Proteins.AsParallel()
                                               where ptn.Peptides != null
                                               from pept in ptn.Peptides.AsParallel()
                                               where pept.PSMs.Any(item1 => item1.IsUnique)
                                               select ptn).ToList();
                }
                #endregion

                #region Search Sequence
                if (!String.IsNullOrEmpty(searchSequence))
                {
                    Core.MyResults.Proteins = Core.MyResults.Proteins.Where(item =>
                    item.AccessionNumber.Contains(searchSequence) ||
                    item.ProteinDescription.Contains(searchSequence) ||
                    (item.Proteoforms != null && item.Proteoforms.Any(protfm =>
                        protfm.ProteinAccession.Contains(searchSequence) ||
                        protfm.Sequence.Contains(searchSequence) ||
                        protfm.ProteinDescription.ToLower().Contains(searchSequence.ToLower())
                        )) ||
                    (item.Peptides != null && item.Peptides.Any(pept => pept.Sequence.Contains(searchSequence))
                    )).AsParallel().ToList();


                    List<Proteoform> proteoformFiltered = (from protein in Core.MyResults.Proteins.AsParallel()
                                                           where protein.Proteoforms != null
                                                           from proteoform in protein.Proteoforms
                                                           select proteoform).ToList();

                    //Search proteoforms that contain some part of 'searchSequence' in the sequence
                    proteoformFiltered.AddRange((from proteoform in Core.MyResults.Proteoforms.AsParallel()
                                                 where proteoform.ProteinAccession.Contains(searchSequence) ||
                                                 proteoform.Sequence.Contains(searchSequence) ||
                                                 proteoform.ProteinDescription.ToLower().Contains(searchSequence.ToLower())
                                                 select proteoform).Distinct(new ProteoformComparer()).ToList());

                    Core.MyResults.Proteoforms = proteoformFiltered.Distinct(new ProteoformComparer()).ToList();

                    Core.MyResults.ProteinGroupList = (from proteinGrp in Core.MyResults.ProteinGroupList.AsParallel()
                                                       where (!String.IsNullOrEmpty(proteinGrp.Sequence) && proteinGrp.Sequence.ToLower().Contains(searchSequence.ToLower())) ||
                                                       FindPtnGroupAndProtein(searchSequence, proteinGrp)
                                                       select proteinGrp).Distinct(new ProteinGroupComparer()).ToList();

                    //Search peptides that contain some part of 'searchSequence' in the sequence
                    List<Peptide> peptideFiltered = (from peptide in Core.MyResults.PeptideList
                                                     where peptide.Sequence.ToLower().Contains(searchSequence.ToLower())
                                                     select peptide).ToList();

                    //Check peptides from proteinGroupList
                    Core.MyResults.ProteinGroupList.ForEach(protein =>
                    {
                        peptideFiltered.AddRange(Core.MyResults.PeptideList.Where(item => item.PSMs[0].ProteinGroupIDs.Contains(protein.Params[0].ProteinGroupID)));
                    });

                    Core.MyResults.PeptideList = peptideFiltered.Distinct(new PeptideComparer()).ToList();
                }
                #endregion


                Core.MyResults.PeptideList.Sort((a, b) => b.PSMs[0].PeptideScore.CompareTo(b.PSMs[0].PeptideScore));
                Core.MyResults.Proteoforms.Sort((a, b) => b.PrSMs[0].LogPScore.CompareTo(a.PrSMs[0].LogPScore));

                timerUpdateCombineResults.Enabled = false;
                #endregion
            }

            Console.WriteLine(" Filling data grids...");
            CleanCombineDataGridViews(-1);
            FillDataGridCombineResults();
            FillDataGridBottomUpProteinGroup();
            FillDataGridTopDown();
            FillDataGridInformation();
            CleanBUPDataGridViews(0);
        }

        private bool FindPtnGroupAndProtein(string SequenceIdentifier, ProteinGroup proteinGroupList)
        {
            string[] colsPept = Regex.Split(SequenceIdentifier, ";");
            if (colsPept.Length == 1)
            {
                if (proteinGroupList.ProteinID.Contains(colsPept[0]))
                    return true;
            }
            else
            {
                foreach (string accession in colsPept)
                {
                    if (proteinGroupList.ProteinID.Contains(accession))
                        return true;
                }
            }

            colsPept = Regex.Split(SequenceIdentifier, "\\|");
            if (colsPept.Length > 1)
            {
                foreach (string accession in colsPept)
                {
                    if (accession.Equals("sp") || accession.Equals("tr")) continue;

                    if (proteinGroupList.ProteinID.Contains(accession))
                        return true;
                }
            }

            return false;
        }

        private void buttonResetFilter_Click(object sender, EventArgs e)
        {
            string filterStored = ProteoCombiner.Properties.Settings.Default.UserFilterStored;

            double combScore = 0;
            int spectralCount = 0;
            int peptideCount = 0;

            numericUpDownCombScore.Value = (decimal)combScore;
            numericUpDownSpectralCount.Value = (decimal)spectralCount;
            numericUpDownPeptideCount.Value = (decimal)peptideCount;
            textBoxSearchSeqFilter.Text = string.Empty;
            checkBoxPeptideUnique.Checked = false;

            PrepareFilterResults();
            this.tabControlResults.SelectedIndex = 0;
        }

        private void dataGridViewCombineResults_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewCombineResults.SelectedCells.Count == 0) return;
            int selectedCellCount = dataGridViewCombineResults.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = dataGridViewCombineResults.Rows[selectedCellCount];
            if (selectedRow.Cells["AccessionNumber"].Value != null)
            {
                string accessionNumber = selectedRow.Cells["AccessionNumber"].Value.ToString();

                if (CombineResultsProteoformPeptides == null || CombineResultsProteoformPeptides.Count == 0)
                {
                    FillDataGridCombineResultsProteoform(accessionNumber);
                    return;
                }
                List<string> accesionNumberSplited = Regex.Split(accessionNumber, ";|\\|").ToList();
                List<string> selectedProteinIdFiltered = (from comb in CombineResultsProteoformPeptides.AsParallel()
                                                          select comb.Item1).ToList();

                if (!accesionNumberSplited.Intersect(selectedProteinIdFiltered).Any())
                    FillDataGridCombineResultsProteoform(accessionNumber);
            }
        }

        /// <summary>
        /// Method responsible for resizing datagridview.
        /// </summary>
        public void MeasureDataGridViews(bool isNewResult = false)
        {
            int tabHeight = tabControlResults.Height;

            #region Combine Results DataGridView
            bool dgvProteoform = this.dataGridViewCombineResultsProteoform.Visible;
            double maxEachTab = dgvProteoform || isNewResult ? tabHeight / 3 : tabHeight / 2;
            dataGridViewCombineResults.Height = (int)maxEachTab - 10;
            dataGridViewCombineResultsProteoform.Height = (int)maxEachTab - 10;
            if (dgvProteoform || isNewResult)
            {
                dataGridViewCombineResultsProteoform.Location = new Point(dataGridViewCombineResultsProteoform.Location.X, (int)maxEachTab);
                dataGridViewCombineResultsPeptide.Location = new Point(dataGridViewCombineResultsPeptide.Location.X, (int)(2 * maxEachTab));
            }
            else
                dataGridViewCombineResultsPeptide.Location = new Point(dataGridViewCombineResultsPeptide.Location.X, (int)maxEachTab);
            dataGridViewCombineResultsPeptide.Height = (int)maxEachTab - 35;
            #endregion

            #region Bottom-up Datagridview
            maxEachTab = tabHeight / 3;
            dataGridViewBottomUpPG.Height = (int)maxEachTab - 10;
            dataGridViewBottomUpPept.Height = (int)maxEachTab - 5;
            dataGridViewBottomUpSpec.Height = (int)maxEachTab;
            dataGridViewBottomUpPept.Location = new Point(dataGridViewBottomUpPept.Location.X, (int)maxEachTab);
            dataGridViewBottomUpSpec.Location = new Point(dataGridViewBottomUpSpec.Location.X, (int)(2 * maxEachTab));
            #endregion
        }

        private void dataGridViewCombineResults_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            CleanCombineDataGridViews(0);
            dataGridViewCombineResults_CellClick(sender, e);
        }

        private void dataGridViewCombineResultsProteoform_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                DataGridViewRow currentRow = dataGridViewCombineResultsProteoform.Rows[e.RowIndex];
                if (currentRow.Cells[1].Value != null && !String.IsNullOrEmpty(currentRow.Cells[1].Value.ToString())
                    && e.RowIndex % 2 == 0)
                {
                    e.CellStyle.BackColor = System.Drawing.Color.LightGray;
                }
            }
        }

        private void dataGridViewCombineResultsProteoform_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int selectedCellCount = dataGridViewCombineResultsProteoform.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = dataGridViewCombineResultsProteoform.Rows[selectedCellCount];
            if (selectedRow.Cells["ProteinAccession"].Value != null && !String.IsNullOrEmpty(selectedRow.Cells["ProteinAccession"].Value.ToString()) && selectedRow.Cells["Sequence"].Value != null)
            {
                #region Proteoform
                List<Proteoform> proteoformList = new List<Proteoform>();
                FastaItem fastaItem = null;

                string proteinAccession = selectedRow.Cells["ProteinAccession"].Value.ToString();
                string sequence = selectedRow.Cells["Sequence"].Value.ToString();
                double theoreticalMass = Convert.ToDouble(selectedRow.Cells["TheoreticalMass"].Value.ToString());

                proteoformList = Core.MyResults.Proteoforms.Where(item => (Math.Truncate(100 * item.PrSMs[0].TheoreticalMass) / 100) == theoreticalMass && item.ProteinAccession.Contains(proteinAccession) && item.Sequence.Equals(sequence)).ToList();
                proteoformList.Sort((a, b) => b.PrSMs[0].LogPScore.CompareTo(a.PrSMs[0].LogPScore));

                if (proteoformList != null && proteoformList.Count > 0)
                {
                    fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(proteinAccession)).FirstOrDefault();
                    if (fastaItem == null)
                    {
                        fastaItem = SelectFastaItem(proteoformList, fastaItem, proteinAccession, sequence);
                    }
                }
                else
                    fastaItem = SelectFastaItem(proteoformList, fastaItem, proteinAccession, sequence);

                #endregion

                #region Peptides
                ProteinGroup proteinGroup = null;
                string proteinID = selectedRow.Cells["ProteinAccession"].Value.ToString();
                string[] cols = Regex.Split(proteinID, ";");
                if (cols.Length == 1)
                {
                    proteinGroup = Core.MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(cols[0])).FirstOrDefault();
                    if (proteinGroup != null)
                    {
                        if (fastaItem == null) fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(proteinAccession)).FirstOrDefault();
                    }
                }
                else
                {
                    proteinGroup = Core.MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(proteinID)).FirstOrDefault();
                    foreach (string accession in cols)
                    {
                        if (proteinGroup != null)
                        {
                            if (fastaItem == null) fastaItem = Core.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(accession)).FirstOrDefault();
                            if (fastaItem != null) break;
                        }
                    }
                }

                if (proteinGroup != null)
                {
                    SelectedPeptides = new List<Peptide>();
                    foreach (ProteinGroupParams ptnGroupParam in proteinGroup.Params)
                    {
                        List<ulong> ids = ptnGroupParam.PeptideIds.Distinct().ToList();
                        List<Peptide> pepts = (from pepti in Core.MyResults.PeptideList
                                               where pepti.PSMs.Any(item => ids.Contains(item.PeptideID) && pepti.PSMs.Any(item1 => item1.ProteinGroupIDs.Contains(ptnGroupParam.ProteinGroupID)) && item.SearchEngine.Equals(ptnGroupParam.SearchEngine))
                                               select pepti).ToList();
                        SelectedPeptides.AddRange(pepts);
                    }

                    SelectedPeptides = SelectedPeptides.Distinct(new PeptideComparer()).ToList();

                }

                List<TandemMassSpectrum> selectedMSMS = null;
                if (SelectedPeptides != null)
                {
                    List<ulong> msmsIds = new List<ulong>();
                    foreach (Peptide peptide in SelectedPeptides)
                        msmsIds.AddRange(peptide.PSMs.Select(item => item.BestMSMS));
                    msmsIds = msmsIds.Distinct().ToList();
                    selectedMSMS = (from tms in Core.MyResults.TandemMassSpectraMQList.AsParallel()
                                    where msmsIds.Contains(tms.MSMSID) &&
                                    proteinGroup.Params.Select(item => item.ProteinGroupID).Intersect(tms.ProteinGroupID).Count() > 0
                                    select tms).ToList();
                    selectedMSMS.Sort((a, b) => b.Score.CompareTo(a.Score));
                }

                List<(Proteoform, List<(Peptide, List<TandemMassSpectrum>)>)> validProteoformAndPeptides = Core.FitProteoformsAndPeptides(fastaItem.Sequence, new List<Proteoform>() { proteoformList[0] }, SelectedPeptides, selectedMSMS, SortPeptidesByStartPosition);
                #endregion

                if (fastaItem == null)
                {
                    MessageBox.Show(
                        "Protein sequence not found.",
                        "Warning",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return;
                }

                int windowWidth = (fastaItem.Sequence.Length / 100) < 2 ? 1 : (fastaItem.Sequence.Length / 100) < 4 ? 2 : 3;
                ProteinCoverageForm proteinCoverageFrom = new ProteinCoverageForm();
                proteinCoverageFrom.Width = 1110 + 10 * (windowWidth - 1);
                proteinCoverageFrom.Setup(IsMultilineProteinSequence, MyOriginalCore.MyResults.PTMsUniprot, MyOriginalCore.MyResults.ResidueMass, validProteoformAndPeptides, fastaItem);
                proteinCoverageFrom.ShowDialog();

                UpdateDataGridViewCombineResultsProteoform(proteinCoverageFrom.AnnotationProteoforms);
            }
            else
            {
                MessageBox.Show(
                       "Select a valid row.",
                       "Warning",
                       MessageBoxButtons.OK,
                       MessageBoxIcon.Warning);
            }
        }

        private void UpdateDataGridViewCombineResultsProteoform(List<(string, List<(double, string, bool, double)>)> AnnotationProteoforms)
        {
            if (AnnotationProteoforms == null) return;
            foreach ((string, List<(double, string, bool, double)>) annotatedProteoform in AnnotationProteoforms)
            {
                Proteoform proteoform = (from protfm in ProteoformGroupedList
                                         where protfm.AnnotatedSequence.Equals(annotatedProteoform.Item1) &&
                                         protfm.PrSMs.Count > 0 && annotatedProteoform.Item2.Count > 0 &&
                                         protfm.PrSMs[0].FinalScore == annotatedProteoform.Item2[0].Item1 &&
                                         protfm.PrSMs[0].TheoreticalMass == annotatedProteoform.Item2[0].Item4
                                         select protfm).FirstOrDefault();
                if (proteoform != null)
                    proteoform.PrSMs.ForEach(param => { param.IsValid = annotatedProteoform.Item2[0].Item3; });
            }

            for (int i = 0; i < dataGridViewCombineResultsProteoform.RowCount; i++)
            {
                DataGridViewCheckBoxCell cell = (DataGridViewCheckBoxCell)dataGridViewCombineResultsProteoform.Rows[i].Cells[0];
                cell.Value = ProteoformGroupedList[i].PrSMs[0].IsValid;
            }
        }

        private void dataGridViewBottomUpSpec_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (dataGridViewBottomUpSpec.SelectedCells.Count == 0) return;
            int selectedCellCount = dataGridViewBottomUpSpec.SelectedCells[0].RowIndex;

            TandemMassSpectrum tms = SelectedMSMS[selectedCellCount];

            if (tms.MassesFragIons == null || tms.MassesFragIons.Count == 0)
            {
                MessageBox.Show(
                                "Spectrum not found.",
                                "Warning",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return;
            }

            MSLight tmsLight = new MSLight();
            tmsLight.ChargedPrecursor = tms.Charge;
            tmsLight.Intensity = tms.IntensitiesFragIons;
            tmsLight.MZ = tms.MassesFragIons;

            #region Get Activation Type
            bool IonA = false;
            bool IonB = false;
            bool IonC = false;
            bool IonX = false;
            bool IonY = false;
            bool IonZ = false;

            switch (tms.Fragmentation)
            {
                case ActivationType.EThCD:
                    IonA = false;
                    IonB = true;
                    IonC = true;
                    IonX = false;
                    IonY = true;
                    IonZ = true;
                    break;
                case ActivationType.CID:
                    IonA = true;
                    IonB = true;
                    IonC = false;
                    IonX = false;
                    IonY = true;
                    IonZ = false;
                    break;
                case ActivationType.ETD:
                    IonA = false;
                    IonB = false;
                    IonC = true;
                    IonX = false;
                    IonY = false;
                    IonZ = true;
                    break;
                case ActivationType.HCD:
                    IonA = true;
                    IonB = true;
                    IonC = false;
                    IonX = false;
                    IonY = true;
                    IonZ = false;
                    break;
                case ActivationType.ECD:
                    IonA = false;
                    IonB = false;
                    IonC = true;
                    IonX = false;
                    IonY = false;
                    IonZ = true;
                    break;
                case ActivationType.MPD:
                    IonA = true;
                    IonB = true;
                    IonC = false;
                    IonX = false;
                    IonY = true;
                    IonZ = false;
                    break;
                case ActivationType.PQD://Pulsed Q-dissociation -> similar CID
                    IonA = true;
                    IonB = true;
                    IonC = false;
                    IonX = false;
                    IonY = true;
                    IonZ = false;
                    break;
                default:
                    IonA = true;
                    IonB = true;
                    IonC = false;
                    IonX = false;
                    IonY = true;
                    IonZ = false;
                    break;
            }
            #endregion

            SpectrumViewer2.PeprideSpectrumViewer.MSViewerWindow svf = new SpectrumViewer2.PeprideSpectrumViewer.MSViewerWindow();
            double ppm = 450;
            svf.PlotSpectrum(tmsLight.Ions.Select(a => new Tuple<float, float>((float)a.MZ, (float)a.Intensity)).ToList(), ppm, tms.Sequence, PatternTools.PTMMods.DefaultModifications.TheModifications, true, IonA, IonB, IonC, IonX, IonY, IonZ, (int)tmsLight.ChargedPrecursor);
            svf.ShowDialog();
        }

        private void textBoxSearchSeqFilter_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsLetter(e.KeyChar)
                || Char.IsNumber(e.KeyChar)
                || e.KeyChar == 45 /* hyphen ascii code */
                //|| e.KeyChar == 40 /* '(' */
                //|| e.KeyChar == 41 /* ')' */
                || e.KeyChar == 46 /* '.' */
                || e.KeyChar == 8 /* 'backspace' */
                || e.KeyChar == 22 /* CTRL + V */
                || e.KeyChar == 3 /* CTRL + C */
                || e.KeyChar == 95 /* 'underline' _ */
                || e.KeyChar == 32 /* space */
                ))
            {
                e.Handled = true;
            }
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
                this.buttonFilterResult_Click(sender, e);
        }

        private void numericUpDownSpectralCount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
                this.buttonFilterResult_Click(sender, e);
        }

        private void numericUpDownPeptideCount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
                this.buttonFilterResult_Click(sender, e);
        }

        private void numericUpDownPPMTdFilter_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
                this.buttonFilterResult_Click(sender, e);
        }

        private void numericUpDownPPMBuFilter_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
                this.buttonFilterResult_Click(sender, e);
        }

        private void numericUpDownScoreFilterTD_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
                this.buttonFilterResult_Click(sender, e);
        }

        private void numericUpDownScoreFilterBU_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
                this.buttonFilterResult_Click(sender, e);
        }

        private void dataGridViewCombineResultsPeptide_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                DataGridViewRow currentRow = dataGridViewCombineResultsPeptide.Rows[e.RowIndex];
                if (currentRow.Cells[0].Value != null && !String.IsNullOrEmpty(currentRow.Cells[0].Value.ToString())
                    && e.RowIndex % 2 == 0)
                {
                    e.CellStyle.BackColor = System.Drawing.Color.LightGray;
                }
            }
        }

        private void dataGridViewCombineResultsProteoform_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewCombineResultsProteoform.SelectedCells.Count == 0) return;
            int selectedCellCount = dataGridViewCombineResultsProteoform.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = dataGridViewCombineResultsProteoform.Rows[selectedCellCount];
            if (selectedRow.Cells["ProteinAccession"].Value != null)
            {
                string proteinAccession = selectedRow.Cells["ProteinAccession"].Value.ToString();
                string sequence = selectedRow.Cells["Sequence"].Value.ToString();
                double theoreticalMass = Convert.ToDouble(selectedRow.Cells["TheoreticalMass"].Value.ToString());

                FillDataGridCombineResultsPeptide(null, proteinAccession, sequence, theoreticalMass);
            }
        }

        private void dataGridViewCombineResultsProteoform_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            CleanCombineDataGridViews(1);
            dataGridViewCombineResultsProteoform_CellClick(sender, e);
        }

        private void dataGridViewCombineResultsPeptide_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewCombineResultsPeptide.SelectedCells.Count == 0 || SelectedPeptides == null) return;
            int selectedCellCount = dataGridViewCombineResultsPeptide.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = dataGridViewCombineResultsPeptide.Rows[selectedCellCount];
            string originalPeptideSequence = selectedRow.Cells["PeptideSequence"].Value.ToString();
            string cleanPeptide = Util.CleanPeptide(originalPeptideSequence);
            int peptideIndex = SelectedPeptides.FindIndex(item => item.Sequence.Equals(originalPeptideSequence));
            if (peptideIndex == -1)
            {
                peptideIndex = SelectedPeptides.FindIndex(item => item.Sequence.Contains(cleanPeptide));
                if (peptideIndex == -1)
                {
                    MessageBox.Show(
                                    "Spectrum not found.",
                                    "Warning",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Warning);
                    return;
                }
            }
            Peptide peptide = SelectedPeptides[peptideIndex];
            List<ulong> msmsIds = peptide.PSMs.Select(item => item.BestMSMS).Distinct().ToList();
            SelectedMSMS = (from tms2 in MyOriginalCore.MyResults.TandemMassSpectraMQList.AsParallel()
                            where msmsIds.Contains(tms2.MSMSID) &&
                            peptide.PSMs.Select(item => item.PeptideID).Contains(tms2.PeptideID)
                            select tms2).ToList();

            SelectedMSMS.Sort((a, b) => b.Score.CompareTo(a.Score));
            if (SelectedMSMS.Count == 0) return;
            TandemMassSpectrum tms = SelectedMSMS[0];

            if (tms.MassesFragIons == null || tms.MassesFragIons.Count == 0)
            {
                MessageBox.Show(
                                "Spectrum not found.",
                                "Warning",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return;
            }

            MSLight tmsLight = new MSLight();
            tmsLight.ChargedPrecursor = tms.Charge;
            tmsLight.Intensity = tms.IntensitiesFragIons;
            tmsLight.MZ = tms.MassesFragIons;

            #region Get Activation Type
            bool IonA = false;
            bool IonB = false;
            bool IonC = false;
            bool IonX = false;
            bool IonY = false;
            bool IonZ = false;

            switch (tms.Fragmentation)
            {
                case ActivationType.EThCD:
                    IonA = false;
                    IonB = true;
                    IonC = true;
                    IonX = false;
                    IonY = true;
                    IonZ = true;
                    break;
                case ActivationType.CID:
                    IonA = true;
                    IonB = true;
                    IonC = false;
                    IonX = false;
                    IonY = true;
                    IonZ = false;
                    break;
                case ActivationType.ETD:
                    IonA = false;
                    IonB = false;
                    IonC = true;
                    IonX = false;
                    IonY = false;
                    IonZ = true;
                    break;
                case ActivationType.HCD:
                    IonA = true;
                    IonB = true;
                    IonC = false;
                    IonX = false;
                    IonY = true;
                    IonZ = false;
                    break;
                case ActivationType.ECD:
                    IonA = false;
                    IonB = false;
                    IonC = true;
                    IonX = false;
                    IonY = false;
                    IonZ = true;
                    break;
                case ActivationType.MPD:
                    IonA = true;
                    IonB = true;
                    IonC = false;
                    IonX = false;
                    IonY = true;
                    IonZ = false;
                    break;
                case ActivationType.PQD://Pulsed Q-dissociation -> similar CID
                    IonA = true;
                    IonB = true;
                    IonC = false;
                    IonX = false;
                    IonY = true;
                    IonZ = false;
                    break;
                default:
                    IonA = true;
                    IonB = true;
                    IonC = false;
                    IonX = false;
                    IonY = true;
                    IonZ = false;
                    break;
            }
            #endregion

            SpectrumViewer2.PeprideSpectrumViewer.MSViewerWindow svf = new SpectrumViewer2.PeprideSpectrumViewer.MSViewerWindow();
            double ppm = 20;
            svf.PlotSpectrum(tmsLight.Ions.Select(a => new Tuple<float, float>((float)a.MZ, (float)a.Intensity)).ToList(), ppm, tms.Sequence, PatternTools.PTMMods.DefaultModifications.TheModifications, true, IonA, IonB, IonC, IonX, IonY, IonZ, (int)tmsLight.ChargedPrecursor);
            svf.ShowDialog();
        }

        private void dataGridViewCombineResultsProteoform_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                DataGridViewCheckBoxCell cell = (DataGridViewCheckBoxCell)dataGridViewCombineResultsProteoform.Rows[e.RowIndex].Cells[0];

                if (Convert.ToBoolean(dataGridViewCombineResultsProteoform.Rows[e.RowIndex].Cells[0].EditedFormattedValue) == true)
                    cell.Value = false;
                else
                    cell.Value = true;

                int selectedCellCount = dataGridViewCombineResultsProteoform.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = dataGridViewCombineResultsProteoform.Rows[selectedCellCount];
                if (selectedRow.Cells["ProteinAccession"].Value != null && !String.IsNullOrEmpty(selectedRow.Cells["ProteinAccession"].Value.ToString()) && selectedRow.Cells["Sequence"].Value != null)
                {
                    #region Proteoform
                    List<Proteoform> proteoformList = null;

                    string proteinAccession = selectedRow.Cells["ProteinAccession"].Value.ToString();
                    string sequence = selectedRow.Cells["Sequence"].Value.ToString();
                    double theoreticalMass = Convert.ToDouble(selectedRow.Cells["TheoreticalMass"].Value.ToString());

                    proteoformList = MyOriginalCore.MyResults.Proteoforms.Where(item => (Math.Truncate(100 * item.PrSMs[0].TheoreticalMass) / 100) == theoreticalMass && item.ProteinAccession.Contains(proteinAccession) && item.Sequence.Equals(sequence)).ToList();

                    if (proteoformList != null && proteoformList.Count > 0)
                        proteoformList.ForEach(item => { item.PrSMs[0].IsValid = (bool)cell.Value; });

                    proteoformList = Core.MyResults.Proteoforms.Where(item => (Math.Truncate(100 * item.PrSMs[0].TheoreticalMass) / 100) == theoreticalMass && item.ProteinAccession.Contains(proteinAccession) && item.Sequence.Equals(sequence)).ToList();
                    if (proteoformList != null && proteoformList.Count > 0)
                        proteoformList.ForEach(item => { item.PrSMs[0].IsValid = (bool)cell.Value; });

                    #endregion
                }
            }
        }

        private void timerUpdateCombineResults_Tick(object sender, EventArgs e)
        {
            //if(Core != null && Core.finishCombiningData)
            //{
            //    MyOriginalCore.MyResults.Proteins = Core.MyResults.Proteins;
            //    FillDataGridCombineResults();
            //    FillDataGridInformation();
            timerUpdateCombineResults.Enabled = false;
            //}
        }
    }
}
