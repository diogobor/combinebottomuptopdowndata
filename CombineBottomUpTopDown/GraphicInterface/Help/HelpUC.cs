﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;

namespace CombineBottomUpTopDown.GraphicInterface.Help
{
    public partial class HelpUC : UserControl
    {
        private string manuscript_url = "https://protocolexchange.researchsquare.com/article/958ecb0d-f00e-48aa-a914-e37b4ce45ba1/v1";
        private string website_url = "https://proteocombiner.pasteur.fr/Work.html";

        public HelpUC()
        {
            InitializeComponent();
        }

        private void HelpUC_Load(object sender, EventArgs e)
        {
            if (InternetCS.CanConnectToURL(manuscript_url))
            {
                System.Diagnostics.Process.Start(manuscript_url);
                if (InternetCS.CanConnectToURL(website_url))
                    webBrowserHelp.Navigate(website_url);
            }
            else if (InternetCS.CanConnectToURL(website_url))
            {
                webBrowserHelp.Navigate(website_url);
            }
            else
                LoadOfflineProtocol();
        }

        public void UpdateWebPage()
        {
            HelpUC_Load(null, null);
        }

        public void LoadOfflineProtocol()
        {
            try
            {
                System.Diagnostics.Process.Start("proteocombiner_protocol.pdf");
            }
            catch (Exception e)
            {
                MessageBox.Show("Manual is not available!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonReturnWindow_Click(object sender, EventArgs e)
        {
            ((Form)this.TopLevelControl).Close();
        }

        private void buttonPDFReader_Click(object sender, EventArgs e)
        {
            this.LoadOfflineProtocol();
        }
    }
}
