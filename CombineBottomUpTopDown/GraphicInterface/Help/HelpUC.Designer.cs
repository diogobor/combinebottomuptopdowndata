﻿namespace CombineBottomUpTopDown.GraphicInterface.Help
{
    partial class HelpUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HelpUC));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonReturnWindow = new System.Windows.Forms.Button();
            this.webBrowserHelp = new System.Windows.Forms.WebBrowser();
            this.buttonPDFReader = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.buttonPDFReader);
            this.groupBox1.Controls.Add(this.buttonReturnWindow);
            this.groupBox1.Controls.Add(this.webBrowserHelp);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(562, 342);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // buttonReturnWindow
            // 
            this.buttonReturnWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReturnWindow.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonReturnWindow.Image = ((System.Drawing.Image)(resources.GetObject("buttonReturnWindow.Image")));
            this.buttonReturnWindow.Location = new System.Drawing.Point(481, 10);
            this.buttonReturnWindow.Name = "buttonReturnWindow";
            this.buttonReturnWindow.Size = new System.Drawing.Size(75, 27);
            this.buttonReturnWindow.TabIndex = 13;
            this.buttonReturnWindow.Text = "Return";
            this.buttonReturnWindow.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonReturnWindow.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonReturnWindow.UseVisualStyleBackColor = false;
            this.buttonReturnWindow.Click += new System.EventHandler(this.buttonReturnWindow_Click);
            // 
            // webBrowserHelp
            // 
            this.webBrowserHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webBrowserHelp.Location = new System.Drawing.Point(6, 43);
            this.webBrowserHelp.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowserHelp.Name = "webBrowserHelp";
            this.webBrowserHelp.Size = new System.Drawing.Size(550, 287);
            this.webBrowserHelp.TabIndex = 0;
            // 
            // buttonPDFReader
            // 
            this.buttonPDFReader.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonPDFReader.Image = ((System.Drawing.Image)(resources.GetObject("buttonPDFReader.Image")));
            this.buttonPDFReader.Location = new System.Drawing.Point(6, 10);
            this.buttonPDFReader.Name = "buttonPDFReader";
            this.buttonPDFReader.Size = new System.Drawing.Size(142, 27);
            this.buttonPDFReader.TabIndex = 14;
            this.buttonPDFReader.Text = "Open local protocol";
            this.buttonPDFReader.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonPDFReader.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonPDFReader.UseVisualStyleBackColor = false;
            this.buttonPDFReader.Click += new System.EventHandler(this.buttonPDFReader_Click);
            // 
            // HelpUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "HelpUC";
            this.Size = new System.Drawing.Size(568, 345);
            this.Load += new System.EventHandler(this.HelpUC_Load);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.WebBrowser webBrowserHelp;
        private System.Windows.Forms.Button buttonReturnWindow;
        private System.Windows.Forms.Button buttonPDFReader;
    }
}
