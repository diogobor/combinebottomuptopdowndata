﻿/**
 * Program:     ProteoCombiner - Integrating bottom-up & top-down proteomics data
 * Author:      Diogo Borges Lima
 * Update:      4/3/2019
 * Update by:   Diogo Borges Lima
 * Description: GUI class
 */
using CombineBottomUpTopDown.GraphicInterface;
using CombineBottomUpTopDown.GraphicInterface.Help;
using CombineBottomUpTopDown.GraphicInterface.Results;
using CombineBottomUpTopDown.Utils;
using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace CombineBottomUpTopDown
{
    public partial class GUI : Form
    {
        private TextWriter _writer = null; // That's our custom TextWriter class
        private Thread mainThread { get; set; }
        private Program mainProgramGUI { get; set; }
        private ResultsForm MyResultsForm = null;
        private string[] args { get; set; }

        public GUI(string[] args)
        {
            InitializeComponent();

            this.args = args;

            ActiveListBoxControl();
        }

        private void ActiveListBoxControl()
        {
            // Instantiate the writer
            _writer = new ListBoxStreamWriter(listBoxConsole);
            // Redirect the out Console stream

            // Close previous output stream and redirect output to standard output.
            Console.Out.Close();
            Console.SetOut(_writer);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
            System.Windows.Forms.Application.Exit();
        }

        private void buttonBottomUpResults_Click(object sender, EventArgs e)
        {
            var fsd = new FolderSelectDialog();
            if (fsd.ShowDialog(IntPtr.Zero))
            {
                if (!Util.UserHasAccess(fsd.FileName))
                {
                    MessageBox.Show(
                        "It's not a valid path. Please select another one.",
                        "Warning",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    textBoxBottomUpData.Text = "";
                    return;
                }
                textBoxBottomUpData.Text = fsd.FileName;
            }
        }

        private void buttonTopDownResults_Click(object sender, EventArgs e)
        {
            var fsd = new FolderSelectDialog();
            if (fsd.ShowDialog(IntPtr.Zero))
            {
                if (!Util.UserHasAccess(fsd.FileName))
                {
                    MessageBox.Show(
                        "It's not a valid path. Please select another one.",
                        "Warning",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    textBoxTopDownData.Text = "";
                    return;
                }
                textBoxTopDownData.Text = fsd.FileName;
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            ProgramParams myParams = GetParamsFromScreen();
            if (!CheckParams(myParams)) return;

            if (mainThread == null)
            {
                mainProgramGUI = new Program();
                mainProgramGUI.programParams = myParams;
                mainThread = new Thread(new ThreadStart(mainProgramGUI.Run));
            }

            if (buttonOK.Text.Equals("OK") && mainProgramGUI != null)
            {
                mainThread.Start();
                timerStatus.Enabled = true;

                #region Disabling some fields
                textBoxBottomUpData.Enabled = false;
                textBoxTopDownData.Enabled = false;
                buttonBottomUpResults.Enabled = false;
                buttonTopDownResults.Enabled = false;
                groupBoxAdvancedParams.Enabled = false;
                #endregion

                buttonOK.Text = "Stop";
                //change button icon
                buttonOK.Image = ProteoCombiner.Properties.Resources.button_cancel_little;
            }
            else if (mainProgramGUI != null)
            {
                DialogResult answer = MessageBox.Show("Are you sure you want to stop the process ?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (answer == DialogResult.Yes)
                {
                    timerStatus.Enabled = false;
                    #region Enabling some fields
                    textBoxBottomUpData.Enabled = true;
                    textBoxTopDownData.Enabled = true;
                    buttonBottomUpResults.Enabled = true;
                    buttonTopDownResults.Enabled = true;
                    groupBoxAdvancedParams.Enabled = true;
                    #endregion

                    buttonOK.Text = "OK";
                    //change button icon
                    buttonOK.Image = ProteoCombiner.Properties.Resources.goBtn;

                    if (mainThread != null)//When xlThread is null in this point is because the user stops the process, but before clicking on Yes button, the search is finished.
                    {
                        mainThread.Abort();
                        mainThread = null;
                    }
                }
            }
        }

        private bool CheckParams(ProgramParams myParams)
        {
            if (String.IsNullOrEmpty(myParams.BottomUpDirectory))
            {
                MessageBox.Show(
                        "'Bottom-up results' field is empty. Please, select one directory.",
                        "Warning",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                return false;
            }

            if (String.IsNullOrEmpty(myParams.TopDownDirectory))
            {
                MessageBox.Show(
                        "'Top-down results' field is empty. Please, select one directory.",
                        "Warning",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        private ProgramParams GetParamsFromScreen()
        {
            ProgramParams myParams = new ProgramParams();
            myParams.BottomUpDirectory = textBoxBottomUpData.Text;
            myParams.TopDownDirectory = textBoxTopDownData.Text;
            myParams.RemoveContaminants = checkBoxRemoveContaminants.Checked;
            myParams.RemoveRevSeq = checkBoxRemoveRevSeq.Checked;

            return myParams;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About aboutScreen = new About();
            aboutScreen.ShowDialog();
        }

        private void resultsBrowserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ResultsForm window = new ResultsForm();
            window.SetGUI(this);
            window.ShowDialog();
        }

        private void timerStatus_Tick(object sender, EventArgs e)
        {
            if (mainProgramGUI == null && MyResultsForm != null && MyResultsForm.ReturnSucessful)
            {
                timerStatus.Enabled = false;
                MyResultsForm.SetGUI(this);
                MyResultsForm.Show();
                #region Enable forms
                groupBoxMainWindow.Enabled = true;
                groupBoxAdvancedParams.Enabled = true;
                resultsBrowserToolStripMenuItem.Enabled = true;
                openToolStripMenuItem.Enabled = true;
                #endregion
            }
            else
            {
                if (buttonOK.Text.Equals("OK"))
                {
                    #region Disable forms
                    groupBoxMainWindow.Enabled = false;
                    groupBoxAdvancedParams.Enabled = false;
                    resultsBrowserToolStripMenuItem.Enabled = false;
                    openToolStripMenuItem.Enabled = false;
                    #endregion
                }
            }

            if (mainProgramGUI != null && mainProgramGUI.FinishProcessing)
            {
                mainThread = null;
                timerStatus.Enabled = false;

                if (!mainProgramGUI.ErrorProcessing)
                {
                    MessageBox.Show(
                            "Processing completed succesfully!\nTime: " + mainProgramGUI.FinalTime,
                            "Information",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);

                    ResultsForm resultsForm = new ResultsForm();
                    resultsForm.Setup(mainProgramGUI.mainCore);
                    resultsForm.ShowDialog();
                }

                #region Enabling some fields
                textBoxBottomUpData.Enabled = true;
                textBoxTopDownData.Enabled = true;
                buttonBottomUpResults.Enabled = true;
                buttonTopDownResults.Enabled = true;
                groupBoxAdvancedParams.Enabled = true;
                #endregion

                buttonOK.Text = "OK";
                //change button icon
                buttonOK.Image = ProteoCombiner.Properties.Resources.goBtn;
            }
        }

        public void LoadResultsFromWinResults(string fileName)
        {
            mainProgramGUI = null;
            timerStatus.Enabled = true;
            MyResultsForm = new ResultsForm();
            if (MyResultsForm.LoadResults(fileName))
            {
                #region Enable forms
                groupBoxMainWindow.Enabled = true;
                groupBoxAdvancedParams.Enabled = true;
                resultsBrowserToolStripMenuItem.Enabled = true;
                openToolStripMenuItem.Enabled = true;
                #endregion
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            #region Disable forms
            groupBoxMainWindow.Enabled = false;
            groupBoxAdvancedParams.Enabled = false;
            resultsBrowserToolStripMenuItem.Enabled = false;
            openToolStripMenuItem.Enabled = false;
            #endregion

            mainProgramGUI = null;
            timerStatus.Enabled = true;
            MyResultsForm = new ResultsForm();
            if (MyResultsForm.LoadResults())
            {
                timerStatus.Enabled = false;
                #region Enable forms
                groupBoxMainWindow.Enabled = true;
                groupBoxAdvancedParams.Enabled = true;
                resultsBrowserToolStripMenuItem.Enabled = true;
                openToolStripMenuItem.Enabled = true;
                #endregion
            }
        }

        private void readMeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HelpForm help = new HelpForm();
            help.ShowDialog();
        }

        private void GUI_Load(object sender, EventArgs e)
        {
            #region open automatically a pcmb file
            try
            {
                string[] myAppData = AppDomain.CurrentDomain.SetupInformation?.ActivationArguments?.ActivationData;

                if (myAppData != null)
                {
                    Console.WriteLine(" Loading file: " + myAppData[0]);

                    ResultsForm window = new ResultsForm();
                    if (window.LoadResultsFromScreen(myAppData[0]))
                        window.ShowDialog();
                    else
                        MessageBox.Show("Failed to load file!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception) { }

            if (args != null && args.Length > 0)
            {
                string[] myAppData = args;

                Console.WriteLine(" Loading file: " + myAppData[0]);

                ResultsForm window = new ResultsForm();
                if (window.LoadResultsFromScreen(myAppData[0]))
                    window.ShowDialog();
                else
                    MessageBox.Show("Failed to load file!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            #endregion
        }
    }
}
