﻿/**
 * Program:     ProteoCombiner - Integrating bottom-up & top-down proteomics data
 * Author:      Diogo Borges Lima
 * Created:     4/3/2019
 * Update by:   Diogo Borges Lima
 * Description: Main class
 */
using CombineBottomUpTopDown.Controller;
using CombineBottomUpTopDown.Model;
using CombineBottomUpTopDown.Parser;
using CombineBottomUpTopDown.Utils;
using MathNet.Numerics.Distributions;
using SEPRPackage;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CombineBottomUpTopDown
{
    class Program
    {
        public Core mainCore { get; set; }

        public bool FinishProcessing { get; set; }
        public bool ErrorProcessing { get; set; } = false;
        public ProgramParams programParams { get; set; }
        public string version = "";
        public string FinalTime { get; set; }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            #region Setting Language
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            if (!Microsoft.Win32.Registry.GetValue(@"HKEY_CURRENT_USER\Control Panel\International", "LocaleName", null).ToString().ToLower().Equals("en-us"))
            {
                DialogResult answer = MessageBox.Show("The default language is not English. Do you want to change it to English ?\nThis tool works if only the default language is English.", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (answer == DialogResult.Yes)
                {
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "Locale", "00000409");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "LocaleName", "en-US");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "sCountry", "Estados Unidos");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "sCurrency", "$");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "sDate", "/");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "sDecimal", ".");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "sGrouping", "3;0");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "sLanguage", "ENU");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "sList", ",");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "sLongDate", "dddd, MMMM dd, yyyy");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "sMonDecimalSep", ".");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "sMonGrouping", "3;0");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "sMonThousandSep", ",");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "sNativeDigits", "0123456789");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "sNegativeSign", "-");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "sPositiveSign", "");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "sShortDate", "M/d/yyyy");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "sThousand", ",");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "sTime", ":");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "sTimeFormat", "h:mm:ss tt");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "sShortTime", "h:mm tt");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "sYearMonth", "MMMM, yyyy");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "iCalendarType", "1");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "iCountry", "1");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "iCurrDigits", "2");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "iCurrency", "0");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "iDate", "0");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "iDigits", "2");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "NumShape", "1");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "iFirstDayOfWeek", "6");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "iFirstWeekOfYear", "0");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "iLZero", "1");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "iMeasure", "1");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "iNegCurr", "0");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "iNegNumber", "1");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "iPaperSize", "1");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "iTime", "0");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "iTimePrefix", "0");
                    Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\International", "iTLZero", "0");
                    MessageBox.Show("Software will be restarted!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    System.Environment.Exit(0);
                    System.Windows.Forms.Application.Exit();
                }
                else
                {
                    MessageBox.Show("Software will be closed!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    System.Environment.Exit(0);
                    System.Windows.Forms.Application.Exit();
                }
            }
            #endregion

            #region debug

            #endregion

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new GUI(args));
        }

        public void Run()
        {
            mainCore = new Core();
            mainCore.MyResults.ResidueMass = new ResidueMass();
            FinishProcessing = false;
            ErrorProcessing = false;
            DateTime beginTimeSearch = DateTime.Now;
            string version = "";
            try
            {
                version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
            catch (Exception e1)
            {
                //Unable to retrieve version number
                Console.WriteLine("", e1);
                version = "";
            }

            Console.WriteLine("############################################################################");
            Console.WriteLine("                                                              ProteoCombiner - v. " + version + "\n");
            Console.WriteLine("                         Engineered by Mass Spectrometry for Biology Unit - Institut Pasteur             \n");
            Console.WriteLine("############################################################################");

            //bool isMSFileReadInstalled = false;
            #region Getting spectra files and result files -> Bottom-up and/or Top-down
            string[] filesBottomUp = null;
            string[] filesBottomUpRAW = null;
            string[] filesTopDown = null;
            string[] filesTopDownRAW = null;

            filesBottomUp = Directory.GetFiles(programParams.BottomUpDirectory, "*.*", SearchOption.AllDirectories).Where(file => file.EndsWith(".txt") || file.EndsWith(".fasta") || file.ToLower().EndsWith(".raw") || file.ToLower().EndsWith(".sepr") || file.EndsWith(".xml") || file.EndsWith(".mzid") || file.ToLower().EndsWith(".mzml") || file.ToLower().EndsWith(".mzxml")).ToArray();
            filesBottomUpRAW = Directory.GetFiles(programParams.BottomUpDirectory, "*.*", SearchOption.AllDirectories).Where(file => file.ToLower().EndsWith(".raw") || file.ToLower().EndsWith(".mzml") || file.ToLower().EndsWith(".mzxml")).ToArray();
            filesTopDown = Directory.GetFiles(programParams.TopDownDirectory, "*.*", SearchOption.AllDirectories).Where(file => file.EndsWith("PSMs.txt") || file.EndsWith("PrSMs.txt") || file.EndsWith(".fasta") || file.EndsWith(".xml") || file.EndsWith(".html") || file.EndsWith(".csv")).ToArray();
            filesTopDownRAW = Directory.GetFiles(programParams.TopDownDirectory, "*.*", SearchOption.AllDirectories).Where(file => file.ToLower().EndsWith(".raw") || file.ToLower().EndsWith(".mzml") || file.ToLower().EndsWith(".mzxml")).ToArray();
            #endregion

            StringBuilder sbError = new StringBuilder();
            ParserXMLDB parserXMLdb = new ParserXMLDB();

            #region Reading Bottom-up files
            Console.WriteLine(" Reading Bottom-up files");
            ParserMzIdentML parserMzIdentML = new ParserMzIdentML();
            ParserMQ parserMQ = new ParserMQ();
            ParserPL parserPL = new ParserPL();
            ParserRAWThermo parserRawThermo = new ParserRAWThermo();
            ParserMzML parserMzML = new ParserMzML();
            List<ProteinGroup> proteinGroupMQList = new List<ProteinGroup>();
            List<Peptide> peptideList = new List<Peptide>();
            List<TandemMassSpectrum> tandemMassSpectraList = new List<TandemMassSpectrum>();

            List<FastaItem> fastaItemsBottomUp = new List<FastaItem>();
            if (filesBottomUp != null && filesBottomUp.Length > 0)
            {
                bool hasMQfiles = false;
                StringBuilder sbSeproFiles = new StringBuilder();
                StringBuilder sbMzIdentMLFiles = new StringBuilder();
                for (int i = 0; i < filesBottomUp.Length; i++)
                {
                    try
                    {
                        Console.WriteLine(" Processing file: " + (i + 1) + " of " + filesBottomUp.Length);
                        if (filesBottomUp[i].EndsWith(".fasta"))//Database
                            fastaItemsBottomUp.AddRange(parserMQ.ParseFastaFile(filesBottomUp[i]));
                        else if (filesBottomUp[i].EndsWith(".sepr"))//PatternLab/Comet/Sequest -> SEPro
                        {
                            //Sepro files need to be parsed at the end because the number of ProteinGroupID
                            sbSeproFiles.AppendLine(filesBottomUp[i]);
                        }
                        else if (filesBottomUp[i].EndsWith(".xml"))//Database
                        {
                            parserXMLdb.ParseXMLFile(filesBottomUp[i]);
                            fastaItemsBottomUp.AddRange(parserXMLdb.FastaItemsList);
                        }
                        else if (filesBottomUp[i].EndsWith("proteinGroups.txt"))//MaxQuant/Andromeda
                        {
                            hasMQfiles = true;
                            proteinGroupMQList.AddRange(parserMQ.ParseProteinGroupFile(filesBottomUp[i]));
                        }
                        else if (filesBottomUp[i].EndsWith("peptides.txt"))//MaxQuant/Andromeda
                            peptideList.AddRange(parserMQ.ParsePeptideFile(filesBottomUp[i]));
                        else if (filesBottomUp[i].EndsWith("msms.txt"))//MaxQuant/Andromeda
                            tandemMassSpectraList.AddRange(parserMQ.ParseMSMSFile(filesBottomUp[i]));
                        else if (filesBottomUp[i].EndsWith(".mzid"))//mzIdentML
                        {
                            //Sepro files need to be parsed at the end because the number of ProteinGroupID
                            sbMzIdentMLFiles.AppendLine(filesBottomUp[i]);
                        }

                    }
                    catch (Exception ex)
                    {
                        sbError.AppendLine("Bottom-up/Middle-down: " + filesBottomUp[i]);
                    }
                }

                #region Sepro files
                ulong lastProteinGroupID = proteinGroupMQList.Count > 0 ? proteinGroupMQList.Max(item => item.Params[0].ProteinGroupID) : 0;
                ulong lastPeptideID = peptideList.Count > 0 ? peptideList.Max(item => item.PSMs[0].PeptideID) : 0;
                ulong lastMSMSID = tandemMassSpectraList.Count > 0 ? tandemMassSpectraList.Max(item => item.MSMSID) : 0;

                foreach (string seproFile in System.Text.RegularExpressions.Regex.Split(sbSeproFiles.ToString(), "\r\n"))
                {
                    try
                    {
                        if (String.IsNullOrEmpty(seproFile)) continue;
                        parserPL.LastProteinGroupID = lastProteinGroupID;
                        parserPL.LastPeptideID = lastPeptideID;
                        parserPL.LastMSMSID = lastMSMSID;
                        parserPL.ParseFile(seproFile);
                        proteinGroupMQList.AddRange(parserPL.ProteinGroups);
                        peptideList.AddRange(parserPL.Peptides);
                        tandemMassSpectraList.AddRange(parserPL.TandemMassSpectra);
                        lastProteinGroupID = parserPL.ProteinGroups.Count > 0 ? parserPL.ProteinGroups.Max(item => item.Params[0].ProteinGroupID) : lastProteinGroupID;
                        lastPeptideID = parserPL.Peptides.Count > 0 ? parserPL.Peptides.Max(item => item.PSMs[0].PeptideID) : lastPeptideID;
                        lastMSMSID = parserPL.TandemMassSpectra.Count > 0 ? parserPL.TandemMassSpectra.Max(item => item.MSMSID) : lastMSMSID;
                    }
                    catch (Exception ex)
                    {
                        sbError.AppendLine("Bottom-up/Middle-down: " + seproFile);
                    }
                }

                #endregion

                #region mzIdentML files
                lastProteinGroupID = proteinGroupMQList.Count > 0 ? proteinGroupMQList.Max(item => item.Params[0].ProteinGroupID) : 0;
                lastPeptideID = peptideList.Count > 0 ? peptideList.Max(item => item.PSMs[0].PeptideID) : 0;
                lastMSMSID = tandemMassSpectraList.Count > 0 ? tandemMassSpectraList.Max(item => item.MSMSID) : 0;
                parserMzIdentML.FastaItems = fastaItemsBottomUp;
                foreach (string mzIdentML in System.Text.RegularExpressions.Regex.Split(sbMzIdentMLFiles.ToString(), "\r\n"))
                {
                    try
                    {
                        if (String.IsNullOrEmpty(mzIdentML)) continue;
                        parserMzIdentML.LastProteinGroupID = lastProteinGroupID;
                        parserMzIdentML.LastPeptideID = lastPeptideID;
                        parserMzIdentML.LastMSMSID = lastMSMSID;
                        parserMzIdentML.ParseFiles(mzIdentML);
                        proteinGroupMQList.AddRange(parserMzIdentML.ProteinGroups);
                        peptideList.AddRange(parserMzIdentML.Peptides);
                        tandemMassSpectraList.AddRange(parserMzIdentML.TandemMassSpectra);
                        lastProteinGroupID = parserMzIdentML.ProteinGroups.Count > 0 ? parserMzIdentML.ProteinGroups.Max(item => item.Params[0].ProteinGroupID) : lastProteinGroupID;
                        lastPeptideID = parserMzIdentML.Peptides.Count > 0 ? parserMzIdentML.Peptides.Max(item => item.PSMs[0].PeptideID) : lastPeptideID;
                        lastMSMSID = parserMzIdentML.TandemMassSpectra.Count > 0 ? parserMzIdentML.TandemMassSpectra.Max(item => item.MSMSID) : lastMSMSID;
                    }
                    catch (Exception ex)
                    {
                        sbError.AppendLine("Bottom-up/Middle-down: " + mzIdentML);
                    }
                }

                #endregion

                tandemMassSpectraList.Sort((a, b) => a.ScanNumber.CompareTo(b.ScanNumber));

                for (int i = 0; i < filesBottomUpRAW.Length; i++)
                {
                    Console.WriteLine(" Processing RAW file: " + (i + 1) + " of " + filesBottomUpRAW.Length);
                    try
                    {
                        FileInfo fileName = new FileInfo(filesBottomUpRAW[i]);
                        string rawFile = fileName.Name.Substring(0, fileName.Name.Length - 4);

                        List<int> scanNumbers = (from tms in tandemMassSpectraList.AsParallel()
                                                 where tms.RAWfile.Contains(rawFile)
                                                 select tms.ScanNumber).Distinct().ToList();
                        scanNumbers.Sort();

                        List<TandemMassSpectrum> tandemMassSpectraRAWBottomUpList = null;
                        if (filesBottomUpRAW[i].ToLower().EndsWith(".raw"))
                        {
                            tandemMassSpectraRAWBottomUpList = parserRawThermo.ParseFile(filesBottomUpRAW[i], scanNumbers);
                        }
                        else if (filesBottomUpRAW[i].ToLower().EndsWith(".mzml") || filesBottomUpRAW[i].ToLower().EndsWith(".mzxml"))
                        {
                            tandemMassSpectraRAWBottomUpList = parserMzML.ParseFile(filesBottomUpRAW[i], scanNumbers);
                        }

                        List<TandemMassSpectrum> filteredTandemMassSpectraList = (from spec in tandemMassSpectraList.AsParallel()
                                                                                  where spec.RAWfile.Equals(rawFile)
                                                                                  select spec).Distinct(new TandemMassSpectrumComparer()).ToList();
                        filteredTandemMassSpectraList.Sort((a, b) => a.ScanNumber.CompareTo(b.ScanNumber));

                        Console.WriteLine(" Checking bottom-up spectra.");
                        mainCore.CheckTMS_BottomUp_and_RAWfile(tandemMassSpectraRAWBottomUpList, filteredTandemMassSpectraList);
                    }
                    catch (Exception ex)
                    {
                        sbError.AppendLine("Bottom-up/Middle-down: " + filesBottomUpRAW[i]);
                    }
                }
                mainCore.MyResults.TandemMassSpectraMQList = tandemMassSpectraList;
                mainCore.MyResults.ModificationListMQ = parserMQ.ModificationsList;
                mainCore.MyResults.ProteinGroupList = proteinGroupMQList;
                mainCore.MyResults.PeptideList = peptideList;
                mainCore.SearchEngine_BUP_MzIdentML = parserMzIdentML.SoftwareName;

                if (hasMQfiles)
                {
                    Console.WriteLine(" Removing duplicate fasta entries.");
                    mainCore.MyResults.FastaItems = fastaItemsBottomUp.Distinct(new FastaComparer()).ToList();
                    Console.WriteLine(" Updating fasta sequence in ProteinGroup objects.");
                    mainCore.UpdateProteinGroupFromPtnSequence();
                }
            }

            if (proteinGroupMQList.Count == 0)
                mainCore.MyResults.ProteinGroupList = new List<ProteinGroup>(0);
            if (peptideList.Count == 0)
                mainCore.MyResults.PeptideList = new List<Peptide>(0);
            if (tandemMassSpectraList.Count == 0)
                mainCore.MyResults.TandemMassSpectraMQList = new List<TandemMassSpectrum>(0);

            #endregion

            #region Reading Top-down files
            Console.WriteLine(" Reading Top-down files");
            ParserProsight parserProsight = new ParserProsight();
            ParserTopPic parserTopPic = new ParserTopPic();
            ParserPTop parserPTop = new ParserPTop();
            List<Proteoform> proteoformList = new List<Proteoform>();
            List<FastaItem> fastaItemsTopDown = new List<FastaItem>();
            for (int i = 0; i < filesTopDown.Length; i++)
            {
                try
                {
                    Console.WriteLine(" Processing file: " + (i + 1) + " of " + filesTopDown.Length);
                    Console.WriteLine();
                    if (filesTopDown[i].Contains(".fasta"))//Database
                        fastaItemsTopDown.AddRange(parserMQ.ParseFastaFile(filesTopDown[i]));
                    else if (filesTopDown[i].Contains(".xml"))//Database
                    {
                        parserXMLdb.ParseXMLFile(filesTopDown[i]);
                        fastaItemsTopDown.AddRange(parserXMLdb.FastaItemsList);
                    }
                    else if (filesTopDown[i].EndsWith("_prsm.csv"))//TopPIC
                    {
                        parserTopPic.ParseCSVFile(filesTopDown[i]);
                        proteoformList.AddRange(parserTopPic.ProteoformList);
                    }
                    else if (filesTopDown[i].EndsWith("_filter.csv"))//pTop
                    {
                        parserPTop.ParseFile(filesTopDown[i]);
                        proteoformList.AddRange(parserPTop.ProteoformList);
                    }
                    else if (filesTopDown[i].EndsWith("_PSMs.txt")||
                        filesTopDown[i].EndsWith("_PrSMs.txt"))//Prosight
                        proteoformList.AddRange(parserProsight.ParseFile(filesTopDown[i]));
                }
                catch (Exception ex)
                {
                    sbError.AppendLine("Top-down: " + filesTopDown[i]);
                }
            }

            for (int i = 0; i < filesTopDownRAW.Length; i++)
            {
                Console.WriteLine(" Processing RAW file: " + (i + 1) + " of " + filesTopDownRAW.Length);
                try
                {
                    FileInfo fileName = new FileInfo(filesTopDownRAW[i]);
                    string rawFile = fileName.Name.Substring(0, fileName.Name.Length - 4);

                    List<int> scanNumbers = (from tms in proteoformList
                                             where tms.PrSMs[0].SpectrumFile.Contains(rawFile)
                                             select tms.PrSMs[0].ScanNumber).ToList();
                    scanNumbers.Sort();

                    List<TandemMassSpectrum> tandemMassSpectraRAWTopDownList = null;
                    if (filesTopDownRAW[i].ToLower().EndsWith(".raw"))
                    {
                        tandemMassSpectraRAWTopDownList = parserRawThermo.ParseFile(filesTopDownRAW[i], scanNumbers);
                    }
                    else if (filesTopDownRAW[i].ToLower().EndsWith(".mzml") || filesTopDownRAW[i].ToLower().EndsWith(".mzxml"))
                    {
                        tandemMassSpectraRAWTopDownList = parserMzML.ParseFile(filesTopDownRAW[i], scanNumbers);
                    }

                    List<Proteoform> filteredProteoformList = (from spec in proteoformList.AsParallel()
                                                               where spec.PrSMs[0].SpectrumFile.Contains(rawFile)
                                                               select spec).Distinct(new ProteoformComparer()).ToList();
                    filteredProteoformList.Sort((a, b) => a.PrSMs[0].ScanNumber.CompareTo(b.PrSMs[0].ScanNumber));

                    Console.WriteLine(" Checking top-down spectra.");
                    mainCore.CheckTMS_Topdown_and_RAWfile(tandemMassSpectraRAWTopDownList, filteredProteoformList);
                }
                catch (Exception)
                {
                    sbError.AppendLine("Top-down: " + filesTopDownRAW[i]);
                }
            }

            mainCore.MyResults.Proteoforms = proteoformList;
            #endregion

            #region Reading contaminants files
            List<FastaItem> fastaContaminants = new List<FastaItem>();
            if (!programParams.RemoveContaminants)
            {
                string contaminantSequences = ProteoCombiner.Properties.Settings.Default.Contaminants;
                fastaContaminants = parserMQ.ParseFastaFile(contaminantSequences, false);
            }
            #endregion

            #region Combining results
            List<FastaItem> finalFastaList = fastaItemsBottomUp.Union(fastaItemsTopDown).Distinct(new FastaComparer()).ToList();
            finalFastaList = fastaContaminants.Count > 0 ? finalFastaList.Union(fastaContaminants).Distinct(new FastaComparer()).ToList() : finalFastaList;

            mainCore.MyResults.FastaItems = finalFastaList;

            if (mainCore.MyResults.FastaItems == null || mainCore.MyResults.FastaItems.Count == 0)
            {
                ErrorProcessing = true;
                Console.WriteLine(" ERROR: No database has been found!! Combining process will be stopped!");

                TimeSpan dt = DateTime.Now - beginTimeSearch;
                Console.WriteLine(" Idle : Processing time: {0} Day(s) {1} Hour(s) {2} Minute(s) {3} Second(s)", dt.Days, dt.Hours, dt.Minutes, dt.Seconds);

                Console.WriteLine(" ");
                MessageBox.Show(
                        "No database has been found!!\nCombining process will be stopped!",
                        "Warning",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
            }
            else
            {
                ErrorProcessing = false;
                mainCore.MyResults.ProgramParams = programParams;

                #region Parser Stored PTMs
                ParserPTMuniprot parserPTM = new ParserPTMuniprot();
                parserPTM.ParserPTM();
                mainCore.MyResults.PTMsUniprot = parserPTM.PTMs;
                #endregion

                if (programParams.RemoveContaminants)
                {
                    Console.WriteLine(" Removing contaminant sequences.");
                    mainCore.CheckToRemoveContaminants();
                }
                Console.WriteLine(" Combining results...\n");
                mainCore.CombineData();

                TimeSpan dt = DateTime.Now - beginTimeSearch;
                Console.WriteLine(" Idle : Processing time: {0} Day(s) {1} Hour(s) {2} Minute(s) {3} Second(s)", dt.Days, dt.Hours, dt.Minutes, dt.Seconds);

                Console.WriteLine(" ");
                FinalTime = dt.Days + " Day(s) " + dt.Hours + " Hour(s) " + dt.Minutes + " Minute(s) " + dt.Seconds + " Second(s).";
                programParams.ProcessingTime = System.Text.ASCIIEncoding.Default.GetBytes(FinalTime);
                programParams.ProgramVersion = System.Text.ASCIIEncoding.Default.GetBytes(version);
                mainCore.MyResults.ProgramParams = programParams;

                if (!String.IsNullOrEmpty(sbError.ToString()))
                {
                    MessageBox.Show(
                        "The following files have not been processed.\n" + sbError.ToString(),
                        "Warning",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                }
            }
            #endregion
            
            FinishProcessing = true;
        }
    }
}
