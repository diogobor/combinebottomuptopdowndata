﻿/**
 * Program:     ProteoCombiner - Integrating bottom-up & top-down proteomics data
 * Author:      Diogo Borges Lima
 * Created:     5/16/2019
 * Update by:   Diogo Borges Lima
 * Description: Class responsible for saving all results
 */
 using CombineBottomUpTopDown.Model;
using CombineBottomUpTopDown.Utils;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombineBottomUpTopDown.Controller
{
    [Serializable]
    [ProtoContract]
    public class CombinePackageResults
    {
        /// <summary>
        /// public variables
        /// </summary>
        [ProtoMember(1)]
        public ProgramParams ProgramParams { get; set; }
        [ProtoMember(2)]
        public ResidueMass ResidueMass { get; set; }
        [ProtoMember(3)]
        public List<Proteoform> Proteoforms { get; set; }
        [ProtoMember(4)]
        public List<ProteinGroup> ProteinGroupList { get; set; }
        [ProtoMember(5)]
        public List<Peptide> PeptideList { get; set; }
        [ProtoMember(6)]
        public List<TandemMassSpectrum> TandemMassSpectraMQList { get; set; }
        [ProtoMember(7)]
        public List<FastaItem> FastaItems { get; set; }
        [ProtoMember(8)]
        public List<Protein> Proteins { get; set; }
        [ProtoMember(9)]
        public List<string> ModificationListMQ { get; set; }
        [ProtoMember(10)]
        public List<ModificationItem> PTMsUniprot { get; set; }

        /// <summary>
        /// Null Constructor for serializing Class
        /// </summary>
        public CombinePackageResults() { }

    }
}
