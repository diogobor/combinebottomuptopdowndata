﻿/**
 * Program:     ProteoCombiner - Integrating bottom-up & top-down proteomics data
 * Author:      Diogo Borges Lima
 * Created:     4/3/2019
 * Update by:   Diogo Borges Lima
 * Description: Class responsible for performing all comparisons
 */
using CombineBottomUpTopDown.Model;
using CombineBottomUpTopDown.Utils;
using Ionic.Zip;
using MathNet.Numerics.Distributions;
using ProtoBuf;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CombineBottomUpTopDown.Controller
{
    public class Core
    {

        private const double MAX_VALUE_LN = 0.9999999999;
        private const int MAX_ITEMS_TO_BE_SAVED = 10000;

        public bool finishCombiningData = false;
        public CombinePackageResults MyResults { get; set; }
        public double[] MaxScores { get; set; }
        public double[] MinScores { get; set; }

        public Core() { MyResults = new CombinePackageResults(); }

        public string SearchEngine_BUP_MzIdentML = "";

        /// <summary>
        /// Method responsible for combining top-down and bottom-up data
        /// </summary>
        public void CombineData()
        {
            finishCombiningData = false;

            if (String.IsNullOrEmpty(SearchEngine_BUP_MzIdentML)) SearchEngine_BUP_MzIdentML = string.Empty;

            this.MergeProteinGroups();

            Console.WriteLine(" Identifying proteins");
            Console.WriteLine(" Selecting scores");
            double[] maxScores = null;
            double[] minScores = null;
            Thread scoreThread = new Thread(() => this.SelectScores(out maxScores, out minScores));
            scoreThread.Start();

            Console.WriteLine(" Filtering proteins...");
            List<string> uniqueTopDownAccessionProteins = (from proteoform in MyResults.Proteoforms.AsParallel()
                                                           select proteoform.ProteinAccession).Distinct().ToList();
            List<string> uniqueBottomUpAccesionProteins = (from protein in MyResults.ProteinGroupList.AsParallel()
                                                           select protein.ProteinID).Distinct().ToList();
            List<string> finalAccessionProteinList = uniqueTopDownAccessionProteins.Union(uniqueBottomUpAccesionProteins).Distinct().ToList();

            StringBuilder sbFastasNotFound = new StringBuilder();

            List<FastaItem> identifiedFasta = new List<FastaItem>();
            finalAccessionProteinList.ForEach(accesionProtein =>
            {
                List<FastaItem> fastaItem = new List<FastaItem>();
                if (!accesionProtein.Contains(";"))//There is only one ptn
                {
                    fastaItem.AddRange((from fasta in MyResults.FastaItems.AsParallel()
                                        where fasta.SequenceIdentifier.Contains(accesionProtein) && fasta != null
                                        select fasta));
                    if (fastaItem.Count == 0)
                    {
                        string[] colsId = Regex.Split(accesionProtein, "\\|");
                        foreach (string colId in colsId)
                        {
                            if (colId.Equals("sp") || colId.Equals("tr")) continue;
                            fastaItem.AddRange((from fasta in MyResults.FastaItems.AsParallel()
                                                where fasta.SequenceIdentifier.Contains(colId) && fasta != null
                                                select fasta));
                            if (fastaItem.Count > 0)
                            {
                                identifiedFasta.AddRange(fastaItem);
                                break;
                            }
                        }
                        if (fastaItem.Count == 0)
                            sbFastasNotFound.Append(accesionProtein);
                    }
                    else
                        identifiedFasta.AddRange(fastaItem);
                }
                else
                {
                    string[] cols = Regex.Split(accesionProtein, ";");
                    foreach (string col in cols)
                    {
                        if (fastaItem.Count == 0)
                        {
                            fastaItem.AddRange(from fasta in MyResults.FastaItems.AsParallel()
                                               where fasta.SequenceIdentifier.Contains(col) && fasta != null
                                               select fasta);
                            if (fastaItem.Count == 0)
                            {
                                string[] colsId = Regex.Split(col, "\\|");
                                foreach (string colId in colsId)
                                {
                                    if (colId.Equals("sp") || colId.Equals("tr")) continue;
                                    fastaItem.AddRange(from fasta in MyResults.FastaItems.AsParallel()
                                                       where fasta.SequenceIdentifier.Contains(colId) && fasta != null
                                                       select fasta);
                                    if (fastaItem.Count > 0) break;
                                }
                            }
                        }
                        else
                        {
                            fastaItem.ForEach(item => { item.SequenceIdentifier += ";" + col; });
                        }

                        if (fastaItem.Count > 0) break;
                    }
                    if (fastaItem.Count > 0)
                        identifiedFasta.AddRange(fastaItem);
                    else
                        sbFastasNotFound.Append(accesionProtein);
                }
            });

            if (!String.IsNullOrEmpty(sbFastasNotFound.ToString()))
                Console.WriteLine(" WARNING: Proteins not found in database: " + sbFastasNotFound.ToString() + "\n\n");
            identifiedFasta = identifiedFasta.Distinct(new FastaComparer()).ToList();
            identifiedFasta.RemoveAll(item => item == null);
            MyResults.Proteins = new List<Protein>();

            scoreThread.Join();

            Console.WriteLine(" Updating peptide score...");
            MaxScores = maxScores;
            MinScores = minScores;
            this.UpdateNormalizedPeptidesScore();

            LinkedList<Protein> AllProteins = new LinkedList<Protein>();

            Console.WriteLine(" Merging proteoform results...");
            MergeProteoformsFromDifferentTools();
            MergePeptidesFromDifferentTools();
            this.CombineFastaResults(identifiedFasta, AllProteins);
            MyResults.Proteins = AllProteins.ToList();
        }

        private void UpdateNormalizedPeptidesScore()
        {
            foreach (Peptide pept in MyResults.PeptideList)
            {
                double max = 0;
                double min = 0;
                if (pept.PSMs.Count == 1)
                {
                    if (pept.PSMs[0].SearchEngine.Equals("PatternLab"))
                    {
                        min = this.MinScores[3];
                        max = this.MaxScores[3];
                    }
                    else if (pept.PSMs[0].SearchEngine.Equals("Andromeda"))
                    {
                        min = this.MinScores[4];
                        max = this.MaxScores[4];
                    }
                    else if (pept.PSMs[0].SearchEngine.Equals(SearchEngine_BUP_MzIdentML))
                    {
                        min = this.MinScores[5];
                        max = this.MaxScores[5];
                    }
                    else
                    {
                        min = 1;
                        max = -1;
                    }
                    double dividend = (max - min) == 0 ? 1 : (max - min);
                    pept.PSMs[0].NormalizedPeptideScore = (pept.PSMs[0].PeptideScore - min) / dividend;
                }
                else
                {
                    foreach (PeptideParams peptParam in pept.PSMs)
                    {
                        string searchEngine = peptParam.SearchEngine;
                        if (searchEngine.Equals("PatternLab"))
                        {
                            min = this.MinScores[3];
                            max = this.MaxScores[3];
                        }
                        else if (searchEngine.Equals("Andromeda"))
                        {
                            min = this.MinScores[4];
                            max = this.MaxScores[4];
                        }
                        else if (searchEngine.Equals(SearchEngine_BUP_MzIdentML))
                        {
                            min = this.MinScores[5];
                            max = this.MaxScores[5];
                        }
                        else
                        {
                            min = 1;
                            max = -1;
                        }
                        peptParam.NormalizedPeptideScore = (peptParam.PeptideScore - min) / (max - min);
                    }
                    pept.PSMs.Sort((a, b) => b.NormalizedPeptideScore.CompareTo(a.NormalizedPeptideScore));
                }
            }
            MyResults.PeptideList.Sort((a, b) => b.PSMs[0].NormalizedPeptideScore.CompareTo(a.PSMs[0].NormalizedPeptideScore));
        }

        /// <summary>
        /// Method responsible for selecting the max scores of each search engine
        /// </summary>
        /// <param name="plMaxPeptideScoreNorm"></param>
        /// <param name="andromedaMaxPeptideScoreNorm"></param>
        /// <param name="MaxScores"></param>
        public void SelectScores(out double[] MaxScores, out double[] MinScores)
        {

            #region top-down scores
            double prosightMaxProteoformScoreNorm = 0;
            double pTopMaxProteoformScoreNorm = 0;
            double topPicMaxProteoformScoreNorm = 0;

            double prosightMinProteoformScoreNorm = 0;
            double pTopMinProteoformScoreNorm = 0;
            double topPicMinProteoformScoreNorm = 0;

            if (MyResults.Proteoforms != null && MyResults.Proteoforms.Count > 0)
            {
                var prosightProteoformsScore = MyResults.Proteoforms.Where(a => a.PrSMs[0].SearchEngine.Equals("Prosight")).Select(item => item.PrSMs[0].LogPScore);
                if (prosightProteoformsScore.Count() > 0)
                {
                    prosightMinProteoformScoreNorm = prosightProteoformsScore.Min();
                    prosightMaxProteoformScoreNorm = prosightProteoformsScore.Max();
                }

                var pTopProteoformsScore = MyResults.Proteoforms.Where(a => a.PrSMs[0].SearchEngine.Equals("pTop")).Select(item => item.PrSMs[0].LogPScore);
                if (pTopProteoformsScore.Count() > 0)
                {
                    pTopMinProteoformScoreNorm = pTopProteoformsScore.Min();
                    pTopMaxProteoformScoreNorm = pTopProteoformsScore.Max();
                }

                var topPicProteoformsScore = MyResults.Proteoforms.Where(a => a.PrSMs[0].SearchEngine.Equals("TopPIC")).Select(item => item.PrSMs[0].LogPScore);
                if (topPicProteoformsScore.Count() > 0)
                {
                    topPicMinProteoformScoreNorm = topPicProteoformsScore.Min();
                    topPicMaxProteoformScoreNorm = topPicProteoformsScore.Max();
                }
            }
            #endregion

            #region bottom-up scores
            double plMaxPeptideScoreNorm = 0;
            double andromedaMaxPeptideScoreNorm = 0;
            double mzIdentMLMaxPeptideScoreNorm = 0;

            double plMinPeptideScoreNorm = 0;
            double andromedaMinPeptideScoreNorm = 0;
            double mzIdentMLMinPeptideScoreNorm = 0;

            if (MyResults.PeptideList != null && MyResults.PeptideList.Count > 0)
            {
                var plPeptidesScore = MyResults.PeptideList.Where(a => a.PSMs[0].SearchEngine.Equals("PatternLab")).Select(item => item.PSMs[0].PeptideScore);
                if (plPeptidesScore.Count() > 0)
                {
                    plMinPeptideScoreNorm = plPeptidesScore.Min();
                    plMaxPeptideScoreNorm = plPeptidesScore.Max();
                }

                var andromedaPeptidesScore = MyResults.PeptideList.Where(a => a.PSMs[0].SearchEngine.Equals("Andromeda")).Select(item => item.PSMs[0].PeptideScore);
                if (andromedaPeptidesScore.Count() > 0)
                {
                    andromedaMinPeptideScoreNorm = andromedaPeptidesScore.Min();
                    andromedaMaxPeptideScoreNorm = andromedaPeptidesScore.Max();
                }

                var mzIdentMLMaxPeptideScore = MyResults.PeptideList.Where(a => a.PSMs[0].SearchEngine.Equals(SearchEngine_BUP_MzIdentML)).Select(item => item.PSMs[0].PeptideScore);
                if (mzIdentMLMaxPeptideScore.Count() > 0)
                {
                    mzIdentMLMinPeptideScoreNorm = mzIdentMLMaxPeptideScore.Min();
                    mzIdentMLMaxPeptideScoreNorm = mzIdentMLMaxPeptideScore.Max();
                }
            }
            #endregion

            MaxScores = new double[6];
            MaxScores[0] = prosightMaxProteoformScoreNorm;
            MaxScores[1] = pTopMaxProteoformScoreNorm;
            MaxScores[2] = topPicMaxProteoformScoreNorm;
            MaxScores[3] = plMaxPeptideScoreNorm;
            MaxScores[4] = andromedaMaxPeptideScoreNorm;
            MaxScores[5] = mzIdentMLMaxPeptideScoreNorm;

            MinScores = new double[6];
            MinScores[0] = prosightMinProteoformScoreNorm;
            MinScores[1] = pTopMinProteoformScoreNorm;
            MinScores[2] = topPicMinProteoformScoreNorm;
            MinScores[3] = plMinPeptideScoreNorm;
            MinScores[4] = andromedaMinPeptideScoreNorm;
            MinScores[5] = mzIdentMLMinPeptideScoreNorm;
        }

        private void CombineFastaResults(List<FastaItem> identifiedFasta, LinkedList<Protein> proteins)
        {
            int fasta_processed = 0;
            int old_progress = 0;
            int lengthFile = identifiedFasta.Count;

            MyResults.ProteinGroupList = MyResults.ProteinGroupList.OrderBy(a => a.ProteinID).ToList();

            for (int count = 0; count < identifiedFasta.Count; count++)
            {
                //Varible responsible for checking the maximum score between topdown and bottomup identifications
                double maxScore = 0;
                List<Proteoform> selectedProteoforms = null;
                List<string> proteoformSequences = null;
                Thread threadProteoforms = new Thread(() => SelectProteoforms(identifiedFasta[count], out selectedProteoforms, out proteoformSequences));
                threadProteoforms.Start();

                #region Peptides
                List<Peptide> selectedPeptides = null;
                List<TandemMassSpectrum> selectedMSMS = null;
                List<TandemMassSpectrum> SubsetTandeMassSpectra = null;
                List<string> peptidesSequences = null;
                ProteinGroup proteinGroup = null;
                string[] colsPept = Regex.Split(identifiedFasta[count].SequenceIdentifier, ";");
                if (colsPept.Length == 1)
                {
                    proteinGroup = MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(colsPept[0])).AsParallel().FirstOrDefault();
                    if (proteinGroup != null)
                    {
                        ProteinGroup comparableProteinGroup = new ProteinGroup();
                        comparableProteinGroup.ProteinID = colsPept[0];
                        int _index = MyResults.ProteinGroupList.BinarySearch(comparableProteinGroup, new ProteinGroupBinarySearchComparer());
                        if (_index > -1)
                            proteinGroup = MyResults.ProteinGroupList[_index];
                    }
                }
                else
                {
                    foreach (string accession in colsPept)
                    {
                        proteinGroup = MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(accession)).FirstOrDefault();
                        if (proteinGroup != null)
                            break;
                    }
                }

                if (proteinGroup == null)
                {
                    colsPept = Regex.Split(identifiedFasta[count].SequenceIdentifier, "\\|");
                    if (colsPept.Length > 1)
                    {
                        foreach (string accession in colsPept)
                        {
                            if (accession.Equals("sp") || accession.Equals("tr")) continue;

                            proteinGroup = this.MyResults.ProteinGroupList.Where(item => item.ProteinID.Contains(accession)).AsParallel().FirstOrDefault();
                            if (proteinGroup != null)
                                break;
                        }
                    }
                }

                if (proteinGroup != null)
                {
                    Thread threadSubsetTMS = new Thread(() => SelectTMSFromProteinGroup(proteinGroup, out SubsetTandeMassSpectra));
                    threadSubsetTMS.Start();

                    selectedPeptides = new List<Peptide>();
                    foreach (ProteinGroupParams ptnGroupParam in proteinGroup.Params)
                    {
                        List<ulong> ids = ptnGroupParam.PeptideIds.Distinct().ToList();
                        List<Peptide> pepts = (from pepti in this.MyResults.PeptideList.AsParallel()
                                               where pepti.PSMs.Any(item => ids.Contains(item.PeptideID) && pepti.PSMs.Any(item1 => item1.ProteinGroupIDs.Contains(ptnGroupParam.ProteinGroupID)) && item.SearchEngine.Equals(ptnGroupParam.SearchEngine))
                                               select pepti).ToList();
                        selectedPeptides.AddRange(pepts);
                    }

                    threadSubsetTMS.Join();
                }
                #endregion

                threadProteoforms.Join();
                double ptnCoveragePorcentageVisualization = (Math.Round(ProteoformCoverage(identifiedFasta[count].Sequence, proteoformSequences, peptidesSequences), 3) * 100);

                if (selectedProteoforms != null && selectedProteoforms.Count > 0)
                {
                    if (SubsetTandeMassSpectra != null && selectedPeptides != null)
                    {
                        selectedMSMS = (from peptide in selectedPeptides.AsParallel()
                                        from tms in SubsetTandeMassSpectra.AsParallel()
                                        where peptide.PSMs.Select(item => item.BestMSMS).Contains(tms.MSMSID)
                                        select tms).ToList();

                        selectedMSMS.Sort((a, b) => b.Score.CompareTo(a.Score));
                        selectedMSMS = selectedMSMS.Distinct(new TandemMassSpectrumComparer()).AsParallel().ToList();
                    }
                    selectedProteoforms = selectedProteoforms.OrderBy(x => x.Sequence.Length).ThenBy(x => x.Sequence).ThenBy(x => x.PrSMs[0].SpectrumFile.Length).ThenBy(x => x.PrSMs[0].SpectrumFile.Length).ThenBy(x => x.PrSMs[0].SpectrumFile).ThenBy(x => x.PrSMs[0].ScanNumber).ToList();
                    this.CalculateProteoformScore(identifiedFasta[count].Sequence, selectedProteoforms, selectedPeptides, selectedMSMS);

                    //FinalScore is normalized
                    selectedProteoforms.Sort((a, b) => b.PrSMs[0].FinalScore.CompareTo(a.PrSMs[0].FinalScore));
                    maxScore = selectedProteoforms[0].PrSMs[0].FinalScore;
                }

                if (selectedPeptides == null) selectedPeptides = new List<Peptide>();

                lock (proteins)
                {
                    if (maxScore > 0)
                        proteins.AddLast(new Protein(selectedProteoforms, selectedPeptides, identifiedFasta[count].Sequence, identifiedFasta[count].SequenceIdentifier, identifiedFasta[count].Description, ptnCoveragePorcentageVisualization, maxScore));
                }

                fasta_processed++;
                int new_progress = (int)((double)fasta_processed / (lengthFile) * 100);
                if (new_progress > old_progress)
                {
                    old_progress = new_progress;
                    Console.Write(" Combining data: " + old_progress + "%");
                }
            }

            finishCombiningData = true;
        }

        private void SelectTMSFromProteinGroup(ProteinGroup proteinGroup, out List<TandemMassSpectrum> SubsetTMSFromProteinGroup)
        {
            SubsetTMSFromProteinGroup = (from tms in MyResults.TandemMassSpectraMQList.AsParallel()
                                         from ptnsParams in proteinGroup.Params
                                         where tms.ProteinGroupID.Contains(ptnsParams.ProteinGroupID)
                                         select tms).Distinct(new TandemMassSpectrumComparer()).ToList();
        }

        public void UpdateProteinGroupFromPtnSequence()
        {
            MyResults.ProteinGroupList.ForEach(ptn =>
            {
                if (String.IsNullOrEmpty(ptn.Sequence))
                {
                    FastaItem fastaItem = null;
                    fastaItem = SelectFastaItem(null, fastaItem, ptn.ProteinID);
                    if (fastaItem != null)
                        ptn.Sequence = fastaItem.Sequence;
                }
            });
        }

        private FastaItem SelectFastaItem(List<Proteoform> proteoformList, FastaItem fastaItem, string proteinAccession, string proteoformSequence = null)
        {
            //if (proteoformList == null || proteoformList != null && proteoformList.Count == 0)
            //    proteoformList = this.SelectProteoforms(proteinAccession, proteoformSequence);

            string[] cols = Regex.Split(proteinAccession, ",|;|\\|");
            if (cols.Length == 1)
            {
                if (proteoformList != null && proteoformList.Count > 0)
                {
                    fastaItem = this.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Contains(proteoformList[0].ProteinAccession)).FirstOrDefault();
                }
                else // There is no proteoforms
                {
                    //Try to recovery fasta item based on exact fragmented accession number
                    fastaItem = this.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(cols[0])).FirstOrDefault();
                    if (fastaItem == null)
                    {
                        //Try to recovery fasta item based on fragmented accession number
                        fastaItem = this.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Contains(cols[0])).FirstOrDefault();
                    }
                }
            }
            else
            {
                foreach (string accession in cols)
                {
                    if (String.IsNullOrEmpty(accession) || accession.Equals("sp") || accession.Equals("tr")) continue;
                    if (proteoformList != null && proteoformList.Count > 0)
                    {
                        //Try to recovery fasta item based on exact fragmented accession number
                        fastaItem = this.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(accession) && item.Description.Contains(proteoformList[0].ProteinDescription)).FirstOrDefault();
                        if (fastaItem != null) break;
                        else
                        {
                            //Try to recovery fasta item based on fragmented accession number
                            fastaItem = this.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Contains(accession) && item.Description.Contains(proteoformList[0].ProteinDescription)).FirstOrDefault();
                            if (fastaItem != null) break;
                            else
                            {
                                //Try to compare proteoform description to fasta description
                                fastaItem = this.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(proteinAccession) && item.Description.Contains(proteoformList[0].ProteinDescription)).FirstOrDefault();
                                if (fastaItem != null) break;
                                else
                                {
                                    //Try to compare fasta description to proteoform description
                                    fastaItem = this.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(proteinAccession) && proteoformList[0].ProteinDescription.Contains(item.Description)).FirstOrDefault();
                                    if (fastaItem != null) break;
                                    else
                                    {
                                        fastaItem = this.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Contains(proteoformList[0].ProteinAccession)).FirstOrDefault();
                                        if (fastaItem != null) break;
                                    }
                                }
                            }
                        }
                    }
                    else //There is no proteoforms
                    {
                        //Try to recovery fasta item based on exact fragmented accession number
                        fastaItem = this.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(accession)).FirstOrDefault();
                        if (fastaItem != null) break;
                        else
                        {
                            //Try to recovery fasta item based on fragmented accession number
                            fastaItem = this.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Contains(accession)).FirstOrDefault();
                            if (fastaItem != null) break;
                            else
                            {
                                //Try to compare proteinAccesion
                                fastaItem = this.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Equals(proteinAccession)).FirstOrDefault();
                                if (fastaItem != null) break;
                                else
                                {
                                    //Try to compare fasta description to proteoform description
                                    fastaItem = this.MyResults.FastaItems.Where(item => item.SequenceIdentifier.Contains(proteinAccession)).FirstOrDefault();
                                    if (fastaItem != null) break;
                                }
                            }
                        }
                    }
                }
            }

            return fastaItem;
        }

        private void SelectProteoforms(FastaItem identifiedFasta, out List<Proteoform> selectedProteoforms, out List<string> proteoformSequences)
        {
            List<Proteoform> _selectedProteoforms = new List<Proteoform>();
            MyResults.Proteoforms.ForEach(proteoform =>
            {
                string[] cols = Regex.Split(proteoform.ProteinAccession, ",|;|\\|");
                if (cols.Length == 1)
                {
                    if (identifiedFasta.SequenceIdentifier.Contains(cols[0]))
                        _selectedProteoforms.Add(proteoform);
                }
                else
                {
                    foreach (string accession in cols)
                    {
                        if (String.IsNullOrEmpty(accession) || accession.Equals("sp") || accession.Equals("tr")) continue;
                        if (identifiedFasta.SequenceIdentifier.Contains(accession))
                        {
                            _selectedProteoforms.Add(proteoform);
                            break;
                        }
                    }
                }
            });

            selectedProteoforms = _selectedProteoforms;
            proteoformSequences = (from proteoform in selectedProteoforms.AsParallel()
                                   select proteoform.Sequence).Distinct().ToList();
        }

        public void MergeProteoformsFromDifferentTools()
        {
            MyResults.Proteoforms = MyResults.Proteoforms.Distinct(new MergeProteoformFromDiffTool()).AsParallel().ToList();
            MyResults.Proteoforms.ForEach(item =>
            {
                if (item.PrSMs.Count > 1)
                    item.PrSMs.Sort((a, b) => b.FinalScore.CompareTo(a.FinalScore));
            });
            MyResults.Proteins.ForEach(item =>
            {
                if (item.Proteoforms.Count > 1)
                {
                    if (item.Proteoforms[0].PrSMs.Count == item.Proteoforms.Count &&
                    item.Proteoforms[0].Sequence.Equals(item.Proteoforms[1].Sequence))
                    {
                        item.Proteoforms = new List<Proteoform>() { item.Proteoforms[0] };
                    }
                }
            });
        }

        public void MergeProteinGroups()
        {
            if (MyResults.ProteinGroupList != null)
                MyResults.ProteinGroupList = MyResults.ProteinGroupList.Distinct(new MergeProteinGroupFromDiffTool()).AsParallel().ToList();
        }

        public void MergePeptidesFromDifferentTools()
        {
            MyResults.PeptideList = MyResults.PeptideList.Distinct(new MergePeptideFromDiffTool()).AsParallel().ToList();
        }

        /// <summary>
        /// Method responsible for calculating the protein covarege based on the identified peptides and proteoforms
        /// </summary>
        /// <param name="proteinOrProteoformSequence"></param>
        /// <param name="annotationProteoforms"></param>
        /// <param name="annotationPeptides"></param>
        /// <returns></returns>
        public double ProteoformCoverage(string proteinOrProteoformSequence, List<string> annotationProteoforms = null, List<string> annotationPeptides = null)
        {
            List<int> matchLocations = new List<int>(new int[(int)proteinOrProteoformSequence.Length]);
            if (annotationProteoforms != null)
            {
                foreach (string proteoform in annotationProteoforms)
                {
                    Match location = Regex.Match(proteinOrProteoformSequence, proteoform);
                    for (int i = location.Index; i < location.Index + location.Length; i++)
                    {
                        matchLocations[i] = 1;
                    }
                }
            }

            if (annotationPeptides != null)
            {
                foreach (string peptide in annotationPeptides)
                {
                    string cleanedPeptide = Util.CleanPeptide(peptide);
                    Match locationPept = Regex.Match(proteinOrProteoformSequence, cleanedPeptide);
                    for (int i = locationPept.Index; i < locationPept.Index + locationPept.Length; i++)
                    {
                        matchLocations[i] = 1;
                    }
                }
            }

            int matchCounter = matchLocations.FindAll(a => a == 1).Count;
            return (double)matchCounter / (double)proteinOrProteoformSequence.Length;

        }

        /// <summary>
        /// Method responsible for updating the bottom-up tms objects read from the original RAW file
        /// </summary>
        /// <param name="tandemMassSpectraRAWList"></param>
        /// <param name="tandemMassSpectraSearchEngineList"></param>
        public void CheckTMS_BottomUp_and_RAWfile(List<TandemMassSpectrum> tandemMassSpectraRAWList, List<TandemMassSpectrum> tandemMassSpectraSearchEngineList)
        {
            object progress_lock = new object();
            int spectra_processed = 0;
            int old_progress = 0;

            if (tandemMassSpectraRAWList != null && tandemMassSpectraSearchEngineList != null)
            {
                double lengthFile = tandemMassSpectraRAWList.Count;
                Parallel.ForEach(tandemMassSpectraRAWList,
                new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount },
                rawTMS =>
                {
                    TandemMassSpectrum comparableTMS = new TandemMassSpectrum();
                    comparableTMS.ScanNumber = rawTMS.ScanNumber;
                    comparableTMS.RAWfile = rawTMS.RAWfile;
                    int _index = tandemMassSpectraSearchEngineList.BinarySearch(comparableTMS, new TandemMassSpectrumBinarySearchComparer());
                    TandemMassSpectrum mqTMS = new TandemMassSpectrum();
                    if (_index > -1)
                        mqTMS = tandemMassSpectraSearchEngineList[_index];

                    lock (mqTMS)
                    {
                        if (!String.IsNullOrEmpty(mqTMS.RAWfile))
                        {
                            mqTMS.MassesFragIons = rawTMS.MassesFragIons;
                            mqTMS.IntensitiesFragIons = rawTMS.IntensitiesFragIons;
                            mqTMS.RetentionTime = rawTMS.RetentionTime;
                            mqTMS.Charge = rawTMS.Charge > 0 ? rawTMS.Charge : mqTMS.Charge;
                            mqTMS.Precursor = rawTMS.Precursor;
                            mqTMS.PrecursorScanNumber = rawTMS.PrecursorScanNumber;
                            mqTMS.Fragmentation = rawTMS.Fragmentation;
                        }
                    }

                    lock (progress_lock)
                    {
                        spectra_processed++;
                        int new_progress = (int)((double)spectra_processed / (lengthFile) * 100);
                        if (new_progress > old_progress)
                        {
                            old_progress = new_progress;
                            Console.Write(" Checking bottom-up spectra: " + old_progress + "%");
                        }
                    }
                }
                );
            }
        }

        /// <summary>
        /// Method responsible for updating the top-down tms objects read from the original RAW file
        /// </summary>
        /// <param name="tandemMassSpectraRAWList"></param>
        /// <param name="proteoforms"></param>
        public void CheckTMS_Topdown_and_RAWfile(List<TandemMassSpectrum> tandemMassSpectraRAWList, List<Proteoform> proteoforms)
        {
            object progress_lock = new object();
            int spectra_processed = 0;
            int old_progress = 0;

            if (tandemMassSpectraRAWList != null && proteoforms != null)
            {
                double lengthFile = tandemMassSpectraRAWList.Count;
                foreach (TandemMassSpectrum rawTMS in tandemMassSpectraRAWList)
                {
                    Proteoform comparableProteoform = new Proteoform();
                    ProteoformParams proteoformParams = new ProteoformParams();
                    proteoformParams.SpectrumFile = rawTMS.RAWfile;
                    proteoformParams.ScanNumber = rawTMS.ScanNumber;
                    comparableProteoform.PrSMs = new List<ProteoformParams>() { proteoformParams };

                    int _index = proteoforms.BinarySearch(comparableProteoform, new ProteoformSimpleBinarySearchComparer());
                    Proteoform prosightTMS = new Proteoform();
                    if (_index > -1)
                        prosightTMS = proteoforms[_index];

                    lock (prosightTMS)
                    {
                        if (prosightTMS != null)
                        {
                            List<Tuple<double, double>> ions = new List<Tuple<double, double>>();
                            for (int i = 0; i < rawTMS.MassesFragIons.Count; i++)
                                ions.Add((rawTMS.MassesFragIons[i], rawTMS.IntensitiesFragIons[i]).ToTuple());
                            prosightTMS.PrSMs[0].FragmentIons = ions;
                            prosightTMS.PrSMs[0].PrecursorCharge = rawTMS.Precursor.Item1;
                            prosightTMS.PrSMs[0].ActivationType = rawTMS.Fragmentation;
                            prosightTMS.PrSMs[0].RetentionTime = rawTMS.RetentionTime;
                            prosightTMS.PrSMs[0].Charge = rawTMS.Charge > 0 ? rawTMS.Charge : prosightTMS.PrSMs[0].Charge;
                            prosightTMS.PrSMs[0].Intensity = rawTMS.IntensitiesFragIons.Sum();
                        }
                    }

                    lock (progress_lock)
                    {
                        spectra_processed++;
                        int new_progress = (int)((double)spectra_processed / (lengthFile) * 100);
                        if (new_progress > old_progress)
                        {
                            old_progress = new_progress;
                            Console.Write(" Checking top-down spectra: " + old_progress + "%");
                        }
                    }

                }
            }
        }

        /// <summary>
        /// Method responsible for removing contaminants protein sequences
        /// </summary>
        public void CheckToRemoveContaminants()
        {
            #region Remove Contaminants
            if (MyResults.ProgramParams.RemoveContaminants)
            {
                Console.WriteLine(" Removing contaminants");
                if (MyResults.ProteinGroupList != null && MyResults.ProteinGroupList.Count > 0)
                {
                    MyResults.ProteinGroupList = MyResults.ProteinGroupList.Where(item => !item.ProteinID.Contains("CON_") && !item.ProteinID.Contains("contaminant_")).ToList();//MaxQuant/Andromda || PatternLab / Comet
                }
                if (MyResults.FastaItems.Count > 0)
                    MyResults.FastaItems = MyResults.FastaItems.Where(item => !item.SequenceIdentifier.Contains("CON_") && !item.SequenceIdentifier.Contains("contaminant_")).ToList();
            }
            if (MyResults.ProgramParams.RemoveRevSeq)
            {
                Console.WriteLine(" Removing reverse sequences");
                if (MyResults.ProteinGroupList != null && MyResults.ProteinGroupList.Count > 0)
                    MyResults.ProteinGroupList = MyResults.ProteinGroupList.Where(item => !item.ProteinID.Contains("REV_") && !item.ProteinID.Contains("Reverse_")).ToList();
                if (MyResults.FastaItems.Count > 0)
                    MyResults.FastaItems = MyResults.FastaItems.Where(item => !item.SequenceIdentifier.Contains("REV_") && !item.SequenceIdentifier.Contains("Reverse_")).ToList();
            }
            #endregion
        }

        /// <summary>
        /// Method responsible for serializing results
        /// </summary>
        /// <param name="fileName"></param>
        public void SerializeResults(string fileName)
        {
            if (MyResults.FastaItems.Count > MAX_ITEMS_TO_BE_SAVED ||
                MyResults.PeptideList.Count > MAX_ITEMS_TO_BE_SAVED ||
                MyResults.ProteinGroupList.Count > MAX_ITEMS_TO_BE_SAVED ||
                MyResults.Proteins.Count > MAX_ITEMS_TO_BE_SAVED ||
                MyResults.Proteoforms.Count > MAX_ITEMS_TO_BE_SAVED ||
                MyResults.TandemMassSpectraMQList.Count > MAX_ITEMS_TO_BE_SAVED)
            {

                Console.WriteLine(" Saving results:");

                object progress_lock = new object();
                int old_progress = 0;

                using (ZipFile zipFile = new ZipFile())
                {
                    zipFile.Password = "C0mb!neTD&BUD@ta";
                    int fileIndex = 0;
                    int biggestLength = 0;

                    if (MyResults.FastaItems.Count > biggestLength) biggestLength = MyResults.FastaItems.Count;
                    if (MyResults.PeptideList.Count > biggestLength) biggestLength = MyResults.PeptideList.Count;
                    if (MyResults.ProteinGroupList.Count > biggestLength) biggestLength = MyResults.ProteinGroupList.Count;
                    if (MyResults.Proteins.Count > biggestLength) biggestLength = MyResults.Proteins.Count;
                    if (MyResults.Proteoforms.Count > biggestLength) biggestLength = MyResults.Proteoforms.Count;
                    if (MyResults.TandemMassSpectraMQList.Count > biggestLength) biggestLength = MyResults.TandemMassSpectraMQList.Count;


                    //When there are many results (more than MAX_ITEMS_TO_BE_SAVED), it's necessary to split the object in different
                    //small pieces. These pieces are saved in the same zip file, but in different FileCompressed subfiles.

                    for (int count = 0; count < biggestLength; count += MAX_ITEMS_TO_BE_SAVED, fileIndex++)
                    {
                        CombinePackageResults currentResults = new CombinePackageResults();
                        currentResults.ProgramParams = MyResults.ProgramParams;
                        currentResults.PTMsUniprot = MyResults.PTMsUniprot;
                        currentResults.ResidueMass = MyResults.ResidueMass;

                        //Split the list in sublist with a maximum of MAX_ITEMS_TO_BE_SAVED items.
                        currentResults.FastaItems = MyResults.FastaItems.Skip(count).Take(MAX_ITEMS_TO_BE_SAVED).ToList();
                        currentResults.PeptideList = MyResults.PeptideList.Skip(count).Take(MAX_ITEMS_TO_BE_SAVED).ToList();
                        currentResults.ProteinGroupList = MyResults.ProteinGroupList.Skip(count).Take(MAX_ITEMS_TO_BE_SAVED).ToList();
                        currentResults.Proteins = MyResults.Proteins.Skip(count).Take(MAX_ITEMS_TO_BE_SAVED).ToList();
                        currentResults.Proteoforms = MyResults.Proteoforms.Skip(count).Take(MAX_ITEMS_TO_BE_SAVED).ToList();
                        currentResults.TandemMassSpectraMQList = MyResults.TandemMassSpectraMQList.Skip(count).Take(MAX_ITEMS_TO_BE_SAVED).ToList();

                        MemoryStream fileToCompress = new MemoryStream();
                        Serializer.SerializeWithLengthPrefix(fileToCompress, currentResults, PrefixStyle.Base128, 1);

                        fileToCompress.Seek(0, SeekOrigin.Begin);   // <-- must do this after writing the stream!

                        string namespaceFile = "FileCompressed" + fileIndex;
                        zipFile.AddEntry(namespaceFile, fileToCompress);

                        lock (progress_lock)
                        {
                            int new_progress = (int)((double)count / (biggestLength) * 100);
                            if (new_progress > old_progress)
                            {
                                old_progress = new_progress;
                                Console.Write(" Saving results: " + old_progress + "%");
                            }
                        }
                    }
                    zipFile.AddEntry("TotalFiles", fileIndex.ToString());
                    zipFile.Save(fileName);

                    Console.WriteLine(" Saving results: 100%");
                }
            }
            else //Don't split results
            {
                MemoryStream fileToCompress = new MemoryStream();
                Serializer.SerializeWithLengthPrefix(fileToCompress, MyResults, PrefixStyle.Base128, 1);

                fileToCompress.Seek(0, SeekOrigin.Begin);   // <-- must do this after writing the stream!

                using (ZipFile zipFile = new ZipFile())
                {
                    zipFile.Password = "C0mb!neTD&BUD@ta";
                    zipFile.AddEntry("FileCompressed", fileToCompress);
                    zipFile.Save(fileName);
                }
            }
        }

        /// <summary>
        /// Method responsible for loading MyResults object
        /// </summary>
        /// <param name="fileName"></param>
        public void LoadResults(string fileName)
        {
            MyResults = this.DeserializeResults(fileName);

            if (MyResults.FastaItems == null)
                MyResults.FastaItems = new List<FastaItem>();
            if (MyResults.PeptideList == null)
                MyResults.PeptideList = new List<Peptide>();
            if (MyResults.ProteinGroupList == null)
                MyResults.ProteinGroupList = new List<ProteinGroup>();
            if (MyResults.Proteins == null)
                MyResults.Proteins = new List<Protein>();
            if (MyResults.Proteoforms == null)
                MyResults.Proteoforms = new List<Proteoform>();
            if (MyResults.TandemMassSpectraMQList == null)
                MyResults.TandemMassSpectraMQList = new List<TandemMassSpectrum>();
        }

        /// <summary>
        /// Method responsible for deserializing results
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private CombinePackageResults DeserializeResults(string fileName)
        {
            object progress_lock = new object();
            int set_processed = 0;
            int old_progress = 0;

            using (ZipFile zip = ZipFile.Read(fileName))
            {
                ZipEntry entry = zip["TotalFiles"];
                if (entry == null)//Small file
                {
                    using (var ms = new MemoryStream())
                    {
                        entry = zip["FileCompressed"];
                        entry.ExtractWithPassword(ms, "C0mb!neTD&BUD@ta");// extract uncompressed content into a memorystream 

                        ms.Seek(0, SeekOrigin.Begin); // <-- must do this after writing the stream!

                        List<CombinePackageResults> toDeserialize = Serializer.DeserializeItems<CombinePackageResults>(ms, PrefixStyle.Base128, 1).ToList();
                        return toDeserialize[0];
                    }
                }
                else
                {
                    Console.WriteLine(" Loading results:");
                    int total_files = 0;
                    using (var ms = new MemoryStream())
                    {
                        entry.ExtractWithPassword(ms, "C0mb!neTD&BUD@ta");// extract uncompressed content into a memorystream 
                        ms.Seek(0, SeekOrigin.Begin); // <-- must do this after writing the stream!
                        total_files = Convert.ToInt32(System.Text.ASCIIEncoding.Default.GetString(ms.GetBuffer()));
                    }
                    CombinePackageResults TotalResults = new CombinePackageResults();
                    TotalResults.FastaItems = new List<FastaItem>();
                    TotalResults.PeptideList = new List<Peptide>();
                    TotalResults.ProteinGroupList = new List<ProteinGroup>();
                    TotalResults.Proteins = new List<Protein>();
                    TotalResults.Proteoforms = new List<Proteoform>();
                    TotalResults.TandemMassSpectraMQList = new List<TandemMassSpectrum>();

                    for (int count = 0; count < total_files; count++)
                    {
                        using (var ms = new MemoryStream())
                        {
                            string namespace_entry = "FileCompressed" + count;
                            ZipEntry currentEntry = zip[namespace_entry];
                            currentEntry.ExtractWithPassword(ms, "C0mb!neTD&BUD@ta");// extract uncompressed content into a memorystream 

                            ms.Seek(0, SeekOrigin.Begin); // <-- must do this after writing the stream!

                            CombinePackageResults toDeserialize = Serializer.DeserializeItems<CombinePackageResults>(ms, PrefixStyle.Base128, 1).LastOrDefault();

                            if (toDeserialize != null)
                            {
                                TotalResults.ProgramParams = toDeserialize.ProgramParams;
                                TotalResults.PTMsUniprot = toDeserialize.PTMsUniprot;
                                TotalResults.ResidueMass = toDeserialize.ResidueMass;

                                if (toDeserialize.FastaItems != null)
                                    TotalResults.FastaItems.AddRange(toDeserialize.FastaItems);
                                if (toDeserialize.PeptideList != null)
                                    TotalResults.PeptideList.AddRange(toDeserialize.PeptideList);
                                if (toDeserialize.ProteinGroupList != null)
                                    TotalResults.ProteinGroupList.AddRange(toDeserialize.ProteinGroupList);
                                if (toDeserialize.Proteins != null)
                                    TotalResults.Proteins.AddRange(toDeserialize.Proteins);
                                if (toDeserialize.Proteoforms != null)
                                    TotalResults.Proteoforms.AddRange(toDeserialize.Proteoforms);
                                if (toDeserialize.TandemMassSpectraMQList != null)
                                    TotalResults.TandemMassSpectraMQList.AddRange(toDeserialize.TandemMassSpectraMQList);
                            }
                            else
                                throw new Exception("Error to load ProteoCombiner file.");
                        }

                        lock (progress_lock)
                        {
                            set_processed++;
                            int new_progress = (int)((double)set_processed / (total_files) * 100);
                            if (new_progress > old_progress)
                            {
                                old_progress = new_progress;
                                Console.Write(" Loading results: " + old_progress + "%");
                            }
                        }

                    }
                    Console.WriteLine(" Loading results: 100%");
                    return TotalResults;
                }
            }
        }

        /// <summary>
        /// Method responsible for finding out all peptides that fit with a corresponding proteoform
        /// </summary>
        /// <param name="fastaItem"></param>
        /// <param name="proteoformList"></param>
        /// <param name="selectedPeptides"></param>
        /// <param name="selectedMSMS"></param>
        /// <returns></returns>
        public static List<(Proteoform, List<(Peptide, List<TandemMassSpectrum>)>)> FitProteoformsAndPeptides(string sequence, List<Proteoform> proteoformList, List<Peptide> selectedPeptides, List<TandemMassSpectrum> selectedMSMS, bool sortPeptByStartPos = false)
        {
            List<(Proteoform, List<(Peptide, List<TandemMassSpectrum>)>)> result = new List<(Proteoform, List<(Peptide, List<TandemMassSpectrum>)>)>();
            foreach (Proteoform proteoform in proteoformList)
            {
                int ProtOffset = sequence.IndexOf(proteoform.Sequence);
                if (ProtOffset != -1)
                {
                    Proteoform newProt = new Proteoform(proteoform.Sequence, proteoform.ProteinAccession, proteoform.ProteinDescription, proteoform.AnnotatedSequence, proteoform.SequenceCoverage, proteoform.PrSMs);
                    List<(string, int, string)> protMods = (from mod in newProt.PrSMs.SelectMany(item => item.Modifications)
                                                            select (mod.Item1, (mod.Item2 + ProtOffset), mod.Item3)).Distinct().ToList();

                    newProt.PrSMs[0].FinalScore = proteoform.PrSMs[0].FinalScore;
                    List<(Peptide, List<TandemMassSpectrum>)> validPeptides = SelectedPeptides(selectedPeptides, selectedMSMS, proteoform, ProtOffset, newProt, protMods);
                    if (sortPeptByStartPos)//Sort peptides by start position
                        validPeptides.Sort((a, b) => a.Item1.StartPosition.CompareTo(b.Item1.StartPosition));
                    else
                        validPeptides.Sort((a, b) => b.Item1.PSMs[0].PeptideScore.CompareTo(a.Item1.PSMs[0].PeptideScore));
                    result.Add((newProt, validPeptides));
                }
            }

            return result;
        }

        static List<(Peptide, List<TandemMassSpectrum>)> SelectedPeptides(List<Peptide> selectedPeptides, List<TandemMassSpectrum> selectedMSMS, Proteoform proteoform, int ProtOffset, Proteoform newProt, List<(string, int, string)> protMods)
        {
            if (selectedMSMS == null) return new List<(Peptide, List<TandemMassSpectrum>)>();

            List<(Peptide, List<TandemMassSpectrum>)> validPeptides = new List<(Peptide, List<TandemMassSpectrum>)>();
            if (selectedPeptides != null)
            {
                foreach (Peptide peptide in selectedPeptides)
                {
                    var tms = (from massMQ in selectedMSMS.AsParallel()
                               where peptide.PSMs.SelectMany(item => item.MSMSIds).Contains(massMQ.MSMSID)
                               select massMQ).ToList();
                    tms.Sort((a, b) => b.Score.CompareTo(a.Score));
                    bool isProteoformAndPeptideWithMods = false;
                    bool isPeptideWithMods = false;

                    if ((peptide.StartPosition - 1) != -1)
                    {
                        List<TandemMassSpectrum> validSpectra = new List<TandemMassSpectrum>();
                        foreach (TandemMassSpectrum spectrum in tms)
                        {
                            List<Tuple<string, int, string>> peptMods = (from mod in spectrum.Modifications
                                                                         select Tuple.Create(mod.Item1, (mod.Item2 + (peptide.StartPosition - 1)), mod.Item3)).ToList();
                            if (peptMods.Count > 0) isPeptideWithMods = true;

                            var commonMods = protMods.Where(a => peptMods.Any(a1 => a1.Item2 == a.Item2));
                            if (commonMods.Count() > 0)
                            {
                                TandemMassSpectrum newTms = new TandemMassSpectrum(spectrum.RAWfile, spectrum.ScanNumber, spectrum.Sequence, spectrum.ModifiedSequence, spectrum.Modifications, spectrum.Proteins, spectrum.Charge, spectrum.MZ, spectrum.Mass, spectrum.PPMError, spectrum.RetentionTime, spectrum.Score, spectrum.MatchedIons, spectrum.IntensitiesFragIons, spectrum.MassesFragIons, spectrum.NumberOfMatches, spectrum.MSMSID, spectrum.ProteinGroupID, spectrum.PeptideID);
                                newTms.Modifications = peptMods;
                                validSpectra.Add(newTms);
                                isProteoformAndPeptideWithMods = true;
                            }
                        }

                        if (isProteoformAndPeptideWithMods)
                        {
                            validSpectra = validSpectra.Distinct(new TandemMassSpectrumSimpleComparer()).ToList();

                            List<int> peptModPositions = (from spec in validSpectra.AsParallel()
                                                          from mod in spec.Modifications
                                                          select mod.Item2).ToList();

                            List<int> protfmModPositions = (from ptfm in protMods
                                                            where ptfm.Item2 >= (peptide.StartPosition - 1) && ptfm.Item2 <= (peptide.StartPosition - 1 + peptide.Sequence.Length)
                                                            select ptfm.Item2).ToList();

                            var exclusionList = protfmModPositions.Except(peptModPositions);
                            if (exclusionList.Count() == 0)
                                validPeptides.Add((peptide, validSpectra));
                        }
                        else if (!isPeptideWithMods)
                        {
                            //Check if peptide fits with the proteoform
                            if ((peptide.StartPosition - 1) >= ProtOffset && (peptide.StartPosition - 1 + peptide.Sequence.Length) <= (ProtOffset + proteoform.Sequence.Length))
                            {
                                if (!(protMods.Where(item => item.Item2 > (peptide.StartPosition - 1) && item.Item2 <= (peptide.StartPosition - 1) + peptide.Sequence.Length).Count() > 0))
                                {
                                    validPeptides.Add((peptide, new List<TandemMassSpectrum>()));
                                }
                            }
                        }
                    }
                }
            }

            return validPeptides;
        }

        internal bool PeptideIsModified(Peptide peptide, List<TandemMassSpectrum> selectedMSMS, out int numberOfModPeptides)
        {
            var tms = (from massMQ in selectedMSMS
                       where peptide.PSMs[0].MSMSIds.Contains(massMQ.MSMSID)
                       select massMQ).ToList();

            numberOfModPeptides = 0;

            bool isPeptideWithMods = false;
            foreach (TandemMassSpectrum spectrum in tms)
            {
                if (spectrum.ModifiedSequence.Contains("("))
                {
                    numberOfModPeptides++;
                    isPeptideWithMods = true;
                }
            }

            return isPeptideWithMods;
        }

        public void CalculateProteoformScore(string sequence, List<Proteoform> proteoformList, List<Peptide> selectedPeptides, List<TandemMassSpectrum> selectedMSMS = null)
        {
            List<(Proteoform, List<(Peptide, List<TandemMassSpectrum>)>)> fitResults = FitProteoformsAndPeptides(sequence, proteoformList, selectedPeptides, selectedMSMS);

            foreach ((Proteoform, List<(Peptide, List<TandemMassSpectrum>)>) eachProtfm in fitResults)
            {
                string searchEngineTDP = eachProtfm.Item1.PrSMs[0].SearchEngine;

                double maxScoreTDP = 0;
                double minScoreTDP = 0;
                switch (searchEngineTDP)
                {
                    case "Prosight":
                        minScoreTDP = this.MinScores[0];
                        maxScoreTDP = this.MaxScores[0];
                        break;
                    case "pTop":
                        minScoreTDP = this.MinScores[1];
                        maxScoreTDP = this.MaxScores[1];
                        break;
                    case "TopPIC":
                        minScoreTDP = this.MinScores[2];
                        maxScoreTDP = this.MaxScores[2];
                        break;
                    default:
                        maxScoreTDP = -1;
                        break;
                }
                eachProtfm.Item1.PrSMs.Sort((a, b) => b.LogPScore.CompareTo(a.LogPScore));
                double dividend = (maxScoreTDP - minScoreTDP) == 0 ? 1 : (maxScoreTDP - minScoreTDP);
                double Stdp = (eachProtfm.Item1.PrSMs[0].LogPScore - minScoreTDP) / dividend;

                double Sbup = 0;
                double combScore = Stdp;

                #region Sbup
                List<string> peptSeqs = new List<string>();

                if (eachProtfm.Item2.Count > 0)
                {
                    double degreesOfFreedom_Sbup = 0;
                    double Scp = 0;
                    // 1 - Fisher's method => Scp: 'sum' of unique peptide scores
                    List<Peptide> commonPepts = (from pept in eachProtfm.Item2.AsParallel()
                                                 where !pept.Item1.PSMs[0].IsUnique
                                                 select pept.Item1).ToList();

                    if (commonPepts.Count > 0)
                    {
                        double SumScpPvalues = 0;
                        double degreeOfFreedom_Scp = 0;
                        foreach (Peptide pept in commonPepts)
                        {
                            if (pept.PSMs[0].NormalizedPeptideScore > 0)
                            {
                                double pept_score = pept.PSMs[0].NormalizedPeptideScore;
                                if (pept_score == 1) pept_score = MAX_VALUE_LN;
                                degreeOfFreedom_Scp++;
                                pept_score = Math.Exp(-pept_score);
                                SumScpPvalues += Math.Log(1 - pept_score);
                            }
                        }
                        SumScpPvalues = -2 * SumScpPvalues;
                        ChiSquared chiSquareSup = new ChiSquared(2 * degreeOfFreedom_Scp);
                        Scp = chiSquareSup.CumulativeDistribution(SumScpPvalues);
                    }

                    if (Scp == 1) Scp = MAX_VALUE_LN;
                    if (Scp > 0)
                        degreesOfFreedom_Sbup++;

                    double p_cp = 1 - Scp;

                    double Sup = 0;
                    // 1 - Fisher's method => Sup: 'sum' of unique peptide scores
                    List<Peptide> uniquePepts = (from pept in eachProtfm.Item2.AsParallel()
                                                 where pept.Item1.PSMs[0].IsUnique
                                                 select pept.Item1).ToList();

                    if (uniquePepts.Count > 0)
                    {
                        double SumSupPvalues = 0;
                        double degreeOfFreedom_Sup = 0;
                        foreach (Peptide pept in uniquePepts)
                        {
                            if (pept.PSMs[0].NormalizedPeptideScore > 0)
                            {
                                double pept_score = pept.PSMs[0].NormalizedPeptideScore;
                                if (pept_score == 1) pept_score = MAX_VALUE_LN;
                                degreeOfFreedom_Sup++;
                                pept_score = Math.Exp(-pept_score);
                                SumSupPvalues += Math.Log(1 - pept_score);
                            }
                        }
                        SumSupPvalues = -2 * SumSupPvalues;
                        ChiSquared chiSquareSup = new ChiSquared(2 * degreeOfFreedom_Sup);
                        Sup = chiSquareSup.CumulativeDistribution(SumSupPvalues);
                    }

                    if (Sup == 1) Sup = MAX_VALUE_LN;
                    if (Sup > 0)
                        degreesOfFreedom_Sbup++;

                    double p_up = 1 - Sup;

                    if (degreesOfFreedom_Sbup > 0)
                    {
                        double SumSbupPvalues = Math.Log(p_cp) + Math.Log(p_up);
                        SumSbupPvalues = -2 * SumSbupPvalues;
                        ChiSquared chiSquareSbup = new ChiSquared(2 * degreesOfFreedom_Sbup);
                        Sbup = chiSquareSbup.CumulativeDistribution(SumSbupPvalues);
                    }
                }
                #endregion

                #region Fisher's Method for Top-down and Bottom-up scores
                if (Stdp == 1) Stdp = MAX_VALUE_LN;
                if (Sbup == 1) Sbup = MAX_VALUE_LN;
                if (Stdp > 0 && Sbup > 0)
                {
                    double SumCombScorePvalues = Math.Log((1 - Stdp)) + Math.Log((1 - Sbup));
                    SumCombScorePvalues = -2 * SumCombScorePvalues;
                    ChiSquared chiSquareCombScore = new ChiSquared(4);
                    combScore = chiSquareCombScore.CumulativeDistribution(SumCombScorePvalues);
                }
                #endregion

                Proteoform comparableProteoform = new Proteoform();
                comparableProteoform.Sequence = eachProtfm.Item1.Sequence;
                comparableProteoform.PrSMs = new List<ProteoformParams>() { new ProteoformParams() };
                comparableProteoform.PrSMs[0].TheoreticalMass = eachProtfm.Item1.PrSMs[0].TheoreticalMass;
                comparableProteoform.PrSMs[0].SpectrumFile = eachProtfm.Item1.PrSMs[0].SpectrumFile;
                comparableProteoform.PrSMs[0].ScanNumber = eachProtfm.Item1.PrSMs[0].ScanNumber;
                comparableProteoform.PrSMs[0].LogPScore = eachProtfm.Item1.PrSMs[0].LogPScore;
                comparableProteoform.PrSMs[0].LogEValue = eachProtfm.Item1.PrSMs[0].LogEValue;
                int _index = proteoformList.BinarySearch(comparableProteoform, new ProteoformBinarySearchComparer());
                Proteoform proteoform = new Proteoform();
                if (_index > -1)
                {
                    proteoform = proteoformList[_index];
                    proteoform.PrSMs[0].FinalScore = combScore;
                    proteoform.SequenceCoverage = this.ProteoformCoverage(sequence, new List<string>() { eachProtfm.Item1.Sequence }, peptSeqs);
                }
            }
        }

        private void CalcultePeptToProteoformScore((Proteoform, List<(Peptide, List<TandemMassSpectrum>)>) eachProtfm, out double finalScore)
        {
            //double normScoreBUPPL = this.NormScores[3];
            //double normScoreBUPAndromeda = this.NormScores[4];
            finalScore = 0;
            //foreach ((Peptide, List<TandemMassSpectrum>) peptide in eachProtfm.Item2)
            //{
            //    //string searchEngineBUP = peptide.Item1.PSMs[0].SearchEngine;
            //    //switch (searchEngineBUP)
            //    //{
            //    //    case "PatternLab":
            //    //        normScoreBUP = this.NormScores[3];
            //    //        break;
            //    //    case "Andromeda":
            //    //        normScoreBUP = this.NormScores[4];
            //    //        break;
            //    //    default:
            //    //        normScoreBUP = -1;
            //    //        break;
            //    //}
            //    peptide.Item1.PSMs.Sort((a, b) => b.PeptideScore.CompareTo(a.PeptideScore));
            //    //int modPept = 0;
            //    //this.PeptideIsModified(peptide.Item1, peptide.Item2, out modPept);
            //    finalScore += /*(0.2 * modPept) +*/ peptide.Item1.PSMs[0].PeptideScore;
            //}

            //int peptFromPL = eachProtfm.Item2.Where(item => item.Item1.PSMs[0].SearchEngine.Equals("PatternLab")).Count();
            //int peptFromAndromeda = eachProtfm.Item2.Where(item => item.Item1.PSMs[0].SearchEngine.Equals("Andromeda")).Count();
            //if (peptFromPL == 0 && peptFromAndromeda == 0) finalScore = 0;
            //else finalScore /= ((peptFromPL * normScoreBUPPL) + (peptFromAndromeda * normScoreBUPAndromeda));
        }
    }
}
