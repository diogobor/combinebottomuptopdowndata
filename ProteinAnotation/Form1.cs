﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProteinAnnotation
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonPeptideAnotation_Click(object sender, EventArgs e)
        {
            //string fastaSeq = "DIOGOBORGESLIMAINSTITUTPASTEURFRANCEABCDEFGHIJKLMNMSERFPNDVDPIETRDWLQAIESVIREEGVERAQYLIDQLLAEARKGGVNVAAGTGISNYINTIPVEEQPEYPGNLELERRIRSAIRWNAIMTVLRASKKDLELGGHMASFQSSATIYDVCFNHFFRARNEQDGGDLVYFQGHISPGVYARAFLEGRLTQEQLDNFRQEVHGNGLSSYPHPKLMPEFWQFPTVSMGLGPIGAIYQAKFLKYLEHRGLKDTSKQTVYAFLGDGEMDEPESKGAITIATREKLDNLVFVINCNLQRLDGPVTGNGKIINELEGIFEGAGWNVIKVMWGSRWDELLRKDTSGKLIQLMNETVDGDYQTFKSKDGAYVREHFFGKYPETAALVADWTDEQIWALNRGGHDPKKIYAAFKKAQETKGKATVILAHTIKGYGMGDAAEGKNIAHQVKKMNMDGVRHIRDRFNVPVSDTDIEKLPYITFPEGSEEHTYLHAQRQKLHGYLPSRQPNFTEKLELPSLQDFGALLEEQSKEISTTIAFVRALNVMLKNKSIKDRLVPIIADEARTFGMEGLFRQIGIYSPNGQQYTPQDREQVAYYKEDEKGQILQEGINELGAGCSWLAAATSYSTNNLPMIPFYIYYSMFGFQRIGDLCWAAGDQQARGFLIGGTSGRTTLNGEGLQHEDGHSHIQSLTIPNCISYDPAYAYEVAVIMHDGLERMYGEKQENVYYYITTLNENYHMPAMPEGAEEGIRKGIYKLETIEGSKGKVQLLGSGSILRHVREAAEILAKDYGVGSDVYSVTSFTELARDGQDCERWNMLHPLETPRVPYIAQVMNDAPAVASTDYMKLFAEQVRTYVPADDYRVLGTDGFGRSDSRENLRHHFEVDASYVVVAALGELAKRGEIDKKVVADAIAKFNIDADKVNPRLA";
            //string proteoform = "DIOGOBORGESLIMAINSTITUTPASTEURFRANCEA";
            //string proteoform2 = "BCDEFGHIJKLMNMSERFPNDVDPIETRDWLQAIESVIREEGVERA";
            //string uniquePepts = "MTVLRA\nKDLE\nSERFPNDVDPI\nERFPNDVDPIETR\nALNVMLK\nAQYLIDQLLAEAR\nAQYLIDQLLAEARK\nARNEQDGGDLVYFQGHISPGVYAR\nATVILAHTIK\nDGAYVR\nDRFNVPVSDTDIEK\nFNVPVSD\nDRFNVPVSDTDIEKLPYITFPEGSEEHTYLHAQR\nDRLVPIIADEAR\nDWLQAIESVIR\nDYGVGSDVYSVTSFTELAR\nEAAEILAK\nEISTTIAFVR\nEQVAYYKEDEK\nFNIDADK\nFNIDADKVNPR\nFNVPVSDTDIEKLPYITFPEGSEEHTYLHAQR\nFPNDVDPIETR\nGFLIGGTSGR\nGYGMGDAAEGK\nHHFEVDASYVVVAALGELAK\nIGDLCWAAGDQQAR\nIINELEGIFEGAGWNVIK\nIYAAFK\nKMNMDGVR\nKVVADAIAK\nLDGPVTGNGK\nLDNLVFVINCNLQR\nLELPSLQDFGALLEEQSK\nLETIEGSK\nLHGYLPSR\nLHGYLPSRQPNFTEK\nLIQLMNETVDGDYQTFK\nLMPEFWQFPTVSMGLGP\nLPYITFPEGSEEHTYLHAQR\nLTQEQLDNFRQEVHGNGLSSYPHPK\nLVPIIADEAR\nMNMDGVR\nNEQDGGDLVYFQGHISPGVYAR\nNIAHQVK\nQENVYYYITTLNENYHMPAMPEGAEEGIR\nQENVYYYITTLNENYHMPAMPEGAEEGIRK\nQIGIYSPNGQQYTPQDR\nQIGIYSPNGQQYTPQDREQVAYYK\nQIGIYSPNGQQYTPQDREQVAYYKEDEK\nQPNFTEK\nSDSRENLR\nSERFPNDVDPIETR\nTFGMEGLFR\nTYVPADDYR\nVLGTDGFGR\nVMWGSR\nVPYIAQVMNDAPAVASTDYMK\nVQLLGSGSILR\nVVADAIAK\nWDELLR\nWNAIMTVLR\nWNMLHPLETPR\nYPETAALVADWTDEQIWALNR";
            //string commonPepts = "DIOGO\nPNDVD\nSKKDLEL\nGGVNVA\nGSRWDEL";

            //string fastaSeq = "MIISAASDYRAAAQRILPPFLFHYMDGGAYSEYTLRRNVEDLSEVALRQRILKNMSDLSLETTLFNEKLSMPVALAPVGLCGMYARRGEVQAAKAADAHGIPFTLSTVSVCPIEEVAPAIKRPMWFQLYVLRDRGFMRNALERAKAAGCSTLVFTVDMPTPGARYRDAHSGMSGPNAAMRRYLQAVTHPQWAWDVGLNGRPHDLGNISAYLGKPTGLEDYIGWLGNNFDPSISWKDLEWIRDFWDGPMVIKGILDPEDARDAVRFGADGIVVSNHGGRQLDGVLSSARALPAIADAVKGDIAILADSGIRNGLDVVRMIALGADTVLLGRAFLYALATAGQAGVANLLNLIEKEMKVAMTLTGAKSISEITQDSLVQGLGKELPAALAPMAKGNAA";
            string fastaSeq = "MGSSHHHHHHSSGLVPRGSHMTYANSTVTDIATTVVTITSCEENKCHETEVTTGVTTVTEVETTYTTYCPLPTAKAPVASTSNSTTTPPVSTAEGDIOGOAKAPVASTSNSTTTPPVSTAEGAKAPVASTSNSTTTPPVSTAEGAKAPVASTSNSTTTPPVSTAEGAKAPVASTSNSTTTPPVSTAEGAKAPVASTSNSTTTPPVSTAEGAKAPVASTSNSTTTPPVSTAEGAKAPVASTSNSTTTPPVSTAEGAKAPVASTSNSTTTPPVSTAEGAKAPVASTSNSTTTPPVSTAEG";
            //string fastaSeq = "MSNIIKQLEQEQMKQDVPSFRPGDTVEVKVWVVEGSKKRLQAFEGVVIAIRNRGLHSAFTVRKISNGEGVERVFQTHSPVVDSISVKRRGAVRKAKLYYLRERTGKAARIKERLN";
            //string proteoform = "MKKDIHPKYEEITASCSCGNVMKIRSTVGHDLNLDVCSKCHPFFTGKQRDVATGGRVDRFNKRFNIPGSK";
            string proteoform = "TDIATTVVTITSCEENKCHETEVTTGVTTVTEVETTYTTYCPLPTAKAPVASTSNSTTTPPVSTAEG";
            string proteoform2 = "HHHHSSGLVPRGSHMTYANSTVTDIATTVVTITSCEENKCHETEVTTGVTTVTEVETTYTTYCPLPTAKAPVASTSNSTTTPPVSTAEG";
            string proteoform3 = "MGSSHHHHHHSSGLVPRGSHMTYANSTVTDIAT";
            string proteoform4 = "mGSSHHHHHHSSGLVPRGSHMTYANSTVTDIATTVVTITScEENKcHETEVTTGVTTVTEVETTYTTYCPLPTAKAPVASTSNSTTTPPVSTAEG";
            string proteoform5 = "MGSSHHHHHHSSGLVPRGSHMTYANSTVTDIATTVVTITSCEENKCHETEVTTGVTTVTEVETTYTTYCPLPTAKAPVASTSNSTTTPPV";
            //string uniquePepts = "MGSSHHHHHHS\nSGLVP";
            //string uniquePept = "MGSSHHHHHHS\nGSSHHHHHHSSGLVP\nHHHHHHSSGLVP\nHSSGLVPRGSHM";
            //string commonPepts = "CHETEVTTGVTTVTEVETT";

            //Tuple<proteinSequence, List<chain(or signal peptide), start position, end position>, List<modification-> ( description, position )>>
            Tuple<string, List<Tuple<string, int, int>>, List<Tuple<string, int>>> protein = Tuple.Create(fastaSeq, new List<Tuple<string, int, int>>() { /*Tuple.Create("signal peptide", 1, 26), Tuple.Create("chain", 26, 70), Tuple.Create("chain", 128, 191),*/ Tuple.Create("chain", 190, 200), Tuple.Create("chain", 189, 200), Tuple.Create("chain", 188, 200)}, new List<Tuple<string, int>>() { Tuple.Create("Oxidantion", 6), Tuple.Create("Acetyl", 6), Tuple.Create("Another one", 51), Tuple.Create("Other PTM", 172) });
            //Tuple<string, List<Tuple<string, int, int>>, List<Tuple<string, int>>> protein = Tuple.Create(fastaSeq, new List<Tuple<string, int, int>>(), new List<Tuple<string, int>>() { Tuple.Create("Oxidantion", 6), Tuple.Create("Acetyl", 6), Tuple.Create("Another one", 20) });

            //Tuple<peptide, isUnique, aminoacid, position, PTMdescription>
            List<Tuple<string, bool, string, int, string>> Peptides = new List<Tuple<string, bool, string, int, string>>();

            //string[] ups = Regex.Split(uniquePepts, "\n");
            //foreach (string pept in ups)
            Peptides.Add(Tuple.Create("M(ox)GSSHHHHHHS", true, "M", 1, "Oxidation"));
            Peptides.Add(Tuple.Create("GS(s)SHHHHHHSSGLVP", true, "S", 3, "Serine"));
            Peptides.Add(Tuple.Create("H(aptm)HHHHHSSGLVP", true, "H", 5, "Another PTM"));
            Peptides.Add(Tuple.Create("HSSGLVPRGSHM", true, "", -1, ""));

            //string[] cps = Regex.Split(commonPepts, "\n");
            //foreach (string pept in cps)
            Peptides.Add(Tuple.Create("CHETEVTTGVTTVTEVETT", false, "", -1, ""));
            Peptides.Add(Tuple.Create("GSS(s)HH", false, "S", 4, "Serine"));

            string[] PTMColorArrayTDPList = new string[4];
            PTMColorArrayTDPList[0] = "Oxidation";
            PTMColorArrayTDPList[1] = "Serine";
            PTMColorArrayTDPList[2] = "Another PTM";

            //Tuple<proteoform, modifications( List<Tuple<aminoacid, position, PTMdescription>>) >
            List<Tuple<string, List<Tuple<string, int, string>>>> Proteoforms = new List<Tuple<string, List<Tuple<string, int, string>>>>();
            //Proteoforms.Add(Tuple.Create(proteoform, new List<Tuple<string, int, string>>()));
            Proteoforms.Add(Tuple.Create(proteoform2, new List<Tuple<string, int, string>>()));
            Proteoforms.Add(Tuple.Create(proteoform3, new List<Tuple<string, int, string>>()));
            Proteoforms.Add(Tuple.Create(proteoform4, new List<Tuple<string, int, string>>()));
            Proteoforms.Add(Tuple.Create(proteoform5, new List<Tuple<string, int, string>>()));
            this.proteinAnnotator1.DrawProteoformsPeptides(false, fastaSeq, Proteoforms, Peptides, PTMColorArrayTDPList);

            //Tuple<proteoform, score, tool, isValid, theoretical mass, modifications( List<Tuple<aminoacid, position, PTMdescription>>), node information >
            List<Tuple<string, double, string, bool, double, List<Tuple<string, int, string>>, int>> Proteoforms2 = new List<Tuple<string, double, string, bool, double, List<Tuple<string, int, string>>, int>>();

            Proteoforms2.Add(Tuple.Create(proteoform3, 3.9, "XXX", false, 0.0, new List<Tuple<string, int, string>>(), 3));
            Proteoforms2.Add(Tuple.Create(proteoform, 4.5, "XXX", false, 0.0, new List<Tuple<string, int, string>>(), 3));
            Proteoforms2.Add(Tuple.Create(proteoform2, 4.3, "XXX", false, 0.0, new List<Tuple<string, int, string>>(), 3));

            //List<Tuple<string, int, string>> mods = new List<Tuple<string, int, string>>();
            //mods.Add(Tuple.Create("}", 1, "N-carbamoyl-L-alanine"));
            //mods.Add(Tuple.Create("C", 41, "Disulfide Bond"));
            //mods.Add(Tuple.Create("C", 46, "Disulfide Bond"));
            //Proteoforms2.Add(Tuple.Create(proteoform4, 3.29522143048484, "XXX", false, 0.0, mods));
            //Proteoforms2.Add(Tuple.Create(proteoform5, 2.9, "XXX", false, 0.0, new List<Tuple<string, int, string>>()));

            //Tuple<peptide, isUnique, score, tool, List<protein>, List<aminoacid, position, PTMdescription>>
            List<Tuple<string, bool, double, string, List<string>, List<Tuple<string, int, string>>>> Peptides2 = new List<Tuple<string, bool, double, string, List<string>, List<Tuple<string, int, string>>>>();

            //Peptides2.Add(Tuple.Create("M(ox)GSSHHHHHHS", true, new List<string>() { proteoform + 4.5 }, new List<Tuple<string, int, string>>() { Tuple.Create("M", 1, "Oxidation") }));

            //Peptides2.Add(Tuple.Create("GLHSAFTVRK", true, new List<string>(), new List<Tuple<string, int, string>>() { Tuple.Create("", -1, "") }));
            //Peptides2.Add(Tuple.Create("QDVPSFRPGDTVEVK", true, new List<string>(), new List<Tuple<string, int, string>>() { Tuple.Create("", -1, "") }));
            //Peptides2.Add(Tuple.Create("QLEQEQMK", true, new List<string>(), new List<Tuple<string, int, string>>() { Tuple.Create("", -1, "") }));
            //Peptides2.Add(Tuple.Create("RLQAFEGVVIAIR", true, new List<string>(), new List<Tuple<string, int, string>>() { Tuple.Create("", -1, "") }));
            //Peptides2.Add(Tuple.Create("VFQTHSPVVDSISVK", true, new List<string>(), new List<Tuple<string, int, string>>() { Tuple.Create("", -1, "") }));
            //Peptides2.Add(Tuple.Create("VFQTHSPVVDSISVKR", true, new List<string>(), new List<Tuple<string, int, string>>() { Tuple.Create("", -1, "") }));
            //Peptides2.Add(Tuple.Create("VWVVEGSK", true, new List<string>(), new List<Tuple<string, int, string>>() { Tuple.Create("", -1, "") }));
            //Peptides2.Add(Tuple.Create("VWVVEGSKK", true, new List<string>(), new List<Tuple<string, int, string>>() { Tuple.Create("", -1, "") }));

            Peptides2.Add(Tuple.Create("GS(s)SHHHHHHSSGLVP", true, 3.3, "YYY", new List<string>() { proteoform + 4.5 }, new List<Tuple<string, int, string>>() { Tuple.Create("S", 3, "Serine") }));
            Peptides2.Add(Tuple.Create("H(aptm)HHHHHSSGLVP", true, 3.4, "YYY", new List<string>() { proteoform2 + 4.3 }, new List<Tuple<string, int, string>>() { Tuple.Create("H", 5, "Another PTM") }));
            Peptides2.Add(Tuple.Create("HSSGLVPRGSHM", true, 3.5, "YYY", new List<string>() { proteoform2 + 4.3 }, new List<Tuple<string, int, string>>() { Tuple.Create("", -1, "") }));
            Peptides2.Add(Tuple.Create("CHETEVTTGVTTVTEVETTYTTYCPLPTAK", false, 5.0, "YYY", new List<string>() { proteoform + 4.5 }, new List<Tuple<string, int, string>>() { Tuple.Create("", -1, "") }));
            Peptides2.Add(Tuple.Create("GSHMTYANSTVTDIATTVVTITSCEENK", true, 4.5, "YYY", new List<string>() { proteoform2 + 4.3 }, new List<Tuple<string, int, string>>() { Tuple.Create("", -1, "") }));
            Peptides2.Add(Tuple.Create("APVASTSNSTTTPPVSTAEG", true, 4.2, "YYY", new List<string>() { proteoform2 + 4.3 }, new List<Tuple<string, int, string>>() { Tuple.Create("", -1, "") }));
            Peptides2.Add(Tuple.Create("AADAHGIPFTLSTVSVCPIEEVAPAIK", false, 3.0, "YYY", new List<string>(), new List<Tuple<string, int, string>>() { Tuple.Create("", -1, "") }));
            //Peptides2.Add(Tuple.Create("FGADGIVVSNHGGR", false, new List<string>(), new List<Tuple<string, int, string>>() { Tuple.Create("", -1, "") }));

            //this.proteinAnnotatorUC1.DrawProteoformsPeptides(true, protein, Proteoforms2, Peptides2, PTMColorArrayTDPList);
            //this.proteinAnnotatorUC1.DrawProteoformsPeptides(false, fastaSeq, Proteoforms2, Peptides2, PTMColorArrayTDPList);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
